# UML

Unified Modeling Language - dziedzina modelowania systemów informatycznych została zdominowana w XXI wieku przez paradygmat modelowania obiektowego. Jest to graficzny język wizualizacji, specyfikacji, tworzenia i dokumentowania składników systemów informatycznych.

Rational Unified Process - metodyka.

Obiektowość stanowi podstawę teoretyczną języka UML.

MOF - Meta Object Facility
XMI - XML Meatadata Interchange
CWM - Common Warehouse Metamodel

## Diagramy

Diagramy dzielimy na:

- przypadków użycia
- klas
- czynności
- maszyny stanowej
- interakcji
- sekwencji
- komunikacji
- harmonogramowania
- sterowania iterakcją
- komponentów
- rozlokowania
- wdrożeniowe
- struktur połączonych
- pakietów

## Modelowanie

Rozróżniamy następujące metodyki modelowania:

- RUP
- modelowanie biznesowe
- modelowanie analityczne
- komputerowe wspomaganie modelowania systemu

## Diagrams

[PlantULM for PHP Storm](https://www.jetbrains.com/help/idea/markdown.html#diagrams)
[PlantUML for VS Code](https://marketplace.visualstudio.com/items?itemName=jebbs.plantuml)

# Source

- Stanisław Wryczka, Bartosz Marcinkowski, Krzysztof Wyrzykowski, _Język UML 2.0 w modelowaniu systemów informatycznych_, ISBN: 83-7361-892-9, HELION 2005

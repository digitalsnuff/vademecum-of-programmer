# Vademecum of Programmer

This is guide throw tools and tips. There are fundamentals of technologies and examples. There are initial code tips for start project.

---

---

# Table of Contents

- [HTML5](/html/index.md)
- [CSS](/css/index.md)
  - [Bootstrap 4](/css/bootstrap.md)
- [Java Script](/javascript.md)
  - [tools](/js/tools.md)

# Links

- [Web Development In 2019 - A Practical Guide](https://www.youtube.com/watch?v=UnTQVlqmDQ0) form [Traversy Media](https://www.youtube.com/channel/UC29ju8bIPH5as8OGnQzwJyA)

# TODO

- Use `Jekyll`. [Link](https://jekyllrb.com/)

# Elasticsearch

Elasticsearch is a Lucene-based distributed search server that allows users to index and search unstructured content with petabytes of data.

Elasticsearch is natively designed for the cloud, so when you need to release a production environment with a huge number of records and you need high availability and good performance, you need to aggregate more nodes in a cluster.

## How it works?

The Elasticsearch package generally contains the following directories:

- `bin`: This contains the scripts to start and manage Elasticsearch.
- `elasticsearch.bat`: This is the main executable script to start Elasticsearch.
- `elasticsearch-plugin.bat`: This is a script to manage plugins.
- `config`: This contains the Elasticsearch configs. The most important ones are as follows:
  - `elasticsearch.yml`: This is the main config file for Elasticsearch
  - `log4j2.properties`: This is the logging config file
- `lib`: This contains all the libraries required to run Elasticsearch.
- `logs`: This directory is empty at installation time, but in the future, it will contain the application logs.
- `modules`: This contains the Elasticsearch default plugin modules.
- `plugins`: This directory is empty at installation time, but it's the place where custom plugins will be installed.

During Elasticsearch startup, the following events happen:

- A node name is generated automatically (that is, `fyBySLM`) if it is not provided in `elasticsearch.yml`. The name is randomly generated, so it's a good idea to set it to a meaningful and memorable name instead.
- A node name `hash` is generated for this node, for example, `fyBySLMcR3uqKiYC32P5Sg`.
- The default installed modules are loaded. The most important ones are as follows:

  - `aggs-matrix-stats`: This provides support for aggregation matrix stats.
  - `analysis-common`: This is a common analyzer for Elasticsearch, which extends the language processing capabilities of Elasticsearch.
  - `ingest-common`: These include common functionalities for the ingest module.
  - `lang-expression/lang-mustache/lang-painless`: These are the default supported scripting languages of Elasticsearch.
  - `mapper-extras`: This provides an extra mapper type to be used, such as `token_count` and `scaled_float`.
  - `parent-join`: This provides an extra query, such as `has_children` and `has_parent`.
  - `percolator`: This provides percolator capabilities.
  - `rank-eval`: This provides support for the experimental rank evaluation APIs. These are used to evaluate hit scoring based on queries.
  - `reindex`: This provides support for `reindex` actions (`reindex/update` by query).
  - `x-pack-\*`: All the `xpack` modules depend on a subscription for their activation.

- If there are plugins, they are loaded.
- If not configured, Elasticsearch binds the following two ports on the localhost `127.0.0.1` automatically:
  - `9300`: This port is used for internal intranode communication.
  - `9200`: This port is used for the HTTP REST API.
- After starting, if indices are available, they are restored and ready to be used.

If these port numbers are already bound, Elasticsearch automatically increments the port number and tries to bind on them until a port is available (that is, 9201, 9202, and so on).

During a node's startup, a lot of required services are automatically started. The most important ones are as follows:

- `Cluster services`: This helps you manage the cluster state and intranode communication and synchronization
- `Indexing service`: This helps you manage all the index operations, initializing all active indices and shards
- `Mapping service`: This helps you manage the document types stored in the cluster
- `Network services`: This includes services such as HTTP REST services (default on port 9200), and internal Elasticsearch protocol (port 9300) if the thrift plugin is installed
- `Plugin service`: This manages loading the plugin
- `Aggregation services`: This provides advanced analytics on stored Elasticsearch documents such as statistics, histograms, and document grouping
- `Ingesting services`: This provides support for document preprocessing before ingestion such as field enrichment, NLP processing, types conversion, and automatic field population
- `Language scripting services`: This allows you to add new language scripting support to Elasticsearch

## Installation

[Installing from the APT repository](https://www.elastic.co/guide/en/elasticsearch/reference/7.5/deb.html#deb-repo)

```sh
sudo apt-get install apt-transport-https
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
add-apt-repository "deb https://artifacts.elastic.co/packages/7.x/apt stable main"
sudo apt-get update
sudo apt-get install elasticsearch
```

## Configuration

```sh
# sudo nano /etc/elasticsearch/elasticsearch.yml
 network.host: 0.0.0.0
 cluster.name: myCluster1
 node.name: "myNode1"

# Setting up a node
# for example
path.conf: /opt/data/es/conf
path.data: /opt/data/es/data1,/opt2/data/data2
path.work: /opt/data/work
path.logs: /opt/data/logs
path.plugins: /opt/data/plugins

index.number_of_shards: 1
index.number_of_replicas:1

# control memory swapping
bootstrap.memory_lock: true

```

- `path.conf` - defines the directory that contains your configurations, mainly `elasticsearch.yml` and `logging.yml`. The path.data parameter is the most important one. This allows us to define one or more directories (in a different disk) where you can store your index data. When you define more than one directory, they are managed similarly to `RAID 0` (their space is sum up), favoring locations with the most free space.
- `path.work` - a location in which Elasticsearch stores temporary files.
- `path.log` - where log files are put. These control how a log is managed in `logging.yml`.
- `path.plugins` - allows you to override the plugins path. It's useful to put system-wide plugins in a shared path (usually using NFS) in case you want a single place where you store your plugins for all of the clusters.

The main parameters are used to control index and shards in `index.number_of_shards`, which controls the standard number of shards for a new created index, and `index.number_of_replicas`, which controls the initial number of replicas.

## Settin up Linux systems

This recipe covers the following two common errors that happen in production:

- Too many open files that can corrupt your indices and your data
- Slow performance in search and indexing due to the garbage collector

Big problems arise when you run out of disk space. In this scenario, some files can get corrupted. To prevent your indices from corruption and possible data, it is best to monitor the storage spaces. Default settings prevent index writing and block the cluster if your storage is over 80% full.

To allow Elasticsearch to manage a large number of files, you need to increment the number of file descriptors (number of files) that a user can manage. To do so, you must edit your /etc/security/limits.conf file and add the following lines at the end:

```
elasticsearch - nofile 65536
elasticsearch - memlock unlimited
```

To fix the memory usage size of the Elasticsearch server, we need to set up the same values for `Xms` and `Xmx` in `$ES_HOME/config/jvm.options` (thatis, we set 1 GB of memory in this case), as follows:

```
-Xms1g
-Xmx1g
```

If you don't set `bootstrap.memory_lock: true`, Elasticsearch dumps the whole process memory on disk and defragments it back in memory, freezing the system. With this setting, the defragmentation step is done all in memory, with a huge performance boost.

## Different node types

Need to aggregate more nodes in a cluster is preffered for production for hight availability and good pefrormance if number of records is huge.

```sh
# must have
## if a node can be a master or not
node.master: true
## if a node must contain data or not
node.data: true
## if a node can work as an ingest node
node.ingest: true
```

`Master node` takes decisions about shard management, keeps the cluster status and is the main controller of every index action. It distributes
the search across all data nodes and aggregates/rescores the result to return them to the user. In big data terms, it's a Redux layer in the Map/Redux search in Elasticsearch.

The number of master nodes must always be even.

`Data node` will be a worker that is responsible for indexing and searching data.

If data node and master node is set as false, the node acts as a search load balancer (fetching data from nodes, aggregating results, and so on). This kind of node is also called a coordinator or client node.

Related to the number of master nodes, there are settings that require at least half of them plus one to be available to ensure that the cluster is in a safe state.

This setting is `discovery.zen.minimum_master_nodes`, and it must be set to the following equation:

```
(master_eligible_nodes / 2) + 1
```

To have a **High Availability (HA)** cluster, you need at least three nodes that are masters with the value of minimum_master_nodes set to 2.

## Coordinator node

To prevent the queries and aggregations from creating instability in your cluster, coordinator (or client/proxy) nodes can be used to provide safe communication with the cluster. Parameters `node.master` and `node.data` are set to `false`.

The coordinator node is a special node that works as a proxy/pass thought for the cluster. Its main advantages are as follows:

- It can easily be killed or removed from the cluster without causing any problems. It's not a master, so it doesn't participate in cluster functionalities and it doesn't contain data, so there are no data relocations/replications due to its failure.
- It prevents the instability of the cluster due to a developers' `/users` bad queries. Sometimes, a user executes aggregations that are too large (that is, date histograms with a range of some years and intervals of 10 seconds). Here, the Elasticsearch node could crash. (In its newest version, Elasticsearch has a structure called **circuit breaker** to prevent similar issues, but there are always borderline cases that can bring instability using scripting, for example. The coordinator node is not a master and its overload doesn't cause any problems for cluster stability.
- If the coordinator or client node is embedded in the application, there are less round trips for the data, speeding up the application.
- You can add them to balance the search and aggregation throughput without generating changes and data relocation in the cluster.

## Ingestion node

The following are the most common scenarios in this case:

- Preprocessing the log string to extract meaningful data
- Enriching the content of textual fields with **Natural Language Processing (NLP)** tools
- Enriching the content using **machine learning (ML)** computed fields
- Adding data modification or transformation during ingestion, such as the following:

  - Converting IP in geolocalization
  - Adding datetime fields at ingestion time
  - Building custom fields (via scripting) at ingestion time

Parameter `node.ingest` is set to `true`.
It's a best practice to disable this in the master and data nodes to prevent ingestion error issues and to protect the cluster. The coordinator node is the best candidate to be an ingest one.
The best practice is to have a pool of coordinator nodes with ingestion enabled to provide the best safety for the cluster and ingestion pipeline.

## Installing plugins

In Elasticsearch, these plugins are native plugins. These are JAR files that contain application code, and are used for the following reasons:

- Script engines
- Custom analyzers, tokenizers, and scoring
- Custom mapping
- REST entry points
- Ingestion pipeline stages
- Supporting new storages (Hadoop, GCP Cloud Storage)
- Extending X-Pack (that is, with a custom authorization provider)

## Logging

If you need to debug your Elasticsearch server or change how the logging works (that is, remoting send events), you need to change the `log4j2.properties` file.
The Elasticsearch logging system is based on the `log4j` library.

## Setting up a node via Docker

```sh
docker run -p 9200:9200 -p 9300:9300 -e "http.host=0.0.0.0" -e "transport.host=0.0.0.0" docker.elastic.co/elasticsearch/elasticsearch:7

```

# Mapping

`Mapping` defines how the search engine should process a document and its fields.

Search engines perform the following two main operations:

- `Indexing`: This is the action to receive a document and to process it and store it in an index
- `Searching`: This is the action to retrieve the data from the index.

Elasticsearch has explicit mapping on an index level. When indexing, if a mapping is not provided, a default one is created, and guesses the structure from the data fields that the document is composed of. This new mapping is then automatically propagated to all cluster nodes.

The default type mapping has sensible default values, but when you want to change their behavior or customize several other aspects of indexing (storing, ignoring, completion, and so on), you need to provide a new mapping definition.

If we consider the index as a database in the SQL world, mapping is similar to the table definition.  
Elasticsearch is able to understand the structure of the document that you are indexing (reflection) and create the mapping definition automatically (explicit mapping creation).

## Mapping base types

Using explicit mapping makes it possible to be faster in starting to ingest the data using a schema-less approach without being concerned about field types.

Fine-tuning mapping brings some advantages, such as the following:

- Reducing the index size on the disk (disabling functionalities for custom fields)
- Indexing only interesting fields (general speed up)
- Precooking data for fast search or real-time analytics (such as facets)
- Correctly defining whether a field must be analyzed in multiple tokens or considered as a single token

```json
PUT test/_mapping
{
    "properties" : {
    "id" : {"type" : "keyword"},
    "date" : {"type" : "date"},
    "customer_id" : {"type" : "keyword"},
    "sent" : {"type" : "boolean"},
    "name" : {"type" : "keyword"},
    "quantity" : {"type" : "integer"},
    "price" : {"type" : "double"},
    "vat" : {"type" : "double", "index":"false"}
    }
}
```

<table>
    <thead>
        <tr>
            <th>Type</th>
            <th>ES-Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>String, VarChar</td>
            <td>keyword</td>
            <td>This is a text field that is not tokenizable: <code>CODE001</code></td>
        </tr>
        <tr>
            <td>String, VarChar, Text</td>
            <td>text</td>
            <td>This is a text field to be tokenizated: a nice text</td>
        </tr>
        <tr>
            <td>Integer</td>
            <td>integer</td>
            <td>This is an Integer (32-bit): 1,2,3, or 4</td>
        </tr>
        <tr>
            <td>long</td>
            <td>long</td>
            <td>This is a long value (64-bit)</td>
        </tr>
        <tr>
            <td>float</td>
            <td>float</td>
            <td>This is a floating-point number (32-bit): 1.2, or 4.5</td>
        </tr>
        <tr>
            <td>double</td>
            <td>double</td>
            <td>This is a floating point number (64 bit)</td>
        </tr>
        <tr>
            <td>boolean</td>
            <td>boolean</td>
            <td>This is a Boolean value: true or false</td>
        </tr>
        <tr>
            <td>date/datetime</td>
            <td>date</td>
            <td>This is a date or datetime value: <code>2013-12-25</code>, <code>2013-12-25T22:21:20</code></td>
        </tr>
        <tr>
            <td>bytes/binary</td>
            <td>binary</td>
            <td>This includes some bytes that are used for binary data, such as file or stream of bytes.</td>
        </tr>
    </tbody>
</table>

Explicit directives when processing the fields:

- `store` (default `false`): This marks the field to be stored in a separate index fragment for fast retrieval. Storing a field consumes disk space, but reduces computation if you need to extract it from a document (that is, in scripting and aggregations). The possible values for this option are `false` and `true`. The stored fields are faster than others in aggregations.
- `index`: This defines whether or not the field should be indexed. The possible values for this parameter are `true` and `false`. Index fields are not searchable (default `true`).
- `null_value`: This defines a default value if the field is null.
- `boost`: This is used to change the importance of a field (default 1.0). Boost works on a term level only, so it's mainly used in term, terms, and match queries.
- `search_analyzer`: This defines an analyzer to be used during the search. If not defined, the analyzer of the parent object is used (default `null`).
- `analyzer`: This sets the default analyzer to be used (default `null`).
- `include_in_all`: This marks the current field to be indexed in the special `_all` field (a field that contains the concatenated text of all fields) (default `true`).
- `norms`: This controls the Lucene norms. This parameter is used to better score queries. If the field is used only for filtering, it's best practice to disable it to reduce resource usage (default `true` for analyzed fields and `false` for `not_analyzed` ones).
- `copy_to`: This allows you to copy the content of a field to another one to achieve functionalities, similar to the `_all` field.
- `ignore_above`: This allows you to skip the indexing string that's bigger than its value. This is useful for processing fields for exact filtering, aggregations, and sorting. It also prevents a single term token from becoming too big and prevents errors due to the Lucene term byte-length limit of 32766 (default `2147483647`).

In the previous version of Elasticsearch, the standard mapping for the string was `string`. In version 5.x, the string mapping is deprecated and migrated to keyword and text mappings.

Mulifield mapping:

- The default processing is `text`. This mapping allows textual queries (that is, term, match, and span queries).
- The `keyword` subfield is used for keyword mapping. This field can be used for exact term matching and for aggregation and sorting.

Another important parameter, available only for text mapping, is the `term_vector` (the vector of terms that compose a string).
The term_vector can accept the following values:

- `no`: This is the default value, skip term vector
- `yes`: This is the store term vector
- `with_offsets`: This is the store term vector with token offset (start, end position in a block of characters)
- `with_positions`: This is used to store the position of the token in the term vector
- `with_positions_offsets`: This stores all term vector data

Term vectors allow fast highlighting, but consume disk space due to storing of additional text information. It's a best practice to only activate in fields that require highlighting, such as title or document content.

## Mappin arrays

An array or multi-value fields are very common in data models (such as multiple phone numbers, addresses, names, aliases, and so on), but not natively supported in traditional SQL solutions.
Elasticsearch, which works natively in JSON, provides support for multi-value fields transparently.

## Mapping object

An object is a base structure (analogous to a record in SQL). Elasticsearch extends the traditional use of objects, thus allowing for recursive embedded objects.

The most important attributes for an object are as follows:

- `properties`: This is a collection of fields or objects (we can consider them as columns in the SQL world).
- `enabled`: This establishes whether or not the object should be processed. If it's set to false, the data contained in the object is not indexed and it cannot be searched (default `true`).
- `dynamic`: This allows Elasticsearch to add new field names to the object using a reflection on the values of the inserted data. If it's set to `false`, when you try to index an object containing a new field type, it'll be rejected silently. If it's set to `strict`, when a new field type is present in the object, an error is raised, skipping the index process. The dynamic parameter allows you to be safe about changes in the document structure (default `true`).
- `include_in_all`: This adds the object values to the special `_all` field (used to aggregate the text of all document fields) (default `true`).

## Mapping a document

Every special field has its own parameters and value options, such as the following:

- `_id`: This allows you to index only the ID part of the document. All the ID queries will speed up using the ID value (default not indexed and not stored).
- `_index`: This controls whether or not the index must be stored as part of the document. It can be enabled by setting the `"enabled"`: `true` parameter (`enabled=false` default).
- `_source`: This controls the storage of the document source. Storing the source is very useful, but it's a storage overhead, so it is not required. Consequently, it's better to turn it off (`enabled=true` default).
- `_routing`: This defines the shard that will store the document. It supports additional parameters, such as `required` (`true/false`). This is used to force the presence of the routing value, raising an exception if not provided.

## Using dynamic templates in document mapping

The root object (document) controls the behavior of its fields and all its children object fields. In document mapping, we can define the following:

- `date_detection`: This enables the extraction of a date from a string (`true` default).
- `dynamic_date_formats`: This is a list of valid date formats. This is used if `date_detection` is active.
- `numeric_detection`: This enables you to convert strings into numbers, if possible (`false` default).
- `dynamic_templates`: This is a list of templates that's used to change the explicit mapping inference. If one of these templates is matched, the rules defined in it are used to build the final mapping.

A dynamic template is composed of two parts: the matcher and the mapping one.

To match a field to activate the template, several types of matchers are available such as:

- `match`: This allows you to define a match on the field name. The expression is a standard GLOB pattern ([http:/​/​en.​wikipedia.​org/​wiki/​Glob\_(programming)]).
- `unmatch`: This allows you to define the expression to be used to exclude matches (optional).
- `match_mapping_type`: This controls the types of the matched fields. For example, string, integer, and so on (optional).
- `path_match`: This allows you to match the dynamic template against the full dot notation of the field, for example, `obj1.\*.value` (optional).
- `path_unmatch`: This will do the opposite of path_match, excluding the matched fields (optional).
- `match_pattern`: This allows you to switch the matchers to `regex` (regular expression); otherwise, the glob pattern match is used (optional).

The dynamic template mapping part is a standard one, but with the ability to use special placeholders, such as the following:

- `{name}`: This will be replaced with the actual dynamic field name
- `{dynamic_type}`: This will be replaced with the type of the matched field

The order of the dynamic templates is very important; only the first one that is matched is executed. It is good practice to order the ones with more strict rules first, and then the others.

## Managing nested objects

There is a special type of embedded object: the nested one. This resolves a problem
related to Lucene indexing architecture, in which all the fields of embedded objects
are viewed as a single object.

We need to index them in different documents and then join them. This entire trip is managed by nested objects and nested queries.

A nested object is defined as the standard object with the nested type.

Nested objects are not searchable with standard queries, only with nested ones. They are not shown in standard query results.
The lives of nested objects are related to their parents: deleting/updating a parent automatically deletes/updates all nested children. Changing the parent means Elasticsearch will do the following:

- Mark old documents as deleted
- Mark all nested documents as deleted
- Index the new document version
- Index all nested documents

Propagate information of nested objects to their parent or root objects. Nested objects require a special query to search for them

- `include_in_parent`: This makes it possible to automatically add the nested fields to the immediate parent
- `include_in_root`: This adds the nested object fields to the root object

## Managing a child document with a join field

Elasticsearch allows you to define child documents to avoid performance overhead while value of nested objest is changing. Nested objects depending from their parent. If value of a nested object is changed,the parent is reindexing.

The final mapping should be a merge of the field definitions of both `Order` and `Item`, plus a special field (`join_field`, in this example) that takes the parent/child relationship.

If we want to store the joined records, we will need to save the parent first and then the children in this way:

```
PUT test/_doc/1?refresh
{
    "id": "1",
    "date": "2018-11-16T20:07:45Z",
    "customer_id": "100",
    "sent": true,
    "join_field": "order"
}
```

```
PUT test/\_doc/c1?routing=1&refresh
{
    "name": "tshirt",
    "quantity": 10,
    "price": 4.3,
    "vat": 8.5,
    "join_field": {
        "name": "item",
        "parent": "1"
    }
}
```

The child item requires special management because we need to add the routing with the parent i. Furthermore, in the object, we need to specify the parent name and its ID.

In Elasticsearch, we have different ways to manage relations between objects, as follows:

- Embedding with `type=object`: This is implicitly managed by Elasticsearch and it considers the embedding as part of the main document. It's fast, but you need to reindex the main document to change a value of the embedded object.
- Nesting with `type=nested`: This allows for more accurate search and filtering of the parent using nested queries on children. Everything works for the embedded object except for query (you must use a nested query to search for them).
- External children documents: Here, the children are the external document, with a `join_field` property to bind them to the parent. They must be indexed in the same shard as the parent. The join with the parent is a bit slower than the nested one, because the nested objects are in the same data block of the parent in Lucene index and they are loaded with the parent, otherwise, the child document requires more read operations.

You do the join query in two steps:

- first, you collect the ID of the children/other documents and then
- you search for them in a field of their parent.

## Adding a field with multiple mappings

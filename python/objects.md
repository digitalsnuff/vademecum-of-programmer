# Objects

Every object in Python has an ID (or identity), a type, and a value.  
Once created, the identity of an object is never changed.
The value can either change or not. If it can, the object is said to be **mutable**, while when it cannot, the object is said to be **immutable**.

`Objects` are Python's abstraction for data.
Everything in Python is an object.

three features: an ID (unique), a type, and a value.
Objects are, in fact, instances of classes. The beauty of Python is that classes are objects themselves. It leads to one of the most advanced concepts of this language: `metaclasses`.
Classes are used to create objects. In fact, objects are said to be `instances of classes`.

A `method` is basically (and simplistically) a function that belongs to a class.

Method, `__init__` is an `initializer`. It uses some Python magic to set up the objects with the values we pass when we create it.

Every method that has leading and trailing double underscore, in Python, is called `magic method`. Magic methods are used by Python for a multitude of different purposes, hence it's never a good idea to name a custom method using two leading and trailing underscores. This naming convention is best left to Python.

# object and type

Data can be simple or complex.

Data can even be about data, that is, `metadata`. Data that describes the design of other data structures or data that describes application data or its context.

In Python, _objects are abstraction for data_.

If the value can change, the object is called `mutable`, while if the value cannot change, the object is called `immutable`.

When the object is created, then has a constant id. When value of property of the object is changed, the id of thr object is not changed.

When a variable has value of some simple type and when is assigned new value, the id of variable's object is changed.

Numbers are immutable objects.

## Integers

Python integers have unlimited range, subject only to the available virtual memory. This means that it doesn't really matter how big a number you want to store: as long as it can fit in your computer's memory, Python will take care of it. Integer numbers can be positive, negative, and 0 (zero). They support all the basic mathematical operations.

Python has two division operators, one performs the so-called `true division` (/), which returns the quotient of the operands, and the other one, the so-called `integer division` (//), which returns the floored quotient of the operands.

The result of an integer division in Python is always rounded towards minus infinity. If instead of flooring you want to truncate a number to an integer, you can use the built-in int function:

```py
int(1.75) # 1
int (-1.75) # -1
```

### Modulo

```py
>>> 10 % 3
 # remainder of the division 10 // 3
1
>>> 10 % 4
 # remainder of the division 10 // 4
2
```

## Booleans

Boolean algebra is that subset of algebra in which the values of the variables are the truth values: true and false.
In Python, `True` and `False` are two keywords that are used to represent truth values.
Booleans are a subclass of integers, and behave respectively like 1 and 0.
The equivalent of the int class for Booleans is the `bool` class, which returns either `True` or `False`. Every built-in Python object has a value in the Boolean context, which means they basically evaluate to either `True` or `False` when fed to the bool function.
Boolean values can be combined in Boolean expressions using the logical operators `and`, `or`, and `not`.

```py
int(True) # True behaves like 1
int(False) # False behaves like 0
bool(1) # 1 evaluates to True in a boolean context
bool(-42) # and so does every non-zero number
bool(0) # 0 evaluates to False
# quick peak at the operators (and, or, not)
not True # False
not False # True
True and True # True
False or True # True
```

You can see that `True` and `False` are subclasses of integers when you try to add them. Python upcasts them to integers and performs addition:

```py
1 + True # 2
False + 42 # 42
7 - True # 6
```

**`Upcasting`** is a type conversion operation that goes from a subclass to its parent. In the example presented here, `True` and `False`, which belong to a class derived from the integer class, are converted back to integers when needed.

# Reals

Real numbers, or floating point numbers, are represented in Python according to the IEEE 754 double-precision binary floating-point format, which is stored in 64 bits of information divided into three sections: sign, exponent, and mantissa.

Usually programming languages give coders two different formats: single and double precision. The former taking up 32 bits of memory, and the latter 64. Python supports only the double format.

Let's make a few considerations here: we have 64 bits to represent float numbers. This means we can represent at most `2 ** 64 == 18,446,744,073,709,551,616` numbers with that amount of bits. Take a look at the max and epsilon value for the float numbers, and you'll realize it's impossible to represent them all. There is just not enough space so they are approximated to the closest representable number. You probably think that only extremely big or extremely small numbers suffer from this issue.

Python gives you the `Decimal` type.

## Complex numbers

They are numbers that can be expressed in the form `a + ib` where `a` and `b` are real numbers, and `i` (or `j` if you're an engineer) is the imaginary unit, that is, the square root of -1. `a` and `b` are called respectively the `real` and `imaginary` part of the number.

```py
c = 3.14 + 2.73j
c.real # 3.14
c.imag # 2.73
c # (3.14+2.73j)

c.conjugate() # conjugate of A + Bj is A - Bj: (3.14-2.73j)
c * 2 # multiplication is allowed: (6.28+5.46j)
c ** 2 # power operation as well: (2.4067000000000007+17.1444j)
d = 1 + 1j # addition and subtraction as well
c - d # (2.14+1.73j)
```

## Fractions and decimals

Fractions hold a rational numerator and denominator in their lowest forms.

```py
from fractions import Fraction
Fraction(10, 6) # mad hatter?
Fraction(5, 3) # notice it's been reduced to lowest terms
Fraction(1, 3) + Fraction(2, 3) # 1/3 + 2/3 = 3/3 = 1/1
Fraction(1, 1)
f = Fraction(10, 6)
f.numerator
5
f.denominator
3
```

# Immutable sequences

Immutable sequences: strings, tuples, and bytes.

## Strings and bytes

Textual data in Python is handled with `str` objects, more commonly known as strings. They are immutable sequences of `unicode code points`. Unicode code points can represent a character, but can also have other meanings, such as formatting data for example. Python, unlike other languages, doesn't have a char type, so a single character is rendered simply by a string of length 1. Unicode is an excellent way to handle data, and should be used for the internals of any application. When it comes to store textual data though, or send it on the network, you may want to encode it, using an appropriate encoding for the medium you're using. String literals are written in Python using single, double or triple quotes (both single or double). If built with triple quotes, a string can span on multiple lines.

### Encoding and decoding strings

Using the `encode/decode` methods, we can encode unicode strings and decode bytes objects. `Utf-8` is a variable length character encoding, capable of encoding all possible unicode code points. It is the dominant encoding for the Web (and not only). Notice also that by adding a literal b in front of a string declaration, we're creating a `bytes` object.

```py
s = "This is üŋíc0de"  # unicode string: code points
type(s) # <class 'str'>
encoded_s = s.encode('utf-8')  # utf-8 encoded version of s
encoded_s # b'This is \xc3\xbc\xc5\x8b\xc3\xadc0de' # result: bytes object
type(encoded_s) # another way to verify it: <class 'bytes'>
encoded_s.decode('utf-8') # let's revert to the original: 'This is üŋíc0de'
bytes_obj = b"A bytes object" # a bytes object
type(bytes_obj) # <class 'bytes'>
```

### Indexing and slicing strings

When manipulating sequences, it's very common to have to access them at one precise position (indexing), or to get a subsequence out of them (slicing). When dealing with immutable sequences, both operations are read-only.

When you get a slice of a sequence, you can specify the `start` and `stop` positions, and the `step`. They are separated with a colon (:) like this: `my_sequence[start:stop:step]`. All the arguments are optional, `start` is inclusive, `stop` is exclusive.

## Tuples

A tuple is a sequence of arbitrary Python objects. In a tuple, items are separated by commas. They are used everywhere in Python, because they allow for patterns that are hard to reproduce in other languages. Sometimes tuples are used implicitly,for example to set up multiple variables on one line, or to allow a function to return multiple different objects and even in the Python console, you can use tuples implicitly to print multiple elements with one single instruction.

```py
t = () # empty tuple
one_element_tuple = (42, ) # you need the comma!
a, b, c = 1, 2, 3 # tuple for multiple assignment
a, b, c # implicit tuple to print with one instruction (1, 2, 3)
3 in three_elements_tuple # membership test: True

```

Notice that the membership operator **`in`** can also be used with `lists`, `strings`, `dictionaries`, and in general with `collection` and `sequence` objects.

One thing that tuple assignment allows us to do, is `one-line swaps`.

```py
a, b = b, a # this is the Pythonic way to do it
```

Python is elegant, where elegance in this context means also economy.

Because they are immutable, tuples can be used as keys for dictionaries. The dict objects need keys to be immutable because if they could change, then the value they reference wouldn't be found any more (because the path to it depends on the key). Tuples are Python's built-in data that most closely represent a mathematical vector. Tuples usually contain an `heterogeneous` sequence of elements, while on the other hand lists are most of the times `homogeneous`.

Tuples are normally accessed via unpacking or indexing, while lists are usually iterated over.

# Mutable sequences

## List

## Byte array

# Tools

[http://pythontutor.com/](http://pythontutor.com/)

```

```

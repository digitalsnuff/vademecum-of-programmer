# Python

Python is helpful to go through milions of lines of data.
It is used to host and design websites.

Python is what is called a **case-sensitive** language. Python cares about uppercase, lowercase, and whitespace. You need to watch what you type. You might get some strange messages from your computer if you make a typing mistake or a syntax error.

If you shut down your Python shell at any time during this chapter, all of your work will be lost. The Python shell does not remember information between sessions, so lists and dictionaries will not be saved.

Python is slower than its compiled brothers.

Python is used in many different contexts, such as system programming, web programming, GUI applications, gaming and robotics, rapid prototyping, system integration, data science, database applications, and much more.

Writing good code is, to some extent, an art.

Regardless of where on the path you will be happy to settle, there is something that you can embrace which will make your code instantly better: `PEP8`.

## Installing

Linux happens to be the most user friendly OS for Python programmers.

## Qualities

- portability
- coherence
- developer productivity
- an extensive library (e.g. Python Package Index - PyPI)
- software quality
- software integration
- satisfaction and enjoyment

## Variables, Functions and Users

To avoid errors, your variable names should use lowercase letters. If your variable
uses more than one word, such as the height_inches variable, then it should have
underscores to connect the words together.

### String

It is any piece of dta that is captured between two single quote marks. Sometimes, double quotation marks are used.

### Integer

Python stores whole number data as integers. It is a plain whole number.

### Float (floating point numner)

Fractional number is called **floating point number** (**floats** as a shortcut).

## Functions

Python functions are blocks of code that we can build to do a specific job. We can reuse them in our code by typing the name.  
Python has a lot of amazing functions that are built in.

```py
# function definition example
def addition():
    first_number = 30
    second_number = 60
    print(first_number + second_number)
```

After a function definition must be two lines separation.

A function needs to use lowercase letters, and when it has many words, there need to be underscores between each word.  
Python's use of indentation in known as **whitespace** and there are rules about whitespace use in Python.  
To use the function in the Python shell, type the name of the function and the parentheses.  
Typing the function is also known as **calling** the function.

`raw_input()` is a function thal allows us to tell the program to ask the user a question.
In Python 3 use simply `input`.

```py
name = raw_input('What is your name?')
print(name)
```

## Converting

To confert scalar value into another type, we can use following functions:
`float()`
`int()`

While float is converting to number, value is rounded down.

## Control flow

### Comparison operators

### Conditional statements

`if`, `elif`, `else` - three language constructs

### Loops

Loops are a kind of control flow, but they rerun the same block of code over and over again until something else tells the loop to stop repeating itselt.  
The two kinds of loop are:

- `while` - the program repeats itself until a given block of code happens.
- `for` - is used when a programmer knows exacly how many times they need the loop to repeat.

`range()` - this function allows us to secify a range of numbers instead of writing them all out.

## Scope

`quit()` function act like an `off` switch, stopping function from running.

Global variable can be used in any function we want throughout the program. If you want to use a global variable in a function, you can type `global` next to the name of the variable inside of the function.

## Comments

Comments beginswith `#`. Make sure that there is a space between the comment you wrote and the first line of computer-readable code.

## Libraries

`random`
`Pandas`
`Numpy`

## Tools

- PyPy
- virtualenv

### Virtualenv

virtualenv is a tool that allows you to create a virtual environment. In other words,
it is a tool to create isolated Python environments, each of which is a folder that
contains all the necessary executables to use the packages that a Python project
would need (think of packages as libraries for the time being).

So you create a virtual environment for project X, install all the dependencies, and
then you create a virtual environment for project Y, installing all its dependencies
without the slightest worry because every library you install ends up within the
boundaries of the appropriate virtual environment.

** always, always create a virtual environment when you start a new project.**

Install on Debian-based system

```sh
sudo apt-get install python-virtualenv
# or
sudo pip install virtualenv
```

`pip` is a package management system used to install and manage software packages written in Python.
Python 3 has built-in support for virtual environments, but in practice, the external libraries are still the default on production systems.

1. Create a folder named `learning.python` under your project root (which in my case is a folder called `srv`, in my home folder). Please adapt the paths according to the setup you fancy on your box.
2. Within the `learning.python` folder, we will create a virtual environment called `.lpvenv`.
3. After creating the virtual environment, we will activate it
4. Then, we'll make sure that we are running the desired Python version by running the Python interactive shell.
5. Finally, we will deactivate the virtual environment using the deactivate command.

Some developers prefer to call all virtual environments using the
same name (for example, .venv). This way they can run scripts
against any virtualenv by just knowing the name of the project
they dwell in. This is a very common technique that I use as
well. The dot in .venv is because in Linux/Mac prepending a
name with a dot makes that file or folder invisible.

```sh
virtualenv -p $( which python3.4 ) .lpvenv
```

## Run programs

### scripts

Python can be used as a scripting language. In fact, it always proves itself very useful. Scripts are files (usually of small dimensions) that you normally execute to do something like a task.

### interactive shell

### service

### GUI apps

- `Tkinter` - Tk: Tk Interface

Among the other GUI frameworks, we find that the following are the most
widely used:

- PyQt
- wxPython
- PyGtk

## Code organization

Starting with the basics, how is Python code organized? Of course, you write your code into files. When you save a file with the extension `.py`, that file is said to be a Python module.

Python gives you another structure, called `package`, which allows you to group modules together. A package is nothing more than a folder, which must contain a special file, `__init__.py` that doesn't need to hold any code but whose presence is required to tell Python that the folder is not just some folder, but it's actually a package (note that as of Python 3.3 `__init__.py` is not strictly required any more).

You can see that within the root of this example, we have two modules, core.py and run.py, and one package: util. Within core.py, there may be the core logic of our application. On the other hand, within the run.py module, we can probably find the logic to start the application. Within the util package, I expect to find various utility tools, and in fact, we can guess that the modules there are called by the type of tools they hold: db.py would hold tools to work with databases, math.py would of course hold mathematical tools (maybe our application deals with financial data), and network.py would probably hold tools to send/receive data on networks.

As explained before, the `__init__.py` file is there just to tell Python that util is a package and not just a mere folder.

A `library` is a collection of functions and objects that provide functionalities that enrich the abilities of a language.

## Execution model

Scope, names and namespaces.

Python names are the closest abstraction to what other languages call variables. Names basically refer to objects and are introduced by name binding operations.

Names that we can use to retrieve data within our code. They need to be kept somewhere so that whenever we need to retrieve those objects, we can use their names to fetch them. We need some space to hold them, hence: namespaces!

A `namespace` is therefore a mapping from names to objects - the global names in a module, and the local names in a function. Even the set of attributes of an object can be considered a namespace.

The beauty of namespaces is that they allow you to define and organize your names with clarity, without overlapping or interference. E.g.:

```py
from library.second_floor.section_x.row_three import book
```

A `scope` is a textual region of a Python program, where a namespace is directly accessible.

Scopes are determined statically, but actually during runtime they are used
dynamically. This means that by inspecting the source code you can tell what the scope
of an object is, but this doesn't prevent the software to alter that during runtime. There
are four different scopes that Python makes accessible (not necessarily all of them
present at the same time, of course):

- The `local` scope, which is the innermost one and contains the local names.
- The `enclosing` scope, that is, the scope of any enclosing function. It contains non-local names and also non-global names.
- The `global` scope contains the global names.
- The `built-in` scope contains the built-in names. Python comes with a set of functions that you can use in a off-the-shelf fashion, such as `print`, `all`, `abs`, and so on. They live in the built-in scope.

The rule is the following: when we refer to a name, Python starts looking for it in the current namespace. If the name is not found, Python continues the search to the enclosing scope and this continue until the built-in scope is searched. If a name hasn't been found after searching the built-in scope, then Python raises a NameError exception, which basically means that the name hasn't been defined (you saw this in the preceding example).

The order in which the namespaces are scanned when looking for a name is therefore: `local`, `enclosing`, `global`, `built-in (LEGB)`.

## Lists

Python lists are **mutable**. Items can be added or removed using functions that act directly on the list. Items in the list can be mixed together. Numbers, floats and strings can all go together in the same list.

Lists, like other kinds of data, are assigned to a variable.Then, the list items are placed in `[]`

```py
fruit = ['apple','banana','kiwi']
# append item
fruit.append('orange')
# remove item
fruit.remove('kiwi')
print(fruit)
```

If you have more than one of the same item in the list, list.remove() will only
remove the first instance of that item. The other items with the same name need
to be removed separately.

Once you create a list, the computer remembers the order of the list, and the order stays constant until it is changed purposefully.  
The first item of a Python list is always counted as 0 (zero).

`iteration` means repeating a process.

```py
# Loop with list
colors = ['green', 'blue', 'red']
for color in colors:
    print('I see ' + color + '.')
```

Lists and for loops are very powerful when used together.

## Dictionaries

May look just like list, but have different jobs, rules and different syntax than list.

Parts of a dictionary like lists, dictionaries have different parts that need to be used to make them work—names, use curly braces to store information.

```py
# key-value pairs in dictionary
numbers = {'one': 1, 'two': 2, 'three': 3}
# get item
print(numbers['one'])
# subscribe (add) new item
numbers['four'] = 4
# update value
numbers.update({'three':33})
# delete item
del numbers['three']
```

Once we store these items in our dictionary, we can add or remove new items (keys), add new amounts (values), or change the amounts of existing items.

Dictionary does not keep items in the order that they were entered.

To add an item to the dictionary, we will use what is called the **subscript** method to add a new key and new value to our dictionary.

# Source

- Jessica Ingrassellino, _Python Projects for Kids_, ISBN 978-1-78217-506-3, Packt Publishing 2016
- Fabrizio Romano, _Learning Python_, ISBN 978-1-78355-171-2, Packt Publishing 2015

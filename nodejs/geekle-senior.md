# How Javascript engine works: learn to write performant Javascript for Node.js

Ignition
Każda klasa literałowa posiada ukrytą klasę z Property attributes

Właściwości klas nalezy deklarować w klamerkach i unikać doisywania posa literałem

np.

```js
const user = {
  name: "Ashen",
  age: 40,
};
```

nie zapisujemy tak

```js
user.color = "blue";
```

gdyż jest to niewydajne

Nie robimy delete user.age, tylko robimy nadpisywanie na wartość 'undefined'

W podobnych obiektach z takimi samymi właściwościami dbaj o tą samą kolejnośćć deklaracji właściwości, gdyż jest to dobre dla ukrytych obiektów naszego obiektu.

---

## Closures

są to obiekty
muszą być alokowane
potrzebuja kontestu
kontekst też musi być alokowany

2 closures = 2 context

Unikajnie niepotrzebnych alokacji

nie deklarujemy funkcji w konteście, tylko poza kontekstem a potem używanmy w konteście

Hot Path - część kodu która jest mocno wykorzystywana, np. funkcja w pętli.

---

Speculative optimization

Parametry funkcji mogabyć dowolnego typu, więc w pętli dostarczaj funkcji argumenty tego samego typu, żeby np. funkcja przyjmowała 2 liczby i zwracała jednką, jak w deklaracji.

---

Turbofan

---

Write optimizable functions

Unikaj polimorfic functions, czyli przyjmujących argumenty innego typu niż trzeba. Nazywaj je `addInt`, `addString`, a nie `add`
Always provide Same Object Shapes

# How to contribute to Node.js

Improve skills
Meet nice people
team work
Network
Your name as contributor
Meaning
Work with different spec
Make it better

Coding Level for confidence

- Low
- Mid
- High
- Hero

# Deep dive into Node.js Streams

# Async Hooks - a journey to a realm where data survives the event loop

# WebAssembly

ASM.js
WASM
caniuse.com
awesome-wasm-lang

# Node.js N-API for Rust

API for building native addons
It allows modules compiled for one major version of Node.js to run on later ones without recompiling.

It id C++ wrapper around N-API

Rust:

- integrates with C APIs

# Aaaaaaaaaaaaaah, They’re Here! ES6 Modules in Node.JS

https://github.com/giltayar/ah-theyre-here-esm-nodejs

# Have your cake and eat it too: GraphQL? REST API? Maybe you can have both!

https://grpc.io/
https://medium.com/swlh/graphql-api-or-rest-api-why-not-have-both-c4171e68900a

# Request context tracking (AsyncLocalStorage use cases and best practices)

# A world of Rest APIs ... from Restify to a faster Fastify

http://sipp.sourceforge.net/

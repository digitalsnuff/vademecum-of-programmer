# Wprowadzenie do środowiska Node.js

Node.js stworzył Ryan Dahl w 2009 r.

Node Package Registry

Model sterowany zdarzeniami w odróżnieniu od modelu wątkowości.
Nodejs implemenuje pulę wielu wątków w tle.

W modelu opartym na wątkach operacje są wykonywane liniowo i sukcesywnie.

Model sterowany zdarzeniami:

- dodaje zadania do wykonania do kolejki zdarzeń
- korzysta z jednego wątku z działającą pętla zdarzeń, który odbiera zdarzenia
- petla pobiera pozycję najyżej położoną w kolejce zdarzeń, wykonuje to zadanie a nastepnie pobiera nastepną pozycję.
  Dowolne żądania, które powodują blokujące wejście-wyjście, są wykonywane w innym wątku w tle. Te zać mogą dodawać nastepne zadania do kolejki.

Projektowanie aplikacji wykorzystującej kolejke zdarzeń

- aby wykorzystać skalowalność i wydajność modelu zdarzeń, nalezy podzielić zadania na porcje, które mogą być realizowane jako seria wywołań zwrotnych
- w aplikacjach Node.js pracę planuje się w kolejce zdarzeń przez przekazanie funkcji wywołania zwrotnego używającej jeden z następujących metod:
  - utworzenie wywołania do jednego z wywołań biblioteki blokującego wejścia-wyjścia (zapisywanie do pliku, połączenie z baza danych)
  - dodawanie procesu nasłuchiwania zdarzenia do wbudowanego zdarzenia
  - tworzenie własnych emiterów zdarzeń i dodawanie do nich niestandardowych procesów nasłuchiwania
  - wykorzystanie opcji `process.nextTick` w celu planowania pracy, która zostanie pobrana w następnym cyklu pętli zdarzeń
  - zastosowania liczników czasu do planowania pracy, która zostanie wykonana po upływie konkretnego czasu lub w okresowych odstępach

Zastosowanie liczników czasu

- typy: limit (`setTimeout()`), interwał (`setInterval()`), natychmiastowy (`setImmediate()`)
- limit czasu powinien być używany w przypadku pracy, którą trzeba wykonać tylko raz
- interwał słuzy do wykjonywania pracy w regularnych odstępach opóźnienia. Powinien być stosowany dla zadań, które muszą być regularnie wykonywane
- natychmiastowe liczniki czasu służą do wykonania pracy od razu po rospoczęciu wykonywania przez wywołania zwrotne zdarzeń i/o, lecz przed wykonaniem jakichkolwiek zdarzeń interwału lub limitu czasu. Powinny być uzywane do zapewniania długotrwałych segmentów wykonywania innym wywołaniom zwrotnym. Funkcja wywołania zwrotnego jest umieszczana w kolejsce zdarzeń i aktywowana jednokrotnie dla każdej iteracji w pętli kolejki zdarzeń po tym, jak zdarzenia i/o uzyskaja szansę na wywołanie (tzn. w następnym cyklu)
- `unref()` - powiadamia pętlę zdarzeń o zaniechaniu kontynuowania działania. Jest ona umieszczana w obiekcie zwracanym przez metody `setInterval()` i `setTimeout()`, gdy metody te są jedynymi zdarzeniami w kolejce.
- `ref()` - ponowne utworzenie odniesienia
- `process.nextTick()` - planuje pracę do wykonania w następnym cyklu pętli zdarzeń.. Jest wykonywana przed aktywowaniem zdarzeń i/o. Funkcja `process.maxTickDepth`, która domyślnie wynosi 1000, ogranicza funkcję nextTick w celu zapobiegania zajęcia zasobów.

Emitery zdarzeń i procesy nasłuchiwania:

- zdarzenia są emitwane za pomocą obiektu `EventEmitter`. Wchodzi on w skład modułu `events`. Funkcja wspomnianego obiektu `emit` wyzwala zadeklarowane zdarzenie o podanej nazwie i dołącza ewentualne argumenty.
- prototyp `events.EventEmitter` można dodać do prototypu własnego obiektu. Jest to dziedziczenie funkcji obiektu. Wtedy wewnątrz obiektu wywołujemy `Events.EventEmitter.call(this)`. Wtedy zdarzenia mogą być emitowane bezpośrednio z instancji obiektu.
- procesy `nasłuchiwania` są dodawane do obiektu `EventEmitter` przy uzyciu jednej z następujących funkcji:
  - `addListener(name,callback)` - każdorazowo po wyzwoleniu zdarzenia funkcja zwrotna jest umieszczana w kolejce zdarzeń i czeka na wykonanie
  - `on(name,callback)` - analogicznie do `addListener()`
  - `once(name,callback)` - tylko za pierwszym razem po wyzwoleniu zdarzenia funkcja jest umieszczana w kolejce zdarzeń i czeka na wykonanie
  - `listeners(event_name)` - funkcja do zarządzania procesami nasłuchiwania. Zwraca tablicę funkcji procesu nasłuchiwania dołączonych do zdarzenia
  - `setMaxListeners(n)` - funkcja do zarządzania procesami nasłuchiwania. Wartość domyślna to 10. Wysyła ostrzeżenie po przekroczeniu ilości dodanych obiektów
  - `removeListeners(event_name, callback)` - usuwa funkcję wywołania zwrotnego

Implementowanie wywołań zwrotnych:

- przekazywanie parametrów do funkcji wywołania zwrotnego
- obsługiwanie parametrów funkcji wywołania zwrotnego wewnątrz pętli
- zagnieżdżanie wywołań zwrotnych

`Domknięcie` - zmienne są powiązane z zasięgiem funkcji a nie z zasięgiem fukcji nadrzędnej.

I/O

```
DeprecationWarning: Buffer() is deprecated due to security and usability issues. Please use the Buffer.alloc(), Buffer.allocUnsafe(), or Buffer.from() methods instead.
```

`JSON.parse(string)` - przekształca poprawny JSON w obiekt JS.
`JSON.stingify(onj)` - analizowanie obiektu JS i generowanie łańcucha JSON.

Standard UNICODE.

Dane buforowane są złozone z serii obiektów w formacie Big Endian lub Little Endian. Zajmuja przez to mniej miejsca, niż dane tekstowe.

`Endian` oznacza kolejność znaczących bitów podczas definiowania słowa. przechowuje najbardziej znaczące słowo jako ostatnie.
`Big Endian` - przechowuje najbardziej znaczące słowo jako pierwsze.

Obiekty Buffer to w rzeczywistości przydziały niesformatowanej pamięci. Podczas tworzenia obiektu nalezy określić jego wielkość.
Trzy metody tworzenia obiektu `Buffer`:

- `new Buffer(wielkosc_w_bajtach)`
- `new Buffer(tablica_obiektowa)`
- `new Buffer(łańcuch, [kodowanie])`

Nie można zwiększyć wielkości obiektu `Buffer` po jego utworzeniu, ale możliwe jest zapisanie danych w dowolnym położeniu w buforze.

Metody zapisywania w buforach:

- `buffer.write(string,[przesunięcie],[długość],[kodowanie])`
- `buffer[przesunięcie] = wartość` - zastępowanie danych
- `buffer.fill(wartość,[przesunięcie],[koniec])` - zapisuje wartość w każdym bajcie bufora
- `writeInt8(wartość, przesunięcie,[bez_asercji])` - metoda z gamy obiektów służących do zapisywania róznego typu liczb
- `writeInt16LE(wartość, przesunięcie,[bez_asercji])` - metoda z gamy obiektów służących do zapisywania róznego typu liczb
- `writeInt16BE(wartość, przesunięcie,[bez_asercji])` - metoda z gamy obiektów służących do zapisywania róznego typu liczb

Odczytywanie buforów:

- metoda `toString()`
- metoda `read()`
- `StringDecoder.write(buffer)`

Żeby określić długość bufora, mierzymy go za pomocą metody `byteLength(string,[kodowanie])`. Metoda mierzy bajty a nie poszczególne znaki, które to mogą być dwubajtowe.

Przy kopiowaniu bufora używamy metody `copy()`. trzeba pamiętac o analogicznym kodowaniu znaków. Można też przepisywać wartości poprzed odniesienie do indeksu.

`Segment` - sekcja bufora między początkowym i końcowym indeksem. Dzielenie bufora na segmenty umożliwia manipulowanie konkretną porcja danych. Są tworzone za pomocą metody `slice([begin],[end])`. Działanie na segmencie powoduje zmiany w obiekcie głównym.

Możemy łączyć dwa lub więcej buforów za pomocą metody `concat(list,[długość_całkowita])`. Dostarczamy tablicę obiektów jako pierwszy parametr.

Moduł `Stream` - strumienie danych są strukturami pamięci, które moga być odczytywane lub zapisywane (pliki, http). Jest to zapewienie transferu danych z jednego miejsca w inne. Strumienie udostępniają zdarzenia `data`, jeśli dane są dostepne do odczytu lub `error` po wystapieniu błędów. Strumienie są powszechnie używane na potrzeby danych i plików HTTP.

Stumienie:

- `Readable` - mechanizm pozwalający na odczyt danych trafiający do aplikacji z innego źródła (http, fs, zlib, crypto, TCP sockets, stdout, stderr, process.stdin). Metoda: `read`, `setEncoding`, `pause`, `resume`,`pipe`,`unpipe`. Zdarzenia: `readable`, `data`, `end`, `close`, `error`
- `Writable`- metoda: `write`; zdarzenia: `drain`,`finish`, `pipe`, `unpipe`
- `Duplex` - łączy funkcje strumieni `Readable` i `Writable`
- `Transform` - rozszerzenie strumeinia `Duplex`. Modyfikuje dane między strumieniami `Readable` i `Writable`

Zlib. Metody kompresji:

- `gzip/gunzip`
- `deflate/inflate` - kodowanie Huffmana
- `deflateRaw/inflateRaw`

Strumieniowanie

- `createWriteStream()`
- `createReadStream()`
- `exists()`
- `stats()` - uzyskanie informacji o plikach
- `readdir()`
- `unlink()` - usuwanie plików
- `truncte()`
- `mkdir()`
- `rmdir()`
- `rename()`
- `watchFile()`

HTTP

Socket

Gniazda umożliwiają jednemu procesowi komunikowanie się z innym procesem z wykorzystywaniem adresu IP i portu.
IPC - komunikacja międzyprocesowa np. dla dwóch procesów działających na tym samym serwerze lub w przypadku uzyskania dostępu do usługi uruchomionej na innym serwerze.
Gniazda to punkty końcowe komunikacji. Występują poniżej warstwy HTTP i zapewniają komunikację typu punkt-punkt między serwerami. Działanie gniazda bazuje na jego adresie, który stanowi kombinację adresu IP i portu.
Gniazdo wysyła niesformatowane dane za pomocą protokołu TCP. Protokuł odpowiada za pakowanie danych i zagwarantowanie, że zostaną pomyślnie wysłane z punku do punktu.
Gniazda to bazowa struktura moduu `http`.

## Skalowanie

Moduły:

- `process` - dostęp do działających procesów; udziela informacji o bazowej architekturze sprzętowej; zapewnia dostęp do `stdin`, `stdout`, `stderr`;
- `child_process` - pozwala na tworzenie procesów porzędnych i komunikowanie się z nimi
- `cluster` - implementowanie serwerów klastrowych współuzytkujących ten sam port

fork

## Plik package.json

## Moduły

Przydatne moduły

- console
  - log
  - info
  - error
  - warn
  - dir - zapisuje reprezentację łańcuchową obiektu JS do konsoli
  - time(etykieta)
  - timeEnd(etykieta)
  - trace
  - assert
- events
- buffer
- stream - readable, writable
- zlib
- http
- https
- url
- queryString
- net - możliwość utworzenia serwera gniazd, jak tez klientów mogących nawiązać połączenie z tym serwerem
- tls - pozwala zaimplementować bezpieczne serwery gniazd i klienty TLS.
- util
- dns
- os

`util.inspect()` - przekształca obiekt w łańcuch. Przydatne przy debugowaniu.



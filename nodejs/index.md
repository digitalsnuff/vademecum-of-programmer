# Nodejs

## Install

[Installing Node.js via package manager](https://nodejs.org/en/download/package-manager/)

[Debian and Ubuntu based Linux distributions, Enterprise Linux/Fedora and Snap packages](https://github.com/nodesource/distributions/blob/master/README.md)

## Installation instructions for Ubuntu

### Node.js v12.x:

```sh
# Using Ubuntu
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get install -y nodejs

# Using Debian, as root
curl -sL https://deb.nodesource.com/setup_12.x | bash -
apt-get install -y nodejs
```

### Node.js v11.x:

```sh
# Using Ubuntu
curl -sL https://deb.nodesource.com/setup_11.x | sudo -E bash -
sudo apt-get install -y nodejs

# Using Debian, as root
curl -sL https://deb.nodesource.com/setup_11.x | bash -
apt-get install -y nodejs
```

### Node.js v10.x:

```sh
# Using Ubuntu
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install -y nodejs

# Using Debian, as root
curl -sL https://deb.nodesource.com/setup_10.x | bash -
apt-get install -y nodejs
```

### Node.js v8.x:

```sh
# Using Ubuntu
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs

# Using Debian, as root
curl -sL https://deb.nodesource.com/setup_8.x | bash -
apt-get install -y nodejs
```

> Finally run:

```sh
 $ source ~/.profile
```

## Install custom npm packages

```sh
# NetBeans 11 tools
npm i -g gulp grunt bower sass less karma karma-cli express express-generator
```

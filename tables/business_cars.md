# Business Cards

## Dimensions

<table>
  <thead>
    <tr>
      <th>Standard</th><th>Wymiary (mm)</th><th>Wymiary (cale)</th><th>Proporcje (dłuższy bok/krótszy bok)</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>ISO 7810 ID-1, wymiar kart kredytowych</td><td>85,60 × 53,98</td><td>3,370 × 2,125</td><td>1,586</td>
    </tr>
    <tr>
      <td>ISO 216, format A8</td><td>74 × 52</td><td>2,913 × 2,047</td><td>1,423</td>
    </tr>
    <tr>
      <td>Australia, Nowa Zelandia</td><td>90 × 55</td><td>3,54 × 2,165</td><td>1,636</td>
    </tr>
    <tr>
      <td>Kanada, USA, Holandia</td><td>89 × 51</td><td>3,5 × 2</td><td>1,75</td>
    </tr>
    <tr>
      <td>Yongō, używane w Japonii</td><td>91 × 55</td><td>3,582 × 2,165</td><td>1,655</td>
    </tr>
    <tr>
      <td>Włochy, Wielka Brytania, Francja, Niemcy, Hiszpania</td><td>85 × 55</td><td>3,346 × 2,165</td><td>1,545</td>
    </tr>
    <tr>
      <td>Białoruś, Czechy, Kazachstan, Mołdawia, Polska, Rosja, Słowacja, Węgry</td><td>90 × 50</td><td>3,543 × 1,968</td><td>1,8</td>
    </tr>
    <tr>
      <td>Chiny</td><td>90 × 54</td><td>3,543 × 2,125</td><td>1,667</td>
    </tr>
  </tbody>
</table>

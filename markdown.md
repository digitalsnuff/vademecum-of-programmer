# Markdown

> Markdown offers two kind of headers: `Setext` and `atx`

> Markdown is a way to write content for the web. It’s written in what nerds like to call “plaintext”, which is exactly the sort of text you’re used to writing and seeing. Plaintext is just the regular alphabet, with a few familiar symbols, like asterisks ( \* ) and backticks ( ` ).

## Italics and Bold

> To make a phrase italic in Markdown, you can surround words with an underscore ( \_ )

> Similarly, to make phrases bold in Markdown, you can surround words with two asterisks ( \*\* )

> Of course, you can use _both italics and bold_ in the same line. You can also span them **across multiple words**.

> In general, it doesn't matter which order you place the asterisks or underscores. In the box below, make the words "This is unbelievable" both bold and italic. Place the asterisks **_on the outside_**, just to make it more legible.

## Headers

> To make headers in Markdown, you preface the phrase with a hash mark (#). You place the same number of hash marks as the size of the header you want. For example, for a header one, you'd use one hash mark (# Header One), while for a header three, you'd use three (### Header Three).

> `Setext` style: underscored by `=` or `-`

> In general, headers one and six should be used sparingly. You can't really make a header bold, but you can italicize certain w

## Links

> There are two different link types in Markdown, but both of them render the exact same way.

> The first link style is called an inline link. To create an inline link, you wrap the link text in brackets ( [ ] ), and then you wrap the link in parenthesis ( ( ) ).

> You can add emphasis to link texts, if you like. In the box below, make the phrase "really, really" bold, and have the entire sentence link to www.dailykitten.com. You'll want to make sure that the bold phrasing occurs within the link text brackets.

> The other link type is called a reference link. As the name implies, the link is actually a reference to another place in the document.

```
     Here's [a link to something else][another place].
     Here's [yet another link][another-link].
     And now back to [the first link][another place].

     [another place]: www.github.com
     [another-link]: www.google.com
```

## Images

> Images also have two styles, just like links, and both of them render the exact same way. The difference between links and images is that images are prefaced with an exclamation point ( ! ).

> For example, to create an inline image link to https://octodex.github.com/images/bannekat.png, with an alt text that says, Benjamin Bannekat, you'd write this in Markdown: ![Benjamin Bannekat](https://octodex.github.com/images/bannekat.png).

> For a reference image, you'll follow the same pattern as a reference link. You'll precede the Markdown with an exclamation point, then provide two brackets for the alt text, and then two more for the image tag. At the bottom of your Markdown page, you'll define an image for the tag.

# Blockquotes

> If you need to call special attention to a quote from another source, or design a pull quote for a magazine article, then Markdown's blockquote syntax will be useful. A blockquote is a sentence or paragraph that's been specially formatted to draw attention to the reader.

> To create a block quote, all you have to do is preface a line with the "greater than" caret (>).

> You can also place a caret character on each line of the quote. This is particularly useful if your quote spans multiple paragraphs.

> Block quotes can contain other Markdown elements, such as italics, images, or links.

## Lists

> There are two types of lists in the known universe: unordered and ordered. That's a fancy way of saying that there are lists with bullet points, and lists with numbers.

> To create an unordered list, you'll want to preface each item in the list with an asterisk ( \* ). Each list item also gets its own line.

> An ordered list is prefaced with numbers, instead of asterisks.

> You can choose to add italics, bold, or links within lists, as you might expect.

> Occasionally, you might find the need to make a list with more depth, or, to nest one list within another. Have no fear, because the Markdown syntax is exactly the same. All you have to do is to remember to indent each asterisk one space more than the preceding item.

> Stupendous! While you could continue to indent and add sub-lists indefinitely, it's usually a good idea to stop after three levels; otherwise, your text becomes a mess.

> There's one more trick to lists and indentation that we'll explore, and that deals with the case of paragraphs. Suppose you want to create a bullet list that requires some additional context (but not another list).

> To create this sort of text, your paragraph must start on a line all by itself underneath the bullet point, and it must be indented by at least one space.

## Paragraphs

# Helpful Links

- [https://www.markdowntutorial.com](https://www.markdowntutorial.com)
- [Wiki](https://pl.wikipedia.org/wiki/Markdown)

# Annotations

> `it's up to you` -> `to zależy od ciebie; wybór należy do ciebie`

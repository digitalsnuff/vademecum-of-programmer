# Sekurak TV Hack Steam 2020-03-18 19:00

## Narzędzia

- ldns-walk
- n3map
- hashcat

* https://regexper.com

# Sekurak TV Hack Steam 2020-03-20 20:00

- serwis crt.sh - Certificate Transpatency: to baza przechowująca informacje o certyfikatach.
- Jest też klient na PostgreSQL wykonywany w cli. Po odpaleniu nazwy domeny w serwisie, informacje są zapisywane do PostgreSQL.
- virustotal.com - rekonesans poddomen
- riskiq.com - PassiveTotal Search. Jest możliwość eksportu wyników do CSV. Portal ma inforamcje historyczne.
- dnstrails - securitytrails.com
- shodan.io
- automatyzacja powyższych czynności:
  - https://github.com/OWASP/Amass -
  - https://github.com/michenriksen/aquatone
- web.archive.org - archiwisuje strony internetowe

Passive DNS Replication - wirtualne domeny.

# Sekurak TV Hack Steam 2020-03-23 19:00

- app.mindmapmaker.org

* Kierunek defefensywny czy ofensywny?
* Defensywa:
  - Hardening - konfigurujemy daną usługę, by była jak najbardziej bezpieczna (systemy operacyjne, bazy danych, uługi sieciowe)
  - Analiza powłamaniowa - (preferowane umiejętności administacji staku technologicznego)
    - w jaki sposób doszło do włamania?
    - czy napastnik nie zostawił backdorów? - (preferowana wiedza, gdzie mozna takie backdory zostawić)
    - rekomendacja zabezpieczeń
  - monitoring (własne konfiguracje i sygnatury dla danej infrastruktury. Własne formuły do wykrywania)
    - IDS
    - IPS
    - SIEM
    - WAF - web application firewall
  - bezpieczna architektura (zasadan najmniejszych uprawnień, separation of duties)
  - zgodność ze standardami
    - RODO
    - PCI DSS
    - różne przepisy
  - Honeypot - systemy, które wystawiamy sobie w infrastrukturze, symulujące błędy bezpieczeństwa. Jest to niejako prynęta dla atakujących. Pozwala dowiedzieć się, jakie techniki ataków są stosowane.
  - analiza bezpieczeństwa kodu źródłowego
    - automatyczne, statyczna analiza
    - manualna - czytamy kod i szukamy błędów. Chodzi m .in. takie błędy, których analiza automatyczna nie potrafi wyłapać.
  - proces rozwoju oprogramowania (S-SDLC)
    - zdefiniowanie wymagań bezpieczeństwa
    - zdefiniowanie procesu zarządzania wersjami zależności
* ofensywa
  - testy aplikacji - przechwytywanie ruchu sieciowego, podmiana parametrów, obserwacja zachowań (najczęstszy typ testów)
    - webowe
    - mobilne
    - desktopowe
  - testy infrastruktury - jakie usługi działają na danych IP, czy mają mocne hasła
    - dostęp z internetu
    - dostęp LAN
  - socjotechnika
    - wysłanie maili fiszingowych
    - telefony - prośba o wykonanie operacji na komputerze
    - prośba wejścia do budynku, prośba o podłączenie pod sieć lokalną
  - DoS/DDoS - doprowadzenie do tego, żeby dany system przestał działać.
  - Bug Bouty - mżliwości testowania infrastruktury prawdziwych firm, bez narażania się na procesy sądowe oraz wypłacanie grantów za znalezienie bugów (google.com/about/appsecurity/reward-program/).
    - https://hackerone.com - strona zrzeszający programy Bug Bounty. Są na stronach udostępniających zasady, co można testować a co nie.
* Wiedza
  - Wiedza o typach ataków
  - uczestnictwo w programach Bug Bounty - Hackerone gromadzi informacje o rozwiązanych problemach. Dzięki temu możemy uczyć się na czyichś błędach. Nawet w, wydaje się, dojrzałych aplikacjach, mogą pojawiać się błędy.
  - opisy realnych błędów
    - pentester.land - najciekawsze wystapienia na prezentacjach z minionego tygodnia, najciekawsze wideeo, tutoriale etc.
    - pathonproject.com - bardziej techniczne
    - hackerone disclosure timeline
    - Twitter: @simsOn
  - metodyki/dokumentacje
    - OWASP TOP 10 - 10 najczęstrzych podatności bezpieczeństwa
    - OWASP ASVS - wymagania architektoniczne, np. konto o możliwie niskich uprawnieniach. Jest tam tylko, co należy sprawdzić, nie ma, jak to sprawdzić i jak ma być poprawnie.
    - OWASP Mobile ASVS -
    - CIS Benchmark - wymagania hardeningowe do różnych usług i różnych systemów.
  - Szkolenia
    - szkolenia Sekurak
  - certyfikaty
    - OSCP/OSCE
    - CEH
    - książki
      - ksiazka.sekurak.pl
      - The Hacker Playbook 3: Practical Guide To Penetration Testing
      - The IoT Hacker's Handbook, Apress
      - Attacking Network Protocols
    - aplikacje z błędami bezpieczeństwa
      - DVWA -
      - OWASP Juice Shop
      - OWASP WebGoat
      - Hack The Box
      - Web Security Academy
      - https://vulnhub.com/ - maszyny wirtualne z błędami
      - Lab OSCP/OSCE
  - CTF - wyzwania (https://ctftime.org). Sa tam też opisy, w jaki sposób CTF został rozwiązany.

## Najczęstsze języki programowaniaw narzędziach:

- Python
- Ruby
- Bash
- JavaScript (frontend)
- Java - w branży bankowej jest najczęstszym językiem.

Umiejetność programowania jest o tyle potrzebne, że można napisać własne rozwiązania i narzędzia.

ARP spoofing
CVE - Common Vulnerabilities and Exposures

# Bezpieczeństwo pracy zdalnej

- haveibeenpwned.com - wpisujemy e-mail, wyświetla czy jesteśmy w wyciekach
- tworzenie baz przez atakujących
- leak-loop.com
- weleakinfo.com

Jak szybko złamać hasło?

- atakujący używa krakera.
- narzędzie https://hashcat.net - jeśli zapomniałeś hasło, możesz je odzyskać (skrakować); szybkość 7 mld haseł na sekundę

Jak wygląda dobre hasło?

- min. 8 znaków, mała, duża litera - jest to minimum
- Polacy bardzo często stosują imiona w hasłach.
- https://password.kaspersky.com/ - wskaźniki, czy hasło jest silne czy słabe, należy traktować z ostrożnością.
- hasło powinno być długie. FBI zaleca 15 znaków. Można zrobić 4+ względnie losowych słów. Można zastosować 20+ znaków.
- manadżer haseł: KeePass. Dobry manager haseł przechowuje hasła w pliku, który jest zaszyfrowany.

Czy hasła należy często zmieniać?

- hasło należy zmieniać w ściśle określonych sytuacjach, np. w wyciekach.
- raz w miesiącu nie ma konieczności, jeśli nic się nie stało. Według zaleceń, nie ma konieczności wymuszania zmiany hasła.
- artykuł na Sekuraku: Czy wymuszać okresową zmianę hasła?

## Bezpieczeństwo domowej sieci WiFi

- sieci dzielimy na otwarte (bez hasła), i zabezpieczone (z hasłem)

Nie łączyć się do sieci otwartych w kawiarniach itp. gdyż hasła i dokumenty mogą być widoczne na radiu. Może przez to wyciągnąć hasło.
Każdy może się dostać do takiej sieci. Delikwent może w sieci atakować nasz komuter.

Nalezy łączyć się do sieci zabezpiecznonej: WPA (minimum), WPA2+AES (zalecane), WPA3 (w przyszłości)
Pamiętajmy o złożonych hasłach.
Należy zwrócić uwagę na domyślne hasła routera, które często są proste.
Można atakowac sieć nawet z odległości kilkaset metrów. Eksperymentalny projekt: kot został wyposażony w obroży w elektronikę atakującą sieci.

Należy zmienić hasło dostępowe do routera WIFI.

Zaleca się wyłączyć WPS oraz UPnP (protokół dla gier).
Aktualizacja firmware.
Wydzielenie osobnej sieci WIFi do pracy.
Aktualizowana checklista https://routersecurity.org

## Poczta elektroniczna

- czujność, by nie mylić adresów e-mail
- opjonalnie ustawienie wysyłki e-maili z opóźnieniem.
- dodatki, np. mozliwość cofnięcia wysyłki
- używanie opcji ukrytej kopii BCC
- szyfrowanie poczty - po to, by nikt inny nie odczytał wiadomości.
- miejsca szyfrowania poczty
  - komputer-serwer, swój komuter, wysyłający-czytający
  - ustawienia programu pocztowego: konfihuracja kont (w jaki sposób wysyłam a wjaki sposób odbieram e-maile)
  - preferowane SSL/TLS
  - szyfrowanie end2end (np. PGP). Przy wysyłce jest przycisk do szyfrowania wiadomości oraz do podpisywania. Można też ustawić hasło na wiadomość.
  - enigmail - dla Thunderbird (patrz: artykuł na Sekuraku "Szyfrowanie poczty w Thunderbird")
  - szyfrowanie dla poufnej i wrażliwej wiadomości
- szyfrowanie załączników (np. 7zip). Innym kanałem (np. sms) wysyłamy hasło. (metoda mandżurska)
- Webmail - weryfikujmy czy wchodząc na webmail używamy poprawnej domeny.
- Trzeba uważać na linki w sms.
- Nie wchodzimy na strony z błędem bezpieczeństwa. Burp Suite - to narzędzie do nasłuchiwania ruch sieciowego. Można zaakceptować niepożądany certyfikat.
- pamiętajmy o wylogowaniu się z webmail.

## Podstawowe zasady bezpieczeństwa pracy na komputerze

- Rozdzielenie sprzętu prywatnego od firmowego
- nie pożyczaj komutera firmowego innym osobom
- blokujemy komuter po odejściu od stanowiska
- nie wyłączajmy firewalla
- kopie zapasowe - zabezpieczone, przetestowane, czy się dobrze robią, harmonogram kopii, rodzaj kopii (np. przyrostowa)
- zabezpieczenie pendrive
- recuva - narzędzie do odzyskiwania plików.
- komunikatory szyfrujące komunikację
- zaklejanie kamery -
- drukowanie/skanowanie dokumentów w domu
- aktualizacja
- eksploity - programy wykorzystujące podatność w narzędziach.

## VPN

- przynajmniej uwieżytelnienie hasłem
- poleca się dwustopniowe uwieżytelnienie.
- monitorowanie naruszeń

# Remote Securak Hacking Party 03.2020

... XXS

## Path Traversal

- Nuxeo - oprogramowanie do zarządzania dokumentami. Błąd path traversal czyli doklejanie ścieżek tylu '../../../'
- Tomcat - serwer dla Java. Błąd '/..;/'
- Reading ASP secrets - GET any//any.aspx?f=.+./.+./web.config
- Przejęcie serwerów Twitter - path traversal w urządzeniu VPN (lekki backdor)
- fortigate SSL VPN: CVE-2018-13379 - snprintf(s, 0x40, "migadmin/lang/%s.json", lang); s można ustawić na '../../../' aż funkcja obetnie znaki i dostaniemy dostęp.
- Cisco - /+CSCOE+/files; /+CSCOU+/
- robots.txt - np. tripadvisor.com/robots.txt; jest też https://facebook.com/.well-known/apple-app-site-association
- DNSSEC recon (\*)
- XXE - XML z DOCTYPE z ENTITY shp SYSTEM (encja zewnętrzna), która ma wartość "/etc/passwd"
- Github - autoryzowanie przez OAuth
- CSRF - zmuszenie przeglądarki ofiary do wykonania żądania
- Drupal - żadanie POST do API. GET były nieuwierzytenione. Można było to samo wyciągnąć na pomoca POST.
- phar:// - rce


## zadanie

- cztery poddomeny hackingparty.pl

# CMS

> Content management Systems allow for quick development and give your clients the ability to update their content. May not be a bad idea to pick one up. Great for freelancers.

- PHP based - Wordpress, Drupal
- JS based - Ghost, Keynote
- Python based - Mezzazine
- .NET - Piranha, Orchard CMS

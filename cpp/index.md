# Język C++. Szkoła programowania

Proces przechodzenia od struktur niższego poziomu (klas) do całego programu nazywamy programowaniem **od szczegółu do ogółu**.

Paradygmaty:
- programowanie obiektowe
- programowanie uogólnione
- programowanie proceduralne

`Programowanie uogólnione` (albo rodzajowe, generic) to paradygmat programowania dążący do uproszczenia wielokrotnego wykorzystywania kodu oraz technika wydzielania abstrakcyjnych pojęć ogólnych.
W programowaniu obiektowym największy nacisk kładziony jest na dane. Ma też ułatwić zarządzanie dużymi projektami.
W programowaniu uogólnionym akcentuje się uniezależnienie operacji od typu danych. Ma ułatwić wykonywaie typowych zadań, np. sortowanie, łączenie list. Jest to kod niezależny od konkretnych typów.
Jego cechą wspólną z programowaniem obiektowym jest dążenie do uproszczenia wielokrotnego wykorzystywania kodu oraz technika wydzielnia abstrakcyjnych pojęć ogólnych. 
Programowanie uogólnione ma zastosowanie jako pomoc w rozwiązywaniu problemów - wykonywania typowych zadań, takich jak sortowanie, łączenie list. 
Programowanie obiektowe ma za zadanie ułatwić zarządzanie dużymi projektami.
Pojęcie `uogólnione` odnosi się do tworzenia kodu niezależnego od konkretnych typów.

`Program przenośny` jest wtedy, gdy, jeśli możliwe jest uruchomenie programu po rekompilacji na inych platformach i normalne używanie go.
Program, który zależy od kofiguracji sprzętowej, nie będzie przenośny.

ANSI X3J16 - standard dla języka c++ z 1990 roku. Potem ISO-WG-21. Są to standardy głównie dla kompilatorów, jak też samego języka.

ISO/IEC 14882:1998 - określany mianem C++98.
RTTI - mechanizm identyfikacji czasu wykonania.

C++03 - ISO/IEC 14882:2003

C++11 - ISO/IEC 14882:2011 (C++0x)

Standard ISO C++ wykorzystuje standard ASCI C

Standard ISO C++ posiada nieformalne nazwy: C++99 i C++03, a czasami C++99/03.
Następna wersja to C++0x, która przyjeła nazwę C++11.

`RTTI` - wykrywanie typu w czasie wykonywania programu.
`STL` - Standard Template Library - standardowa biblioteka szablonów. Służy programowaniu uogólnionemu.

Funkcje to podstawowe elementy składowe programu w C++.

`Klasa` to typ danych zdefiniowany przez uzytkownika.

Typ referencyjny r-wartości.

Semantyka przenoszenia.

Wyrażenia lambda.

C++ to język
- wydajny
- zwarty
- szybki
- przenośny


Standard dla języka C to standard ANSI C.

`Konsolidacja` - polega na łączeniu własnego kodu wynikowego z kodem wynikowym używanych funkcji oraz dodakowym kodem startowym, który pozwala nasz program w ogóle uruchomić.

W systemie UNIX używamy rozszerzenia plików `*.C` dla C++. Dla C używamy rozszerzenia `*.c`. Można też stosować rozszerzenia `*.cc` lub `*.cxx`, chociażby dlatego, że system Windows nie rozróżnia wielkości liter. 
Stosuje się również rozszerzenia `*.cpp` oraz `*.c++`.


Instrukcja reprezentuje czynność do wykonania. W c++ instrukcję kończy średnik.

Nagłówek funkcji opisuje interfejs funkcji wykonywanej na potrzeby funkcji wywołującej.

`Preprocesor` - program przetwarzający wstępnie pliki źródłowe przed zasadniczą kompilacją.

Manipulatory (np. endl).

endl wymusza wypróżnienie, czyli natychmiastowe wyświetlenie na ekran, przed wykonaniem następnujących po nim instrukcji.

W C++ znak nowego wiersza jest traktowany tak samo, jak spacje i tabulatory.

`Raw string` - ciąg surowy.

`Token` - element języka - niepodzielny element w wierszu kodu.

## Styl pisania kodu

- w jednym wierszu jedna instrukcja
- otwierający i zamykający nawias funkcji mają swoje własne wiersze 
- onstrukcje objete funkcją są wcięte względem nawiasów klamrowych
- przy nawiasach związanych z nazwą funkcji nie umieszcza się białych znaków
- po deklaracji zmiennych zostawiamy pustą linię, jako separator sekcji deklaracji i dalszej części kodu


Aby zapisać na komputerze jakąkolwiek informację, trzeba wskazać położenie tej danej oraz określić ilość zajmowanego przez nią miejsca.
`Instrukcja deklaracji` określa rodzaj zajmowanej pamięci i nadaje temu miejscu nazwę.
Kompilator alokuje tyle pamięci, ile potrzeba dla typu, przykładowo, _int_. Przy tym oznacza miejsce w pamięci.
Odwołując się do nazwy, program zawsze będzie sie odnosił do tego samego miejsca w pamięci.

W C++ wszystkie zmienne należy deklarować. Zmienne należy deklarować tuż przed ich pierwszym użyciem.

`Deklaracja definiująca`, czyli `definicja`.

`Deklaracje referencji` - uzywanie zmiennej już gdzieś indziej zdefiniowanej.

`Instrukcja przypisania` powoduje przypisanie wartości pewnemu miejscu w pamięci.

## cout

- zastępuje zmienne odpowiednią wartością
- wartość ta zamieniana jest na odpowiednie znaki
- może działać z łańcuchami  znakowumu i z  liczbami

Operator `<<` dostosowuje swoje zachowanie do typu danych, na których operuje. Jest to przykład przeciążania operatorów. 
Posiada możliwość "odgadywania" typów.
Operator ten mozna przedefiniować dla nowych typów.

# cin

- strumień wejściowy jest jest strumieniem znakowym wpływajacym do programu
- potrafi konwertować dane wejściowe będące łańcuchem znaków z klawiatury na postać odpowiednią dla zmiennej

## klasy

`Klasa` to typ (typ obiektowy) danych zdefiniowany przez użytkownika. Definicja klasy mówi, jak dane są zbudowane i jak można ich używać.

## Funkcje

- to moduły, z których składają się programy C++. 
- są kluczowe dla definicji obiektowych
- funkcje moga zwracać wartośc lub nie zwracać wartości
- funkcja mająca wartość zwracaną jest generatorem wartości. Jest to wartość niejako podstawiana w miejsce wywoływanej funkcji

Program w C++ powinien zawierać prototypy wszystkich uzywanych w nim funkcji. 
Prototyp to taki rodzaj deklaracji funkcji, który informuje o używanych typach na wejściu i wyjściu. 
Instrukcja taka kończy się średnikiem - w przeciwnym wypadku instrukcja jest traktowana jako `nagłówek funkci`.
Można prototyp dołączać w kodzie źródłowym, jak też za pomocą dołącznia pliku z nagłówkami.

Nie należy mylić definicji funkcji z prototypem. Prototyp opisuje jedynie interfejs funkcji, czyli mówi, jakie informacje są przekazywane do funkcji i jakie informacje ta funkcja zwraca.
Definicja z kolei zawiera kod realizujący zadania funkcji.

Powszechna praktyka jest umieszczanie prototypu przed definicją funkcji `main()` a przynajmniej przed pierwszym wywołaniem funkcji.
Dołączamy plik nagłówkowy a kompilator szuka w bibliotekach użyte funkcje. Jeśli kompilator nie znajdzie funkcji w bibliotekach mozna posłuzyć się opcją `-lm` (library math).

W C++ nie można zagnieżdżać definicji funkcji wewnątrz innej definicji funkcji.
Każda definicja funkcji jest niezależna i wszystke funkcje są sobie równe.

Program w języku C++ składa się  z jednego lub wielu modułów nazywanych funkcjami.

# Dane

Typy wbudowane całkowitliczbowe:
- unsigned long
- long
- unsigned int
- int
- unsigned short
- short
- char
- unsigned char
- signed char
- bool
- unsigned long long (new)
- long long (new)

Wbudowane typy zmiennoprzecinkowe:
- float
- double
- long double

Samą istotą programowania obiektowego jest projektowanie i rozbudowywanie własnych typów danych.

Istnieje kategoria typów prostych (podstawowych) i kategoria typów złożonych.

Trzy podstawowe cechy danej:
- miejsce przechowywania informacji
- przechowywana wartość
- rodzaj przechowywanej informacji

Za pomocą operatora `&` można odczytać adres zmiennej w pamięci.

Zasady tworzenia nazw zmiennych:
- jedyne znaki dopuszczalne w nazwach to litery, cyfry oraz podkreślenie
- pierwszy znak nie może być cyfrą
- wielkie litery są odróżniane od małych liter
- nazwa nie może być słowek kluczowym języka C++
- nazwy zaczynające się od dwóch podkreśleń lub od podkreślenia i za nim wielkiej litery są zarezerwowane dla implementacji, to znaczy dla kompilatora i wykorzystywanych przezeń zasobów. 
  Nazwy zaczynające się od pojedynczego podkreślenia zarezerwowane są jako identyfikatory globalne implementacji
- W C++ długość nazwy zmiennej nie jest ograniczona i wszystkie znaki są brane pod uwagę. Trzeba jednak pamiętać, że na niektórych platformach implementacje wprowadzają własne ograniczenia
- zaleca się stosowanie znaczących naz zmiennych. Nazwa zmiennej ma opisywać daną zmienną.
- Jeśli chcemy stworzyć nazwę składającą się z paru słów, poszczególnme segmenty należy rodzielać podkreślnikiem (notacja snake_case) lub w notacji camelCase

Określając ilość pamięci zajmowanej przez typy całkowitoloczbowe, mówi się zwykle o `szerokości`. Im wiecej miejsca dana wartość zajmuje, tym jest szersza.

Podstawowe jednostki pamięci komuterowej to `bity`. To taki przełącznik elektroniczny.
`Bajt` to 8-bitowy fragment pamięci. Tak rozumiany bajt jest jednostką miary pamięci komputera.
W C++ bajt składa się z przynajmniej tylu sąsiadujących bitów, ilu trzeba na zapisanie podstawowego zestawu znaków używanego w danej implementacji. Wobec tego liczba możliwych wartości bajta musi być przynajmniej równa liczbie różnych znaków.
Jak dotąd kraje stosujące zmodyfikowany alfabet łaciński zwykle bazują na zestawach znaków ASCII i EBCDIC mających 8 bitów na znak. Wobec tego C++ ma przeważnie 8 bitów. 
Jednakże w przypadku programowania w środowisku bazującym na większych zestawach znaków, jak UNICODE, bajt może mieć 16, a nawet 32 bity. 

C++, jeśli chodzi o typy całkowitoliczbowe, jest elastycznym standardem i określa tylko gwarantowaną wielkość minimalną. Taka konstrukcja pochodzi jeszcze z C. Oto przyjęte ograniczenia:
- liczby typu `short` muszą mieć przynajmniej 16 bitów
- liczby typu `int` muszą być nie mniejsze od liczb typu `short`
- liczby typu `long` muszą mieć przynajmniej 32 bity i być nie mniejsze od licczb typu `int`
- liczby typu `long long` muszą mieć przynajmniej 64 bity i być nie mniejsze od liczb typu `long`

Typy całkowite obecnie dla wielu systemów są ustalone na poziomie minimalnym:
- short -> 16 bitów
- long -> 32 bity
- int -> 16, 24, 32 bity
- long long -> 64 bity

`Operator` to wbudowany w język element działający na jednym lub większej liczbie parametrów i zwracający wartość.

`Stałe symboliczne procesora`
- podczas kompilacji kod najpierw jest przetwarzany przez preprocesor
- `#define` jest dyrektywą preprocesora np. `#define INT_MAX 32767`, jak też `#include`. Działa na zasadzie znajdź wszystkie wystąpienia i zamień na wartość.

`Inicjalizacja` stanowi połączenie deklaracji i przypisania.
Miejsce inicjalizacji ma znaczenie, gdyż zmienna bez wartości (a dokładnie z wartością losową pamięci) spowoduje błąd w programie lub nieoczekiwane wyniki. Wartość taka nazwa się `wartością nieokreśloną`
Dwa sposoby inicjalizacji:
```cpp
int sowy = 101; // sposób C
int strzyzyki(432); // sposób C++; nie działa w C
int hamburgers = {} // sposób  C++ 11 na zasadzie tablicy
int emus{7} // jak wyżej, sposób C++ 11, z tym, że bez znaku przypisania
int rocs = {}; int physics{} // ustawia wartość na zero
```
Składnia inicjalizacji klamrowej lepiej zabezpiecza przed błędami konwersji typów.
Dodanie  klamrowej składni inicjalizacji ma ujednolicić inicjalizowanie zmiennych prostych z inicjalizowaniem obiektów klas i struktur.

Domyślnie typy bez prefiksu są `signed`. Żeby były bez znaku należy poprzedzić nazwę typu `unsigned`.
Samo `unsigned` oznacza `unsigned int`.

## Problem przepełnienia liczb całkowitych

Jeśli do smiennej o maksymalnej mozliwej wartości`SHRT_MAX` dodamy jeden, nastąpi zerowanie i wartość będzie równała się zero.

## Literał całkowitoliczbowy

`Literał całkowitoliczbowy` (stała całkowitoliczbowa; integer literal) - to taka stała, którą zapisujemy jawnie w programie. Mozna je zapisywać w trzech systemach liczbowych: dziesiętnym, ósemkowym (Unix), szesnastkowym (hakerzy).
Pierwsza lub dwie początkowe cyfry mówią, w jakim systemie zapisano stałą. Jeśłi pierwsza cyfra jest z zakresu 1-9, to stała jest zapisana w systemie dziesiętnym.
Jeżeli  pierwsza cyfra jest zerem, a druga jest z zakresu 1-7, podstawą systemu jest osiem.
Jeżeli dwa pierwsze znaki to `0x` lub `0X`, mamy do czynenia z systemem szesnastkowym. Tutaj liczby od 10 do 15 są zapisywane literami od a do f.
Domyślnie `cout` wyświetla liczby całkowite dziesiętne, niezależnie od tego, jak zostały zapisane w programie.
`iostream` zawiera manimpulator `endl`.  Zawiera również manipulatory `dec`, `oct`, `hex`, które nakazują `cout` wyświetlać liczby całkowite odpowiednio dziesiętnie, ósemkowo lub szesnastkowo.
System dziesiętny jest domyślny. Każdy system pozostaje aktywny tak długo, aż zostanie zmieniony na inny. 
C++ traktuje wszystkie stałe całkowite jako wartości typu `int`, o ile nie nastąpią przesłanki do innej interpretacji. Jedną taka przesłanką jest dodanie do stałej przyrostka określającego typ, drugą wartość zbyt duża dla typu `int`.
`Przyrostki` to litery dodawane na końcu liczby, określające typ:
- `L, l` -> typ long
- `U, u` -> typ unsigned int
- `UL, ul, LU, lu` -> typ unsigned int
- `LL, ll` -> typ long long
- `ULL, Ull, uLL, ull` -> typ unsigned long long

Wielkość liczb stałych jest różna dla każdego z trzech systemów liczbowych. 

## char

Typ `char` ma przechowywać znaki takie jak litery czy cyfry. Języki programowania zamiast liter, przechowuja ich kody liczbowe, dlatego ten typ jest kolejnym typem liczbowym.
Najczęściej uzywanym w Polsce zestawem znaków jest jeden z rozszerzonych zestawów ASCII, ISO 8859-2 lub Windows 1250.
`cin` i `cout` przeprowadzają konwesje na bierząco z wartości liczbowej typu `char` na znak. O zachowaniu decyduje typ zmiennej.
`Literały znakowe` otaczamy apostrofem. Cudzysłów jest dozwolony, jednakże ogranicza łańcuch znakowy.
`cout.put('M')` - wyświetla pojedynczy znak lub stałą znakową. Metoda put pozwala na wyprowadzenie na zewnątrz znaków. Jest to alternatywa operatora `<<`.
Każda implementacja C++ obsługuje podstawowy zestaw znaków, w jakim zapisywane są programy - czyli zestaw, w którym zakodowane są źródła. Zestaw ten składa się z liter (wielkich i małych) znajdujących się na klawiaturze amerykańskiej oraz cyfr i symboli używanych w C oraz znaki specjalne, wykonawcze. Do tego dochodzą znaki miedzynarodowe.
`Uniwersalne nazwy znaków` używa sie podobnie, jak znaków specjalnych. Nazwa znaku uniwersalnego zaczyna się `\u` lub `\U` po czym kolejno następuje osiem i szesnaście cyfr szesnastkowych. Cyfry  te służą do zapisu kodu znaku zgodnie z ISO 10646.

Typ `char` nie jest typem ani ze znakiem ani bez znaku. Żeby takim się stał, należy jawnie zadeklarować zakres. Zmienne `unsigned char` mają zakres od 0 do 255. Taka deklaracja ma sens, gdy wykorzystujemy ten typ jako typowo liczbowy.

### wchar_t

Służy do obsługi znaków niemieszczących się w pojedynczych, 8-bitowych bajtach.
Można też ustawić typ `char` na 16-bitowy lub większy.
Drugim sposobem są dwa typy: na podstawowy zestaw znaków (`char`) i rozszerzony zestaw znaków (`wchat_t`).
`wchar_t` jest typem całkowitoliczbowym. Trzeba ustalić w systemie typ bazowy dla określenia wielkości tego typu.
Obiekty z biblioteki `iostream` do przetwarzania tego typu to `wcin` i `wcout`. Literał będący znakiem rozszerzonym definiujemy poprzedzając go literą `L`.

### char16_t i char32_t

Są to nowe typy standardu C++ 11. 
Typ `char16_t` jest bez znaku i zawsze o szerokości 16 bitów.
Typ `char32_t` jest bez znaku i zawsze o szerokości 32 bitów.
Jest to ustalenie konkretnego rozmiaru, niezależnego od relatywnych implementacji wielkości typów. Bazują jednak na jednym z wbudowanych typów całkowitoliczbowych. Konkretny dobór typów może być różny w różnych systemach.
W C++11 literały znakowe i napisowe dla typu `char16_t` są poprzedzane literą `u`, natomiast literały znakowe i napisowe dla stałych `char32_t` mają przedrostek `U`.

## Bool

George Bool stworzył matematyczny zapis praw logiki.

Literały `true` i `false` można rzutować na typ `int`: `true` jest zamieniane na `1` a `false` na `0`.
Każda liczba i każdy  wskaźnik mogą być konwertowane niejawnie na typ `bool`. Wartości niezerowe są konwertowane na `true` a zerowe na `false`.

## Kwalifikator const

Nazwy symboliczne stałych - powinny sugerować, co stała opisuje.
Słowo kluczowe `const` modyfikuje deklarację zmiennej  i jej sposób inicjalizacji. Np. `const int Miesiace =12;`. Takiej wartości kompilator nie pozwoli zmienić (read-only).
Słowo kluczowe `const` to `kwalifikator`, bo kwalifikuje deklarację, jako stałą.
W deklaracji `const` inicjalizacja występuje od razu.

## Liczby zmiennoprzecinkowe

Posiadają część ułamkową. Służą do zapisywania ogromnych liczb. Słuzy do zapisu liczb z częścią ułamkową. Komputer przechowuje takie wartości w dwóch częściach: jednej oznaczającej rzeczywistą część liczby, a drugiej określającej przesunięcie tej wartości w lewo lub w prawo. Czynnik  skalujący powoduje przesunięcie przecinka dziesiętnego, stąd mowa o `zmiennoprzecinkowości`.

Istnieja dwa rodzaje zapisu liczb zmiennoprzecinkowych:
- zapis z kropką dziesiętną - można też zmienić ustawienia lokalne i jako separator ustawić przecinek, jednak dotyczy to jedynie wyświetlania wyników w systemie
- notacja naukowa - używa się oznaczenia EN, co oznacza, że N jest liczna naturalną i jest to oznaczenie liczby 10 podniesionej do potęgi N. Np.  E6 oznacza 1 000 000. E jest wykładnikiem a liczba po lewej stronie to mantysa.

Typy zmiennoprzecinkowe to
- float - cyfry znaczące muszą być zapisywane przynajmniej na 32 bitach
- double - cyfry znaczące muszą być zapisywane przynajmniej na 48 bitach i nie mniej, niż w typie float
- long double - wymaga się przynajmniej tylu bitów, ile jest w typie double

Zwykle typ float ma 32 bity, double 64 bity a long double 80,96 lub 128.
Zakres składników dla wszystkich trzech typów nie może być mniejszy od -37 do +37.

Typy te opisuje się przez liczbę cyfr znaczących oraz dopuszczalny zakres wykładników.
Cyfry znaczące to  cyfry, które mają wpływ na wielkość liczby.

`cout` domyślnie pomija końcowe zera. `setf` zmienia ten sposób działania.

Stałe zmiennoprzecinkowe domyślnie są typu `double`. Jeśłi ma być to `float` należy dodac przyrostek `f` lub `F`. W przypadku `long double` należy dodać `l` lub `L`.
Działania na liczbach zmiennoprzecinkowych są wolniejsze od działań na liczbach całkowitych.

Typy całkowite i zmiennoprzecinkowe to `typy arytmetyczne`.

## operatory arytmetyczne

Obliczenia arytmetyczne wykonuje się za pomocą operatorów. Każdy z pięciu podstawowych operatorów pobiera dwie wartości nazywane `argumentami` lub `operandami`. Operatory i operandy tworzą `wyrażenia`. Uzycie operatora `%` z liczbami zmiennoprzecinkowymi powoduje błąd kompilacji.

Operatory arytmetyczne mają priorytety podobne do zwykłych, algebraicznych zadad wykonywania działań. Kolejność działań wymuszamy poprzez zastosowanie nawiasów.

Jeżeli operatory maja ten sam priorytet, C++ sprawdza, które operatory mają łączność od lewej do prawej, a które od prawej do lewej.

Użycie tego samego symbolu operatora do więcej niż jednego działania nazywamy `przeciążaniem operatora`.

- C++ konwertuje wartości w chwili przypisywania wartości jednego typu arytmetycznego zmiennej innego typu arytmetycznego
- C++ konwertuje wartości, jeśli w wyrażeniu użyto różnych typów
- C++ konwertuje wartości   przekazywane funkcjom jako parametry

`list initialization` - inicjowanie listą to inicjalizacja z uzyciem klamer. Ten zapis zasadniczo służy także do podawania listy wartości do inicjalizacji zmiennych typów złozonych.
Inicjalizacja listą nie pozwala na zawężanie typu.

W konwersji automatycznej C++ typy `bool`, `char`, `unsigned char`, `signed char` i `short` konwertuje na typ `int`. Jest to `promocja typów całkowitych`.
W działaniach arytmetycznych typ mniejszy jest promowany do większego.
Porządek typu `type rank`.

Jawne rzutowanie typów:
```cpp
(long) thorn // C style
long (thorn) // C++ style. Looks like function
static_cast<long> (thorn) // jeden z czterech operatorów rzutowania typu
```

Automatyczne deklaracje typów polega na uzyciu słowa kluczowego `auto` zamiast konkretnego typu. Jest ot użyteczne przy typach skomplikowanych, np. przy pracy z typami szablonów STL.+

## Typy złożone

Najbardziej zaawansowanym  typem złożonym jest klasa.
Tablica może zawierać wiele wartości tego samego typu. Specjalnym typem tablicy jest łańcuch zawierający grupę znaków.
Struktury mogą zawierać wiele wartości  różnych typów.
Wskaźniki to zmienne wskazujące na położenie danych.

Aby uzywac tablicy, używa się deklaracji. Deklaracja tablicy powinna zawierać trzy elementy:
- typ wartości poszczegółnych elementów
- nazwę tablicy
- liczbę elementów tablicy

```cpp
short miesiace[12]; // tworzy tablicę 12 liczb typu short
```

Każdy element tablicy to zwykła zmienna. Wielkość tablicy nie może być zmienną, której wartość jest ustawiana w trakcie działania programu. Operator new obchodzi to  ograniczenie. Nie istnieje ogólny typ tablicowy, ale wiele konkretnych typów tablicowych. 

Indeksy tablicy  służą do odwoływania się do poszczególnych elementów tablicy oraz numerują każdy element. Elementy indeksujemy zawsze od zera.

Odwołanie do nieistniejącego elementu tablicy nie powoduje błędu, jednakże przypisanie wartości do tablicy z  nieistniejącym indeksem spowoduje błąd w postaci np. zawieszenia programu lub uszkodzenie danych.

Tablice można inicjalizować tylko podczas definiowania tablicy. Nie można tego robić później, nie mozna też przypisać jednej tablicy w całości do innej tablicy. Mozna za pomocą indeksów przypisywać wartości  poszczególnym elementom tablicy. Podczas inicjalizacji tablicy można podać mniej wartości, niż ma tablica. Jeżeli tablica zostanie zainicjalizowana częściowo, kompilator ustawi pozostałe jej elementy na zera. Wobec tego łatwo jest inicjalizować wszystkie elementy tablicy zerami - wystarczy jawnie zainicjalizować zerem pierwszy element tablicy, a kompilator zajmie się resztą. Jeżeli zostawimy   nawiasy kwadratowe puste, kompilator policzy elementy inicjalizujące i ustawi odpowiednią wielkość tablicy (jest to dobre przy inicjalizacji tablic znakowych łańcuchami znaków).

W  C++ 11 można inicjalizowac listą. Mozna pominąć znak  przypisania `=`.
```cpp
double earnings[4] {1.2e4, 1.6e4, 1.1e4, 1.7e4}
unigned int counts[10] = {}; // wszystkie elementy ustawione na zero
float balances[100] {}; // wszystkie elementy ustawione na zero
long plifs[] = {25,92,3.0}; // niedozwolone
char slifs[4] {'h', 'i', 1122011, '\0'}; // niedozwolone
char tlifs[4] {'h', 'i', 112, '\0'}; // dozwolone
```

Standardowa Bibioteka Szablonów (STL)  zawiera alternatywę dla tablic, szablon klasy vektor, a w C++ 11 wprowadzono  dodatkowo szablon klasy array.

`Łańcuch` - to ciąg znaków zapisywanych kolejno w pamięci. Łańcuchy obsługujemy na styl C lub za pomocą klasy string. Z przechowywania znaków w kolejnych bajtach wynika, że łańcuch można zapisać w tablicy elementów `char`, gdzie każdy znak ma swój element tablicy. Łańcuchy w stylu C posiadaja znak   `NUL` ('\0', znak pusty), jako ostatni element. Jest to znacznik końca łańcucha.
```cpp
char dog[5] = {'b', 'e', 'a', 'u', 'x', ' ', 'I', 'I'}; // to nie jest łańcuch
char cat[5] = {'f', 'a', 't', 'e', 's', 's', 'a', '\0'}; // to jest łańcuch
```

Funkcje przetwarzaja tablicę znak po znaku. Jak napotkają znak NUL, przerywają swoje działanie. Bez znaku pustego będą przetwarzane kolejne bajty z pamięci jako znaki, aż do dotarcia do bajtu zerowego.

Do zainicjalizowania tablicy znakowej można użyć łańcuchów zapisywanych w cudzysłowie, nazywanych `stałymi napisowymi` lub `literałami napisowymi`, ewentualnie literałami łańcuchowymi.
```cpp
char bird[11] = "Mr. Cheeps"; // Znak \0 zostanie dobrze zinterpretowany
char fish[] = "Bubbles"; // niech kompilator sobie policzy
```

Łańcuchy podawane w cudzysłowach zawsze zawierają końcowy znak NUL. Jeżeli łańcuch znakowy posiada mniej znaków niż tablica znakowa, po ostatnim znaku jest dodawany NUL a reszta indeksów jest uzupełniana również tym znakiem.

Stała łańcuchowa jest ujnowana w poddójny cudzysłów.. Nie może być używana ze stałą znakową ujmowaną w pojedynczy cudzysłów. Np. 'S' to pojedynczy znak o kodzie 83, za to "S" to łańcuch składajacy się ze znaku 'S' i '\0'. "S" to tak na prawdę adres w pamięci, dzie odpowiedni łańcuch się znajduje.

W przypadku łączenia łańcuchów. Jeżeli dwa łańcuchy są rozdzielane jedynie białymi znakami (spacje, tabulatory, znaki nowego wiersza), to są automatycznie łączone w całość. Ostani znak '\0' pierwszego łańcucha jest zastępowany pierwszym znakiem drugiego łańcucha.

Operator `sizeof`  podaje wielkość całej tablicy, natomiast funkcja `strlen()`  zwraca wielkość łańcucha zapisanego w tablicy, a nie wielkość samejj tablicy w bajtach.  `strlen()` zlicza znaki widoczne, bez znaku NUL. 

Do poszczególnych znaków tablic można odnosić się za pomocą indeksów.
Gdy do tablicy  znakowej  chcemy dodac łańcuch za pomocą `cin`, wtedy z łańcucha, który składa się z wyrazów rozdzielonych białymi znakami pobierany jest pierwszy wyraz aż do spacji i wstawiany jest na końcu znak NUL a dalsza część łańcucha jest w kolejce wejściowej. tak więc przy powtórnym pojawieniu się komendy `cin` zostanie odczytany następny wyraz. W tym wypadku należy wczytywać łańcuchy znakowe wierszami. `getline()`  i  `get()` to funkcje obiektu `cin` do obsługi całych wierszy. Obie funkcje odczytują całe wiersze, czyli wszystkie znaki aż do napotkania znaku nowego wiersza. Jednakże `getline()` odrzuca znak nowego wiersza, a `get()` zostawia go w kolejce wejściowej.
Funkcja `getline()` wczytuje całe wiersze, uznając za koniec linii znak nowego wiersza generowany przez klawisz `Enter`. Posiada dwa parametry: `target` - nawa tablicy na wczytane dane, maksymalna liczba wczytywanych  znaków.

Funkcja `get()` nie odrzuca znaku końca wiersza i nie zastępuje go NUL, tylko zostawia ten znak w kolejce wejściowej. Wywołanie `cin.get()` bezparametrowo  powoduje wczytanie następnego znaku, choćby był to znak nowego wiersza. Dzięki temu mozna oprzucić znak nowego wiersza pozostały po poprzednim wywołaniu. Można też łączyć wywołania: `cin.get(name, ArSize).get();`.
`cin.clear()`  jest wykorzystywany przy wczytywaniu pustego wiersza.
`getline()` ustawia bit błędu w przypadku wtawienia dłuższego łańcucha niż miejsce w tablicy i czyści pozostałe znaki nie mieszczące się w tablicy usuwając je z kolejki wejściowej.
Programy w C++ często do  obsługi łańcuchów znakowych wykorzystują wskaźniki zamiast tablic.

Klasa `string`

- można obiekt string inicjalizować  tak, jak inicjalizuje się łańcuchy w C
- można za pomocą obiektu `cin` umieszczać w obiekcie `string` dane z klawiatury
- można  za pomocą obiektu `cout` pokazywać zawartość obiektu `string`
- można przy użyciu zapisu tablicowego odwoływać się do poszczególnych znaków obiektu `string`

Obiekt `string` deklaruje się jak zwykłą zmienną a nie jak tablicę.

Uzycie klasy pozwala zrzucić   na program odpowiedzialność za zmianę rozmiaru łańcucha.

Jeśli chodzi o przypisanie, to nie można  tego robić w przypadku tablic, za to obiekt `string` można przypisywać.
Można za pomocą operatora `+` dodawać do siebie obiekty `string`, a za pomocą operatora `+=` dołączać jeden obiekt do końca innego.

Kokatenacja na styl C może odbywac się za pomocą funkcji `strcpy()` i `strcat()`;
```cpp
strcpy(charr1, charr2); // kopiowanie charr2 do charr1
strcat(charr1, charr2); // dołączenie zawartości charr2 do charr1
```

Funkcje `strncat()` i `strncopy()` posiadają trzeci parametr określający maksymalny rozmiar tablicy docelowej.

Metody klasy `istream` przetwarzają typy takie jak `double` czy `int`, jednak nie mają metod przetwarzających obiekty `string`. Zajmują się tym funkcje zaprzyjaźnione klasy `string`.

```cpp
wchar_t title[] = L"Chief Astrogator"; // łańcuch znaków typu w_char
char16_t name[] = u"Felonia Ripowa"; //  łąńcuch znaków typu char_16
char32_t car[] = U"Humber Super Snipe"; // łańcuch znakóœ typu char_32
```

W C++ 11 wprowadzono obsługę dla wariantu kodowania znaków o nazwie UTF-8. W tym kodowaniu poszczególne znaki mogą być reperezentowane jednym, dwoma, trzeba bądź czterema 8-bitwymi jednostkami (bajtami albo oktetami), zaleznie od wartości liczbowej kodu znaku w UNICODE. W C++ literały napisowe tego typu są oznaczone przedrostkiem `u8`.

`Łańcuch literalny` bądź łańcuch surowy (raw string) w C++11 to taki, którego znaki nie są interpretowane przez kompilator. Przez to można wprost zapisywać w łlańcuchach znaków symbole sekwencji specjalnych, takich jak '\n' i nie zostaną one zamienione na znaki sterujące nowego  wiersza. Znaki specjalne, by móc być traktowane literalnie, muszą być poprzedzone znakiem '\'. W łąńcuchu  literalnym jest to niepotrzebne. Łańcuch literalny jest ograniczony symbolami " ( i  )  "  z  przedrostkiem R:
```cpp
cout << R"(Jik "kim" Tutt używa "\n" zamiast endl.)" << '\n';
```
Ograniczniki w takim łańcuchu moga być dowolne, jednak bez stosowania nawiasów, znaku `\` ani znakóœ strujących.

## Struktury

`Struktura` to bardziej uniwersalna postać danych niż tablica, gdyż jedna struktura może zawierać wiele danych różnych  typów. Mozna też utworzyć tablicę struktur.
To typ definiowany przez uzytkownika, gdzie deklaracja struktury opisuje właściwości odpowiedniego typu. Najpierw definiujemy opis struktury mwiący, jak nazywają się poszczególne dane w strukturze, a następnie tworzymyu zmienne strukturalne albo ogólniej strukturalne obiekty danych zgodne z wcześniejszym opisem.
Poszczególne elemety listy to `pola struktury`. Deklaracja struktury to deklaracja nowego typu. Aby dostać się do poszczególnych pól, używamy operatora kropki.

Jeśli deklaracja pojawia się poza funkcją jest to `deklaracja zewnętrzna`.
`Deklaracja lokalna` to taka, która znajduje się wewnątrz funkcji i za razem na jej początku, czyli przed inicjalizacją.

```cpp
inflatable duck = {"Daphne", 0.12, 9.98}; 
inflatable duck {"Daphne", 0.12, 9.98}; // C++11
inflatable mayor {}; // Ustawienie wszystkich wartości na wartość zero
```

W strukturach mozna uzywac pola typu `string`. Przybiera wtedy formę `std::string` lub string po uprzedniej implementacji przestrzeni nazw `std`.
Struktury można przypisywać z zachowaniem deklaracji pól.

Można połączyć definiowanie struktury z tworzeniem zmiennych strukturalnych:
```cpp
struct perks
{
    int key_number;
    char car[12];
} mr_smith, ms_jones;
// lub
struct perks
{
    int key_number;
    char car[12];
} mr_glitz =
{
    7,
    "Packard"
};
```

Mozna utworzyć strukturę bez nazwy typu:
```cpp
struct
{
    int x;
    int y;
} position;
```

Nazwa struktury jest nazwą nowego typu utworzonego przez  użytkownika.
W C++ struktury mogą posiadać, w przeciwieństwie do C, funkcje.

Tablica struktur
```cpp
inflatable gifts[100];

inflatable guests[2] =
{
    {"Bambi",0.5,21.99},
    {"Godzilla",2000,565.99}
}
```

Pola bitowe - wymuszają określoną ilość bitów. Za typem  podaje się po  dwukropku ilość bitów. Mozna też uzyć pól bez nazwy, aby zapewnić właściwe wyrównanie bitów. Każde tak zdefiniowane pole to `pole bitowe`.
```cpp
struct torgle_register
{
    unsigned int SN : 4; // 4 bity na wartość SN
    unsigned int : 4; // 4 bity nieużywane
    bool goodIn : 1;  // poprawnie (1bit)
    bool goodTorgle : 1; // też dobrze
}
```

Pól bitowych uzywa się w przypadku programowania niskopoziomowego. Alternatywnym rozwiązaniem jest użycie typu całkowitego i operatorów bitowych.

`Unia` - to taka postać danych, która pozwala zapisywać różene typy danych, ale zawsze tylko jeden raz. MA indentyczną składnię do struktur.
Wielkość unii to wielkość największego pola. Unia może zawierac na raz tylko jedną wartość. Za pomoca unii oszczędzamy pamięć, zarządzanie zbiorem kontrolek.
Unia może być polem struktury.
Unia anonimowa nie ma nazwy a jej pola stają się zmiennymi o tym samym adresie. Jeśli jest polem w strukturze, to pola unii stają się niejako polami struktury mające ten sam adres.
Unie są  stosowane w celu wyeliminowania narzutów pamięci. Najczęściej są wykorzystywane w embedded systems, w niskopoziomowych operacjach systemowych i strukturach danych odnoszących się do sprzętu (np. sterowniki).

Typy wyliczeniowe
`enum` stanowi alternatywny mechanizm do `const`.
Można za jego pomocą deklarować nowe typy danych, ale w ograniczonym zakresie. Składnia `enum` przypomina składnię deklaracji struktur.
Struktura taka robi dwie rzeczy:
- powoduje, że nazwa staje się nazwą nowego typu: jest to typ wyliczeniowy
- wartości stają się stałymi symbolicznymi odpowiadające liczbom całkowitym. Stałe te nazywamy `enumeratorami`.

Zmienna typu `enum` może przyjąć tylko wartość ujętą w deklaracji.
Do zmiennej typu `enum` można jedynie przypisywać. Nie można wykonywać żadnych działań (to tez zależy od implementacji C++).
Typy wyliczeniowe są typami całkowitymi, więc można je promowac do typu `int`, ale tyly całkwite nie są konwertowane na wyliczeniowe automatycznie.
Zmiennej typu wyliczeniowego można przypisać wartość `int` pod dwoma warunkami: jej wartość musi odpowiadać jednemu z enumeratorów tego typu oraz trzeba zastosować jawne rzutowanie typów:
```cpp
pasmo = spectrum(3); // rzutowanie 3 na typ spectrum
```

W praktyce typy wyliczeniowe są częściej używane jako definicje stałych symbolicznych wykorzystywanych w instrukcji `switch`.

```cpp
// jawne ustawianie wartości enumeratorów
enum bity {jeden = 1, dwa = 2, cztery = 4, osiem = 8};
// użyte wartości muszą być liczbami całkowitymi
// można też jawnie zdefiniować tylko niektóre enumeratory
enum wielkiskok {pierwszy, drugi = 100, trzeci}; 
// w tym przypadku piewszy ma wartość zero
// następny niezainicjalizowany będzie o jeden większy od poprzednika, czyli 101
```

Każde wyliczenie ma zakres i zmiennej takiej można przypisać dowolną wartość całkowitą z tego zakresu, nawet, jeśli nie wystąpi ona jako wartość enumeratora. - wystarczy przeprowadzić rzutowanie na typ wyliczeniowy. 
Żeby zdefinoować zakres określamy najpierw kres górny - jest to najwiuększa istniejąca wartość enumeratora. Następnie znajduje się najmniejszą potęgę dwójki większą od kresu górnego i odejmuje się od niej jeden. Wynik jest gónym ograniczeniem zakresu. Jeżeli największa wartość to 101, wtedy najmniejsza potęga dwójki większa od 101 to 128, więc górne ograniczenie to 127.
Najmniejsze ograniczenie wyznaczamy następująco: jeśli najmniejsza wartość jest równa 0 lub więcej, to ogranicczenie dolne to zero; jeśli wartość jest ujemna, to wyznaczamy ograniczenie analogicznie do wyznaczania górnego zakresu,  z tym ż eodrzucamy minus.

`scoped enumeration` - typ wyliczeniowy z zasięgiem (C++11).

## Wskaźniki

Aby okreslić adres zwykłej zmiennej stosujemy operator adresu `&` do zmiennej.
Adresy podajemy w formie szesnastkowej.
W programowaniu obiektowym słowo kluczowe `new` słuzy do pobierania potrzebnej pamieci oraz stosuje wskaźniki do zapamietywania przydzielonej pamięci. Służy  to do podejmowania decyzji w trakcje wykonywania programu. Pamięć jest wtedy dynamicznie przydzielana: korzystamy z takiej ilości pamięci, ile je potrzebujemy. Unikamy przez to nadmiernego lub niedostatniego rezerwowania pamięci przez program.

Nazwy zmiennych to inaczej nazwane wielkości.
`*` to operator wyłuskania albo dereferencji. Pozwala pobrać wartość umieszczoną pod danym adresem.
Komputer musi kontrolować typ wartości wskazywanej przez wskaźnik.

Zapis `int* p1, p2` to deklaracja wskaźnika p1 i jednek zmiennej p2 typu int.
Kombinacja `int *` to typ złożony.
Zwykle adres ma 2 lub 4 bajty.
W chwili deklarowania wskaźnika jest alokowana pamięć na adres, ale nie na dane, na które wskazuje. Wskaźnik należy zawszew inicjować ustalonym, prawidłowym adresem.

Jeśli chcemy użyć liczby jako adresu, musimy jawnie rzutować tę liczbę na odpowiedni typ:
```cpp
int * pt;
pt = (int *) 0x9fffffffdd2c
```

W języku C pamięć alokuje się funkcją biblioteczną malloc(). To samo można zrobić w C++, ale istnieje inny sposób, mianowicie  operator `new`.
Zmienna to nazwany obszar pamięci alokowany na etapie kompilacji.
Informujemy `new`, na jakie dane potrzebna jest nam pamięć; `new` znajduje odpowiedni blok pamięci i zwraca jego  adres. Adres ten przypisujemy wskaźnikowi:
```cpp
int * pn = new int;
```
Do wskaźnika możemy przypisać adres już istniejącej zmiennej lub adres przydzielonej pamięci przez operator `new` dla deklarowanego typu danych. Do takiego wskaźnika mozna potem przypisać adres konkretnej zmiennej.
Skoro pamięć wskazywana przez `pn` nie ma nazwy, wtedy nazywamy ją `obiektem danych`. Jest to pojęcie ogólniejsze od zmiennej i oznacza dowolny blok pamięci zalokowany na dane. Zmienna jest obiektem danych, jednak pamięć wskazywana przez wskaźnik nie jest zmienną.

`new` używa dla przydzielanych zmiennych innego bloku pamięci niż zwyczajne zmienne.
Zmienne mają wartości przchowywane w obszarze pamięci zwanym `stosem` (stack), podczas gdy obszar pamięci przydzielany przez `new` to obszar tak zwanej `sterty` (heap) albo `pula wolnej pamięci` (free store).
W C++ wskaźnik o wartości zero, to wskaźnik pusty (null). Standard języka gwarantuje, że wskaźnik pusty nigdy nie wskazuje poprawnych  danych.

Operator `delete` umozliwia zwracanie do puli niepotrzebnej pamięci. Używa się go wraz ze wskaźnikiem bloku, który był wcześniej zaalokowany za pomocą operatora `new`.
```cpp
int * ps = new int;
// ...
delete ps;
```
Zwalniana jest pamięć a nie sam wskaźnik.
Zawsze użycie operatorów `new` i `delete` powinno się bilansować. W przeciwnym wypadku dojdzie do `wycieku pamięci`, czyli pamięć będzie alokowana, ale kiedy przestanie być uzywana, nie będzie zwalniana.
Nie nalezy próbowac zwalniac już raz zwolnionego bloku pamięci.
Nie można za pomocą `delete` zwalniać pamięci przeznaczonej na zwykłe zmienne. Można go stosować na puste wskaźniki.

Tablice dynamiczne
Alokacje w trakcie kompilacji nazywamy `wiązaniem statycznym`.
`Wiązanie dynamiczne` - alokacja w trakcie działania programu.
Tablice powstałe na zasadzie wiązania dynamicznego nazywamy `tablicami dynamicznymi`.
```cpp
int * psome = new int [10]; // pobranie bloku na 10 liczb int
delete [] psome; // specjalna składnia zwalniania pamięci tablicy dynamicznej
```
Operator `new` zwraca adres pierwszego elementu z  bloku.
Program śledzi alokowaną pamięć.
Żeby dostać się do któregokolwiek elementu tablicy dynamicznej, traktujemy wskaźnik jak zwykłą tablicę. Tak więc wskaźnik `*psome` wskaże pierwszy element tablicy, podobnie, jak `psome[0]`.
Tablice wewnętrzne są obsługiwane jako wskaźniki.
Użycie wskaźnika tablicy różni sie tym od zwykłej tablicy, że na zmiennej tablicy nie mozna przeprowadzać działań arytmetycznych. W przypadku działań na wskaźniku po dodaniu lub odjęciu wartości liczbowej wskaźnik będzie wskazywał na element oznaczony wynikiem działania.

`arytmetyka wskaźników` - dodanie jedynki do wskaźnika zwiększa wskazywany adres o liczbe bajtów odpowiadających danemu typowi.
C++ interpretuje nazwę tablicy jako adres.
Na samych tablicach można wykonywac operacje arytmetyczne, jednakże, gdy zmienna tablicowa jest wykorzystywana w kontekście wskaźnika.
Wskaźnik inicjalizuje się wartością tablicy, czyli adresem pierwszego elementu tablicy.
```cpp
short tell[10]; // tell to tablica 20 bajtów
cout << tell << endl; // wypisuje &tell[0]
cout << &tell << endl; // wypisuje adres całej tablicy
```
Podobnie rzecz się ma z tablicami typu char, czyli łańcuchami.
Łańcuchy zapisywane jako  tablice, napisy w cudzysłowiach oraz wskaźniki na znaki  są obsługiwane tak samo - wszystkie są przekazywane jako adresy.
Nalezy unikać niezainicjalizowanych wskaźników.
Standard wymaga, aby literały łańcuchowe były traktowane jako stałe, czyli tylko do odczytu.
Kiedy wczytujemy do programu łańcuch znakowy, zawsze powinniśmy użyć adresu wcześniej zaalokowanej pamięci.
Żeby zobaczyć adres wskaźnika `char*` nalezy rzuotwać go na `int *`, gdyż `cout` wyświetli wartość.

`strncpy()` - za pomocą trzeciego parametru, oznaczającego maksymalną ilość znaków jakie moga być skopiowane. Dzięki  temu unikamy wycieku pamięci, czyli kopiowanie nadmiarowych bajtów zaraz za docelową tablicą, co może spowodować nadpisanie innych danych znajdujacych się w pamięci. Trzeba pamiętac o wstawieniu znaku NUL na oznaczenie końca łańcucha.
```cpp
// jak należy używać funkcji strncpy()
strncpy(food, "koszyk piknikowy wypełniony mnóstwem łakoci"),19;
food[19] = '\0';
```

Aby przypisać łańcuch do tablicy znakowej, zamiast operatora przypisania należy używać funkcji `strcpy()` lub `strncpy()`.

`new` uzywamy do tworzenia struktur dynamicznych. Pamięć trzeba rezerwować tylko dla tylu struktur, ilu będziemy używać w danym przypadku.

Operator `->` ma dla wskaźników takie znaczenie jak kropka dla nazw struktur.
```cpp
inflatable * ps = new inflatable;
ps->price; // wskaźnik wskazuje pole price nienazwanej struktury
(*ps).price; // można tez tak przy wskaźniku

inflatable ps2 = {1, 2.0,33.3};
ps2.price; // wskazanie pola nazwanej struktury
```

Alokacja pamięci
- automatyczna
- statyczna
- dynamiczna (sterta)
- pamięć lokalna wątku (C++11)

Alokowane obiekty pamięci różnią się czasem, przez jaki są dostępne.

Zwykle zmienne definiowane w funkcjach nazwywane są `zmiennymi automatycznymi`. Zmienna jest tworzona automatycznie, kiedy tylko wywołana jest funkcja zawierająca deklarację tej zmiennej. Zmienna taka znika, kiedy kończy się wykonywanie zawierającej ją funkcji. Zmienne takie są lokalne dla zawierającego je bloku.

`Blok` to fragment kodu ujęty między nawiasy klamrowe.

Zmienne automatyczne są przydzielane w obszarze pamięci stosu. Przydział i zwalnianie pamięci stosu odbywa się w modelu LIFO (last in, firs out), tzn. ostatnia przydzielona zmienna w bloku jest zwalniana jako pierwsza.

Alokacja statyczna polega na tym, ze pamięć jest dostępna przez cały czas wykonywania programu.
Zmienną mozna uczynić statyczną na dwa sposoby: można zmienną definiować na zewnątrz wszelkich funkcji lub użyć   słowa kluczowego `static` w deklaracji.

Alokacja dynamiczna z apomocą operatorów `new` i `delete`  dają możliwość zarządzania  `pulą wolnej pamięci` albo inaczej `stertą`. 
Kolejność pomiędzy wywołaniami `new` i `delete` jest nieokreślona dla róznych przydziałów, co prowadzi do powstawania dziur w puli wolnej pamięci (a więc jej fragmentacji), co z kolei utrudnia efektywną realizację kolejnych przydziałów.
Wyciek pamięci to pamięć zaalokowana i nie zwolniona i  nie da się jej zwolnić do końca działania programu.
Inteligentne wskaźniki pomagają zwolnić pamięć.

Wskaźniki należą do najpotężniejszych narzędzi C++, jak też za razem sąto narzędzia najbardziej niebezpieczne.

`**` operator deklaracji wskaźnika do wskaźnika do niemodyfikowalnej struktury (const).

Szablony `vector` i `array`

Szablon klasy `vector` jest podobny do klasy szablonowej `string` w tym sensie, że jest tablicą dynamiczną. Rozmiar obiektu `vector` można ustawić w czasie wykonania programu, mozna też do wektora dopisywac nowe elementy, a także wstawiać nowe elementy danych pomiędzy inne elementy.
Indentyfikator `vector` jest elementem przestrzeni nazw `std`. Należy też dodać do programu plik nagłówkowy: `#include <vector>`.

Obiekt klasy `array` posiadastatyczny, stały rozmiar a jego pamięć jest przydzielana na stosie. Należy dołączyć plik nagłówkowy `array`.

Metoda `at()`:
```cpp
a2.at(1) = 2.3; // przypisanie 2.3 do elementu a2[1]
```
Metody `begin()` i `end()`.
# Basic tools for web development

> Source: [Web Development In 2019 - A Practical Guide](https://www.youtube.com/watch?v=UnTQVlqmDQ0) form [Traversy Media](https://www.youtube.com/channel/UC29ju8bIPH5as8OGnQzwJyA)

> content may be different to source above

## Text Editor or IDE:

- `Visual Studio Code`
- `Sublime Text`
- `Atom`

## Web browser

- `Chrome`
- `Firefox`

## Browser Dev Tools

-

## Design & Mockup (Optional)

- `XD`
- `Photoshop`
- `Sketch`
- `Figma`

## 3rd party terminal (Optional)

- `Git Bash` (Windows)
- `WSL` - A terminal emulator for Windows Subsystem for Linux (WSL), based on mintty, fatty and wslbridge. [GitHub Link](https://github.com/goreliu/wsl-terminal)
- `iTerm2` - iTerm2 is a replacement for Terminal and the successor to iTerm. It works on Macs with macOS 10.12 or newer. [Link](https://www.iterm2.com/)
- `Hyper` - is an Electron-based Terminal, builr on html5/css/js. Fully extensible. [Link](https://hyper.is/)
- `Terminator` (Linux)
- `Yakuake` (KDE)
- `Guake` (GTK+)

## Deployment

- register a domain name (`namecheap`, `Google Domains`)
- managed shared hosting or VPS (`Inmotion`, `Hostgator`, `Bluehost`)
- FTP, SFTP File Upload (`Filezilla`, `Cyberduck`)
- static hosting (nned to know Git) ([Netlify](https://www.netlify.com/), [Github pages](https://www.youtube.com/watch?v=2MsN8gpT6jY))

> GIT is absolutely necessary for all web developers.

## Tools

- Basic Command Line
  - Bash
  - PowerShell
  - Git
- Git
- NPM or Yarn
- Webpack or Parcel
- Gulp or Grunt
- Editor Extensions: ESLint, Prettier, Live Server, etc.

# Queue

- RabbitMQ
- Apache Kafka

# Search

- Elasticsearch

# Logs

- Kibana
- Logstash

# Server Rendered Pages

> Frameworks like React, Vue and Angular can also be rendered on the server which can actually make things relatively easier

- Next.js (React)
- Nuxt.js (Vue)
- Angular Universal (Angular)

# Serverless Architecture

> Eliminate the need for creating and managing your own server

- use 3rd party services to execute Serverless Functions (FaaS)
- Examples are AWS, Netlify and Firebase
- popular with Gatsby static sites
- serverless framework - toolkit for building serverless apps

# Blockchain technology

> Companies are using Blockchain for digital transactions in order to make them more efficient and secure

- solidity - language for implementing contracts
- mist - used for storing Ethereum, sending transactions and contracts
- Coinbase API - blockchain devs can easily build apps and integrate Bitcoin

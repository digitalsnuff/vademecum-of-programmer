# Uninstall MySQL

```bash
sudo apt-get purge mysql-server mysql-client mysql-common mysql-server-core-_ mysql-client-core -_ -y
sudo rm -rf /etc/mysql /var/lib/mysql
sudo apt-get autoremove -y
sudo apt-get autoclean
```

# Install MariaDB

```bash
sudo apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8
sudo add-apt-repository 'deb [arch=amd64,arm64,ppc64el] http://ftp.icm.edu.pl/pub/unix/database/mariadb/repo/10.3/ubuntu bionic main'
sudo apt update
sudo apt install mariadb-server -y
```

# Install MySQL Connectors

> [MySQL Connectors Download Page](https://dev.mysql.com/downloads/connector/)

## Connector/J

> [Download](https://cdn.mysql.com//Downloads/Connector-J/mysql-connector-java_8.0.16-1ubuntu18.04_all.deb)

## Configuration

- configuration path: `/etc/mysql/`
- configuration file: `/etc/mysql/my.cnf`
- connection string: `jdbc:mysql://localhost:3306/mysql?serverTimezone=UTC`
- connection string: `jdbc:mysql://localhost:3306/mysql?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC`

## Create user

```sql
CREATE USER 'user1'@'localhost' IDENTIFIED BY 'user1';
```

## Create database

```sql
CREATE DATABASE test;
```

## Grats

```sql
GRANT ALL PRIVILEGES ON test.* TO 'user1'@'localhost';
FLUSH PRIVILEGES;
```

## Privilege Levels

> Privileges can be set globally, for an entire database, for a table or routine, or for individual columns in a table. Certain privileges can only be set at certain levels.

- Global privileges are granted using `*.*` for priv_level. Global privileges include privileges to administer the database and manage user accounts, as well as privileges for all tables, functions, and procedures. Global privileges are stored in the `mysql.user` table.
- Database privileges are granted using `db_name.*` for priv_level, or using just \* to use default database. Database privileges include privileges to create tables and functions, as well as privileges for all tables, functions, and procedures in the database. Database privileges are stored in the `mysql.db` table.
- Table privileges are granted using `db_name.tbl_name` for priv_level, or using just tbl_name to specify a table in the default database. The TABLE keyword is optional. Table privileges include the ability to select and change data in the table. Certain table privileges can be granted for individual columns.
- Column privileges are granted by specifying a table for priv_level and providing a column list after the privilege type. They allow you to control exactly which columns in a table users can select and change.
- Function privileges are granted using FUNCTION `db_name.routine_name` for priv_level, or using just FUNCTION routine_name to specify a function in the default database.
- Procedure privileges are granted using PROCEDURE `db_name.routine_name` for priv_level, or using just PROCEDURE routine_name to specify a procedure in the default database.

## Helpful queries

`show variables like '%sql_mode%' ;`

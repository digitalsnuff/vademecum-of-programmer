# Install PHP 7.3

> [Link](https://thishosting.rocks/install-php-on-ubuntu/)

```bash
sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
```

## Install various version of PHP (Apache):

```
sudo apt install php5.6 php7.0 php7.1 php7.2 php7.3
sudo apt install php5.6-fpm php7.0-fpm php7.1-fpm php7.2-fpm php7.3-fpm
```

## Run Multiple PHP Versions with Apache on Ubuntu 18.04

```
sudo apt install libapache2-mod-fcgid
sudo a2enmod actions alias proxy_fcgi fcgid
sudo systemctl restart apache2
```

## Virtual Hosts Method

> Open up your Apache `.conf` file and add the `<FilesMatch>` directive to your Virtual Host. This instructs Apache which PHP socket to use.

### PHP 5.6 Example

```sh
# ./etc/apache2/sites-available/000-default.conf
<VirtualHost *:80>

...

    <FilesMatch \.php$>
        # Apache 2.4.10+ can proxy to unix socket
        SetHandler "proxy:unix:/var/run/php/php5.6-fpm.sock|fcgi://localhost/"
    </FilesMatch>

...

</VirtualHost>
```

```sh
sudo systemctl restart apache2
```

### PHP 7.2 Example

```sh
# ./etc/apache2/sites-available/000-default.conf
<VirtualHost *:80>

...

    <FilesMatch \.php$>
        # Apache 2.4.10+ can proxy to unix socket
        SetHandler "proxy:unix:/var/run/php/php7.2-fpm.sock|fcgi://localhost/"
    </FilesMatch>

...

</VirtualHost>
```

```sh
sudo systemctl restart apache2
```

## htaccess Method

> You can also add the `<FilesMatch>` directive to your `.htaccess` file. Before you do, make sure that `AllowOverride` is enabled, otherwise Apache will ignore `.htaccess`.

```sh
sudo nano /etc/apache2/apache2.conf
```

> Scroll down the the following section and make sure that AllowOverride is set to All.

```
<Directory /var/www/>
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
</Directory>
```

```sh
sudo systemctl restart apache2
```

### PHP 5.6 Example

```sh
# .htaccess
<FilesMatch \.php$>
    # Apache 2.4.10+ can proxy to unix socket
    SetHandler "proxy:unix:/var/run/php/php5.6-fpm.sock|fcgi://localhost/"
</FilesMatch>
```

### PHP 7.2 Example

```sh
# .htaccess
<FilesMatch \.php$>
    # Apache 2.4.10+ can proxy to unix socket
    SetHandler "proxy:unix:/var/run/php/php7.2-fpm.sock|fcgi://localhost/"
</FilesMatch>
```

---

## Switch version

```bash
# update-alternatives --set php /usr/bin/php7.0
# update-alternatives --set php /usr/bin/php7.1
sudo update-alternatives --set php /usr/bin/php7.2
sudo update-alternatives --set phar /usr/bin/phar7.2
sudo update-alternatives --set phar.phar /usr/bin/phar.phar7.2
sudo update-alternatives --set phpize /usr/bin/phpize7.2
sudo update-alternatives --set php-config /usr/bin/php-config7.2
# update-alternatives --set php /usr/bin/php7.3
```

## enable php module in Apache2.4

```bash
a2enmod php7.0
a2enmod php7.1
a2enmod php7.2
a2enmod php7.3
systemctl restart apache2
```

# Install PhpBrew

## [Git Hub](https://github.com/phpbrew/phpbrew)

## [Requirement](https://github.com/phpbrew/phpbrew/wiki/Requirement)

```bash
sudo apt-get install -y php7.2 php7.2-cli \
php7.2-xml php7.2-mbstring php7.2-pdo \
php7.2-curl php7.2-zip \
php-pear php7.2-dev pkg-php-tools shtool \
composer
sudo apt-get install -y build-essential autoconf automake autotools-dev re2c
sudo apt-get install -y \
 libxml2 libxml2-dev \
 libssl-dev openssl \
 gettext \
 libicu-dev \
 libmcrypt-dev libmcrypt4 \
 libmhash-dev libmhash2 \
 libfreetype6 libfreetype6-dev \
 libgd-dev libgd3 \
 libpng-dev libpng16-16 \
 libjpeg-dev libjpeg8-dev libjpeg8 \
 libxpm4 libltdl7 libltdl-dev libreadline-dev

sudo apt-get install lcov
sudo apt-get install libcurl4-gnutls-dev
sudo ln -s /usr/include/x86_64-linux-gnu/gmp.h /usr/include/gmp.h

vim /etc/apt/sources.list # uncomment the deb-src lines
apt-get update # update the package index

# if you only want to see the dependencies, you use apt-rdepends to see the build dependencies:

# apt-get install -y apt-rdepends

# apt-rdepends --build-depends php7.2

curl -L -O https://github.com/phpbrew/phpbrew/raw/master/phpbrew
chmod +x phpbrew
sudo mv phpbrew /usr/local/bin/phpbrew
phpbrew init

# phpbrew

export PHPBREW_SET_PROMPT=1
export PHPBREW_RC_ENABLE=1
source /home/serenus/.phpbrew/bashrc
[[ -e ~/.phpbrew/bashrc ]] && source ~/.phpbrew/bashrc

# example

# phpbrew install 5.6 +everything +curl=/usr/include/x86_64-linux-gnu/curl -openssl
```

## Phpbrew with Apache2.4

```sh
sudo apt-get install apache2-dev
phpbrew install x.x.x +apxs2=/usr/bin/apxs2
```

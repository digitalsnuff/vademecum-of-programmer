# Install developer stack

## It is first step

```bash
#!/bin/bash
sudo su
sudo apt update
sudo apt upgrade -y
sudo apt install terminator yakuake build-essential \
virtualbox virtualbox-ext-pack vagrant \
curl net-tools ufw iptables-persistent exfat-fuse exfat-utils dislocker \
vim apache2 nginx git-core git-flow \
software-properties-common \
keepassx thunderbird filezilla \
gnome-keyring openvpn easy-rsa -y
sudo apt --fix-broken install -y

sleep 10
```

# Install Google Chrome

```bash
#!/bin/bash
echo 'Install google-chrome'
wget -O ~/chrome.deb https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i ~/chrome.deb
```

# Install Opera browser

```bash
echo 'Install opera browser...';
wget -qO- https://deb.opera.com/archive.key | sudo apt-key add -
sudo add-apt-repository "deb [arch=i386,amd64] https://deb.opera.com/opera-stable/ stable non-free"
sudo apt install opera-stable -y
sleep 10
```

# Install Java

```bash
echo 'Install java...'
sudo apt install openjdk-8-jdk -y
sudo apt install openjdk-11-jdk -y
```

# Install AMD drivers (Radeon RX 590)

## [https://www.amd.com/en/support](https://www.amd.com/en/support)

# Add previous version repository

```bash
sudo apt-add-repository ppa:kubuntu-ppa -y
sudo apt-get update
sudo apt-get upgrade -y
```

# Install Netbeans 10

## [webpage: https://netbeans.apache.org/download/nb100/nb100.html](https://netbeans.apache.org/download/nb100/nb100.html)

```bash
cd ~/Downloads
netbeans="netbeans-10"
curl http://archive.apache.org/dist/incubator/netbeans/incubating-netbeans/incubating-10.0/incubating-netbeans-10.0-bin.zip -o "$netbeans".zip
sudo unzip "$netbeans".zip -d /opt/
```

# Install Netbeans 11

## [webpage: https://netbeans.apache.org/download/nb110/nb110.html](https://netbeans.apache.org/download/nb110/nb110.html)

```bash
cd ~/Downloads
netbeans="netbeans-11"
curl http://ftp.man.poznan.pl/apache/incubator/netbeans/incubating-netbeans/incubating-11.0/incubating-netbeans-11.0-bin.zip -o "$netbeans".zip;
sudo unzip "$netbeans".zip -d /opt/
sudo mv /opt/netbeans /opt/"$netbeans"
sudo ln -s /opt/"$netbeans" netbeans
sudo ln -s /opt/netbeans/bin/netbeans /usr/local/bin/netbeans
source ~/.bashrc
sudo tee -a /usr/share/applications/"\$netbeans".desktop << END
[Desktop Entry]
Name=Netbeans 11
Comment=Netbeans IDE
Exec=/usr/local/bin/netbeans
Icon=/opt/netbeans/nb/netbeans.png
Terminal=false
Type=Application
Categories=Development,IDE;
StartupNotify=false
END
```

# Install WebStorm

```bash
sudo apt install gnome-themes-standard
cd ~/Download
webstorm="webstorm"
curl 'https://download-cf.jetbrains.com/webstorm/WebStorm-2019.1.tar.gz' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86*64; rv:66.0) Gecko/20100101 Firefox/66.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/\_;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Referer: https://www.jetbrains.com/webstorm/download/download-thanks.html?platform=linux' -H 'Connection: keep-alive' -H 'Cookie: \_gcl_au=1.1.591970217.1555319083; \_ga=GA1.2.52664931.1555319084; \_gid=GA1.2.912184635.1555319084' -H 'Upgrade-Insecure-Requests: 1' -o "$webstorm".tar.gz
sudo tar xfz "$webstorm".tar.gz -C /opt/
find /opt/WebStorm-\* -maxdepth 0 -type d -exec bash -c 'sudo ln -s "{}" /opt/webstorm;' \;
```

# Install Visual Studio Code

```bash
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
sudo install -o root -g root -m 644 microsoft.gpg /etc/apt/trusted.gpg.d/
sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
sudo apt-get install apt-transport-https
sudo apt-get update
sudo apt-get install code # or code-insiders

# Menu > File > (check) Auto Save

# Medu > Preferences > Settings

# - "editor:fontSize": 18, "terminal.integrated.fontSize": 16

# install modules: Ctrl+P:

# ext install
# ritwickdey.liveserver
# dsznajder.es7-react-js-snippets
# oenraads.bracket-pair-colorizer-2
# ritwickdey.live-sass
# ecmel.vscode-html-css
# formulahendry.auto-rename-tag
# vscode-icons-team.vscode-icons
# esbenp.prettier-vscode
# xabikos.javascriptsnippets
# chakrounanas.turbo-console-log
# oderwat.indent-rainbow
# coenraads.bracket-pair-colorizer
# streetsidesoftware.code-spell-checker
# humao.rest-client
# eamodio.gitlens
# hnw.vscode-auto-open-markdown-preview
# dsznajder.es7-react-js-snippets
# octref.vetur
# johnpapa.angular2
# mikael.angular-beastcode
```

# Install NVM

```bash
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash
source ~/.bashrc
nvm install node
nvm install 11
nvm install 10
nvm install 9
nvm install 8
nvm use node
npm i -g npm
node -v
npm -v
```

# Install PHP >> [Go To](php.md)

# Install NGINX

# Install MariaDB >> [Go To](mariadb.md)

# Install PostgreSQL

```bash
echo 'deb http://apt.postgresql.org/pub/repos/apt/ bionic-pgdg main' | sudo tee -a /etc/apt/sources.list.d/pgdg.list
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get update
sudo apt install postgresql -y
```

# Install pgAdmin4

# Install MongoDB

## [Page](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/)

```bash
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4
echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.0.list
sudo apt-get update
sudo apt-get install -y mongodb-org
sudo systemctl start mongodb
sudo systemctl enable mongodb
sudo systemctl restart mongodb
```

# Install Docker

## Get Docker CE for Ubuntu

```bash
# Uninstall old versions
sudo apt-get remove docker docker-engine docker.io containerd runc
# Docker CE on Ubuntu supports overlay2, aufs and btrfs storage drivers.
sudo apt-get update
# Install packages to allow apt to use a repository over HTTPS:
sudo apt-get install -y \
 apt-transport-https \
 ca-certificates \
 curl \
 gnupg-agent
# Add Docker’s official GPG key:
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
# Set up stable repo
sudo add-apt-repository \
 "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
 \$(lsb_release -cs) \
 stable"
sudo apt-get update
# Install the latest version of Docker CE and containerd, or go to the next step to install a specific version:
sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose
# sudo docker run hello-world
sudo groupadd docker
sudo usermod -aG docker $USER
sudo chown "$USER":"$USER" /home/"$USER"/.docker -R
sudo chmod g+rwx "\$HOME/.docker" -R
```

# Install Postman

```bash
cd ~/Downloads
curl https://dl.pstmn.io/download/latest/linux64 -o postman.tar.gz
sudo tar -xzf postman.tar.gz -C /opt
sudo ln -s /opt/Postman/Postman /usr/local/bin/postman
sudo tee -a /usr/share/applications/postman.desktop <<EOL
[Desktop Entry]
Encoding=UTF-8
Name=Postman
Exec=postman
Icon=/opt/Postman/app/resources/app/assets/icon.png
Terminal=false
Type=Application
Categories=Development;
EOL
```

# Autoremove

```bash
echo 'Autoremove...';
sudo apt autoremove --purge -y
```

# Create paths for applications repositories

```bash
mkdir ~/workspace
mkdir ~/workspace/challenge
mkdir ~/workspace/project
```

# Secure .ssh folder

```bash
chmod 700 ~/.ssh
chown -R $USER:$GROUP ~/.ssh
find ~/.ssh/_ -type d -exec chmod 700 "{}" \;
find ~/.ssh/_ -type f -exec chmod 600 "{}" \;
find ~/.ssh/_ -type f -name "_.pub" -exec chmod 644 "{}" \;
# touch /home/\$USER/.ssh/authorized_keys
chmod 600 /home/$USER/.ssh/authorized_keys
chmod 644 /home/$USER/.ssh/known_hosts
chmod 644 /home/\$USER/.ssh/config
```

# Command Line

In 1991, Linus Torvalds wrote the first version of the Linux kernel.

Richard Stallman began the GNU Project to create a free Unix-like operating system.

## Why I Don't Call It “GNU/Linux”

In some quarters, it's politically correct to call the Linux operating system the
“GNU/Linux operating system.” The problem with “Linux” is that there is no
completely correct way to name it because it was written by many different peo-
ple in a vast, distributed development effort. Technically speaking, Linux is the
name of the operating system's kernel, nothing more. The kernel is very important
of course, since it makes the operating system go, but it's not enough to form a
complete operating system.
Enter Richard Stallman, the genius-philosopher who founded the Free Software
movement, started the Free Software Foundation, formed the GNU Project, wrote
the first version of the GNU C Compiler (gcc), created the GNU General Public
License (the GPL), etc., etc., etc. He insists that you call it “GNU/Linux” to prop-
erly reflect the contributions of the GNU Project. While the GNU Project predates
the Linux kernel, and the project's contributions are extremely deserving of recog-
nition, placing them in the name is unfair to everyone else who made significant
contributions. Besides, I think “Linux/GNU” would be more technically accurate
since the kernel boots first and everything else runs on top of it.
In popular usage, “Linux” refers to the kernel and all the other free and open
source software found in the typical Linux distribution, that is, the entire Linux
ecosystem, not just the GNU components. The operating system marketplace
seems to prefer one-word names such as DOS, Windows, macOS, Solaris, Irix,
and AIX. I have chosen to use the popular format. If, however, you prefer to use
“GNU/Linux” instead, please perform a mental search-and-replace while reading
this book. I won't mind.

## What Is the Shell?

When we speak of the command line, we are really referring to the shell. The shell is a
program that takes keyboard commands and passes them to the operating system to carry
out. Almost all Linux distributions supply a shell program from the GNU Project called
bash. The name “bash” is an acronym for “Bourne Again SHell”, a reference to the fact
bash is an enhanced replacement for sh, the original Unix shell program written by
Steve Bourne.

### Terminal Emulators

When using a graphical user interface (GUI), we need another program called a terminal
emulator to interact with the shell. If we look through our desktop menus, we will proba-
bly find one. KDE uses konsole and GNOME uses gnome-terminal, though it's
likely called simply “terminal” on our menu. A number of other terminal emulators are
available for Linux, but they all basically do the same thing; give us access to the shell.
You will probably develop a preference for one or another terminal emulator based on the
number of bells and whistles it has.

The Console Behind the Curtain

Even if we have no terminal emulator running, several terminal sessions continue
to run behind the graphical desktop., We can access these sessions, called virtual
terminals or virtual consoles, by pressing Ctrl-Alt-F1 through Ctrl-Alt-
F6 on most Linux distributions. When a session is accessed, it presents a login
prompt into which we can enter our username and password. To switch from one
virtual console to another, press Alt-F1 through Alt-F6. On most system we
can return to the graphical desktop by pressing Alt-F7.

## Keystrokes

> If the last character of the prompt is a pound sign (“#”) rather than a dollar
> sign, the terminal session has superuser privileges. This means either we are
> logged in as the root user or we selected a terminal emulator that provides supe-
> ruser (administrative) privileges.

### Command History

Most Linux distributions re-
member the last 1000 commands by default. Press the down-arrow key and the previous
command disappears.

### Cursor Movement

A Few Words About Mice and Focus
While the shell is all about the keyboard, you can also use a mouse with your ter-
minal emulator. A mechanism built into the X Window System (the underlying
engine that makes the GUI go) supports a quick copy and paste technique. If you
highlight some text by holding down the left mouse button and dragging the
mouse over it (or double clicking on a word), it is copied into a buffer maintained
by X. Pressing the middle mouse button will cause the text to be pasted at the cur-
sor location.

Don't be tempted to use Ctrl-c and Ctrl-v to perform copy and paste
inside a terminal window. They don't work. These control codes have different
meanings to the shell and were assigned many years before the release of
Microsoft Windows.
Your graphical desktop environment (most likely KDE or GNOME), in an effort
to behave like Windows, probably has its focus policy set to “click to focus.” This
means for a window to get focus (become active) you need to click on it. This is
contrary to the traditional X behavior of “focus follows mouse” which means that
a window gets focus just by passing the mouse over it. The window will not come
to the foreground until you click on it but it will be able to receive input. Setting
the focus policy to “focus follows mouse” will make the copy and paste technique
even more useful. Give it a try if you can (some desktop environments such as
Ubuntu's Unity no longer support it). I think if you give it a chance you will pre-
fer it. You will find this setting in the configuration program for your window
manager.

## Commands

`date` - current date
`cal` - calendar
`df` - disk space
`free` - free memory
`exit` - ending terminal session

### Navigation

`pwd` - Print name of current working directory
`cd` - Change directory
`cd -` - Changes the working directory to the previous working directory.
`cd ~user_name` - Changes the working directory to the home directory of
user_name. For example, cd ~bob will change the directory to
the home directory of user “bob.”
`ls` - list directory contents
`ls ~ /usr` - We can even specify multiple directories. In the following example, we list both the user's home directory (symbolized by the “~” character) and the /usr directory.
`file` - Determine file type
`less` - View file content

## `ls`

<table>
  <tr>
    <th>Option</th>
    <th>Long Option</th>
    <th>Description</th>
  </tr>
  <tbody>
    <tr>
      <td>-a</td>
      <td>--all</td>
      <td>
        List all files, even those with names that begin with a period, which are normally not listed (that is, hidden).
      </td>
    </tr>
    <tr>
      <td>-A</td>
      <td>--almost-all
      </td>
      <td>
        Like the -a option above except it does not list . (current directory) and .. (parent directory).
      </td>
    </tr>
    <tr>
      <td>-d</td>
      <td>--directory</td>
      <td>
        Ordinarily, if a directory is specified, ls will list the contents of the directory, not the directory itself. Use this option in conjunction with the -l option to see details about the directory rather than its contents.
      </td>
    </tr>
    <tr>
      <td>-F</td>
      <td>--classify</td>
      <td>
        This option will append an indicator character to the end of each listed name. For example, a forward slash (/ if the name is a directory.
      </td>
    </tr>
    <tr>
      <td>-h</td>
      <td>--human-readable
      </td>
      <td>
        In long format listings, display file sizes in human readable format rather than in bytes.
      </td>
    </tr>
    <tr>
      <td>-l</td>
      <td></td>
      <td>Display results in long format.</td>
    </tr>
    <tr>
      <td>-r</td>
      <td>--reverse</td>
      <td>
        Display the results in reverse order. Normally, ls displays its results in ascending alphabetical order.
      </td>
    </tr>
    <tr>
      <td>-S</td>
      <td></td>
      <td>Sort results by file size.</td>
    </tr>
    <tr>
      <td>-t</td>
      <td></td>
      <td>Sort by modification time.</td>
    </tr>
  </tbody>
</table>

## Understanding the File System Tree

Like Windows, a Unix-like operating system such as Linux organizes its files in what is
called a hierarchical directory structure. This means they are organized in a tree-like pat-
tern of directories (sometimes called folders in other systems), which may contain files
and other directories. The first directory in the file system is called the root directory. The
root directory contains files and subdirectories, which contain more files and subdirecto-
ries and so on.
Note that unlike Windows, which has a separate file system tree for each storage device,
Unix-like systems such as Linux always have a single file system tree, regardless of how
many drives or storage devices are attached to the computer. Storage devices are attached
(or more correctly, mounted) at various points on the tree according to the whims of the
system administrator, the person (or people) responsible for the maintenance of the sys-
tem.

## Important Facts About Filenames

On Linux systems, files are named in a manner similar to other systems such as
Windows, but there are some important differences.

1. Filenames that begin with a period character are hidden. This only means that
   ls will not list them unless you say `ls -a`. When your account was created,
   several hidden files were placed in your home directory to configure things
   for your account. In addition, some applications place their configuration and settings files in your home directory as
   hidden files.
2. Filenames and commands in Linux, like Unix, are case sensitive. The file-
   names “File1” and “file1” refer to different files.
3. Linux has no concept of a “file extension” like some other operating systems.
   You may name files any way you like. The contents and/or purpose of a file is
   determined by other means. Although Unix-like operating systems don’t use
   file extensions to determine the contents/purpose of files, many application
   programs do.
4. Though Linux supports long filenames that may contain embedded spaces and
   punctuation characters, limit the punctuation characters in the names of files
   you create to period, dash, and underscore. Most importantly, do not embed
   spaces in filenames. If you want to represent spaces between words in a file-
   name, use underscore characters. You will thank yourself later.

## Options and Arguments

Most commands use options which consist of a single character preceded by a dash, for
example, “-l”. Many commands, however, including those from the GNU Project, also
support long options, consisting of a word preceded by two dashes. Also, many com-
mands allow multiple short options to be strung together. In the following example, the
ls command is given two options, which are the l option to produce long format output,
and the t option to sort the result by the file's modification time.

# Source

[The Linux Command Line](http://linuxcommand.org/tlcl.php)

# lvm2

## Identify encrypted device

```sh
sudo lsblk -f /dev/sdx
```

## Open LUKS device

```sh
sudo cryptsetup luksOpen /dev/sdxy encrypted_device
```

## Identify volume group

```sh
sudo vgdisplay --short
```

## List logical volumes

```sh
sudo lvs -o lv_name,lv_size -S vg_name=kubuntu-vg
```

## Activate logical volumes

```sh
sudo lvchange -ay kubuntu-vg/root
```

## Access encrypted file system

```sh
sudo mount /dev/kubuntu-vg/root /mnt/kubuntu1
```

## Deactivate logical volumes

```sh
 sudo lvs -S "lv_active=active && vg_name=kubuntu-vg"
```

## Deactivate active volume group

```sh
sudo lvchange -an mint-vg/root
```

## deactivate every logical volume on this volume group

```sh
sudo lvchange -an kubuntu-vg
```

## Close LUKS device

```sh
sudo cryptsetup luksClose encrypted_device
```

## Double-named group volumes

In the case, when two or more virtual group has the same name, then it can to change name of one:

```sh
vgrename [UUID] new-name-of-vg
```

# Encrypt LVM on LUKS

1. Create LUKS partition

```sh
cryptsetup luksFormat --hash=sha512 --key-size=512 --cipher=aes-xts-plain64 --verify-passphrase /dev/sda
```

2. Open the encrypted device: the command below opens the luks device and maps it as “sda_crypt”

```sh
cryptsetup luksOpen /dev/sda sda_crypt
```

3. Now we fill this device with 0s using dd and /dev/zero as source:

```sh
dd if=/dev/zero of=/dev/mapper/sda_crypt bs=1M
```

4. Override the header

- first destroy the mapping

```sh
cryptsetup luksClose sda_crypt
```

- override the header

```sh
dd if=/dev/urandom of=/dev/sda bs=512 count=20480
```

5. Repeat steps 1 and 2 but this time use a very secure passhrase, because it will be the key to unlock your disk.

6. Now we will use the device as phisical volume…

```sh
pvcreate /dev/mapper/sda_crypt
```

7. Now create a volume group (i will name it “vg00” ) that will contain the phisical device /dev/mapper/sda_crypt

```sh
vgcreate vg00 /dev/mapper/sda_crypt
```

8. Create the logical volumes.

```sh
lvcreate -n lv00_swap -L 4G vg00
lvcreate -n lv01_root -L 30G vg00
lvcreate -n lv02_home -L 10G vg00
lvcreate -n lv03_data -l +100%FREE vg00
```

Please rembember that encryption protects your data only on a pre-boot situation when the machine is not on. After you boot and decrypt the disk you will have no added protection.

9. Format and Mount the Logical Volume

```sh
mkfs -t ext4 /dev/vg00/lv02_home
```

10. mount partitin

```sh
mount /dev/vg00/lv02_home /mnt/system_001/home
```

# Source

- [How To Full Encrypt Your Linux System With Lvm On Luks](https://www.linux.com/tutorials/how-full-encrypt-your-linux-system-lvm-luks/)

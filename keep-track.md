# Keep Track

Bądź na bieżąco i bądź zawsze pierwszy!!!

Poniżej znajdują się linki z kanałami wiedzy. Są to wybrane źródła w celu aktualizacji danych na temat programowania i nie tylko.  
Jest ważną rzeczą, by być na bieżąco z wiedzą.

# HTML

# UML

# JavaScript

- [V8](https://v8.dev/) - V8 is Google’s open source high-performance JavaScript and WebAssembly engine, written in C++.
- [JavaScript Weekly](https://javascriptweekly.com/)
- [Przewodnik JavaScript](https://developer.mozilla.org/pl/docs/Web/JavaScript/Guide)

# PHP

[PHP. The Right Way](https://phptherightway.com/)

# SQL

- [Model Relacyjny Wiki](https://pl.wikipedia.org/wiki/Model_relacyjny)

# ICO

[Dev Icons](https://devicon.dev/)

# DDD

- [Domain Language](https://www.domainlanguage.com/)

# Bestsellers

- software engineering, architecture
  - E. Gamma, R. Helm, R. Johnson, J. Vlissides, _Design Patterns: Elements of Reusble Object-Oriented Software_
  - Gynvael Coldwind, _Zrozumieć programowanie_
  - Gynvael Coldwind, _Praktyczna inżynieria wsteczna. Metody, techniki i narzędzia_
  - Kent Beck, Agile Manifesto
  - Kent Beck, eXtreme programming
  - Robert C. Martin, Clean Code: A Handbook of Agile Software Craftsmanship
- refactoring
  - Martin Fowler, _Refactoring: Improving the the Design of Existing Code_
- testing
  - http://junit.sourceforge.net/doc/testinfected/testing.htm
- DDD
  - Eric Evans, _Domain-Driven Design: Tackling Complexity in the Heart of Software_

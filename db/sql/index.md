# SQL

DML - Data Manipulation Language
DCL - Data Control Language
DDL - Data Definition Language
DQL - Data Query Language

## ACID

zbiór właściwości gwarantujących poprawne przetwarzanie transakcji w bazach danych.

Skrótowiec od:

- `atomicity` - niepodzielność - każda transakcja albo zostanie wykonana w całości, albo w ogóle
- `consistency` - spójność - po wykonaniu transakcji system będzie spójny, czyli nie zostaną naruszone zasady integralności.
- `isolation` - izolacja - jeśli dwie transakcje wykonują się współbieżnie, to zwykle (w zależności od poziomu izolacji) nie widzą wprowadzanych przez siebie zmian. Poziom izolacji w bazach danych jest zazwyczaj konfigurowalny i określa, jakich anomalii możemy się spodziewać przy wykonywaniu transakcji. Przykładowe typy izolacji to (model ANSI):
  - `read uncommitted` – jedna transakcja może odczytywać wiersze, na których działają inne transakcje (najniższy poziom izolacji)
  - `read committed` – transakcja może odczytywać tylko wiersze zapisane
  - `repeatable read` – transakcja nie może czytać ani zapisywać na wierszach odczytywanych lub zapisywanych w innej transakcji
  - `serializable (szeregowalne)` – wyniki współbieżnie realizowanych zapytań muszą być identyczne z wynikami tych samych zapytań realizowanych szeregowo (pełna izolacja).
- `durability` - trwałość - system potrafi uruchomić się i udostępnić spójne, nienaruszone i aktualne dane zapisane w ramach zatwierdzonych transakcji, na przykład po nagłej awarii zasilania.

## JOIN

### Iloczyn kartezjański

- `CROSS JOIN` - iloczyn kartezjański

będzie zbiór, w którym każdy wiersz z pierwszej tabeli połączony będzie z każdym wierszem w drugiej tabeli.

### INNER

Z iloczynu kartezjańskiego wybiera ono te wiersze, dla których warunek złączenia jest spełniony. W żadnej z łączonych tabel kolumna użyta do łączenia nie może mieć wartości NULL

- `[INNER] JOIN`: Returns records that have matching values in both tables
- SELF JOIN - A self JOIN is a regular join, but the table is joined with itself.
- EQUI JOIN - gdy posiada warunek w którym występuje znak równości (`ON`, `WHERE`)
- NATURAL JOIN - gdy obie kolumny występujące w warunku łączącym są tej samej nazwy (`ON`, `USING`, `WHERE`, `NATURAL JOIN`) - nierównozłączenia.
- THETA JOIN - Tym określeniem oznacza się złączenia, w których w warunku występuje inny symbol porównania wartości niż =, np. >, BETWEEN, !=
- ANTI JOIN - Jest to szczególny przypadek nierównozłączeń, w którym łącznikiem jest operator !=
- SEMI JOIN - O złączeniu częściowym mówimy, kiedy w klauzuli SELECT danego złączenia wymieniamy kolumny tylko z jednej z tabel. Ten typ złączenia służy głównie jako sposób filtrowania informacji. Zamiast tego typu często w tym samym celu stosuje się podzapytania.

### OUTER

- `LEFT [OUTER] JOIN`: Returns all records from the left table, and the matched records from the right table
- `RIGHT [OUTER] JOIN`: Returns all records from the right table, and the matched records from the left table
- `FULL [OUTER] JOIN`: Returns all records when there is a match in either left or right table

# Model relacyjny

> model organizacji danych bazujący na matematycznej `teorii mnogości`, w szczególności na pojęciu relacji. Na modelu relacyjnym oparta jest relacyjna baza danych (ang. Relational Database) – baza danych, w której dane są przedstawione w postaci relacyjnej.

> Cały model relacyjny jest oparty na matematycznym pojęciu relacji.

`Relacje` są pewnym zbiorem rekordów o identycznej strukturze wewnętrznie powiązanych za pomocą związków zachodzących pomiędzy danymi. Relacje zgrupowane są w tzw. schematy bazy danych. Twórcą teorii relacyjnych baz danych jest Edgar Frank Codd.

W modelu relacyjnym każda relacja (prezentowana w postaci np. tabeli) posiada `unikatową nazwę`, `nagłówek` i `zawartość`.

`Nagłówek` relacji to zbiór atrybutów, gdzie `atrybut` jest parą `nazwa_atrybutu:nazwa_typu`, `zawartość` natomiast jest `zbiorem krotek` (reprezentowanych najczęściej w postaci wiersza w tabeli).

Każda krotka (wiersz) wyznacza zależność pomiędzy danymi w poszczególnych komórkach

Każda relacja (tabela) posiada tzw. `klucz główny (ang. primary key)`. Klucz ten jest unikatowym identyfikatorem w relacji i może być kombinacją kilku kolumn, często jednak obejmuje jedną kolumnę (jeden atrybut). Klucz ma za zadanie jednoznacznie identyfikować każdą krotkę (wiersz) – wartości w wyznaczonych kolumnach są jako zestaw niepowtarzalne w danej tabeli.

`Klucz obcy (ang. foreign key)` - jest to zbiór atrybutów jednej tabeli (relacji) wskazujący wartości klucza kandydującego innej tabeli. Służy do wskazywania zależności pomiędzy danymi składowanymi w różnych tabelach. Klucze w modelu relacyjnym służą m.in. do sprawdzania spójności danych w bazie. Głównie dotyczy to kluczy obcych, na które nałożony jest wymóg, że w tabeli wskazywanej musi istnieć wartość klucza wskazującego.

Dodatkowym elementem modelu relacyjnego jest zbiór operacji służących do przeszukiwania i manipulacji danymi. Od strony formalnej takie zbiory operacji kojarzone są z tzw. `algebrą relacji` oraz z `rachunkiem relacyjnym`.

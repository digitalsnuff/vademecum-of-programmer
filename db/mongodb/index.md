# MongoDB

## NoSQL

Not Only SQL

- uproszczone projekty
- skalowanie poziome
- dokładniejsza kontrola dostępności danych

Technologie:

- Hbase - struktura kolumn
- Redis - klucz-wartość
- Virtuoso - struktura grafu
- model dokumentów

Obiekty danych są przechowywane jako osobne dokumenty w obrębie kolekcji.

`Kolekcja` - grupa dokumentów o takim samym lub podobnym przeznaczeniu. Jest to rtodzaj grupowania danych. Kolekcja działa podobnie do tabeli, z tym że nie jest wymuszana przez ścisły schemat.

`Dokumnet` - reprezentacja pojedynczej porcji danych. Jest złożona z conajmniej jednego powiązanego obiektu. W odróżnieniu od relacyjnych baz danych, gdzie na każda wartość w wierszu przypada jedna kolumna, dokumenty mogą zawierać osadzone dokumenty. Daje to model danych bardziej zbliżony do naturalnego. Rekordy w MongoDB reprezentują dokumenty. Przechowywane są w formacie BSON - binarnej odmianie JSON. Nazwy pól nie mogą zawierac znaków o kodzie zero, kropek, lub znaków dolara. Ponadto nazwa pola `_id` jest zarezerwowana dla identyfikatora obiektu. Pole `_id` to unikalny identyfikator systemu, który jest złożony z następujących części

- 4-bajtowej wartości reprezentującej liczbę sekund, jaka upłynęła od ostatniej epoki
- 3-bajtowego identyfikatora komputera
- 2-bajtowego identyfikatora procesu
- 3-bajtowego licznika rozpoczynającego się od wartości losowej

Maksymalna wielkość dokumentu w bazie danych to 16 MB. Baza każdemu typowi danych przypisuje identyfikator, który jest liczbą całkowitą z zakresu od 1 do 255. Identyfikator jest uzywany podczas odpytywania z wykorzystaniem typów.

`Normalizowanie danych` - to proces polegający na organizowaniu dokumentów i kolekcji w celu minimalizacji nadmiarowości i zależności. Dane są normalizowane przez identyfikowanie właściwości obiektów, które są obiektami podrzędnymi i powinny być przechowywane jako osobny dokument w innej kolekcji niż dokument obiektu ndrzędnego. Jest to zwykle wykorzystywane dla obiektów, które z obiektami podrzednymi tworzą relację jeden-do-wielu lub wiele-do-wielu. Zapobiega to duplikowaniu obiektów w obiektach pojedynczej kolekcji.

`Denormalizowanie danych` - to proces identyfikowania obiektów podrzednych głównego obiektu, które powinny zostać osadzone bezpośrednio w jego dokumencie. Zwykle jest to wykonanie dla obiektów, które przede wszystkim mają relację jeden-do-fendego, a ponadto są stosunkowo niewielkie i rzadko aktualizowane. Po denoramlizacji można z powrotem uzyskać pełny obiekt w ramach jednego wyszukiwania be zkonieczności przeprowadzania dodatkowych wyszukiwań w celupołączenia obiektów podrzednych z innych kolekcji.

`Ograniczona kolekcja` - kolekacja o ustalonej wielkości. Gdy w kolekcji wymagane jest zapisanie nowego dokumentu, który przekracza jej wielkość, z kolekcji usuwany jest najstarszy dokument, po czym nowy dokument zostaje wstawiony na jego miejsce. Ograniczone kolekcje sprawdzają się znakomicie w przypadku obiektów, z którymi wiąże się duża liczba operacji wstawiania, pobierania i usuwania.

- gwarantują zachowanie kolejności wstawiania. Zapytania nie wymagają użycia indeksu do zwrócenia dokumentów w kolejności, w jakiej zostały one zapisane, co eliminuje obciazenie związane z indekowaniem
- kolejność wstawiania jest taka sama, jak kolejność na dysku, przez to, że zakazują aktualizacji, które zwiększają wielkość dokumentu. Eliminuje to obiążenie wynikające ze zmiany lokalizacji i zarządzania nowym położeniem dokumentów
- automatycznie usuwają najstarsze dokumenty z kolekcji

Ograniczenia:

- nie mozna zwiększac wielkości dokumentów po wstawieniu ich do ograniczonej kolekcji
- dokumenty nie mogą być usuwane z kolekcji.

Przydają się podczas wycofywania dziennnika transakcji w systemie.

Na poziomie dokumentu operacje zapisu są `niepodzielne`. W danej chwili możliwy jest tylko jeden zapis. Tylko jeden proces może aktualizować pojedynczy dokument lub kolekcję.

## Optymalizacja

- optymalizacja wydajności
- skalowanie
- zapewnienie niezawodności

Należy zwracać uweagę na

- `indeksowanie` - zwieksza wydajność w przypadku często uzywanych zapytań.
- `tworzenie instancji partycji` - to proces podziału dużych kolekcji danych, które moga być rozszerzane na wielu serwerach w klastrze. Każdy serwer traktowany jest jako instancja partycji. Jest to zapwenienie skalowania poziomego przy dużej ilości żądań.
- `replikacje` - to proces duplikacji danych na wielu instancjach bazy danych w klastrze

Duża ilośc elementów kolekcji jest mało wydajna. Nalezy rozważyć fragmentację. Duża liczba kolekcji nie wpływa znacząco na wydajność.

TTL (time-to-live) - zostawiamy używane dane. Nieużywane usuwamy lub przenosimy.
MongoDB posiada mechanizm automatycznego czyszeczenia danych po określonym czasie lub w określonym momencie.

Dwa ważne aspekty:

- przydatność danych
- wydajność danych

# Installation

[Download community version](https://www.mongodb.com/try/download/community)
[Install MongoDB Community Edition on Ubuntu](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/)
[How to Install MongoDB on Ubuntu 20.04](https://www.digitalocean.com/community/tutorials/how-to-install-mongodb-on-ubuntu-20-04)

Ubuntu’s official package repositories include a stable version of MongoDB. However, as of this writing, the version of MongoDB available from the default Ubuntu repositories is 3.6, while the latest stable release is 4.4.

```sh
# Installation on Ubuntu 20.04
#
# Import the public key used by the package management system.
wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -
# Create the list file and add line with source
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/4.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list
#
sudo apt-get update
# Install the MongoDB packages
sudo apt-get install -y mongodb
```

```sh
ls -la /var/lib/mongodb # data directory
ls -la /var/log/mongodb # log directory
cat /etc/mongod.conf   # configuration file
#
ps --no-headers -o comm 1 # shows init system name (e.g. systemd)
# start
chown -R mongodb:mongodb /var/lib/mongodb
chown -R mongodb:mongodb /var/log/mongodb

sudo systemctl enable mongod
sudo systemctl start mongod
sudo systemctl daemon-reload
sudo systemctl status mongod
# sudo systemctl stop mongod
# sudo systemctl restart mongod
#
# Mongo shell
mongo
```

# Usage

Monitoring

```sh
> db.enableFreeMonitoring()
{
        "state" : "enabled",
        "message" : "To see your monitoring data, navigate to the unique URL below. Anyone you share the URL with will also be able to view this page. You can disable monitoring at any time by running db.disableFreeMonitoring().",
        "url" : "https://cloud.mongodb.com/freemonitoring/cluster/CHMOGEKFH3LNARQX6MCJ4HVJ6XYBVIA7",
        "userReminder" : "",
        "ok" : 1
}
```

Help

```sh
> help
        db.help()                    help on db methods
        db.mycoll.help()             help on collection methods
        sh.help()                    sharding helpers
        rs.help()                    replica set helpers
        help admin                   administrative help
        help connect                 connecting to a db help
        help keys                    key shortcuts
        help misc                    misc things to know
        help mr                      mapreduce

        show dbs                     show database names
        show collections             show collections in current database
        show users                   show users in current database
        show profile                 show most recent system.profile entries with time >= 1ms
        show logs                    show the accessible logger names
        show log [name]              prints out the last segment of log in memory, 'global' is default
        use <db_name>                set current database
        db.mycoll.find()             list objects in collection mycoll
        db.mycoll.find( { a : 1 } )  list objects in mycoll where a == 1
        it                           result of the last line evaluated; use to further iterate
        DBQuery.shellBatchSize = x   set default number of items to display on shell
        exit                         quit the mongo shell
```

# Administration

- `load(JS_script)`
- `UUID(string)`
- `db.auth(login,password)`

```sh
# add new system user
db.addUser(
    {
        user: "testUser",
        userSource: "test",
        roles: ["read"],
        otherDBRoles: {testDB2: ["readWrite"]}
    }
)
# finding list of system users
# db.system.users - account of system users are placed in this collection
db.system.users.find()
# run scripts
mongo test --eval "printjson(db.getCollectionNames())"
# or
mongo load(`/path/to/script.js`)
```

## Users

Obiekt `Users` składa się z pól

- `_id`
- `user`
- `pwd`
- `roles`
- `otherDBRoles`

Dodawanie użytkownika:

```sh
addUser()
# Fields: user: String, roles: Array[], pwd: String, userSource: [database], otherDBRoles: {db: Array[],db: Array[]}
# admin roles: readAnyDatabase, readWriteAnyDatabase, dbAdminAnyDatabase, userAdminAnyDatabase - only for users in `admin` database
# roles: raed, readAnyDatabase, readWrite, readWriteAnyDatabase, dbAdmin, dbAdminAnyDatabase, clusterAdmin, userAdmin, userAdminAnyDatabase
# dbAdmin: odczyt, zapis, czyszczenie, modyfikowanie, kompresowanie i pobieranie profilu statystyk, sprawdzanie poprawności
# clusterAdmin: ogólne administrowanie komponentem MongoDB (połączenia, tworzenie klastra, replikację, wyświetlanie listy baz danych, tworzenie i usuwanie baz danych)
# userAdmin: tworzenie i modyfikowanie kont użytkowników w bazie danych
```

Aby utworzyć użytkownika, należy dokonać przełączenia na konkretną bazę danych, a następnie użyć metody `addUser()` w celu utworzenia obiektu użytkownika.

Usuwanie użytkonika:

```sh
removeUser(user_name)
```

Konfiguracja

Żeby otrzymać dostęp do utworzonej bazy danych, należy utworzyć nowe konto użytkownika dla tej bazy.

Uwierzytenienie

```sh
mongo
db.auth('useradmin','test')
# or
mongo admin --username "useradmin" --password "test"
```

# Source

> Brad Dayley, _Node.js, MongoDB, AngularJS. Kompendium Wiedzy_, ISBN: 978-83-283-0111-5,Pearson Education, Inc,publishing as Addison Wesley, 2014

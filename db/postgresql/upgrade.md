# Upgrade

1. Install new server version:

```sh
sudo apt-get install postgresql-12
```

2. Compare configs

```sh
diff /etc/postgresql/11/main/postgresql.conf /etc/postgresql/12/main/postgresql.conf
diff /etc/postgresql/11/main/pg_hba.conf /etc/postgresql/12/main/pg_hba.conf
```

3. Stop PostgreSQL service:

```sh
sudo systemctl stop postgresql.service
```

4. Log in as the `postgres` user

```sh
sudo su postgres
```

5. Check clusters (notice the `--check` argument, this will not change any data).

```sh
/usr/lib/postgresql/12/bin/pg_upgrade \
  --old-datadir=/var/lib/postgresql/11/main \
  --new-datadir=/var/lib/postgresql/12/main \
  --old-bindir=/usr/lib/postgresql/11/bin \
  --new-bindir=/usr/lib/postgresql/12/bin \
  --old-options '-c config_file=/etc/postgresql/11/main/postgresql.conf' \
  --new-options '-c config_file=/etc/postgresql/12/main/postgresql.conf' \
  --check
```

6.  Migrate the data (without the `--check` argument).

```sh
/usr/lib/postgresql/12/bin/pg_upgrade \
  --old-datadir=/var/lib/postgresql/11/main \
  --new-datadir=/var/lib/postgresql/12/main \
  --old-bindir=/usr/lib/postgresql/11/bin \
  --new-bindir=/usr/lib/postgresql/12/bin \
  --old-options '-c config_file=/etc/postgresql/11/main/postgresql.conf' \
  --new-options '-c config_file=/etc/postgresql/12/main/postgresql.conf'
```

7. Go back to the regular user.

```sh
exit
```

8. Swap the ports for the old and new PostgreSQL versions.

```sh
sudo vim /etc/postgresql/12/main/postgresql.conf
# ...and change "port = 5433" to "port = 5432"

sudo vim /etc/postgresql/11/main/postgresql.conf
# ...and change "port = 5432" to "port = 5433"
```

9. Start the PostgreSQL service.

```sh
sudo systemctl start postgresql.service
```

10. Log in as the postgres user again.

```sh
sudo su postgres
```

11. Check the new PostgreSQL version.

```sh
psql -c "SELECT version();"
```

12. Run the generated `analyze_new_cluster` script and back to normal user

```sh
./analyze_new_cluster.sh
exit
```

13. Check which old PostgreSQL packages are installed.

```sh
apt list --installed | grep postgresql
```

14. Remove the old PostgreSQL packages (from the listing above).

```sh
sudo apt-get remove postgresql-11
```

15. Remove the old configuration.

```sh
sudo rm -rf /etc/postgresql/11/
```

16. Log in as the postgres user once more.

```sh
sudo su postgres
```

17. Finally, drop the old cluster data.

```sh
./delete_old_cluster.sh
```

# Source

- [How to upgrade PostgreSQL from 11 to 12](https://www.kostolansky.sk/posts/upgrading-to-postgresql-12/)

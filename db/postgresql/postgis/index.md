# PostGIS

## Intro

> PostGIS is a spatial extender for the PostgreSQL relational database that was created by Refractions Research Inc, as a spatial database technology research project. Refractions is a GIS and database consulting company in Victoria, British Columbia, Canada, specializing in data integration and custom software development.
>
> PostGIS is now a project of the OSGeo Foundation and is developed and funded by many FOSS4G Developers as well as corporations all over the world that gain great benefit from its functionality and versatility.
>
> The PostGIS project development group plans on supporting and enhancing PostGIS to better support a range of important GIS functionality in the areas of OpenGIS and SQL/MM spatial standards, advanced topological constructs (coverages, surfaces, networks), data source for desktop user interface tools for viewing and editing GIS data, and web-based access tools.

## Spatial standards

- OpenGIS
- SQL/MM

## Install PostGIS

```sh
CREATE EXTENSION postgis;
CREATE EXTENSION postgis_topology;
-- if you built with sfcgal support --
CREATE EXTENSION postgis_sfcgal;

-- if you want to install tiger geocoder --
CREATE EXTENSION fuzzystrmatch;
CREATE EXTENSION postgis_tiger_geocoder;

-- if you installed with pcre
-- you should have address standardizer extension as well
CREATE EXTENSION address_standardizer;
-- e.g. --
CREATE EXTENSION address_standardizer_data_us;
# SELECT num, street, city, state, zip FROM parse_address('1 Devonshire Place PH301, Boston, MA 02109');
```

// TODO - extension for standardize polish addresses

## Create spatial database using EXTENSION

> When it creates extensions, then spatial structures are created: schemas, sequences, tables, views, datas.

## Customize

- Change tiger files path

```sh
mkdir /var/gisdata
```

```sql
# was `/gisdata`
UPDATE tiger.loader_variables SET staging_fold = '/var/gisdata';
```

- create `temp` folder

```sh
mkdir /var/gisdata/temp
```

- generate sh script

```sh
psql -c "SELECT Loader_Generate_Nation_Script('debbie')" -d gis -tA > /var/gisdata/nation_script_load.sh
```

or

```sql
COPY (SELECT Loader_Generate_Nation_Script('debbie')) TO '/var/gisdata/nation_script_load.sh';
```

- Run the generated nation load commandline scripts

```sh
cd /var/gisdata
sh nation_script_load.sh
# open file before run and change data if needed
```

> After you are done running the nation script, you should have three tables in your tiger_data schema and they should be filled with data. Confirm you do by doing the following queries from psql or pgAdmin

```sql
SELECT count(*) FROM tiger_data.county_all;
SELECT count(*) FROM tiger_data.state_all;
```

> By default the tables corresponding to bg, tract, tabblock are not loaded. These tables are not used by the geocoder but are used by folks for population statistics. If you wish to load them as part of your state loads, run the following statement to enable them.

```sql
UPDATE tiger.loader_lookuptables SET load = true WHERE load = false AND lookup_name IN ('tract', 'bg', 'tabblock');
```

> Alternatively you can load just these tables after loading state data using the `Loader_Generate_Census_Script`

> For each state you want to load data for, generate a state script `Loader_Generate_Script`. DO NOT Generate the state script until you have already loaded the nation data, because the state script utilizes county list loaded by nation script.

```sql
psql -c "SELECT Loader_Generate_Script(ARRAY['MA'], 'debbie')" -d geocoder -tA > /gisdata/ma_load.sh

```

> Run the generated script

```sh
cd sh/gisdata
ma_load.sh
```

## Exapmles

- use address_standarizer_data_us

```sql
SELECT num, street, city, state, zip FROM parse_address('1 Devonshire Place PH301, Boston, MA 02109');
```

---

> Create a new record in tiger.loader_platform table with the paths of your executables and server. So for example to create a profile called debbie that follows sh convention. You would do:

```sql
INSERT INTO tiger.loader_platform(os, declare_sect, pgbin, wget, unzip_command, psql,path_sep,loader, environ_set_command, county_process_command)
SELECT 'debbie', declare_sect, pgbin, wget, unzip_command, psql, path_sep, loader, environ_set_command, county_process_command
FROM tiger.loader_platform
WHERE os = 'sh';
```

# Links

[Postgis Homepage](https://postgis.net/)

[Documentation PDF v2.5](https://postgis.net/stuff/postgis-2.5.pdf)

[OpenGIS](http://www.opengeospatial.org/)

[Natural Earth Maps](http://naturalearthdata.com/)

[GeoJSON maps](https://geojson-maps.ash.ms/)

[GeoJSON leafletjs Tool](https://leafletjs.com/examples/geojson/)

[D3.js](https://d3js.org/)

[sfcgal](http://www.sfcgal.org/)

[Dane udostępniane bez opłat na podstawie ustawy Prawo geodezyjne i kartograficzne](http://www.gugik.gov.pl/pzgik/dane-udostepniane-bez-oplat)

[Dane z państwowego rejestru granic i powierzchni jednostek podziałów terytorialnych kraju](http://www.gugik.gov.pl/pzgik/dane-bez-oplat/dane-z-panstwowego-rejestru-granic-i-powierzchni-jednostek-podzialow-terytorialnych-kraju-prg)

[GIS Support](https://gis-support.pl/granice-administracyjne/)

[Źródłowe dane adresowe gmin](https://integracja.gugik.gov.pl/daneadresowe/)

[WALIDACJA PLIKÓW XML/GML](http://geoinformatyka.com.pl/walidacja-plikow-xml-gml-dlaczego-jest-potrzebna/#more-2148)

[Schematy aplikacyjne](http://www.gugik.gov.pl/bip/prawo/schematy-aplikacyjne)

[emuia](https://emuia.gugik.gov.pl/?page_id=842)

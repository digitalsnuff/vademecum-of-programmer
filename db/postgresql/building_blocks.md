# Building Blocks

The conceptual model will be translated to a physical model.

Data modeling techniques.

Coding best practices.

Hierarchy of the database objects in PostgreSQL.

How to configure the database cluster and tune its settings.

## The software engineering principles

### Database naming conventions

A naming convention describes how names are to be formulated. Naming
conventions allow some information to be derived based on patterns, which helps the developer to easily search for and predict the database object names. Database naming conventions should be standardized across the organization. There is a lot of debate on how to name database objects.

Prefixes or suffixes?

With regard to database object names, one should try to use descriptive names, and avoid acronyms and abbreviations if possible. Also, singular names are preferred, because a table is often mapped to an entity in a high programming language; thus, singular names lead to unified naming across the database tier and the business logic tier. Furthermore, specifying the cardinality and participation between tables is straightforward when the table names are singular.

In the database world, compound object names often use underscore but not camel case due to the ANSI SQL standard specifications regarding identifiers quotation and case sensitivity. In the ANSI SQL standard, non-quoted identifiers are **case-insensitive**.

In existing projects, do not invent any new naming conventions, unless the new naming conventions are communicated to the team members.

- The names of tables and views are not suffixed
- The database object names are unique across the database
- The identifiers are singulars including table, view, and column names
- Underscore is used for compound names
- The primary key is composed of the table name and the suffix "id"
- A foreign key has the same name of the referenced primary key in the linked table
- Use the internal naming conventions of PostgreSQL to rename the primary keys, foreign keys, and sequences.
- Do not use keywords to rename your database objects.

### PostgreSQL identifiers

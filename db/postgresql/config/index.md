# Custom configuration of PostgreSQL

PostgreSQL instance has two parts:

- memory
- processes
- physical files

The Memory divides into:

- shared memory
  - Shared buffers
  - WAL Buffers
  - CLOG Buffers
  - Memory for locks
- process memory:
  - vacuum buffers
  - work memory
  - maintenance work memory
  - temp buffers

The Processes divides into:

- postmaster
- writer
- stats collector
- checkpointer
- WAL writer
- archiver
- logger
- autovacuum launcher
- WAL sender
- WAL receiver

The physical files divides into:

- data files
- WAL files
- Temp files
- CLOG files
- stat files
- log files
- WAL archive files

## shared_memory

It is always faster to read or write data in memory than on any other media. A database server also needs memory for quick access to data, whether it is READ or WRITE access. In PostgreSQL, this is referred to as "shared buffers" and is controlled by the parameter shared_buffers. The amount of RAM required by shared buffers is always locked for the PostgreSQL instance during its lifetime. The shared buffers are accessed by all the background server and user processes connecting to the database.

The data that is written or modified in this location is called "dirty data" and the unit of operation being database blocks (or pages), the modified blocks are also called "dirty blocks" or "dirty pages". Subsequently, the dirty data is written to disk containing physical files to record the data in persistent location and these files are called "data files".

# Custom postgresql.conf

Create config file:

```sh
sudo touch /etc/postgresql/11/main/conf.d/10-postgresql.conf
```

and add content:

```sh
cat <<EOF | sudo tee /etc/postgresql/11/main/conf.d/10-postgresql.conf
listen_addresses = '*'
port = 5432
max_connections = 100

#unix_socket_group = ''
#unix_socket_permissions = 0777

#bonjour = off
#bonjour_name =  ''

#tcp_keepalives_idle = 0
#tcp_keepalives_interval = 0
#tcp_keepalives_count = 0

#authentication_timeout = 1min
password_encryption = scram-sha-256
#db_user_namespace = off

#ssl_ciphers = 'HIGH:MEDIUM:+3DES:!aNULL' # allowed SSL ciphers
#ssl_prefer_server_ciphers = on
#ssl_ecdh_curve = 'prime256v1'
#ssl_dh_params_file = ''
#ssl_passphrase_command = ''
#ssl_passphrase_command_supports_reload = off

shared_buffers = 512MB

#huge_pages = try
#temp_buffers = 8MB
#max_prepared_transactions = 0
#work_mem = 4MB
#maintenance_work_mem = 64MB
#autovacuum_work_mem = -1
#max_stack_depth = 2MB

#temp_file_limit = -1

#max_files_per_process = 1000

#effective_io_concurrency = 1
#max_worker_processes = 8
#max_parallel_maintenance_workers = 2
#max_parallel_workers_per_gather = 2
#parallel_leader_participation = on
#max_parallel_workers = 8

#old_snapshot_threshold = -1

#backend_flush_after = 0

EOF
```

# Source

[Memory components](https://www.postgresql.fastware.com/blog/back-to-basics-with-postgresql-memory-components)

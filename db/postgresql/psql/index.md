# psql

- connection information

```sh
\conninfo
```

- list of databases

```sh
# SELECT datname FROM pg_database WHERE datistemplate = false;
\l
\list
\l+ /* list with size of each database */
```

- connect to database

```sh
\c [DATABASE]
\connect [DATABASE]
```

- list of tables

```sh
# SELECT * FROM pg_catalog.pg_tables;
# select * from information_schema.tables where table_schema='public';
\dt /* this is list of tables of the public schema */
\dt <schema-name>.* /* this is list of tables of certain schema */
\dt *.*  /* list tables of all schemas */
```

- show relations

```sh
\d
```

- show all tables of schema

```sh
\dt+
```

- expanded dispaly

```sh
\x
\dx postgis*
```

- `\ef` - asking for editor selection

- Query result format control: psql supports different formats such as html and latex.

- `\d+ [pattern]`: This describes all the relevant information for a relation.

- `\df+ [pattern]`: Describes a function

- `\z [pattern]`: Shows the relation access privileges

For shell scripting, there are several options that makes psql convenient, these options are:

- `-A`: The output is not aligned;
- `-q` (quiet): This option forces psql not to write a welcome message or any other informational output.
- `-t`: This option tells psql to write the tuples only, without any header information.
- `-X`: This option informs psql to ignore the psql configuration that is stored in `~/.psqlrc file`.
- `-o`: This option specifies psql to output the query result to a certain location.
- `-F`: This option determines the field separator between columns. This option can be used to generate comma-separated values (CSV), which are useful for importing data to excel files.
- PGOPTIONS: psql can use PGOPTIONS to add command-line options to send to the server at run-time. This can be used to control statement behavior such as to allow indexing only, or to specify the statement timeout.

## Psql advanced settings

The psql client can be personalized. The psqlrc file is used to store the user preference for later use. There are several aspects of psql personalization, including:

- Look and feel
- Behavior
- Shortcuts

### Recipe 1

Show the connection string information including the server name, database name, user name, and port.
The \set meta command is used to assign a psql variable to a value. In this case, it assigns PROMPT1 to(%n@%M:%>) [%/]%R%#%x >. The percent sign (%) is used as a placeholder for substitution. The substitutions in the example will be as follows:

- `%M`: The full host name. In the example, [local] is displayed, because we use the Linux socket
- `%>`: The PostgreSQL port number
- `%n`: The database session user name
- `%/`: The current database name
- `%R`: Normally substituted by =; if the session is disconnected for a certain reason, then it is substituted with (!).
- `%#`: Used to distinguish super users from normal users. The (#) hash sign indicates that the user is a super user. For a normal user, the sign is (>).
- `%x`: The transaction status. The \* sign is used to indicate the transaction block, and (!) sign to indicate a failed transaction block.

e.g.

```sh
sudo -su postgres psql -d template1
```

`template1=# \set PROMPT1 '(%n@%M:%>) [%/]%R%#%x > '`
`(postgres@[local]:5432) [template1]=# >`

### Recipe 2

Add a shortcut for a common query such as showing the current database activities. psql can be used to assign arbitrary variables using the \set" meta command. Again, the : symbol is used for substitution.
The \x meta command changes the display setting to expanded display.

`template1=# \set activity 'select datname, pid, username, application_name, query, state from pg_stat_activity;'`

### Recipe 3

Control statement and transaction execution behavior. psql provides three variables, which are AUTOCOMMIT, ON_ERROR_STOP, and ON_ERROR_ROLLBACK.

- `ON_ERROR_STOP`: By default, psql continues executing commands even after encountering an error. This is useful for some operations, including as dumping and restoring the whole database, where some errors can be ignored, such as missing extensions. However, in developing applications, such as deploying new application, errors cannot be ignored, and it is good to set this variable to ON. This variable is useful with the -f, \i, \ir options.
- `ON_ERROR_ROLLBACK`: When an error occurs in a transaction block, one of three actions is performed depending on the value of this variable. When the variable value is off, then the whole transaction is rolled back—this is the default behavior. When the variable value is on, then the error is ignored, and the transaction is continued. The interactive mode ignores the errors in the interactive sessions, but not when reading files.
- `AUTOCOMMIT`: An SQL statement outside a transaction, committed implicitly. To reduce human error, one can turn this option off

### Other recipes

- `\timing`: Shows the query execution time.
- `\pset null 'NULL'`: Displays null as NULL. This is important to distinguish an empty string from the NULL values. The \pset meta command is used to control the output formatting.

## Backup and replication

PostgreSQL supports physical and logical backup.

- `pg_dump` - Creates a logical dump of a single PostgreSQL database into an SQL script file or archive file.
- `pg_dumpall` - Creates a logical dump for the whole PostgreSQL cluster
- `pg_restore` - Restores a PostgreSQL database or PostgreSQL cluster from an archive file created by pg_dump or pg_dumpall
- `pg_basebackup` - Dumps the whole database cluster by taking a snapshot of the hard disk
- `pg_receivexlog` - Streams transaction logs from a PostgreSQL cluster

## Utilities

PostgreSQL is shipped with two utilities: `pg_config` and `pg_isready.pg_config`.

They come with the postgresql package, and are used to provide information about the installed version of PostgreSQL such as binaries and cluster folder location.

`pg_config` provides valuable information since the PostgreSQL installation location and build information varies from one operating system to another.

`pg_isready` checks the connection status of a PostgreSQL server and provides several exit codes to determine connection problems. Exit code zero means successfully connected. If the exit code is one, then the server is rejecting connections. If there is no response from the server, two will be returned as the exit code. Finally, exit code three indicates that no attempt is made to connect to the server.

In addition to the above, `vacuumdb` is used for garbage collection and for statistical data collection. `vacuumdb` is built around the PostgreSQL-specific statement, `vacuum`.

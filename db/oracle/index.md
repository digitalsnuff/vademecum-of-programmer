# Oracle Database Kickstart

## Prepare environment

- download Oracle Linux (https://www.oracle.com/linux/technologies/oracle-linux-downloads.html)
- download Oracle Database installation file. RPM is recommended. (https://www.oracle.com/pl/database/technologies/oracle-database-software-downloads.html)
- create virtual host using Oracle Linux iso
- It is possible  to use Vagrant: https://yum.oracle.com/boxes/
- install missing kernel headers: `yum install kernel-uek-devel-`uname -r` kernel-headers`
- install packages: `yum install gcc perl make`
- run `sudo /sbin/rcvboxadd quicksetup all` (for Virtual Box Addons)
- add additional repo `sudo dnf install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm && sudo dnf upgrade`
- install `sudo yum install -y epel-release terminator guake`
- run `dnf install oracle-database-preinstall-19c`
- install Oracle Database from file: `yum install oracle-database-ee-19c-1.0-1.x86_64.rpm`
- To configure a sample Oracle Database you can execute the following service configuration script as root: `/etc/init.d/oracledb_ORCLCDB-19c configure`
- install SQLDeveloper: `yum install jdk-11.0.10_linux-x64_bin.rpm && yum install sqldeveloper-20.4.1.407.0006-20.4.1-407.0006.noarch.rpm`
`
- for remove: `rpm -e oracle-database-preinstall-19c --nodeps`
- for remove duplicates `rpm -ev  oracle-database-ee-19c-1.0-1.x86_64  --nodeps --allmatches --justdbs`

@see https://oracle-base.com/articles/19c/oracle-db-19c-installation-on-oracle-linux-8

## set env

```shell
cat > /home/oracle/scripts/setEnv.sh <<EOF
# Oracle Settings
export TMP=/tmp
export TMPDIR=$TMP

export ORACLE_HOSTNAME=oracle-1.localdb
export ORACLE_UNQNAME=cdb1
export ORACLE_BASE=/opt/oracle
export ORACLE_HOME=$ORACLE_BASE/product/19c/dbhome_1
export ORA_INVENTORY=/opt/oracle/oraInventory
export ORACLE_SID=cdb1
export PDB_NAME=pdb1
export DATA_DIR=/opt/oracle/oradata

export PATH=/usr/sbin:/usr/local/bin:$PATH
export PATH=$ORACLE_HOME/bin:$PATH

export LD_LIBRARY_PATH=$ORACLE_HOME/lib:/lib:/usr/lib
export CLASSPATH=$ORACLE_HOME/jlib:$ORACLE_HOME/rdbms/jlib
EOF
```

```shell
cat > /home/oracle/scripts/start_all.sh <<EOF
#!/bin/bash
. /home/oracle/scripts/setEnv.sh

export ORAENV_ASK=NO
. oraenv
export ORAENV_ASK=YES

dbstart \$ORACLE_HOME
EOF


cat > /home/oracle/scripts/stop_all.sh <<EOF
#!/bin/bash
. /home/oracle/scripts/setEnv.sh

export ORAENV_ASK=NO
. oraenv
export ORAENV_ASK=YES

dbshut \$ORACLE_HOME
EOF
```

```shell
sudo chown -R oracle:oinstall /home/oracle/scripts
sudo chmod u+x /home/oracle/scripts/*.sh
```

## Installation

```shell
# run xhost
# xhost +oracle-1
DISPLAY=:0.0; export DISPLAY

# Unzip software.
cd $ORACLE_HOME
unzip -oq /path/to/software/LINUX.X64_193000_db_home.zip

# Fake Oracle Linux 7.
export CV_ASSUME_DISTID=OEL7.6

# Interactive mode.
./runInstaller

# Silent mode.
./runInstaller -ignorePrereq -waitforcompletion -silent                        \
    -responseFile ${ORACLE_HOME}/install/response/db_install.rsp               \
    oracle.install.option=INSTALL_DB_SWONLY                                    \
    ORACLE_HOSTNAME=${ORACLE_HOSTNAME}                                         \
    UNIX_GROUP_NAME=oinstall                                                   \
    INVENTORY_LOCATION=${ORA_INVENTORY}                                        \
    SELECTED_LANGUAGES=en,en_GB                                                \
    ORACLE_HOME=${ORACLE_HOME}                                                 \
    ORACLE_BASE=${ORACLE_BASE}                                                 \
    oracle.install.db.InstallEdition=EE                                        \
    oracle.install.db.OSDBA_GROUP=dba                                          \
    oracle.install.db.OSBACKUPDBA_GROUP=dba                                    \
    oracle.install.db.OSDGDBA_GROUP=dba                                        \
    oracle.install.db.OSKMDBA_GROUP=dba                                        \
    oracle.install.db.OSRACDBA_GROUP=dba                                       \
    SECURITY_UPDATES_VIA_MYORACLESUPPORT=false                                 \
    DECLINE_SECURITY_UPDATES=true
```

```shell
# As a root user, execute the following script(s):
# 1. 
/opt/oracle/oraInventory/orainstRoot.sh
# 2. 
/opt/oracle/product/19.0.0/dbhome_1/root.sh
```

## Create database

```shell
# Start the listener.
lsnrctl start

# Interactive mode.
dbca

# Silent mode.
dbca -silent -createDatabase                                                   \
     -templateName General_Purpose.dbc                                         \
     -gdbname ${ORACLE_SID} -sid  ${ORACLE_SID} -responseFile NO_VALUE         \
     -characterSet AL32UTF8                                                    \
     -sysPassword SysPassword1                                                 \
     -systemPassword SysPassword1                                              \
     -createAsContainerDatabase true                                           \
     -numberOfPDBs 1                                                           \
     -pdbName ${PDB_NAME}                                                      \
     -pdbAdminPassword PdbPassword1                                            \
     -databaseType MULTIPURPOSE                                                \
     -memoryMgmtType auto_sga                                                  \
     -totalMemory 2000                                                         \
     -storageType FS                                                           \
     -datafileDestination "${DATA_DIR}"                                        \
     -redoLogFileSize 50                                                       \
     -emConfiguration NONE                                                     \
     -ignorePreReqs
```

# Post installation

```shell
# Edit the "/etc/oratab" file setting the restart flag for each instance to 'Y'.
# cdb1:/u01/app/oracle/product/19.0.0/db_1:Y
```

Enable Oracle Managed Files (OMF) and make sure the PDB starts when the instance starts.
```shell
sqlplus / as sysdba <<EOF
alter system set db_create_file_dest='${DATA_DIR}';
alter pluggable database ${PDB_NAME} save state;
exit;
EOF
```

### Vagrant

- Create working directory
- use one of urls on init:
```
- Oracle Linux 8: https://oracle.github.io/vagrant-projects/boxes/oraclelinux/8.json
- Oracle Linux 7: https://oracle.github.io/vagrant-projects/boxes/oraclelinux/7.json
- Oracle Linux 6: https://oracle.github.io/vagrant-projects/boxes/oraclelinux/6.json
```
- run following commands:
```shell
vagrant init oraclelinux/{release} <box json url>
vagrant up
vagrant ssh
# example:
# vagrant init oraclelinux/8 https://oracle.github.io/vagrant-projects/boxes/oraclelinux/8.json
```
- updating Oracle Linux Box
```shell
# checking
vagrant box outdated --global
# update 
vagrant box update --box oraclelinux/8
```

```shell
#  start listener
lsnrctl start
```

- examples: https://github.com/oracle/vagrant-projects

## Tips

```shell
[SEVERE] The install cannot proceed because the directory "/opt/oracle/product/19c/dbhome_1"
is not empty. Remove its contents and retry the installation.
```

```shell
To configure a sample Oracle Database you can execute the following service configuration script as root: /etc/init.d/oracledb_ORCLCDB-19c configure
```

```shell
[root@localhost oracle]# /etc/init.d/oracledb_ORCLCDB-19c configure
Configuring Oracle Database ORCLCDB.
Prepare for db operation
8% complete
Copying database files
31% complete
Creating and starting Oracle instance
32% complete
36% complete
40% complete
43% complete
46% complete
Completing Database Creation
51% complete

54% complete
Creating Pluggable Databases
58% complete
77% complete
Executing Post Configuration Actions
100% complete
Database creation complete. For details check the logfiles at:
 /opt/oracle/cfgtoollogs/dbca/ORCLCDB.
Database Information:
Global Database Name:ORCLCDB
System Identifier(SID):ORCLCDB
Look at the log file "/opt/oracle/cfgtoollogs/dbca/ORCLCDB/ORCLCDB1.log" for further details.

Database configuration completed successfully. The passwords were auto generated, you must change them by connecting to the database using 'sqlplus / as sysdba' as the oracle user.

```

## Fixing

```shell
# source https://www.youtube.com/watch?v=7ByqWKv249w
cd /opt/oracle/product/19c/dbhome_1/dbs
cp initorcl.ora initcdb1.ora 

# change
# db_name='ORCL' to db_name='orcl'
# audit_file_dest='<ORACLE_BASE>/admin/orcl/adump' to audit_file_dest='/opt/oracle/admin/ORCLCDB/adump'
# db_recovery_file_dest='<ORACLE_BASE>/fast_recovery_area' to db_recovery_file_dest='/opt/oracle/fast_recovery_area'
# diagnostic_dest='<ORACLE_BASE>' to diagnostic_dest='/opt/oracle'
# set db_recovery_file_dest='/opt/oracle/admin/ORCLCDB/pfile'
# set audit_file_dest='/opt/oracle/admin/ORCLCDB/adump'
```

```shell
sqlplus / as sysdba
# SQL> startup
# ORA-01261: Parameter db_recovery_file_dest destination string cannot be translated
# ORA-01262: Stat failed on a file destination directory
# Linux-x86_64 Error: 2: No such file or directory

# solution
# db_recovery_file_dest must have directory path
```

```shell
# create new spfile
CREATE SPFILE FROM PFILE='/opt/oracle/product/19c/dbhome_1/dbs/initcdb1.ora';
#
SELECT name, VALUE, ISDEFAULT, ISSES_MODIFIABLE, ISSYS_MODIFIABLE FROM v$parameter WHERE name='processes';
#
find / -name control*.ctl -type f 2>/dev/null
# /opt/oracle/oradata/ORCLCDB/control01.ctl
# /opt/oracle/oradata/ORCLCDB/control02.ctl
#
ALTER SYSTEM SET control_files='/opt/oracle/oradata/ORCLCDB/control01.ctl','/opt/oracle/oradata/ORCLCDB/control02.ctl' scope=spfile;

```
# Relational

- PostgreSQL
- MySQL
- MS SQL
- Oracle

# NoSQL

- MongoDB
- Couchbase

# key - value

- Redis
- Varnish

# Cloud

- Firebase
- AWS
- Azure DocumentDB

# Lightweight

- SQLite
- NeDB
- Redis

# Relational Databases

## History

The term database can be used to present a collection of things.

A database can be defined as a collection or a repository of data, which has a certain structure, managed by a database management system (DBMS).

Data can be structured as:

- tabular data,
- semi-structured as XML documents,
- unstructured data that does not fit a predefined data model.

With the introduction of object-oriented languages, new paradigms of database management systems appeared such as object-relational databases and object-oriented databases.

With the advent of web applications such as social portals, it is now necessary to
support a huge number of requests in a distributed manner. This has led to another
new paradigm of databases called NoSQL (Not Only SQL) which has different
requirements such as performance over fault tolerance and horizontal scaling
capabilities.

The timeline of database evolution was greatly affected by many factors such as:

- Functional requirements: The nature of the applications using a DBMS has
  led to the development of extensions on top of relational databases such
  as PostGIS (for spatial data) or even dedicated DBMS such as SCI-DB (for
  scientific data analytics).
- Nonfunctional requirements: The success of object-oriented programming
  languages has created new trends such as object-oriented databases. Object
  relational database management systems have appeared to bridge the
  gap between relational databases and the object-oriented programming
  languages. Data explosion and the necessity to handle terabytes of data on
  commodity hardware have led to columnar databases, which can easily scale
  up horizontally.

## Database management systems

Understanding the different database categories enables the developer to utilize the best in each world.

Support diverse application scenarios, use cases, and requirements.

## Relational algebra

## Data modeling

## Database categories

- relational object-relational databases
- NoSQL databases

## The NoSQL databases

The NoSQL databases are affected by the CAP theorem, also known as Brewer's
theorem. In 2002, S. Gilbert and N. Lynch published a formal proof of the CAP
theorem in their article: "Brewer's conjecture and the feasibility of consistent,
available, partition-tolerant web services". In 2009, the NoSQL movement began.
Currently, there are over 150 NoSQL databases (nosql-database.org).

Often, the main difference between a relational database and a NoSQL database is consistency. A relational database enforces ACID. ACID is the acronym for the following properties:

- Atomicity,
- Consistency,
- Isolation,
- Durability.

In contrast, many NoSQL databases adopt the **basically available soft-state, eventual-consistency (BASE)** model, which prizes availability over consistency, and informally guarantees that if no new updates are made on a data item, eventually all access to that data item will return the latest version of that data item.

The advantages of this approach include the following:

- Simplicity of design
- Horizontal scaling and easy replication
- Schema free
- Huge amount of data support

A NoSQL database provides a means for data storage, manipulation, and retrieval
for non-relational data. The NoSQL databases are distributed, open source and
horizontally scalable.

### The CAP theorem

The CAP theorem states that it is impossible for a distributed computing system to
simultaneously provide all three of the following guarantees:

- Consistency: All clients see (immediately) the latest data even in the case of
  updates.
- Availability: All clients can find a replica of some data even in the case of
  a node failure. That means even if some part of the system goes down, the
  clients can still access the data.
- Partition tolerance: The system continues to work regardless of arbitrary
  message loss or failure of part of the system.

## Types of NoSQL databases

### Key-value databses

Storage is based on maps or hash tables.  
Some key-value databases allow complex values to be stored as lists and hash tables.  
Some of the existing open source key-value databases are Riak, Redis, Memebase, and MemcacheDB.

### Columnar databases

Columnar or column-oriented databases are based on columns. Data in a certain
column in a two dimensional relation is stored together. Unlike relational databases,
adding columns is inexpensive, and is done on a row-by-row basis. Rows can have
a different set of columns. Tables can benefit from this structure by eliminating the
storage cost of the null values. This model is best suited for distributed databases.
HBase is one of the most famous columnar databases. It is based on the Google
big table storage system. Column-oriented databases are designed for huge data
scenarios, so they scale up easily. For small datasets, HBase is not a suitable
architecture. First, the recommended hardware topology for HBase is a five-node
or server deployment. Also, it needs a lot of administration, and is difficult to
master and learn.

### Document databases

A document-oriented database is suitable for documents and semi-structured data.
The central concept of a document-oriented database is the notion of a document.
Documents encapsulate and encode data (or information) in some standard
formats or encodings such as XML, JSON, and BSON. Documents do not adhere
to a standard schema or have the same structure; so, they provide a high degree of
flexibility. Unlike relational databases, changing the structure of the document is
simple, and does not lock the clients from accessing the data.
Document databases merge the power of relational databases and column-oriented
databases. They provide support for ad-hoc queries, and can be scaled up easily.
Depending on the design of the document database, MongoDB is designed to handle
a huge amount of data efficiently. On the other hand, CouchDB provides high
availability even in the case of hardware failure.

### Graph databases

Graph databases are based on the graph theory, where a database consists of nodes
and edges. The nodes as well as the edges can be assigned data. Graph databases
allow traversing between the nodes using edges. Since a graph is a generic data
structure, graph databases are capable of representing different data. A famous
implementation of an open source commercially supported graph databases is Neo4j.

## Relational and object relational databases

A relational
database is formally described by relational algebra, and is modeled on the relational
model. Object-relational database (ORD) are similar to relational databases. They
support object-oriented model concepts such as:

- User defined and complex data types
- Inheritance

## ACID properties

In a relational database, a single logical operation is called a **transaction**.  
The technical translation of a transaction is a set of database operations, which are **create, read, update, and delete (CRUD)**.

- **Atomicity**: All or nothing, which means that if a part of a transaction fails, then the transaction fails as a whole.
- **Consistency**: Any transaction gets the database from one valid state to another valid state. Database consistency is governed normally by data constraints and the relation between data and any combination thereof. For example, imagine if one would like to completely purge his account on a shopping service. In order to purge his account, his account details, such as list of addresses, will also need to be purged. This is governed by foreign key constraints, which will be explained in detail in the next chapter.
- **Isolation**: Concurrent execution of transactions results in a system state that would be obtained if the transactions were executed serially.
- **Durability**: The transactions which are committed, that is executed successfully, are persistent even with power loss or some server crashes. This is done normally by a technique called write-ahead log (WAL).

## The SQL Language

The SQL language has several parts:

- **Data definition language (DDL)**: It defines and amends the relational structure.
- **Data manipulation language (DML)**: It retrieves and extracts information from the relations.
- **Data control language (DCL)**: It controls the access rights to relations.

## Basic concepts

A relational model is a first-order predicate logic, which was first introduced by
Edgar F. Codd. A database is represented as a collection of relations. The state of the whole database is defined by the state of all the relations in the database. Different information can be extracted from the relations by joining and aggregating data from different relations, and by applying filters on the data.

The terms relation, tuple, attribute, and unknown, which are used in the formal relational model, are equivalent to table, row, column, and null in the SQL language.

### Relation

Think of a relation as a table with a header, columns, and rows.

The table name and the header help in interpreting the data in the rows. Each row represents a group of related data, which points to a certain object.

A relation is represented by a set of tuples. Tuples should have the same set of ordered attributes. Attributes have a **domain**, that is, a type and a name.

The relation schema is denoted by the relation name and the relation attributes.

**Relation state** is defined by the set of relation tuples; thus, adding, deleting, and amending a tuple will change the relation to another state.

A relation can represent entities in the real world, such as a customer, or can be used to represent an association between relations.

Separating the data in different relations is a key concept in relational database
modeling. This concept called **normalization** is the process of organizing relation
columns and relations to reduce data redundancy.

### Tuple

A tuple is a set of ordered attributes. They are written by listing the elements within parentheses () and separated by commas, such as (john, smith, 1971). Tuple elements are identified via the attribute name.

A tuple has a finite set of attributes.

In the formal relational model, multi-valued attributes as well as composite attributes are not allowed.

Predicates in relational databases uses _three-valued logic (3VL)_, where there are three truth values: true, false, and unknown.

### Attribute

Each attribute has a name and a domain, and the name should be distinct within the relation.

The domain defines the possible set of values that the attribute can have. One way to define the domain is to define the data type and a constraint on this data type.

The formal relational model puts a constraint on the domain: the value should be atomic. Atomic means that each value in the domain is indivisible.

### Constraint

The relational model defines many constraints in order to control data integrity, redundancy, and validity.

- **Redundancy**: Duplicate tuples are not allowed in the relation.
- **Validity**: Domain constraints control data validity.
- **Integrity**: The relations within a single database are linked to each other. An action on a relation such as updating or deleting a tuple might leave the other relations in an invalid state.

We could classify the constraints in a relational database roughly into two categories:

- **Inherited constraints from the relational model**: Domain integrity, entity integrity, and referential integrity constraints.
- **Semantic constraint, business rules, and application specific constraints**: These constraints cannot be expressed explicitly by the relational model. However, with the introduction of procedural SQL languages such as PL/pgsql for PostgreSQL, relational databases can also be used to model these constraints.

#### Domain integrity constraint

The domain integrity constraint ensures data validity. The first step in defining the domain integrity constraint is to determine the appropriate data type. The domain data types could be integer, real, boolean, character, text, inet, and so on.

- **Check constraint**: A check constraint can be applied to a single attribute or a combination of many attributes in a tuple.
- **Default constraint**: The attribute can have a default value.
- **Unique contraint**: A unique constraint guarantees that the attribute has a distinct value in each tuple. It allows null values.
- **Not null constraint**: By default, the attribute value can be null. The not null constraint restricts an attribute from having a null value.

#### Entity integrity constraint

In the relational model, a relation is defined as a set of tuples. By definition, the element of the set is distinct. This means that all the tuples in a relation must be distinct. The entity integrity constraint is enforced by having a **primary key** which is an attribute/set of attributes having the following characteristics:

- The attribute should be unique
- The attributes should be not null

Each relation must have only one primary key, but can have many unique keys.  
A candidate key is a minimal set of attributes which can identify a tuple.  
The set of all attributes form a super key.

If the primary key is generated by the DBMS, then it is called a **surrogate key**. Otherwise, it is called a **natural key**. The surrogate key candidates can be sequences and **universal unique identifiers (UUID)**. A surrogate key has many advantages such
as performance, requirement change tolerance, agility, and compatibility with object relational mappers.

#### Referential integrity constraints

Relations are associated with each other via common attributes. Referential integrity constraints govern the association between two relations, and ensure data consistency between tuples. If a tuple in one relation references a tuple in another relation, then the referenced tuple must exist.

The lack of referential integrity constraints can lead to many problems such as:

- Invalid data in the common attributes
- Invalid information during joining of data from different relations

Referential integrity constraints are achieved via foreign keys. A foreign key is an attribute or a set of attributes that can identify a tuple in the referenced relation.

Unlike a primary key, a foreign key can have a null value. It can also reference a unique attribute in the referenced relation. Allowing a foreign key to have a null value enables us to model different cardinality constraints. Cardinality constraints define the participation between two different relations.

- one-to-many relation
- self-referencing or recursive foreign key

To ensure data integrity, foreign keys can be used to define several behaviors when a tuple in the referenced relation is updated or deleted. The following behaviors are called **referential actions**:

- **Cascade**: When a tuple is deleted or updated in the referenced relation, the tuples in the referencing relation are also updated or deleted.
- **Restrict**: The tuple cannot be deleted or the referenced attribute cannot be updated if it is referenced by another relation.
- **No action**: Similar to restrict, but it is deferred to the end of the transaction.
- **Set default**: When a tuple in the referenced relation is deleted or the referenced attribute is updated, then the foreign key value is assigned the default value.
- **Set null**: The foreign key attribute value is set to null when the referenced tuple is deleted.

#### Semantic constraints

Semantic integrity constraints or business logic constraints describe the database application constraints in general. Those constraints are either enforced by the business logic tier of the application program or by SQL procedural languages. Trigger and rule systems can also be used for this purpose.

The advantages of using the SQL programming language are:

- **Performance**: RDBMSs often have complex analyzers to generate efficient execution plans. Also, in some cases such as data mining, the amount of data that needs to be manipulated is very large. Manipulating the data using procedural SQL language eliminates the network data transfer. Finally, some procedural SQL languages utilize clever caching algorithms.
- **Last minute change**: For the SQL procedural languages, one could deploy bug fixes without service disruption.

## Relational algebra

Relational algebra is the formal language of the relational model. It defines a set of closed operations over relations, that is, the result of each operation is a new relation. Relational algebra inherits many operators from set algebra. Relational algebra operations could be categorized into two groups:

- The first one is a group of operations which are inherited from set theory such as UNION, INTERSECTION, SET DIFFERENCE, and CARTESIAN PRODUCT, also known as CROSS PRODUCT.
- The second is a group of operations which are specific to the relational model such as SELECT and PROJECT.

Relational algebra operations could also be classified as binary and unary operations. Primitive relational algebra operators have ultimate power of reconstructing complex queries.

In addition to the primitive operators, there are aggregation functions such as sum, count, min, max, and average aggregates. Primitive operators can be used to define other relation operators such as left-join, right-join, equi-join, and intersection. Relational algebra is very important due to its expressive power in optimizing and rewriting queries.

SELECT is used to restrict tuples from the relation. If no predicate is given then the whole set of tuples is returned.

The selection predicates are certainly determined by the data types. For numeric data ≠, =<, >, ≥, ≤types, the comparison operator might be ( ). The predicate expression can contain complex expressions and functions.

The \* means all the relation attributes; note that in the production environment, it is not recommended to use \*. Instead, one should list all the relation attributes explicitly.

The SQL equivalent for the PROJECT operator in SQL is SELECT DISTINCT.

## The RENAME operation

The Rename operation is used to alter the attribute name of the resultant relation, or to give a specific name to the resultant relation. The Rename operation is used to:

- Remove confusion if two or more relations have attributes with the same name
- Provide user-friendly names for attributes, especially when interfacing with reporting engines
- Provide a convenient way to change the relation definition, and still be backward compatible

The AS keyword in SQL is the equivalent of the RENAME operator in relational algebra.

## The Set theory operations

The set theory operations are union, intersection, and minus (difference). Intersection is not a primitive relational algebra operator, because it is can be written using the union and difference operators.

## The CROSS JOIN (Cartesian product) operation

The CROSS JOIN operation is used to combine tuples from two relations into a single relation. The number of attributes in a single relation equals the sum of the number of attributes of the two relations. The number of tuples in the single relation equals the product of the number of tuples in the two relations.

Each execution plan has a cost in terms of CPU and hard disk operations. The RDBMS picks the one with the lowest cost. In the preceding execution plans, the RENAME operator was ignored for simplicity.

## Data modeling

Data models describe real-world entitiesand the relation between these entities.

Data models provide an abstraction for the relations in the database. Data models aid the developers in modeling business requirements, and translating business requirements to relations in the relational database. They are also used for the exchange of information between the developers and business owners.

## Data model perspectives

Data model perspectives are defined by ANSI as follows:

- **Conceptual data model**: Describes the domain semantics, and is used to communicate the main business rules, actors, and concepts. It describes the business requirements at a high level and is often called a high-level data model. The conceptual model is the chain between developers and business departments in the application development life cycle.
- **Logical data model**: Describes the semantics for a certain technology,for example, the UML class diagram for object-oriented languages.
- **Physical data model**: Describes how data is actually stored and manipulated at the hardware level such as storage area network, table space, CPUs, and so on.

According to ANSI, this abstraction allows changing one part of the three perspectives without amending the other parts. One could change both the logical and the physical data models without changing the conceptual model. To explain, sorting data using bubble or quick sort is not of interest for the conceptual data model. Also, changing the structure of the relations could be transparent to the conceptual model. One could split one relation into many relations after applying normalization rules, or by using enum data types in order to model the lookup tables.

## The entity-relation model

The entity-relation (ER) model falls in the conceptual data model category. It captures and represents the data model for both business users and developers. The ER model can be transformed into the relational model by following certain techniques.

Conceptual modeling is a part of the Software development life cycle (SDLC). It is normally done after the functional and data requirements-gathering stage. At this point, the developer is able to make the first draft of the ER diagram as well as describe functional requirements using data flow diagrams, sequence diagrams, user case scenarios, user stories, and many other techniques.

During the design phase, the database developer should give great attention to the design, run a benchmark stack to ensure performance, and validate user requirements. Developers modeling simple systems could start coding directly. However, care should be taken when making the design, since data modeling involves not only algorithms in modeling the application but also data. The change in design might lead to a lot of complexities in the future such as data migration from one data structure to another.

While designing a database schema, avoiding design pitfalls is not enough. There are alternative designs, where one could be chosen. The following pitfalls should be avoided:

- Data redundancy: Bad database designs elicit redundant data. Redundant data can cause several other problems including data inconsistency and performance degradation. When updating a tuple which contains redundant data, the changes on the redundant data should be reflected in all the tuples that contain this data.
- Null saturation: By nature, some applications have sparse data, such as medical applications. Imagine a relation called diagnostics which has hundreds of attributes for symptoms like fever, headache, sneezing, and so on. Most of them are not valid for certain diagnostics, but they are valid in general. This could be modeled by utilizing complex data types like JSON, or by using vertical modeling like entity-attribute-value (EAV).
- Tight coupling: In some cases, tight coupling leads to complex and difficult-to-change data structures. Since business requirements change with time, some requirements might become obsolete. Modeling generalization and specialization (for example a part-time student is a student) in a tightly coupled way may cause problems.

Entities should have a name and a set of attributes. They are classified into the following:

- Weak entity: Does not have key attributes of its own
- Strong entity or regular entity: Has a key attribute.

### Enhanced Entity relation diagrams (EER)

## UML class diagrams

**Unified modeling language (UML)** is a standard developed by **Object Management Group (OMG)**.

UML diagrams are widely used in modeling software solutions, and
there are several types of UML diagrams for different modeling purposes including:

- class,
- use case,
- activity,
- implementation diagrams.

A class diagram can represent several types of associations, that is, the relationship between classes. They can depict attributes as well as methods. An ER diagram can be easily translated into a UML class diagram. UML class diagrams also have the following advantages:

- **Code reverse engineering**: The database schema can be easily reversed to generate a UML class diagram.
- **Modeling extended relational database objects**: Modern relational databases have several object types such as sequences, views, indexes, functions, and stored procedures. UML class diagrams have the capability to represent these objects types.

# Sources

Salahaldin Juba, Achim Vannahme, Andrey Volkov, _Learning PostgreSQL_, ISBN 978-1-78398-918-8, 2015 Packt Publishing

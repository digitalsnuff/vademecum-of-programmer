# LATEX

## Installation

Linux:

```sh
# texlive, pdflatex
sudo apt install texlive
```

## Create document

- file extension: `*.tex`

## Control statements

Statements beginning with a backslash `\`

Common structure: `\commandname{option}`

`\documentclass{article}` - The command is obviously named `documentclass` and it does exactly that, it sets the document class (to article). LaTeX uses document classes, to influence the overall layout of your document. For instance, there's one class to layout articles, one class to layout books (called book) and many more, which we probably don't need.

`\begin` ... `\end` - This second example differs slightly from the first one, since this command involves a \begin and \end statement. In fact this is not a command, but defines an environment. An environment is simply an area of your document where certain typesetting rules apply. It is possible (and usually necessary) to have multiple environments in a document, but it is imperative the document environment is the topmost environment.

LaTeX already comes with a few predefined environments and even more come in so called packages.

## Adding a title page

`\title{My first document}`

Obviously the statements \title, \date and \author are not within the the document environment, so they will not directly show up in our document. The area before our main document is called preamble. In this specific example we use it to set up the values for the \maketitle command for later use in our document. This command will automagically create a titlepage for us. The \newpage command speaks for itself.

## Page number

What if we decide, that actually, we don't want to have that page number showing up there. We can remove it, by telling LaTeX to hide the page number for our first page. This can be done by adding the `\pagenumbering{gobble}` command and then changing it back to `\pagenumbering{arabic}` on the next page numbers.

## Paragraphs and sections

logic units
LaTeX offers us commands to generate section headings and number them automatically.

```tex
\section{}
\subsection{}
\subsubsection{}

\paragraph{}
\subparagraph{}
```

The `section` commands are numbered and will appear in the table of contents of your document.

`Paragraphs` aren't numbered and won't show in the table of contents. Here an example output using sections.

## Packages

```tex
\documentclass{article}

\usepackage{PACKAGENAME}

\begin{document}
```

In case of Ubuntu installing `texlive-full` from the package manager would provide all packages available.

most useful:

- To typeset math, LaTeX offers (among others) an environment called `equation`. Everything inside this environment will be printed in math mode, a special typesetting environment for math.
- `amsmath` - remove the equation number

## Math

There are two major modes of typesetting math:

- embedding the math directly into your text by encapsulating your formula in dollar signs
- using a predefined math environment.

```tex
...
This formula $f(x) = x^2$ is an example.
...
```

`align`:

```tex
\begin{align*}
  1 + 2 &= 3\\
  1 &= 3 - 2
\end{align*}
```

The align environment will align the equations at the ampersand &. Single equations have to be seperated by a linebreak \\. There is no alignment when using the simple equation environment. Furthermore it is not even possible to enter two equations in that environment, it will result in a compilation error.

`fractions`

LaTeX is capable of displaying any mathematical notation. It's possible to typeset integrals, fractions and more. Every command has a specific syntax to use.

```tex
\frac{1}{\sqrt{x}}
```

The `Lyx` program.

### Matrices

There is a special matrix environment for this purpose, please keep in mind that the matrices only work within math environments.

```tex
\begin{matrix}
1 & 0\\
0 & 1
\end{matrix}
```

### Brackets in math mode - Scaling

To surround the matrix by brackets, it's necessary to use special statements, because the plain [ ] symbols do not scale as the matrix grows.

```tex
[
\begin{matrix}
1 & 0\\
0 & 1
\end{matrix}
]
```

To scale them up, we must use the following code:

```tex
\left[
\begin{matrix}
1 & 0\\
0 & 1
\end{matrix}
\right]
```

This does also work for parentheses and braces and is not limited to matrices. It can be used to scale for fractions and other expressions

```sh
\left(\frac{1}{\sqrt{x}}\right)
```

## Figures and pictures

### Captioned images / figures

all pictures will be indexed automatically and tagged with successive numbers when using the `figure` environment and the `graphicx` package.

You can use the \ref command to refer to the figure (marked by label) in your text and it will then be replaced by the correct number.

### Image positioning / setting the float

Setting the float by adding [h!] behind the figure environment \begin tag will force the figure to be shown at the location in the document. Possible values are:

h (here) - same location
t (top) - top of page
b (bottom) - bottom of page
p (page) - on an extra page
! (override) - will force the specified location
However, I have only used the [h!] option so far. The float package (\usepackage{float}) allows to set the option to [H], which is even stricter than [h!].

### Multiple images / subfigures in LaTeX

The `subfigure` environment allows you to place multiple images at a certain location next to each other and the usage is pretty straightforward.

## Table of contents

# Links

[LaTeX Tutorial](https://www.latex-tutorial.com/tutorials)

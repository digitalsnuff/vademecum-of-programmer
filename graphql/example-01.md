# GraphQL and Apollo

> This is a revolutionary new way to think about APIs. Query language that is much less rigid than standard REST

- ask for only what you want
- front-end and back-end can collaborate more smoothly
- writing queries are very easy and similar to JSON
- Apollo is a client to make request to a GraphQL server
- used with the [Gatsby](https://www.gatsbyjs.org/) static site generator

# Links

[GraphQL With React & Apollo [1] - Express GraphQL Server](https://www.youtube.com/watch?v=SEMTj8w04Z8) from Traversy Media

# GraphQL

> `GraphQL` is a **query language** for your API, and a server-side runtime for executing queries by using a type system you define for your data. GraphQL isn't tied to any specific database or storage engine and is instead backed by your existing code and data.

> A GraphQL service is created by defining types and fields on those types, then providing functions for each field on each type.

## Queries and Mutations

---

## GraphQL and Apollo

> This is a revolutionary new way to think about APIs. Query language that is much less rigid than standard REST

- ask for only what you want
- front-end and back-end can collaborate more smoothly
- writing queries are very easy and similar to JSON
- Apollo is a client to make request to a GraphQL server
- used with the [Gatsby](https://www.gatsbyjs.org/) static site generator

# Links

[Introduction to GraphQL](https://graphql.org/learn/)

[GraphQL With React & Apollo [1] - Express GraphQL Server](https://www.youtube.com/watch?v=SEMTj8w04Z8) from Traversy Media

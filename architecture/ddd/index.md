# DDD

DDD pasuje do zbioru stylów architektonicznych i technologii utrwalania aplikacji skali korporacyjnej - uwzględniając SOA i REST, NoSQL oraz siatki danych.

Książka Erica Evansa.

Implementacja encji, obiektów wartości, agregatów, usług, zdarzeń, fabryk i repozytoriów.

Wzorzec `CQRS` - Command-Query Responsibility Segregation (podział odpowiedzialności polecenie-kwerenda).

Wzorzec `Event Sourcing` - Magazynowanie Zdarzeń.

Cykl życia projektu.

Wzorce strategiczne: konteksty ograniczone (bounded context) i mapy kontekstu.

Bloki budulcowe: encje, wartości i usługi.

Podejście góra-dół.

Scalanie wzorców strategicznych z bardziej niskopoziomowymi ograniczeniami taktycznymi.

Bloki modelu:

- Zdarzenia Dziedziny (Domain Events)
- Encje
- Obiekty Wartości (Value Objects)

Pojęcie `Wielkiej kuli błota (Big Ball of Mud)` umieszczone na `Mapie Kontekstu`.

Archiektura sześciokątna (porty i adaptery)
Architektura warstwowa
Architektura zorientowana na usługi (REST, CQRS)
Architektura Sterowana Zdarzeniami (potoki i filtry, procesy długotrwałe lub sagi, magazynowanie zdarzeń)
Architektura Data Fabric (bazowanie na siatkach)
Architektura Sterowana Zdarzeniami - Event Driven Architecture
Architektura SOA (Service Oriented Architecture)

Wzorce:

- taktyczne: Entities

Narzędzia:

- taktyczne: Agregaty

Mapowanie jednych obiektów na inne - Mapowanie obiektów na tabele baz danych. Mapujemy obiekty na interfejs użytkownika a następnie wykonujemy mapowanie w drugą stronę. Mapujemy obiekty do różnych reprezentacji występujących w aplikacji (oraz w drugą stronę), włącznie z reprezentacjami wymaganymi przez inne systemy i aplikacje.

Czasami DDD jest postrzegane jako techniczny zestaw narzędzi (DDD-Lite).

Posługiwanie się projektem taktycznym obszarów - mapowanie obszarów na implementację.

Druga połowa DDD - strategiczne wzorce projektowe (wychodzenie poza modelowanie taktyczne).

`Język Wszechobecny` - Ubiquitous Language - wzorzez projektowy wykorzystywany do uchwycenia pojęć i terminów określonej dziedziny biznesowej w samym modelu oprogramowania.

Język Wszechobecny stosuje się w ramach pojedynczego Kontekstu Ograniczonego.

Model oprogramowania obejmuje rzeczowniki,przymiotniki, czasowniki i bogatsze wyrażenia formalne wypowiadane przez zespół developerski - grupę, która ma w swoim składzie jednego lub kilku ekspertów dziedziny biznesowej. Język odzwierciedla model myślenia ekpertów dziedziny biznesowej.

Dziedzina, Poddziedzina, Dziedzina Główna (Core Domain)

Mechanizmy modularyzacji:

- OSGi
- Jigsaw

Znaczące przesunięcie zainteresowania z bz danych w kierunku magazynów danych opartych na dokumentach oraz magazynów klucz-wartość (ukierunkowane na agregaty).

Magazyny typu Data Fabric i Grid.

DDD to rozbudowany język wzorców.

`Język wzorców` to zbiór wzorców oprogramowania, które wzajemnie się przeplatają, ponieważ są od siebie wzajemnie uzaleznione.
Każdy wzorzec odwołuje się do jednego bądź większej liczby innych wzorców, od którego zależy lub które zależą od niego.

Modele oprogramowania:

- taktyczne
- strategiczne

Kontekst Ograniczony wyznacza pojęciowe granice, w których jest przydatny model dziedzony. Dostarcza kontekstu dla Języka Wszechobecnego, którym posługuje się zespół i który jest wyrażony w kokładnie zaprojektowanych modelu oprogramowania.

Modele dziedziny zaprojektowane w sposób strategiczny i taktyczny powinny być architektonicznie neutralne.

Modelowanie taktyczne wewnątrz Kontekstu Organicznego realizujemy za pośrednictwem wzorców, które są blokami budulcowymi metodologii DDD. Jednym z najważniejszych wzorców projektowania taktycznego jest Agregat.

W najprostszej formie Moduł można porównać do pakietu w Javie lub przestrzeni nazw w C#. Moduły powinny zawierać ograniczony zbiór spójnych obiektów dziedziny.

`Model oprogramowania` - sposób, w jaki oprogramowanie wyraża rozwiązanie poszukiwanego problemu biznesowego.

Stosowanie zwinnych technik programowania.

Poznanie metod, które pomagają zyskać głęboki wgląd w Twoją dziedzinę biznesową. Jednocześne dają perspektywę stworzenia modeli oprogramowania, które są testowalne, elastyczne, uważnie zorganizowane i starannie wykonane. Są po prostu wysokiej jakości.

DDD daje nam narzędzia zarówno do modelowania stategicznego, jaki taktycznego. Narzędzia te są konieczne do projektowania wysokiej jakości oprogramowania, które spełnia podstawowe cele biznesowe.

DDD przede wszystkim nie dotyczy technologii. Podstawowe zasady to:

- dyskusja
- słuchanie
- zrozumienie
- odkrywanie
- wartość biznesowa

Wszystko po to, aby scentralizowac wiedzę.

Jeżeli masz zdolność rozumienia biznesu, w którym działa Twoje przedsiębiorstwo, to zgodnie z planem minimum możesz uczestniczyć w procesie odkrywania modelu oprogramowania w celu stworzenia Języka Wszechobecnego.

Umiejętność rozumienia pojęcia swojego biznesu podczas rozwijania oprogramowania.

Zaangażowanie ekspertów dziedziny przynosi duże korzyści. Ekspertami dziedziny są ludzie, którzy bardzo dobrze znają obszar biznesu, w którym pracujesz (projektanci produktu, dział sprzedaży). Nie należy zwracać uwagi na nazwę stanowiska.

```
Model Dziedziny

To model oprogramowania bardzo określonej dziedziny biznesowej, wktórej pracujesz. Często jest on zaimplementowany jako model obiektowy, w którym obiekty zawierają zarówno dane, jak i zachowania w harmonii z dokładnym znaczeniem biznesowym.
Stworszenie unikatowego, uważnie zaprojekotwanego Modelu Dziedziny, stanowiącego sedno podstawowej, strategicznej aplikacji albo podsystemu, ma zasadnicze znaczenie dla stosowania metodologii DDD. Dzięki użyciu DDD Modele Dziedziny będą mniej rozbudowane i bardzo skoncentrowane. Stosując DDD, nigdy nie próbujemy zamodelować całego przedsiębiorstwa biznesowego w pojedynczym, olbrzymim Modelu Dziedziny.

```

Sprawdzone techniki wytwarzania oprogramowania.

DDD jest podejściem do rozwijania oprogramowania.

Inwestujemy w nietrywialne, bardziej złożone komponenty. Najbardziej wartościowe i najważniejsze rzeczy, które mają największe szanse na osiągnięcie zysków (Dziedzina Główna).

Poddziedziny pomocnicze

Zamiast ustalania tego, co jest złożone, łatwiejsze może być określenie tego, co jest nietrywialne.

Karty punktów DDD aby określić, czy projekt kwalifikuje się do zainwestowania w DDD.

`Anemic Domain Model` - Anemiczny Model Dziedziny

Dwa najważniejsze filary DDD to

- Język Wszechobecny
- Kontekst Ograniczony

Modelowanie dziedziny poprzez oprogramowanie - zastanawianie się co robia poszczególne obiekty modelu. Modelowanie sprowadza się do `projektowania zachowania obiektów`.

# Source

Head First Design Patterns [Freeman]  
Design Patterns [Gamma et al.]

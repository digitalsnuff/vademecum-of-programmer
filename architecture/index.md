# Architektura

Architektura IT
- techniczna
- integracji
- infrastruktury
- danych
- biznesowa
- aplikacji
- wdrożenia
- bezpieczeństwa

1. Problem biznesowy lub potrzeba biznesowa

Dobra praktyka: słownik pojęć

2. Wymagania funkcjonalne oraz niefunkcjonalne

Wymagania funkcjonalne - zachowanie (skomponowanie posiłku, wyszukiwanie posiłku)
Wymagania niefunkcjonalne - parametr jakościowy (dostępność, czas odpowiedzi systemu, dostępność 95% czasu, dane  przechowywane przez 5 lat)

Jeśli chodzi o UML można wykorzystać diagram wymagań

Dobra praktyka jest określenie priorytetów wymagań.
Analiza szczegółowa
Procesy biznesowe - 
  - Definicja
    - lista aktywności inicjowana zdarzeniem, która na podstawie otrzymanych na wejściu informacji z wykorzystaniem dostępnych zasobów przekształca w wartość dla klienta na wyjściu. 
    Możeny zamodelować za pomocą diagramu aktywności.
Przypadki użycia
  - Definicja
   - Opisują interakcję pomiedzy aktorem (aktorami) oraz systemem

Wolumetria

3. Wizja architektoniczna

Zawiera zwięzły opis infrastruktury docelowej rozwiązania wraz z wartościami dla organizacji, które wynikna z wprowadzenia tej zmiany. 
To wysokopoziomowy opis komponentów aplikacji, ich relacji oraz zachowania. 
- Architektura referencyjna - dostarczane przez nawiekszych dostawców IT 
- Zasady architektoniczne - wytyczne, którymi nalezy sie kierowac podczas projektowania
- Wzorce architektoniczne - w dużych organizacjach popularna jest cały czas architektura SOA (Service Oriented Architecture), ESB (Enterprise Service Bus), Monolit, Microservices, Event Oriented

Należy rozważyć różne opcje/warianty dkla kluczowych kryteriów.
- Architektura
- Kompetencje
- Czas dostarczenia
- Utrzymanie
- Koszty

Przedstawienie rekomendacji. Akceptacja bądź odrzucenie.

Architektura docelowa.

Fazowanie projektu.

mapa rozwoju produktu
Faza 1 - MVP
Faza 2 - docelowa rozwiązania

4. Architektura biznesowa (dla MVP)
Uszczegółowiony sposób realizacji procesów biznesowych z perspektywy rozwiązania.
Uwzględnia równiez strukturę organizacyjną, relację między aktorami, strategie oraz politykę firmy. Wykorzystujemy tu diagramy sekwencji.


5. Architektura danych
Stuktua encji i powiązania miedzy nimi. Uszczegółowienie zakresu i formatu danych z architektury biznesowej. Model danych

6. Architektura aplikacji
Określana również jako architektura statyczna.
Przedstawia komponenty aplikacji oraz ich powiązania i integracje.
Używamy diagramoów komponentów notacji UML. 

7. Architektura integracji
Zidentyfikowanie oraz przygotowanie specyfikacji interfejsów dla rozwiązania. 
- typy intergracji (online, offline)
- protokoły integracji (https, jdbc)
- wzorce integracyjne (request, response)

Przedstawiamy na diagramach komponentów.
Diagramy przypadków użycia.
Diagram sekwencji ( czy jest to integracja synchronicza czy asynchroniczna)
Kaniniczny  model danych  (Diagram klas)

8. Architektura techniczna

9. Architektura infrastruktury

10. Architektura bezpieczeństwa

- Zakres odpowiedzialności
- Mechanizm  uwierzyteniania użytkowników biznesowych oraz użytkowników administracyjnych
- szyfrowanie danych
- monitorowanie aplikacji
- kopia zapasowa

11. Architektura wdrożenia

- region
- ci/cd
- środowiska
  - prod
  - pre-prod
  - UAT
  - SIT
  - Test
  - Dev

12. Weryfikacja architektury (faza 2)

Przegląd powyższych punktw.

- Role IAM
- polityki  dostępu
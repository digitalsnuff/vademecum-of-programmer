# Monolit

To system, który ma jedną jednostę deploymentową (wdrożeniową).

## Event Storming

## Modular monolit

Dodajemy ściśle określone granice pomiędzy różnymi modułami biznesowymi.

- rozbicie całej aplikacji na moduły/komponenty
- odpowiednia struktura katalogów
- izolacja zależności
  - zdefiniowanie publicznego  interfejsu modułu
  - okreslenie właścicieli danych
  - każdy moduł operuje na własnych danych

Moduł nie powinien posiadać bezpośredniej wiedzy o żadnym innym module, nie ma powiązania z żadną klasa pochodzącą z innego modułu ani nawet z żadnym interfejsem.

Należy wyjść poza Dependency Inversion.

- events - reagowanie na zmiany w innych  modułach
- shared kernel - wszystkie moduły wiedzą o SK, ale nic nie wiedzą o swoich sąsiadach. Nie ma tu logiki biznesowej. powinien być jak najmniejszy. Zmiana nie może pociągać za soba zmian w innych modułach. 
- eventual consistency

## Narzędzia

Taktyka dla modularnego monolitu:

- Hexagonal
- CQRS
- Onion Architecture
- DDD
- CRUD

### Hexagonal

Warstwy:
- user interface - GUI, konsola
- application core (CORE) - logika biznesowa, serce aplikacji. Używana przez warstwę UI
- Infrastructure (I) - zapewnia połączenie z zewnętrznymi mechanizmami np. baza danych, zewnętrzne API

Przepływ żądania:

UI -> Application -> Infrastructure -> Application -> UI

Porty i Adaptery

Porty - dostarczają specyfikację zawierającą informacje
- w jaki sposób zewnętrzna warstwa może używać naszej aplikacji (w jaki sposób ui może użyć logiki biznesowej?)
- w jaki sposób aplikacja może używać zewnętrzną warstwę

Porty definiuje się za pomocą interfejsów.
Porty fizycznie są umiejscowione (konfiguracja portów) wewnątrz logiki biznesowej (CORE)
Generalnie realizuje się to za pomocą Interfejsów.

Adaptery znajdują się poza logiką biznesową (UI, Infrastructure).

Umiejscowienie:
- porty znajdują się wewnątrz logiki biznesowej
- adaptery znajdują się poza logiką biznesową

Adaptery - muszą być dopasowane do logiki biznesowej i odbywa się to za pomocą Portów, do których się dopasowują.
Zapewniaja komunikację warstw UI oraz infrastruktury z warstwą aplikacji
- UI Adapter - nakazuje warstwie aplikacji co ma zrobić (przeprowadzić proces biznesowy)
- Infrastructure adapter - aplikacja mówi infrastrukturze co ma robić

Primary or Driving Adatters
- owijaja sie wokół portu (wstrzykiwany)
- przyjmuja przychodzące dane, transformują i wywołuja metodę udostępniana przez port
- REST/SOAP Controllers, Consumers, Console Commands

Secondary or Driven   Adapters
- implementuja port
- adapter jest wstrzykiwany do   wnętrza naszej aplikacji
- mechanizmy persystencji danych, zewnętrzne API, wyszukiwarki, utrwalanie w kolejkach, itp.

Dobry moduł jest dobryum kandydatem na mikroserwis.

## Rodzaje monolitu:

- monolit
- modularny monolit
- modulit (modulith)
- rozproszony monolit (distributed monolith) - nie do końca monolit. Jest to architektura mikroserwisowa niepoprawnie zrealizowana.
- majestyczny monolit (majestic monolith)
- big ball of mud - słaba w rozwijaniu, spagetti code

Modularność - coś złożone z wielu odseparowanych części, które razem tworzą pewną całość.

Żeby coś było modularne, musi przejść przez proces modularyzacji. Tworzenie części i ich wiązanie.
Co może być modułem? Moduł, Component, Package, Service, Plugin.
Moduł jest logiczną jednostką.
Modularyzacja, modularne programowanie polega na separacji funkcjonalności na moduły.
Moduły powinny być niezależne, mają zawierać wszystko aby dana funkcjonalność dostarczyć, powinny mieć dobrze określone interfejsy, dzieki którymi sie komunikują.
- niezalezność 
  - jak nasz moduł jest niezalezny i ile ma zależności? 
  - siła niezalezności
  - jak często te zależności się zmieniają?
- moduły powinny mieć wszystko by dostarczać funkcjonalność
- interface (contract) - jeżeli moduł A sięga głęboko do modułu B, to kontakt modułu A nie jest dobrze specyfikowany a moduł B nie jest dobrze zenkapsulowany. 
  W tym wypadku nalezy utrzymac kontrakt. Mozna zaimplementować fasadę w module B dla enkapsulacji i utrzymania kontraktu.
  Jeżeli zmieniamy coś w module B i jest utrzymany kontrakt, nie potrzeba zmian w module A.
  W event-driven, kiedy to jeden moduł robi publish a inne subscribe po tym zdarzeniu, to też jest kontrakt. W tym wypadku kontrakt kest zachowywany za pomocą adapterów w subskrybentach.

Innymi słowy modularność jest zachowana jeśli mody są:
- independence
- business-driven
- interface, encapsulation

Drivery architektoniczne. Co wybrać?
Kategoryzacja:
- wymahania funkcjonalne - jaki problem mamy rozwiazać
- quality attribute - charakterystyki architektury - zestaw atrybutów, jaki ta architektura spełnia
- technical constraint
- business constraints - budżet, deadline

Atrybuty:
Poziom złożoności - jeden proces
Produktywność - repozytoria, środowisko uruchomieniowe, mała złożoność wdrożeniowa
Deployability - zawsze wdrażamy wszystko, zaś w mikroserwisach tylko to, co potrzebujemy
Skalowalność - monolit też można skalować, jednakże jest ograniczenie tylko do jednego procesu, który obsługuje wszystkie funkcjonalności, bez względu na zajęte zasoby
Failure Impact - jak unikać przestojów? - monolit powoduje, że całość pada.
Heterogenious technology - różnorodność technologiczna. Jest ograniczenie, co do jeżyka ze wzgledu np. na platformę.

Source: Simon Brown

--- 

Design
- compile-time - private by default -> public module api. Wszystko ma być  prywatne a potem idziemy do góry
- automated architecture test - 
- code review - ADR - architecture decision record - dokumentacja decyzji

Integration:
- 1st choice - event-driven - powinny sie komunikowac w sposób asynchrniczny
- 2nd choice - direct call
- 3rd choice - shared data -  każdy moduł  powinien mieć osobną baze danych lub osobny schemat.

Komunikacja
- gateway <-> facade - jeśli synchronicznie
- event bus - komunikacja między modułami za pomoca szyny

Prawo Constantine - system jest stabilny, kiedy coupling jest niski a kohezja jest wysoka. 
Publiczne interfejsy, które wystawiaja te moduły - tu tylko powinna zachodzić komunikacja między modułami. Każdy moduł wystawia jakieś API. 
Nie ujawniamy tego, co jest w środku. 
Opieramy się na jednym kontrakcie, który wymieniamy z innym modułem.
Zachodzi tu pewna alanogia do mikroserwisów, jednakże, jest to zamknięte w jednej jednostce wdrożeniowej.
DTO.

Komunikacja między modułami.
Komunikacja synchroniczna i asynchroniczna.
Coś na zasadzie komunikacji e2e.
Synchroniczna:
- Moduły nawzajem pytają się o dane. Moduł prosi o dane pochodzące z innego modułu
- posiada dokładnie jednego odbiorcę
- przy migracji na mikroserwisy zostanie zastąpiona przez HTTP
Asynchroniczna:
- moduł informuje o czymś, co miało miejsce w systemie publikując zdarzenie integracyjne
- może mieć 0-N odbiorców
- przy migracji na mikroserwisy zostanie zastąpiona przez brokera wiadomości


Komunikacja oparta o współdzielone kontrakty
Moduły nie mają referencji do warstw (np. db), nie widzą siebie nazwzajem. Może jednak udostęniać zbiór kontraktów i interfejsy zaimplementowane po swojej stronie.
Komunikacja oparta o lokalne kopie kontraktów
Chcemy zachować 100% autonomii, jeden proces, zero kouplingu na poziomie kodu.
Każdy moduł ma kopie kontraktu. Jest to powielony kod z innym namespace.
Rejestr modułowy - wywołujemy rządanie asynchroniczne, żądamy odpowiedniego obiektu, zagladamy do rejestru modułów i wywołujemy funkcjonalność innego modułu.




# Sources

https://herbertograca.com/








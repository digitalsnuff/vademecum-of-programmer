# RESTfull

# What is REST?

- Representation State Transfer
- Architecture Style for designing networked applications
- Repies on a stateless, client-server protocol, almost always HTTP
- Treats server objects as resources that can be created or destroyed
- Can be used by virtually any programming language

> An architectural style called REST (Representational State Transfer) advocates that web applications should use HTTP as it was originally envisioned. Lookups should use GET requests. PUT, POST, and DELETE requests should be used for mutation, creation, and deletion respectively. - Stack Overflow

> RESTful Web services (RWS)

## HTTP Methods

- GET: retrieve data from a specified resource
- POST: Submit data to be processed to a specified resource
- PUT: Update a specified resource
- DELETE: delete a specified resource
- HEAD: same as get but does not return a body
- OPTIONS: Returns the supported HTTP methods
- PATH: Update partial resources

## Endpoints

> The URI/URL where api/service can be accessed by a client application

- GET https://mysite.com/api/users
- GET https://mysite.com/api/users/1 OR https://mysite.com/api/users/details/1
- POST https://mysite.com/api/users
- PUT https://mysite.com/api/users/1 OR https://mysite.com/api/users/update/1
- DELETE https://mysite.com/api/users/1 OR https://mysite.com/api/users/delete/1

## Authentication

> Some API's require authentication to use their service. This could be free or paid

`curl -H "Authorization: token OAUTH-TOKEN" https://api.github.com`
`curl https://api.github.com/?access_token=OAUTH-TOKEN`
`curl 'https://api.github.com/users/whatever?client_id=xxxx&client_secret=yyy'`

## OpenAPI Specification

The OpenAPI Specification (OAS) defines a standard, language-agnostic interface to RESTful APIs which allows both humans and computers to discover and understand the capabilities of the service without access to source code, documentation, or through network traffic inspection. When properly defined, a consumer can understand and interact with the remote service with a minimal amount of implementation logic.

An OpenAPI definition can then be used by documentation generation tools to display the API, code generation tools to generate servers and clients in various programming languages, testing tools, and many other use cases.

## Status Codes

## Tools

- POSTMAN
- cURL

## Examples

> [Link to https://developer.github.com/v3 - REST API v3](https://developer.github.com/v3/)

> [Link to https://developer.github.com/v4 - GraphQL API v4](https://developer.github.com/v4/)

---

# Source

[OpenAPI Specification](https://swagger.io/specification/)
[Status Codes - rfc7231](https://tools.ietf.org/html/rfc7231#section-6)
[Hypertext Transfer Protocol (HTTP) Status Code Registry](https://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml)

- [What Is A RESTful API? Explanation of REST & HTTP](https://www.youtube.com/watch?v=Q-BpqyOT3a8) - Youtube, Traversy Media
- [What exactly is RESTful programming?
  ](https://stackoverflow.com/questions/671118/what-exactly-is-restful-programming) - Stack Overflow
- [Representational state transfer](https://en.m.wikipedia.org/wiki/Representational_state_transfer) - Wikipedia

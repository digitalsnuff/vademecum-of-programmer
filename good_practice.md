# Good practice

Good code is short, fast, elegant, easy to read and understand, simple, easy to modify and extend, easy to scale and refactor, and easy to test. It takes time to be able to write code that has all these qualities at the same time (...) [<sup>1</sup>](#fn-1)

# Source

<span id="fn-1">1.</span> Fabrizio Romano, _Learning Python_, ISBN 978-1-78355-171-2, Packt Publishing 2015

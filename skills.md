# Basic Front-End Web Developer

- Build static websites (small business, informational)
- Build UI layouts (Take a design and create the html/css)
- add dynamic functionality (modals, slideshows, etc.)
- Deploy and maintain websites

# Full Fledged Front-End Web Developer

> You should be able to get front-end job or work for yourself and do pretty well.

- Build incredible front-end applications
- smooth and steady front-end workflow
- work well with teams and familiar with Git
- connect to backend APIs and work with data

# What to learn?

- HTML / CSS Frameworks
- GIT and tooling
- Frond End JS Framework
- Server Side Language & Database

# Server Side

- Nodejs
- Python
- PHP
- C#
- Go

## Thing to learn:

- fundamental syntax
- structure and workflow
- package manager
- http / routing

## Frameworks

- Nodejs - Express, Koa, [Adonis](https://adonisjs.com/)
- Python - Django, Flask
- PHP - Laravel, Symfony
- C# - ASP.NET

# Full Stack Badass (kozak)

- Setup full stack dev environments and workflows
- build back-end APIs and micro-services
- work with databases
- construct full-stack apps (Front-end framework and server)
- Deploy to the cloud (SSH, Git, Servers, etc.)

> You are a frigging rockstar!

# Mobile Developer

- React Native - build native appswith React
- NativeScript - Angular, TypeScript, JavaScript
- Ionic - Hybrid apps with HTML/CSS/JS
- Flutter - Mobile SDK for Android and iOS (Uses Dart)
- Xamarin - mobile apps with C#

# Desktop Apps With Electron

> Electron is used to build powerfull cross-platform desktop applications using JavaScript

- uses Chromium and Node.js
- Compatible with Windows, Mac and Linux
- Crash reporting, debugging and profiling

# PWA

> Progressive Web App are regular web apps but give the user a native app experience in terms of layout and functionality.

- Responsive to fit any form factor
- Service Workers for offline availability
- App-like interactions
- https
- reliable, fast and engaging

# Web Assembly (Wasm)

> Assembly-like binary format for code that can be executed by web browsers. Can be generated from higher level languages like C/C++ and Rust. [Link](https://webassembly.org/)

- Faster than JavScript
- Secure - Enforces same origin and security policies in the browser
- Open and debuggable

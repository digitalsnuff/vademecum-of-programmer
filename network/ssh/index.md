# SSH

## Create tunel on nackground

```sh
# add to ~/.ssh/config
sudo vim ~/.ssh/config
# following line
ControlPath ~/.ssh/control-%h-%p-%r
# on top of file on host definition
```

```sh
ssh -f -N -T -M -L 3306:localhost:3306 remotehost-proxy
mysql -e 'SHOW DATABASES;' -h 127.0.0.1
ssh -T -O "exit" remotehost-proxy
```

## Custom config of server

```sh
# sudo cat /etc/ssh/sshd_config
# custom
Port  2222
LogLevel DEBUG

ClientAliveInterval 300
ClientAliveCountMax 0
MaxStartups 5
MaxAuthTries 7
MaxSessions 3

X11Forwarding no

PermitRootLogin no
PermitEmptyPasswords no
PubkeyAuthentication yes
PasswordAuthentication no
StrictModes yes
AuthorizedKeysFile .ssh/authorized_keys

LoginGraceTime 5s

AllowUsers 
# DenyUsers
# AllowGroups
# DenyGroups

```

## Secure SSH using TCP wrappers

A TCP wrapper provides host-based access control to network services used to filter network access to the Internet. Edit your “/etc/hosts.allow” file to allow SSH only from 192.168.1.2 and 172.16.23.12.

```sh
sudo nano /etc/hosts.allow

# Add the following line:
sshd : 192.168.1.2 172.16.23.12
```

## Secure Debian

- Avoid Using FTP, Telnet, And Rlogin / Rsh Services on Linux

```sh
sudo apt-get --purge remove xinetd nis yp-tools tftpd atftpd tftpd-hpa telnetd rsh-server rsh-redone-server
```

[Link](https://www.cyberciti.biz/tips/linux-security.html)

# Links

[Opening and closing an SSH tunnel in a shell script the smart way](https://gist.github.com/scy/6781836)

[SSH ControlMaster and ControlPath](https://ldpreload.com/blog/ssh-control)

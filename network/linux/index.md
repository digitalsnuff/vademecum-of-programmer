# Linux Networking

> Network administration is one of the main tasks of Linux system administration.
>
> By knowing how to configure system network interfaces in a reliable and optimal manner, Linux
> administrators can deploy and configure several network services including file, web,
> mail, and servers while working in large enterprise environments.

> **Routers** are devices that are configured to span multiple networks and forward packets
> between them as needed. They also perform **`Network Address Translation (NAT)`** in order
> to allow your private network to share a single public IPv4 address.

## Setting up the physical network

> Before we start configuring the networking within Linux, we need to physically connect the systems. The simplest configuration involves connecting the two computers with a single cable, although connecting them to a switch may make more sense for additional expansion. Once physically connected, we need to confirm that they are working as expected.

> On each Linux system, use the ip command to check for a network link as shown:

```sh
ip link set dev eth0 up
ip link show eth0
# 2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast
# state UP mode DEFAULT group default qlen 1000
# link/ether 00:0c:29:6e:8f:ab brd ff:ff:ff:ff:ff:ff
```

> The first command brings up the network interface card (NIC). This activates the interface and allows it to start the process to check for a network link or electrical connection between the two systems.

> Next, the show command gives you a bunch of information about the link. You should see a state showing UP. If it shows DOWN, then you have a link issue of some sort. This could be a disconnected/bad cable, a bad switch, or you forgot to bring up the network interface.

## Configuring IPv4

Now that we've established a link between the machines, let's put some IP addresses on
the systems so that we can communicate between them. For now, let's look at manually
configuring IP addresses rather than auto-configuring them via DHCP.

We need to manually configure the IP addresses using the ip command. Let's start
with server 1:

```sh
with server 1:
ip addr add dev eth0 10.0.0.1/24
ip addr list eth0
# 2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast
# state UP group default qlen 1000
# link/ether 00:0c:29:6e:8f:ab brd ff:ff:ff:ff:ff:ff
# inet 10.0.0.1/24 brd 192.168.251.255 scope global eth0
# valid_lft forever preferred_lft forever
# inet6 fe80::20c:29ff:fe6e:8fab/64 scope link
# valid_lft forever preferred_lft forever

```

Now we need to perform the same action on server 2, but with 10.0.0.2/24 instead
of 10.0.0.1/24.

There are a few things in play here, so it probably makes sense to go through them one at a time.

First, let's start off by looking at the IP address that we're configuring. The 10.0.0.1 and 10.0.0.2 are a part of a series of netblocks set aside for private networks by RFC1918, IP Address Allocation for Private Internets. RFC1918 sets aside three large ranges, 10.0.0.0-10.255.255.255 (10.0.0.0/8), 172.16.0.0-172.31.255.255 (172.16.0.0/12), and 192.168.0.0-192.168.255.255 (192.168.0.0/16).

For our purpose, we're configuring 10.0.0.1/24, which is an IP range that includes 10.0.0.0-10.0.0.255. This includes 256 addresses, of which 254 are usable after setting aside 10.0.0.0 as the network address and 10.0.0.255 as the broadcast address. Both our systems get one IP in that range, which should allow them to communicate between them.

Next, we use the ip command to define an address on the eth0 device using one of the IP addresses in that range. You need to make sure that each machine in that range has a different IP address in order to prevent IP address conflicts, which would make communication between the two systems impossible and communication with different systems difficult.

Some people may be accustomed to seeing the `ifconfig` command rather than the `ip` command used here. While it will certainly do the job in most cases, `net-tool` (and its `ifconfig` command) **has been deprecated** by most distributions since the turn of the century, in favor of `iproute2` and its `ip` command.

Once the commands have been run on both servers, you should be able to ping them from each other. Log in to 10.0.0.1 and run the following:

```
ping –c 2 –n 10.0.0.2
```

If everything is configured properly, you will be able to see successful ping responses at this point.

## Configuring IPv4 permanently

In the previous section we configured the network interface, but this configuration is only valid while the system is up and running. A reboot will clear this configuration, unless you take steps to make sure that it is configured on each boot. This configuration will be specific to the distribution that you are running, although most distributions fall under either the Debian or Red Hat methods.

### Debian/Ubuntu

```sh
sudo nano /etc/network/interfaces

# auto eth0
# iface eth0 inet static
#     address 10.0.0.1
#     netmask 255.255.255.0
#
# bring up the network interface
ifup eth0
```

### Red Hat/CentOS

```sh
sudo nano /etc/sysconfig/network-scripts/ifcfg-eth0

# DEVICE=eth0
# BOOTPROTO=none
# ONBOOT=yes
# NETWORK=10.0.0.0
# NETMASK=255.255.255.0
# IPADDR=10.0.0.1
# USERCTL=no
# bring up the network interface
ifup eth0
```

Linux distributions are configured through init systems, such as Upstart, SystemD, or SysVInit. During the initialization process, the interfaces, or ifcfg-eth0 files, are used as a configuration for the networking setup scripts. These scripts then use the same ip commands, or possibly ifconfig commands to set up and bring up the network interface.

## Connecting two networks

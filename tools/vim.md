# VIM

## config

Konfiguracja w zasięgu użytkownika:

```sh
" vim ~/.vimrc
set exrc
set secure
set number
syntax on
colorscheme desert
set tabstop=4
set autoindent
set expandtab
filetype plugin indent on
"set softtabstop=4
"set cursorline " Highlight the Current Line
set encoding=utf-8
```

Plik `.vimrc` można umieścić w głównym folderze projektu.

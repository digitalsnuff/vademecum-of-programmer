# Build Container Images

## Dockerfile

> A Dockerfile is simply a plain text file that contains a set of user-defined commands, which when executed by the `docker image build` command, assemble a container image.

### Create `Dockerfile`

```Dockerfile
FROM nginx:latest
WORKDIR /usr/share/nginx/html
COPY . .
```

```Dockerfile
FROM alpine:latest
LABEL maintainer="Russ McKendrick <russ@mckendrick.io>"
LABEL description="This example Dockerfile installs NGINX."
RUN apk add --update nginx && \
rm -rf /var/cache/apk/* && \
mkdir -p /tmp/nginx/
COPY files/nginx.conf /etc/nginx/nginx.conf
COPY files/default.conf /etc/nginx/conf.d/default.conf
ADD files/html.tar.gz /usr/share/nginx/
EXPOSE 80/tcp
ENTRYPOINT ["nginx"]
CMD ["-g", "daemon off;"]
```

### Run build and create image from a `Dockerfile`

> Now, it may seem like a lot to digest, but out of all of these options, we only need to use `--tag` or its shorthand `-t` to name our image.
>
> You can use the other options to limit how much CPU and memory the build process will use. In some cases, you may not want the build command to take as much CPU or memory as it can have. The process may run a little slower, but if you are running it on your local machine or a production server and it's a long build process, you may want to set a limit. Typically, you don't use the `--file / -f` switch as you run the docker build command from the same folder that the Dockerfile is in. Keeping the Dockerfile in separate folders helps sort the files and keeps the naming convention of the files the same.
>
> It also worth mentioning that while you are able to pass additional environment variables as arguments at build time, they are used at build time and your container image does not inherit them; this is useful for passing information such as proxy settings, which may only be applicable to your initial build/test environment.

```
$ docker build -t [name:tag] [path]
$ docker image build --file <path_to_Dockerfile> --tag <REPOSITORY>:<TAG> .
```

> Now, `<REPOSITORY>` is typically the username you signed up for on Docker Hub and `<TAG>` is a unique container value you want to provide. Typically, this will be a version number or other descriptor.

> The most important thing to remember is the dot (or period) at the very end. This is to tell the `docker build` command to build in the current folder.

### Run container based on built image

```
$ docker container run -d -p 8080:80 --name [customname] [image_built_name]
```

## Tools to generate a bundle of an operating system

- **Debootstrap**: https://wiki.debian.org/Debootstrap/
- **Yumbootstrap**: https://github.com/dozzie/yumbootstrap/
- **Rinse**: http://collab-maint.alioth.debian.org/rinse/
- **Docker contrib scripts**: https://github.com/moby/moby/tree/master/contrib/

## Commands

### `FROM`

> The `FROM` command tells Docker which base you would like to use for your image.

### `LABEL`

> The LABEL command can be used to add extra information to the image. This information can be anything from a version number to a description. It's also recommended that you limit the number of labels you use. A good label structure will help others who have to use our image later on. However, using too many labels can cause the image to become inefficient as well--I would recommend using the label schema detailed at http://label-schema.org/.
>
> You can view the containers' labels with the docker inspect command:
>
> ```
> $ docker image inspect <IMAGE_ID>
> ```
>
> Alternatively, you can use the following to filter just the labels:
>
> ```
> $ docker image inspect -f {{.Config.Labels}} <IMAGE_ID>
> ```
>
> Generally though, it is better to define your labels when you create a container from your image rather than at build time, so it is best to keep labels down to just metadata about the image and nothing else.

### `RUN`

> The RUN command is where we interact with our image to install software and run scripts, commands, and other tasks.

### `COPY`

> copying files from the folder on the host they will in built image

### `ADD`

> automatically uploads, uncompresses, and puts the resulting folders and files at the path we tell it to.
>
> The `ADD` command can also be used to add content from remote sources. Archive files from a remote source are treated as files and are not uncompressed, so you will have to take this into account when using them.

### `EXPOSE`

> The EXPOSE command lets Docker know that when the image is executed, then the port and protocol defined will be exposed at runtime. This command does not map the port to the host machine, but instead opens the port to allow access to the service on the container network. For example, in our Dockerfile, we are telling Docker to open port 80 every time the image runs:

```Dockerfile
EXPOSE 80/tcp
```

### `ENTRYPOINT`

> can be used by itself, but remember that you would want to use ENTRYPOINT by itself only if you wanted to have your container be executable.

### `CMD`

## Other Dockerfile commands

### `USER`

> The `USER` instruction lets you specify the username to be used when a command is run. The `USER` instruction can be used on the `RUN` instruction, the `CMD` instruction, or the `ENTRYPOINT` instruction in the `Dockerfile`. Also, the user defined in the `USER` command has to exist or your image will fail to build. Using the `USER` instruction can also introduce permission issues, not only on the container itself but also if you mount volumes.

### `WORKDIR`

> The `WORKDIR` command sets the working directory for the same set of instructions that the `USER` instruction can use (`RUN`, `CMD`, and `ENTRYPOINT`). It will allow you to use the CMD and `ADD` instructions as well.

### `ONBUILD`

> The `ONBUILD` instruction lets you stash a set of commands that will be used when the image is used again as a base image for a container. For example, if you want to give an image to developers and they all have different code they want to test, you can use the `ONBUILD` instruction to lay the groundwork ahead of the fact of needing the actual code. Then, the developers will simply add their code to the directory you tell them and, when they run a new docker build command, it will add their code to the running image. The `ONBUILD` instruction can be used in conjunction with the `ADD` and `RUN` instructions. Look at this example:
>
> ```Dockerfile
> ONBUILD RUN apk update && apk upgrade && rm -rf /var/cache/apk/*
> ```
>
> This would run an update and package upgrade every time our image is used as a base for another image.

### `VOLUME`

### `ENV`

> The `ENV` command sets environment variables within the image both when it is built and when it is executed. These variables can be overridden when you launch your image.

## Environmental variables

> To use environmental variables in your `Dockerfile`, you can use the `ENV` instruction. The structure of the `ENV` instruction is as follows:
>
> ```Dockerfile
> ENV <key> <value>
> ENV username admin
> ```
>
> Alternatively, you can always use an equals sign between the two:
>
> ```Dockerfile
> ENV <key>=<value>
> ENV username=admin
> ```
>
> Now, the question is why do they have two and what are the differences?With the first example, you can only set one ENV per line. With the second ENV example, you can set multiple environmental variables on the same line:
>
> ```Dockerfile
> ENV username=admin database=db1 tableprefix=pr2\_
> ```
>
> You can view what environmental variables are set on an image using the Docker inspect command:
>
> ```Dockerfile
> $ docker image inspect <IMAGE_ID>
> ```

## Dockerfile - best practices

- You should try to get into the habit of using a `.dockerignore` file. We will cover the `.dockerignore` file in the next section; it will seem very familiar if you are used to using a `.gitignore` file. It will essentially ignore the items you have specified in the file during the build process.
- Remember to only have **one** `Dockerfile` per folder to help you organize your containers.
- Use version control for your `Dockerfile`; just like any other text-based document, version control will help you move forward, but only backward as necessary.
- Minimize the number of packages you need per image. One of the biggest goals you want to achieve while building your images is to keep them as small as possible. Not installing unnecessary packages will greatly help in achieving this goal.
- Execute only one application process per container. Every time you need a new application, it is best practice to use a new container to run that application in. While you can couple commands into a single container, it's best to separate them. Keep things simple; over complicating your `Dockerfile` will add bloat and also potentially cause you issues further down the line.
- Learn by example, Docker themselves have quite a detailed style guide for publishing the official images they host on `Docker Hub`, documented at https://github.com/docker-library/official-images/.

# Examples

## Example 1

```Dockerfile
FROM alpine:latest
LABEL maintainer="Russ McKendrick <russ@mckendrick.io>"
LABEL description="This example Dockerfile installs Apache & PHP."
ENV PHPVERSION 7
RUN apk add --update apache2 php${PHPVERSION}-apache2 php${PHPVERSION} && \
rm -rf /var/cache/apk/* && \
mkdir /run/apache2/ && \
rm -rf /var/www/localhost/htdocs/index.html && \
echo "<?php phpinfo(); ?>" > /var/www/localhost/htdocs/index.php && \
chmod 755 /var/www/localhost/htdocs/index.php
EXPOSE 80/tcp
ENTRYPOINT ["httpd"]
CMD ["-D", "FOREGROUND"]

```

---

> **Alpine Linux** is a small, independently developed, non-commercial Linux
> distribution designed for security, efficiency, and ease of use. For more
> information on Alpine Linux, visit the project's website at https://www.alpinelinux.org/.
>
> Alpine Linux, due to both its size and also how powerful it is, has become the default image
> base for the official container images supplied by Docker.

# Sources

- Russ McKendrick, Scott Gallagher, _Mastering Docker_, second edition, Packt Publishing, 2017

```

```

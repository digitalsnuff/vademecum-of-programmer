# Docker Swarm

> With `Docker Swarm`, you can create and manage Docker clusters. Swarm can be used to distribute containers across multiple hosts. Swarm also has the ability to scale containers.

Verify that Docker Swarm is available on your installation:

```sh
$ docker swarm --help
```

## Docker Swarm roles

### Swarm Manager

> The `Swarm manager` is the host that is the central management point for all Swarm hosts. The Swarm manager is where you issue all your commands to control those nodes. You can switch between the nodes, join nodes, remove nodes, and manipulate those hosts.
>
> Each cluster can run several Swarm managers. For production, it is recommended that you run a minimum of five Swarm managers: this would mean that our cluster can take a maximum of two Swarm manager nodes failures before you start to have any errors. Swarm managers use the `Raft Consensus Algorithm` to maintain a consistent state across all of the manager nodes.

### Swarm Worker

> The `Swarm workers`; which we have seen earlier referred to as Docker hosts, are those that run the Docker containers. Swarm workers are managed from the `Swarm manager`.

> The `Swarm Worker` (host) may have several running containers.
> Each worker is managed by `Swarm Manager`

## Using Docker Swarm

### Creating a cluster

> Let's start by creating a cluster, which starts with a Swarm manager. Since we are going to be creating a multi-node cluster on our local machine, we should use Docker Machine to launch a host by running this command:

```sh
$ docker-machine create \
-d virtualbox \
swarm-manager
```

The `swarm-manager` node is now up running using VirtualBox. we can confirm this by running:

```sh
$ docker-machine ls
```

> Now let's point Docker Machine at the new Swarm manager. From the preceding output when we created the Swarm manager, we can see it tells us how to point to the node:

```sh
$ docker-machine env swarm-manager
```

> This will show you the commands needed to configure your local Docker client to talk to our newly launched Docker host: following you can see the configuration I had returned when I ran the command:

```sh
export DOCKER_TLS_VERIFY="1"
export DOCKER_HOST="tcp://192.168.99.100:2376"
export DOCKER_CERT_PATH="/Users/russ/.docker/machine/machines/swarm-
manager"
export DOCKER_MACHINE_NAME="swarm-manager"
# Run this command to configure your shell:
# eval $(docker-machine env swarm-manager)
```

Upon running the previous command, we are told to run the following command to point to the Swarm manager:

```sh
$ eval $(docker-machine env swarm-manager)
```

> Now that we have the first host up-and-running, we should add the two worker nodes. To do this, simply run the following command to launch two more Docker hosts:

```sh
$ docker-machine create \
-d virtualbox \
swarm-worker01
$ docker-machine create \
-d virtualbox \
swarm-worker02
```

> Let's bootstrap our Swarm manager. To do this, we will pass the results of a few Docker Machine commands to our host. The command to run to create our manager is:

```sh
$ docker $(docker-machine config swarm-manager) swarm init \
--advertise-addr $(docker-machine ip swarm-manager):2377 \
--listen-addr $(docker-machine ip swarm-manager):2377
```

> As you can see from the output, once your manager is initialized, you are given a unique token. This token will be needed for the worker nodes to authenticate themselves and join our cluster.

### Joining workers

> To add our two workers to the cluster, run the following commands, making sure you replace the token with the one you received when initializing your own manager:

```sh
$ docker $(docker-machine config swarm-worker01) swarm join \
$(docker-machine ip swarm-manager):2377 \
--token SWMTKN-1-3fxwovyzh24120myqbzolpen303uzk562y26q4z1wgxto5avm3-4aiagep3e2a1nwyaf4yjp7ic5
```

> For the second worker, you need to run this:

```sh
docker $(docker-machine config swarm-worker02) swarm join \
$(docker-machine ip swarm-manager):2377 \
--token SWMTKN-1-3fxwovyzh24120myqbzolpen303uzk562y26q4z1wgxto5avm3-4aiagep3e2a1nwyaf4yjp7ic5
```

> Both times, you should get confirmation that your node has joined the cluster:

```
This node joined a swarm as a worker.
```

### Listing nodes

> You can check the Swarm by running the following command:

```sh
$ docker-machine ls
```

> Check that your local Docker client is still configured to connect to the `swarm-manager` node, and if it isn't, rerun the following command:

```sh
$ eval $(docker-machine env swarm-manager)
```

> Now that we are connecting to the `swarm-manager` node, you can run the following:

```sh
$ docker node ls
```

> This will connect to the `swarm-master` and query all of the nodes that form our cluster.

### Managing a cluster

> So there are two ways you can go about managing these Swarm hosts and the containers on each host that you are creating, but first, you need to know some information about them.
>
> As we have already seen, we can list the nodes within the cluster using our local Docker client, as it is already configured to connect to the Swarm manager host. We can simply type this:

```sh
$ docker info
```

> As you can see, there is information about the cluster in the `Swarm` section; however, we are only able to run the `docker info` command against the host our client is currently configured to communicate with; luckily, the `docker node` command is cluster aware, so we can use that to get information on each node within our cluster, like this, for example:

```sh
$ docker node inspect swarm-manager --pretty
```

> Run the same command, but this time targeting one of the worker nodes:

```sh
$ docker node inspect swarm-worker01 --pretty
```

> But as you can see, it is missing the information about the state of the manager functionality. This is because the worker nodes do not need to know about the status of the manager nodes; they just need to know they are allowed to receive instructions from the managers.
>
> So we can see the information about this host, such as the number of containers, the numbers of images on the host, and information about the CPU and memory as well as other interesting information about the host.

### Promoting a worker node

> Say you wanted to perform some maintenance on your single manager node, but you wanted to maintain the availability of your cluster. No problem; you can promote a worker node to a manager node.
>
> While we have our local three-node cluster up and running, let's promote `swarm-worker01`
> to be a new manager. To do this, run the following:

```sh
$ docker node promote swarm-worker01
```

> You should receive a message confirming that your node has been promoted immediately after executing the command:

```
Node swarm-worker01 promoted to a manager in the swarm.
```

List the nodes by running this:

```sh
$ docker node ls
```

Our `swarm-manager` node is still the primary manager node though.

### Demoting a manager node

> You may have already put two and two together, but to demote a manager node to a worker, you simply need to run this:

```sh
$ docker node demote swarm-manager
```

> Again, you will receive immediate feedback saying the following:

```
Manager swarm-manager demoted in the swarm.
```

Now we have demoted our node, and you can check the status of the nodes within the cluster by running this command:

```sh
$ docker node ls
```

As your local Docker client is still pointing toward, the newly demoted node, you will receive a message saying the following:

```
Error response from daemon: This node is not a swarm manager. Worker nodes can't be used to view or modify cluster state. Please run this command on a manager node or promote the current node to a manager.
```

> As we have already learned, it is easy to update our local client configuration to communicate with other nodes using Docker Machine. To point your local client to the new manager node, run the following:

```sh
$ eval $(docker-machine env swarm-worker01)
```

> Now that out client is talking to a manager node again, rerun this:

```sh
$ docker node ls
```

### Draining a node

> To temporarily remove a node from our cluster so that we can perform maintenance, we need to set the status of the node to Drain. Let's look at draining our former manager node. To do this, we need to run the following command:

```sh
$ docker node update --availability drain swarm-manager
```

> This will stop any new tasks, such as new containers launching or being executed against the node we are draining. Once new tasks have been blocked, all running tasks will be migrated from the node we are draining to nodes with an `ACTIVE` status.
> Listing the nodes now shows that `swarm-manager` is listed as Drain in the `AVAILABILITY` column.

> Now that our node is no longer accepting new tasks and all running tasks have been migrated to our two remaining nodes, we can safely perform our maintenance, such as rebooting the host. To reboot `swarm-manager`, run the following two commands, ensuring that you are connected to the Docker host (you should see the `boot2docker` banner, like the screenshot after the commands):

```sh
$ docker-machine ssh swarm-manager
$ sudo reboot
```

> Once the host has been rebooted, run this:

```sh
$ docker node ls
```

> It should show that the node has an `AVAILABILITY` of pause. To add the node back into the cluster, simply change the AVAILABILITY to active by running this:

```sh
$ docker node update --availability active swarm-manager
```

## Docker Swarm services

> The Service and Stack commands allow us to execute tasks that in turn launch, scale, and
> manage containers within our Swarm cluster.

> The service command is a way of launching containers that take advantage of the Swarm cluster. Let's look at launching a really basic single-container service on our Swarm cluster. To do this,run the following command:

```sh
$ docker service create --name cluster --constraint "node.role == worker" - p:80:80/tcp russmckendrick/cluster
```

> This will create a service called cluster that consists of a single container with port 80 mapped from the container to the host machine, and it will only be running on nodes that have the role of worker.
>
> Before we look at doing more with the service, we can check whether it worked on our browser. To do this, we will need the IP address of our two worker nodes. First of all, we need to double-check which are the worker nodes by running this command:

```sh
$ docker node ls
```

> Once we know which node has which role, you can find the IP addresses of your nodes by running this:

```sh
$ docker-machine ls
```

> E.g. My worker nodes are `swarm-manager` and `swarm-worker02`, whose IP addresses are `192.168.99.100` and `192.168.99.102` respectively.

> Now that we have our service running on our cluster, we can start to find out more information about it. First of all, we can list the services again by running this command:

```sh
$ docker service ls
```

> As you can see, it is a replicated service and 1/1 containers active. Next, you can drill-down to find out more information about the service by running the inspect command:

```sh
$ docker service inspect cluster --pretty
```

> You may have noticed that so far, we haven't had to care about which of our two Worker nodes the service is currently running on. This is quite an important feature of Docker Swarm, as it completely removes the need for you to worry about the placement of individual containers.
>
> Before we look at scaling our service, we can take a quick look at which host our single container is running on by running these commands:

```sh
$ docker node ps
$ docker node ps swarm-manager
$ docker node ps swarm-worker02
```

> This will list the containers running on each of our hosts.

> Let's look at scaling our service to six instances of our application container. Run the following commands to scale and check our service:

```sh
$ docker service scale cluster=6
$ docker service ls
$ docker node ps swarm-manager
$ docker node ps swarm-worker02
```

> We are only checking two of the nodes since we originally told our service to launch on worker nodes. As you can see from the following Terminal output, we now have three containers running on each of our worker nodes.

> Before we move on to look at stacks, let's remove our service. To do this, run the following:

```sh
$ docker service rm cluster
```

> This will remove all of the containers, while leaving the downloaded image on the hosts.

## Docker Swarm stacks

> It is more than possible to create quite complex, highly available multi-container applications using Swarm and services. In a non-Swarm cluster, manually launching each set of containers for a part of the application can get to be a little laborious and also difficult to share. To this end, Docker has created functionality that allows you to define your services in Docker Compose files.
>
> The following Docker Compose file will create the same service we launched in the previous section:

```Dockerfile
version: "3"
services:
  cluster:
    image: russmckendrick/cluster
    ports:
      - "80:80"
    deploy:
      replicas: 6
      restart_policy:
        condition: on-failure
      placement:
        constraints:
          - node.role == worker
```

> As you can see, the stack can be made up of multiple services, each defined under services section of the Docker Compose file.
>
> In addition to the normal Docker Compose commands, you can add a deploy section; this is where you define everything relating to the Swarm element of your stack. In the previous example, we said we would like six replicas, which should be distributed across our two worker nodes. Also, we updated the default restart policy, which you saw when we inspected the service from the previous section and it showed up as paused, so that, if a container becomes unresponsive, it is always restarted.

> To launch our stack, copy the previous contents into a file called `docker-compose.yml`, and then run the following command:

```sh
$ docker stack deploy --compose-file=docker-compose.yml cluster
```

> Docker will, like when launching containers with Docker Compose, create a new network and then launch your services on it.
>
> You can check the status of your stack by running this:

```sh
$ docker stack ls
```

> This will show that a single service has been created. You can get details of the service created by the stack by running this command:

```sh
$ docker stack services cluster
```

> Finally, running the following command will show where the containers within the stack are running:

```sh
$ docker stack ps cluster
```

> Again, you will be able to access the stack using the IP addresses of your nodes, and you will be routed to one of the running containers.
>
> To remove a stack, simply run this command:

```sh
$ docker stack rm cluster
```

> This will remove all services and networks created by the stack when it is launched.

## Deleting a Swarm cluster

> Before moving on, as we no longer require it for the next section, you can delete your Swarm cluster by running the following command:

```sh
$ docker-machine rm swarm-manager swarm-worker01 swarm-worker02
```

> Should you need to relaunch the Swarm cluster for any reason, simply follow the instructions from the start of the chapter to recreate a cluster.

## Load balancing, overlays, and scheduling

### Ingress load balancing

> Docker Swarm has an ingress load balancer built in, making it easy to distribute traffic to our public facing containers. This means that you can expose applications within your Swarm cluster to services, for example an external load balancer such as Amazon Elastic Load Balancer, knowing that your request will be routed to the correct container(s) no matter which host happens to be currently hosting it.

> This means that our application can be scaled up or down, fail, or be updated, all without the need to have the external load balancer reconfigured.

### Network overlays

> In our example, we launched a simple service running a single application. Say we wanted to add a database layer in our application, which is typically a fixed point within the network; how could we do this?
>
> Docker Swarm's network overlay layer extends the network you launch your containers in across multiple hosts, meaning that each service or stack can be launched into its own isolated network. This means that our database container, running MongoDB, will be accessible to all other containers running on the same overlay network on port `27017`, no matter which of the hosts the containers are running on.

> You may be thinking to yourself _Hang on a minute; does this mean I have to hard-code an IP address into my application's configuration?_ Well that wouldn't fit well with the problems Docker Swarm is trying to resolve, so no, you don't.
>
> Each overlay network has its own inbuilt DNS service, which means that every container launched within the network is able to resolve the hostname of another container within the same network to its currently assigned IP address. This means that when we configure our application to connect to our database instance, we simply need to tell it to connect to, say, `mongodb:27017`, and it will connect to our MongoDB container.

### Scheduling

> At the time of writing, there is only a single scheduling strategy available within Docker Swarm, called Spread. What this strategy does is to schedule tasks to be run against the least loaded node that meets any of the constraints you defined when launching the service or stack. For the most part, you should not need to add too many constraints to your services.
>
> One feature that is not currently supported by Docker Swarm is affinity and anti-affinity; while it is easy to get around this using constraints, I urge you not to overcomplicate things as it is very easy to end up overloading hosts or creating single points of failure if you put too many constraints in place when defining your services.

---

> For a detailed explanation of the Raft consensus algorithms, I recommend working through the excellent presentation by The Secret Lives of Data, which can be found at http://thesecretlivesofdata.com/raft. It explains the processes taking place in the background on the manager nodes.

> You may have noticed that one of the columns when running the `docker-machine ls` command is `SWARM`. This only contains information if you have launched your Docker hosts using the standalone Docker Swarm command, which is built into Docker Machine.

```

```

# Source

[HAProxy on Docker Swarm: Load Balancing and DNS Service Discovery](https://www.haproxy.com/blog/haproxy-on-docker-swarm-load-balancing-and-dns-service-discovery/)

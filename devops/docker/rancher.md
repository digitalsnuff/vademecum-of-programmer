# Rancher

> Rancher is another open source project that helps with the deployment of Docker environments by using a GUI to allow you to control just about everything that you can with the CLI, as well as many other additional features.

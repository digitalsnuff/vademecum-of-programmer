# Docker Compose

## History

> In July 2014, Docker purchased a small British start-up called Orchard Laboratories; their website is still accessible at https://www.orchardup.com/.

> The second product was an open source project called Fig. Fig lets you use a YAML file to define how you would like your multi-container application to be structured. It would then take the YAML file and automate the launch of the containers as defined. The advantage of this was because it was a YAML file, it was really easy for developers to start shipping fig.yml files alongside their Dockerfiles within their code bases.

> Of these two products, Docker purchased Orchard Laboratories for Fig. After a short while, the Orchard service was discontinued, and in February 2015, Fig became Docker Compose.

> You can read more about Orchard Laboratories joining Docker in the announcement blog post at https://blog.docker.com/2014/07/welcoming-the-orchard-and-fig-team/.

## Introduction

> `Docker Compose` uses a YAML file, typically named `docker-compose.yml`, to define what your multi-container application should look like.

```yml
# version of the Docker Compose definition language
version: '3'

# where our containers are defined
services:
  redis:
    image: redis:alpine
    volumes:
      - redis_data:/data
    restart: always
  mobycounter:
    depends_on:
      - redis
    image: russmckendrick/moby-counter
    ports:
      - '8080:80'
    restart: always

volumes: redis_data:
```

Run docker-compose

```sh
$ docker-compose up
```

Docker Compose did the following:

1. Created a network called `dockercompose_default` using the default network driver (nowhere did we ask Docker Compose to do this).
2. Created a volume called `dockercompose_redis_data`, again using the default driver. This time we did actually instruct Docker Compose to create us this part of the multi-container application.
3. Launched two containers, one called `dockercompose_redis_1` and the second `dockercompose_mobycounter_1`.

> Names of containers are prefixed by folder name, where `docker-compose up` was launched.

## Docker Compose YAML file

Flags:

- `image`: This tells Docker Compose which image to download and use. This does not exist as an option when running docker container run on the command line as you can only run a single container; as we have seen in previous chapters, the image is always defined toward the end of the command without the need of a flag being passed.
- `volume`: This is the equivalent of the `--volume` flag, but it can accept multiple volumes. It only uses the volumes that are declared in the Docker Compose YAML file.
- `depends_on`: This would never work as a docker container run invocation because the command is only targeting a single container. When it comes to Docker Compose, `depends_on` is used to help build some logic into the order your containers are launched in. For example, only launch container B when container A has successfully started.
- `ports`: This is basically the `--publish` flag, which accepts a list of ports.
- `restart`: This is the same as using the `--restart` flag and accepts the same input.
- `volume`: declare volumes

## Commands

### `Up` and `PS`

Run sets in detached mode

```sh
$ docker-compose up -d
```

Once control of your Terminal is returned, you should be able to check that the containers are running using the following command:

```sh
# Run only in folder with supported filenames: docker-compose.yml docker-compose.yaml
$ docker-compose ps
```

### `Config`

Running the following command will validate our `docker-compose.yml` file:

```sh
$ docker-compose config
```

Running to check for errors only. No render copy of Docker Compose YAML:

```sh
$ docker-compose config -q
```

### `Pull`

Read your Docker Compose YAML file and `pull` any of the images it finds.

```sh
$ docker-compose pull
```

### `build`

> These commands are useful when you are first defining your Docker Compose-powered application and want to test without launching your application. The docker-compose build command can also be used to trigger a build if there are updates to any of the Dockerfiles used to originally build your images.

execute any build instructions it finds in your file:

```sh
$ docker-compose build
```

### `create`

> This will `create` but not launch the containers. In the same way that the docker `container create` command does, they will have an `Exited` state until you start them. The `create` command has a few useful flags you can pass:

- `--force-recreate`: This recreates the container even if there is no need to as nothing within the configuration has changed
- `--no-recreate`: This doesn't recreate a container if it already exists; this flag cannot be used with the preceding flag
- `--no-build`: This doesn't build the images, even if an image that needs to be built is missing
- `--build`: This builds the images before creating the containers

```sh
$ docker-compose create
```

### `start`, `stop`, `restart`, `pause`, `unpause`

> They effect change on all of the containers

```sh
$ docker-compose start
$ docker-compose stop
$ docker-compose restart
$ docker-compose pause
$ docker-compose unpause
```

> It is possible to target a single service by passing its name; for example, to pause and unpause the db service we would run:

```sh
$ docker-compose pause db
$ docker-compose unpause db
```

### `top`, `logs`, `events`

> The next three commands all give us feedback on what is happening within our running containers and Docker Compose.

Display information on the process running within each of Docker Compose-launched containers:

```sh
$ docker-compose top
```

Display information one of the services:

```sh
$ docker-compose top db
```

Stream the logs from each of the running containers to screen:

```sh
$ docker-compose logs
```

Stream the logs with keep following

```sh
$ docker-compose logs -f
$ docker-compose logs -f db
```

> `events` streams events, such as the ones triggered by the other commands we have been discussing above, in real time:

```sh
$ docker-compose events
```

### `exec` and `run`

```sh
$ docker-compose exec worker ping -c 3 db
```

> The `run` command is useful if you need to run a containerized command as a one-off within your application. For example, if you use a package manager such as `composer` to update the dependencies of your project that is stored on a volume, you could run something like this:

```sh
$ docker-compose run --volume data_volume:/app composer install
```

> This would run the `composer` container with the install command and mount the `data_volume` to `/app` within the container.

### `scale`

> The `scale` command will take the service you pass the command and scale it to the number you define; for example, to add more worker containers I just need to run:

```sh
$ docker-compose scale worker=3
```

> However, this actually gives the following warning:

```
WARNING: The scale command is deprecated. Use the up command with the -- scale flag instead.
```

> What we should now be using is the following command:

```sh
$ docker-compose up -d --scale worker=3
$ docker-compose up -d --scale vote=3
```

> While the scale command is in the current version of Docker Compose, it will be removed from future versions.

### `kill`, `rm`, `down`

`kill` stops running containers by immediately stopping running container processes. It does not wait for containers to gracefully stop like when running `docker-compose stop`, meaning that using the `docker-compose kill` command may result in data loss.

```sh
$ docker-compose kill
```

`rm` command removes any containers with the state of exited:

```sh
$ docker-compose rm
```

`down` has opposite effect than `up`

```sh
$ docker-compose down
```

> That will remove the containers and the networks created when running `docker-compose up`. If you want to remove everything, you can do so by running the following:

```sh
$ docker-compose down --rmi all --volumes
```

> This will remove all of the containers, networks, volumes and images (both pulled and built) when you ran the `docker-compose up` command; this includes images that may be in use outside of your Docker Compose application--there will, however, be an error if the images are in use, and they will not be removed.

---

## `docker-compose.yaml` file options

### `command`

[Link](https://docs.docker.com/compose/compose-file/)

```yml
version: '3.1'
services:
  db:
    image: postgres
  web:
    build: .
    command:
      - /bin/bash
      - -c
      - |
        python manage.py migrate
        python manage.py runserver 0.0.0.0:8000

    volumes:
      - .:/code
    ports:
      - '8000:8000'
    links:
      - db
```

multiline example:

```yml
version: '3.1'

services:
  app:
    build:
      context: .
    command: >
      sh -c "python manage.py wait_for_db &&
             python manage.py migrate &&
             python manage.py runserver 0.0.0.0:8000"
```

```yml
version: '3.1'
services:
  db:
    image: postgres
  web:
    build: .
    command:
      - /bin/bash
      - -c
      - |
        python manage.py migrate
        python manage.py runserver 0.0.0.0:8000

    volumes:
      - .:/code
    ports:
      - '8000:8000'
    links:
      - db
```

or create Dockerfile and declare commands after RUN

---

## Example

### Example 1

> In the repository, which is available at https://github.com/russmckendrick/mastering-docker/, you will find a folder in the chapter06 directory called `example-voting-app`. This is a fork of the voting application from the official Docker sample repository, which can be found at https://github.com/dockersamples/.

```yml
version: '3'

services:
  vote:
    build: ./vote
    command: python app.py
    volumes:
      - ./vote:/app
    ports:
      - '5000:80'
    networks:
      - front-tier
      - back-tier
  redis:
    image: redis:alpine
    container_name: redis
    ports: ['6379']
    networks:
      - back-tier
  worker:
    build:
      context: ./worker
    networks:
      - back-tier
  db:
    image: postgres:11
    container_name: db
    volumes:
      - 'db_data:/var/lib/postgresql/data'
    networks:
      - back-tier
  result:
    build: ./result
    command: nodemon --debug  server.js
    volumes:
      - ./result:/app
    ports:
      - '5001:80'
      - '5858:5858'
    networks:
      - front-tier
      - back-tier

volumes:
  db-data:

networks:
  front-tier:
  back-tier:
```

> The build instruction here tells Docker Compose to build a container using the `Dockerfile`, which can be found in the `./vote` folder. The Dockerfile itself is quite straightforward for a Python application. Once the container launches, we are then mounting the folder from our host machine into the container, which is achieved by passing the path of the folder we want to mount and where within the container we would like it mounted.

> We are telling the container to run the python `app.py` when it launches. We are mapping port `5000` on our host machine to port `80` on the container, and finally, we are further attaching two networks to the container, one called `front-tier` and the second called `back-tier`.

> The `front-tier` network will have the containers that have to have ports mapped to the host machine; the `back-tier` network is reserved for containers that do not need their ports to be exposed and acts as a private isolated network.

> The `vote` application sends the votes to the redis container.

> This container uses the official Redis image and is not built from a `Dockerfile`; we are making sure that port `6379` is available, but only on the `back-tier` network.

> We are also specifying the name of the container, setting it to `redis` by using `container_name`. This is to avoid us having to make any considerations on the default names generated by Docker Compose within our code.

> Once our vote has been cast and it has found its way into the Redis database, a worker process is executed.

> The worker container runs a .NET application whose only job is to connect to `redis` and register each vote by transferring it into a PostgreSQL database running on a container called `db`. The container is again built using a Dockerfile, but this time rather than passing the path to the folder where the Dockerfile and application are stored, we are using context. This sets the working directory for the `docker build` and also allows you to define additional options such as labels and changing the name of the Dockerfile.

> As this container is doing nothing other than connecting to `redis` and the db container, it does not need any ports exposed; it also does not need to communicate with either of the containers running on the `front-tier` network, meaning we just have to add the back-tier network.

> The next container is `db`.

> As this is where our votes will be stored, we are creating and mounting a volume to act as
> persistent storage for our PostgreSQL database.

> The final container launched is the result container; this is a Node.js application that connects to the PostgreSQL database running on `db` and displays the results in real time as votes are cast using the `vote` container.

> The final part of the `docker-compose.yml` file declares the volume for the PostgreSQL
> database and our two networks.

### Example 2

- create in a root `docker-compose.yml`

```yml
version: '3'

services:
  app:
    container_name: docker-node-mongo
    restart: always
    build: .
    ports:
      - '80:3000'
    links:
      - mongo
  mongo:
    container_name: mongo
    image: mongo
    ports:
      - '27017:27017'
```

- run `docker-compose up`
- run `docker-compose down`

---

> `YAML` is a recursive acronym which stands for **YAML Ain't Markup Language**. It is used by a lot of different applications for both configuration and also for defining data in a human-readable structured data format. The indentation you see in the examples is very important as it helps define the structure of the data. For more information, see the project's home page at http://www.yaml.org/.

# Links

[Compose file version 3 reference](https://docs.docker.com/compose/compose-file/)

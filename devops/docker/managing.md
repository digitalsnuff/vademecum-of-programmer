# Managing Containers

> We cannot assign more than one process to a port on a host machine

## Docker Container Commands

### Get list of running containers

```
docker container ls
```

### Get list of all containers

```
docker container ls -a
```

### Remove selected container

```
docker container rm [id] or [name]
```

## Interacting with containers

> Containers have been running a single process.

### `Attach`

> The first way of interacting with your running container is to attach to the running process.
> It is useful if you need to connect to the process your container is running.
>
> ```sh
> $ docker container attach nginx-test
> ```
>
> Pressing Ctrl + C will terminate the process and return your Terminal to normal.

> return to Terminal, leaving the container in a detached state
>
> ```sh
> $ docker container attach --sig-proxy=false nginx-test
> ```

### `Exec`

> This spawns a second process within the container that you can interact with.
>
> ```sh
> $ docker container exec nginx-test cat /etc/debian_version
> ```

> Forking a bash process and using the -i and -t flags to keep open console access to our container
>
> ```sh
> $ docker container exec -i -t nginx-test /bin/bash
> ```

## Logs and process information

### `logs`

> It allows you to interact with the `stdout` stream of your containers, which Docker is keeping track of in the background
>
> ```sh
> $ docker container logs --tail 5 nginx-test
> ```

> To view logs in real time:
>
> ```sh
> $ docker container logs -f nginx-test
> ```

> view everything that has been logged since 15:00 today
>
> ```sh
> $ docker container logs --since 2017-06-24T15:00 nginx-test
> ```

> The logs command shows the timestamps of stdout recorded by Docker and not the time within the container.
>
> There is an hour's time difference between my host machine and the container due to British Summer Time (BST) being in effect on my host.
>
> ```sh
> $ date
> $ docker container exec nginx-test date
> ```

### `top`

> The `top` command lists the processes running within the container
> you specify:
>
> ```sh
> $ docker container top nginx-test
> ```

### `stats`

> The stats command provides real-time information on either the specified container or, if you don't pass a container name or ID, all running containers (CPU, RAM, NETWORK, DISK IO):
>
> ```sh
> $ docker container stats nginx-test
> ```

## Resource limits

```sh
# set limits
$ docker container run -d --name nginx-test --cpu-shares 512 --memory 128M -p 8080:80 nginx
# update limits
$ docker container update --cpu-shares 512 --memory 128M nginx-test
```

> Inspect configuration data:
>
> ```sh
> $ docker container inspect nginx-test
> $ docker container inspect nginx-test | grep -i memory
> ```

> Set with swap
>
> ```sh
> $ docker container update --cpu-shares 512 --memory 128M --memory-swap 256M nginx-test
> ```

## Container States

- Up
- Up (paused)
- Exited
- Created

> create several containers:
>
> ```sh
> $ for i in {1..5}; do docker container run -d --name nginx$(printf "$i")
> nginx; done
> ```

### `Pause` and `Unpause`

```sh
$ docker container pause nginx1
```

```sh
$ docker container unpause nginx1
```

> With the `cgroups` freezer, the process is unaware it has been suspended, meaning that it can be resumed.
>
> For more information on the `cgroups` freezer function, read the official kernel documentation at https://www.kernel.org/doc/Documentation/cgroup-v1/freezer-subsystem.txt.

### `start`

> The `start` command to resume a container with a status of `Exited`.

### `stop`

> It works in exactly the same way as when we used `Ctrl + C` to detach from your container running in the foreground.

> With this, a request is sent to the process for it to terminate, called a `SIGTERM`. If the process has not terminated itself within a grace period, then a kill signal, called a `SIGKILL`, is sent. This will immediately terminate the process, not giving it anytime to finish whatever is causing the delay, for example, committing the results of a database query to disk.

> Wait up to 60 seconds before sending a `SIGKILL`:
>
> ```sh
> $ docker container stop -t 60 nginx3
> ```

### `restart`

> The `restart` command is a combination of the following two commands; it stops and then starts the container ID or name you pass it. You can pass the `-t` flag.

### `kill`

> Finally, you also have the option sending a `SIGKILL` immediately to the container by running the `kill` command:
>
> ```sh
> $ docker container kill nginx5
> ```

## Removing containers

### `prune`

> Removing all unused objects with `Exited` status
>
> ```sh
> $ docker system prune
> ```

### `rm`

> Remove container
>
> ```sh
> $ docker container rm [name]
> ```

> Remove all containers
>
> ```sh
> $ docker rm $(docker ps -aq)
> ```

> Remove all containers with force
>
> ```sh
> $ docker rm $(docker ps -aq) -f
> ```

## Miscellaneous commands

### `create`

> The `create` command is pretty similar to the run command, except that it does not start the container, but instead prepares and configures one:
>
> ```sh
> $ docker container create --name nginx-test -p 8080:80 nginx
> ```

### `port`

> This displays port along with any port mappings for the container:
>
> ```sh
> $ docker container port nginx-test
> ```

### `diff`

> This command prints a list of all of the files that have been added or changed since the container started to run--so basically, a list of the differences on the filesystem between the image we used to
> launch the container and now.

## Docker networking

> [Go To](networking.md)

## Docker volumes

## Flags

- `-d`, `-detach` - If we hadn't added this flag, then our container would have executed in the foreground, which means that our terminal would have been frozen until we passed the process an escape command by pressing `Ctrl + C`.
- `-i`, `--interactive` - keep `stdin` open
- `-t`, `--tty` (with `exec`) - allocates a pseudo-TTY to the session.
- `-t`, `--timestamp` (with `logs`)
- `-f`, `--follow` - show in real time current `stdout`

---

> Early user terminals connected to computers were called `teletypewriters`. While these devices are no longer used today, the acronym TTY has continued to be used to described text-only consoles in modern computing.

> It is not recommend making any changes to your containers as they are running using the pseudo-TTY. It is more than likely that those changes will not persist and will be lost when your container is removed.

# Storing and Distributing Images

## Docker Hub

> [Link](https://hub.docker.com/)

Command to download remote images

```sh
$ docker image pull
```

- store repositories as public or private
- creating an automated build: link to `GitHub` or `Bitbucket`

## Docker Store

## Docker Registry

> [Link](https://docs.docker.com/registry/)

> Docker Registry is an open source application that you can run anywhere you please and store your Docker image in.
> Docker Registry makes a lot of sense if you want to deploy your own registry without having to pay for all the private features of Docker Hub.

- Host and manage your own registry, from which you can serve all the
  repositories as private, public, or a mix between the two
- Scale the registry as needed based on how many images you host or how many pull requests you are serving out
- Everything is command-line-based

### Deploying your own registry

```sh
$ docker image pull registry:2
$ docker container run -d -p 5000:5000 --name registry registry:2
```

```sh
$ docker image tag alpine localhost:5000/localalpine #tagging
$ docker image push localhost:5000/localalpine #push to locally hosted Docker Registry
$ docker image rm alpine localhost:5000/localalpine
$ docker image pull localhost:5000/localalpine # pulling from local registry
$ docker image ls
```

### Stop and removing the Docker Registry

```sh
$ docker container stop registry
$ docker container rm -v registry
```

### Supporting

- Filesystem: This is exactly what it says; all images are stored on the filesystem at the path you define. The default is `/var/lib/registry`.
- Azure: This uses Microsoft Azure storage, https://azure.microsoft.com/en-gb/services/storage/.
- GCS: This uses Google Cloud storage, https://cloud.google.com/storage/.
- S3: This uses Amazon Simple Storage Service (Amazon S3), https://aws.amazon.com/s3/.
- Swift: This uses OpenStack Swift, https://wiki.openstack.org/wiki/Swift/.

### Docker Trusted Registry

> One of the components that ships with the commercial Docker Datacenter is Docker Trusted Registry (DTR). Think of it as a version of Docker Hub that you can host in your own infrastructure. DTR adds the following features on top of the ones provided by the free Docker Hub and Docker Registry:

- Integrated into your authentication services such as Active Directory or LDAP
- Deployed on your own infrastructure (or cloud) behind your firewall
- Image signing to ensure your images are trusted
- Built in security scanning
- Access to prioritized support directly from Docker

> More information on Docker Trusted Registry can be found at https://ocs.docker.com/datacenter/dtr/2.2/guides/.

## Third-party register

### OpenShift

### JFrog

### Quay

### Amazon EC2 Container Registry (ECR)

## Microbadger

> Microbadger is a great tool when you are looking at shipping your containers or images around. It will take into account everything that is going on in every single layer of a particular Docker image and give you the output of how much weight it has in terms of actual size or what amount of disk space it will take up.

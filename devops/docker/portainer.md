# Portainer

> `Portainer` is a tool that allows you to manage Docker resources from a web interface.

> The following is what you will see when navigating your browser to the Portainer website, http://www.portainer.io/

## Getting Portainer up and running

```sh
$ docker image pull portainer/portainer
$ docker image ls
$ docker container run -d -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock portainer/portainer
```

> As you can see from the command we have just run, we are mounting the socket file for the Docker Engine on our Docker Host machine. Doing this will allow Portainer full unrestricted access to the Docker Engine on our host machine. It needs this so it can manage Docker on the host; however, it does mean that your Portainer container has full access to your host machine, so be careful in how you give access to it and also when publicly exposing Portainer on remote hosts.

> For the most basic type of installation, that is all we need to run. There are a few more steps to complete the installation; they are all performed in the browser. To complete them, go to http://localhost:9000/

# Docker

[Homepage](https://www.docker.com/)

# Ecosystem

## Docker Engine

> This is the core of Docker, and all of the other tools we will be covering use it.
> There are two versions of Docker Engine:

- Docker Enterprice Edition (Docker EE)
- Docker Community Edition (CE)

# Tools

## Docker Compose

> A tool that allows you to define and share multi-container definitions.

## Docker Machine

> A tool to launch Docker hosts on multiple platforms.

## Docker Hub

> A repository for Docker images.

## Docker Store

> A storefront for official Docker images and plugins as well as licensed products.

## Docker Swarm

> A multi-host-aware orchestration tool.

## Docker for Mac

>

## Docker Cloud

> Docker's `Container as a Service (CaaS)` offering, Docker Cloud.

## Docker for Windows

>

## Docker for Amazon Web Services

> A best-practice Docker Swarm installation that targets AWS.

## Docker for Azure

> A best-practice Docker Swarm installation that targets Azure.

# Installation

[Get Docker CE for Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
[Post-installation steps for Linux](https://docs.docker.com/install/linux/linux-postinstall/)

# Commands

```
docker
```

```
docker version
```

```
docker info
```

### _hello-world_ container

```sh
docker container run hello-world
```

### Run container with interactive mode

```
docker container run -it -p 8011:80 --name nginx-test nginx
```

### Remove selected image

```
docker image rm [id]
```

### Pull image from Docker Hub

```
docker pull nginx
```

### Run container

```
docker container run -d -p 8080:80 --name mynginx nginx
```

### Run container with mapping pwd with nginx web file location

> You are allowed to change files locally. Changes will show immediately in browser.

```
docker container run -d -p 8080:80 -v $(pwd):/usr/share/nginx/html --name nginx-webside nginx
```

### Show running docker processes

```
docker ps
```

### Run httpd container

```
docker container run -d -p 8081:80 --name myapache httpd
```

### Run MySQL container with set password

```
docker container run -d -p 3307:3306 --name mysql --env MYSQL_ROOT_PASSWORD=12345 mysql
```

### Find the IP of the container using following. Check out the “IPAddress” from the output, this will tell you the ip address.

```
docker inspect mysql
```

### Stop myapache container

```
docker container stop myapache
```

### Remove myapache container

```
docker container rm myapache
```

### Remove myapache container with force option

```
docker container rm myapache -f
```

### Run container in iteractive mode for running commands by shell

> Do changes in running container are strongly **not recomended**

```sh
$ docker container run -it --name alpine-test alpine /bin/sh
```

### Save image as `tar` file

```sh
$ docker image save -o <name_of_file.tar> <REPOSITORY>:<TAG>
```

### Commit as image

> It is useful after changes in container through interactive access. The changes will be saved and will be in new container.
>
> It is useful in the case then container was crashed.

```sh
$ docker container commit <container_name> <REPOSITORY>:<TAG>
```

### Run bash inside mynginx container

```
docker container exec -it mynginx bash
```

# Flags

- `-it` flag is for interactive mode
- `-d` flag is for detached mode

# Dockerfile

[Go To](building-container-images)

# Dockerize

> [Go to](dockerize.md)

# Docker Compose

> [Go To](docker-compose.md)

## Dockerignore

```
node_modules
```

# Helpful Links

[Exploring Docker [1] - Getting Started](https://www.youtube.com/watch?v=Kyx2PsuwomE&t=880s)

# Sources

- Russ McKendrick, Scott Gallagher, _Mastering Docker_, second edition, Packt Publishing, 2017

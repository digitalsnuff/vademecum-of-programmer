# Docker Machine

[Install Docker Machine on Ubuntu](https://docs.docker.com/machine/install-machine/)

```sh
$ base=https://github.com/docker/machine/releases/download/v0.16.0 &&
  curl -L $base/docker-machine-$(uname -s)-$(uname -m) >/tmp/docker-machine &&
  sudo install /tmp/docker-machine /usr/local/bin/docker-machine
```

---

> Launch and bootstrap Docker hosts targeting various platform, including locally or in a cloud environment.

> You can control your Docker hosts with it as well.

> Docker Machine's biggest strength is that it provides a consistent interface to several public cloud providers:

- **Amazon Web Services**: https://aws.amazon.com/
- **Microsoft Azure**: https://azure.microsoft.com/
- **DigitalOcean**: https://www.digitalocean.com/
- **Exoscale**: https://www.exoscale.ch/
- **Google Compute Engine**: https://cloud.google.com/
- **Rackspace**: https://www.rackspace.com/
- **IBM SoftLayer**: https://www.softlayer.com/

> It also supports the following self-hosted VM/cloud platforms:

- **Microsoft Hyper-V**: https://www.microsoft.com/en-gb/cloud-platform/server-virtualization/
- **OpenStack**: https://www.openstack.org/
- **VMware vSphere**: https://www.vmware.com/uk/products/vsphere.html

> And finally, the following locally hosted hypervisors are supported as well:

- **Oracle VirtualBox**: https://www.virtualbox.org/
- **VMware Fusion**: https://www.vmware.com/uk/products/fusion.html
- **VMware Workstation**: https://www.vmware.com/uk/products/workstation.html

## Deploying local Docker hosts with Docker Machine

```sh
$ docker-machine create --driver virtualbox docker-local
```

> This will start the deployment, during which you will get a list of tasks that Docker Machine is running. To launch your Docker host, each host launched with Docker Machine goes through the same steps.

> Docker Machine creates a unique SSH key for the virtual machine, this means that you will be able to access the virtual machine over SSH, more on that later. Once the virtual machine has booted, Docker Machine then makes a connection to the virtual machine.

> As you can see, Docker Machine detects the operating system being used and chooses the appropriate bootstrap script to deploy Docker. Once Docker is installed, Docker Machine generates and shares certificates between your local host and the Docker host. It then configures the remote Docker installation for certificate authentication, meaning that your local client can connect to and interact with the remote Docker server.
>
> Once Docker is installed, Docker Machine generates and shares certificates between your local host and the Docker host, it then configures the remote Docker installation for certificate authentication meaning that your local client can connect to and interact with the remote Docker server:

Show how to make a connection

```sh
$ docker-machine env docker-local
```

Listing the currently configured Docker hosts:

```sh
$ docker-machine ls
```

Connects you to the Docker host using SSH:

```sh
$ docker-machine ssh docker-local
```

Find out the IP address of your Docker host

```sh
$ docker-machine ip docker-local
```

Stop, start, restart, remove

```sh
$ docker-machine stop docker-local
$ docker-machine start docker-local
$ docker-machine restart docker-local
$ docker-machine rm docker-local
```

More details about your Docker host:

```sh
$ docker-machine inspect docker-local
$ docker-machine config docker-local
$ docker-machine status docker-local
$ docker-machine url docker-local
```

## Launching Docker hosts in the cloud

### DigitalOcean

> To launch a Docker host in DigitalOcean using Docker Machine, you only need an API access token. Rather than explaining how to generate one here, you can follow the instructions at https://www.digitalocean.com/help/api/.

> To launch a Docker host called docker-digtialocean, we need to run the following command:

```sh
$ docker-machine create \
--driver digitalocean \
--digitalocean-access-token
ba46b9f97d16edb5a1f145093b50d97e50665492e9101d909295fa8ec35f20a1 \
docker-digitalocean
```

Reconfigure your local client to connect to the remote host by running:

```sh
$ eval $(docker-machine env docker-digitalocean)
```

Also, you can run `docker version` and `docker-machine inspect digitalocean` to find out more information about the Docker host.

SSH login to host

```sh
 docker-machine ssh docker-digitalocean
```

Add defaults:

```sh
following:
$ docker-machine create \
--driver digitalocean \
--digitalocean-access-token
ba46b9f97d16edb5a1f145093b50d97e50665492e9101d909295fa8ec35f20a1 \
--digitalocean-image ubuntu-16-04-x64 \
--digitalocean-region nyc3 \
--digitalocean-size 512mb \
--digitalocean-ipv6 false \
--digitalocean-private-networking false \
--digitalocean-backups false \
--digitalocean-ssh-user root \
--digitalocean-ssh-port 22 \
docker-digitalocean
```

### Amazon Web Services

- **AWS security credentials**: http://docs.aws.amazon.com/general/latest/gr/aws-sec-cred-types.html
- **AWS command line interface docs**: http://docs.aws.amazon.com/cli/latest/userguide/cli-chap-welcome.html

> Before we launch our Docker host in AWS, we need to know the ID of the `Virtual Private Cloud (VPC)` in the `us-east-1` region, which is where Docker Machine will launch hosts by default. To do this, run the following command:

```sh
$ aws ec2 --region us-east-1 describe-vpcs
```

> Once you have your VPC ID, run the following command:

```sh
docker-machine create \
--driver amazonec2 \
--amazonec2-vpc-id vpc-35c91750 \
docker-aws
```

Check the status of your Docker host

```sh
$ aws ec2 --region us-east-1 describe-instances
```

Extend Docker Machine

```sh
docker-machine create \
--driver amazonec2 \
--amazonec2-ami AWS_AMI ami-5f709f34 \
--amazonec2-region us-east-1 \
--amazonec2-vpc-id vpc-35c91750 \
--amazonec2-zone a \
--amazonec2-instance-type t2.micro \
--amazonec2-device-name /dev/sda1 \
--amazonec2-root-size 16 \
--amazonec2-volume-type gp2 \
--amazonec2-ssh-user ubuntu \
docker-aws
```

## Dockerize existing app in git repo

- git clone
- create `Dockerfile` in a root

```
FROM node:10

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3000

CMD ["npm","start"]

```

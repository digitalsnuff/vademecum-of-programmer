# Docker Volumes

## Exaples

> [See networking](networking.md)

Stop and remove `redis` container

```sh
$ docker container stop redis
$ docker container rm redis
```

Relaunch the `redis`

```sh
$ docker container run -d --name redis --network moby-counter redis:alpine
# Previous icons on the webpage of application disappear and you are left with a clean slate
```

Remove `redis` again

```sh
$ docker container stop redis
$ docker container rm redis
```

> The Dockerfile for the Redis offical image we used, toward the end of the file, there are the `VOLUME` and `WORKDIR` directives declared. This means that when our container was launched, Docker actually created a volume and then ran `redis-server` from within the volume.
>
> We can see this by running the following command:

```sh
$ docker volume ls
# Returns in columns DRIVER names and VOLUME NAME as ID
```

> We know from the `Dockerfile` that the volume was mounted at `/data` within the container, so all we have to do is tell Docker which volume to use and where it should be mounted at runtime.
>
> To do this, run the following command, making sure you replace the volume ID with that of your own:

```sh
$ docker container run -d --name redis -v 719d0cc415dbc76fed5e9b8893e2cf547f0ac6c91233451604fdba31f0dd2d2a:/data --network moby-counter redis:alpine
```

Restarting the application container

```sh
$ docker container restart moby-counter
```

View the contents of the `/data` folder on the `redis` container

```sh
$ docker container exec redis ls -lhat /data
# Result
# total 12
# drwxr-xr-x    2 redis    redis       4.0K Apr 28 09:17 .
# -rw-r--r--    1 redis    redis        153 Apr 28 09:17 dump.rdb
# drwxr-xr-x    1 root     root        4.0K Apr 28 09:16 ..
```

Create volume

```sh
$ docker volume create redis_data
```

Run `redis` container with name of created volume

```sh
$ docker container run -d --name redis -v redis_data:/data --network moby-counter redis:alpine
```

Inspect volume

```sh
$ docker volume inspect redis_data
```

Remove two containers and network

```sh
$ docker container stop redis moby-counter
$ docker container prune
$ docker network prune
```

Remove all the volumes

```sh
$ docker volume prune
```

# Docker networking

> The containers we have been launching would have been able to communicate with each other without having to use any of the host networking.

## Create network

Create new network for communicating between containers

```sh
$ docker network create moby-counter
```

## Steps

- Pull images from repository
- create network
- run container in created network
- run application in the same network

## more information about the configuration of the networksnd:

```sh
$ docker network inspect moby-counter
# Returns JSON array
```

## Example

Download the container images which will be using

```sh
$ docker image pull redis:alpine
$ docker image pull russmckendrick/moby-counter
$ docker network create moby-counter
```

Run `redis` container in network

```sh
$ docker container run -d --name redis --network moby-counter redis:alpine
```

Run application in the same network

```sh
# In this case there is exposed external port only to application.
# Redis container has no external access. It has expose default internal port (6379).
$ docker container run -d --name moby-counter --network moby-counter -p
8080:80 russmckendrick/moby-counter
# The application is connecting to 'redis' by add in 'server.js' file following values:
# var port = opts.redis_port || process.env.USE_REDIS_PORT || 6379
# var host = opts.redis_host || process.env.USE_REDIS_HOST || 'redis'
# The application is looking to connect to the host called 'redis' on port '6379'
```

Ping `redis` container

```sh
$ docker container exec moby-counter ping -c 3 redis
# Result
# $ docker container exec moby-counter ping -c 3 redis
# PING redis (172.18.0.2): 56 data bytes
# 64 bytes from 172.18.0.2: seq=0 ttl=64 time=0.083 ms
#64 bytes from 172.18.0.2: seq=1 ttl=64 time=0.130 ms
# 64 bytes from 172.18.0.2: seq=2 ttl=64 time=0.130 ms
#
# --- redis ping statistics ---
# 3 packets transmitted, 3 packets received, 0% packet loss
# round-trip min/avg/max = 0.083/0.114/0.130 ms

# Container resolves 'redis' to the IP address of the Redis container, which is 172.18.0.2.
```

Get content of `/etc/hosts` in `moby-counter` container

```sh
$ docker container exec moby-counter cat /etc/hosts
# Result
# 127.0.0.1       localhost
# ::1     localhost ip6-localhost ip6-loopback
# fe00::0 ip6-localnet
# ff00::0 ip6-mcastprefix
# ff02::1 ip6-allnodes
# ff02::2 ip6-allrouters
# 172.18.0.3      1912c336bd37
#
# The last line is a resolving IP address to name.
# '1912c336bd37' is a name of container's host and simultaneously ID of container
#
```

Checking `resolv` in container

```sh
$ docker container exec moby-counter cat /etc/resolv.conf
# Result
# nameserver 127.0.0.11
# options ndots:0
```

DNS lookup on `redis`

```sh
$ docker container exec moby-counter nslookup redis 127.0.0.11
# Result
# Server:    127.0.0.11
# Address 1: 127.0.0.11

# Name:      redis
# Address 1: 172.18.0.2 redis.moby-counter
```

Create a second network and launch another application container:

```sh
$ docker network create moby-counter2
$ docker run -itd --name moby-counter2 --network moby-counter2 -p 9090:80
russmckendrick/moby-counter
```

Ping `redis` container from `moby-counter2`

```sh
$ docker container exec moby-counter2 ping -c 3 redis
# Result
# ping: bad address 'redis'
```

Checking `resolv.conf` from `moby-counter2`

```sh
$ docker container exec moby-counter2 cat /etc/resolv.conf
# Result
# nameserver 127.0.0.11
# options ndots:0
```

DNS lookup on `redis` from `moby-counter2`

```sh
$ docker container exec moby-counter2 nslookup redis 127.0.0.11
# Result
# Server:    127.0.0.11
# Address 1: 127.0.0.11

# nslookup: can't resolve 'redis': Name does not resolve
```

> Second network is running completely isolated from our first network,
> meaning that we can still use the DNS name of redis; to do this, we need to add the `--network-alias` flag:

```sh
$ docker container run -d --name redis2 --network moby-counter2 --network-alias redis redis:alpine
```

> As you can see, we have named the container `redis2` but set the `--network-alias` to be `redis`; this means that when we perform the lookup, we see the correct IP address returned:

```sh
$ docker container exec moby-counter2 nslookup redis 127.0.0.11
# Result
# Server:    127.0.0.11
# Address 1: 127.0.0.11

# Name:      redis
# Address 1: 172.19.0.3 redis2.moby-counter2
```

Remove one of the applications and associated network

```sh
$ docker container stop moby-counter2 redis2
$ docker container prune
$ docker network prune
```

## Extensions

### Weave Net

> This allows multihost networking and is a third-party extension written and provided by Weave (https://weave.works/).

---

> `Redis` is an in-memory data structure store that can be used as a database, cache, or message broker. It supports different levels of on-disk persistence. For more information, refer to https://redis.io/.

> `IP address management (IPAM)` is a means of planning, tracking, and
> managing IP addresses within the network. `IPAM` has both DNS and
> DHCP services so that each service is notified of changes in the other; for example, DHCP assigns an address to `container2`. The DNS service is
> updated to return the IP address assigned by DHCP whenever a lookup is
> made against `container2`.

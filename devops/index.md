# DevOps

> learning languages and frameworks is one thing, setting up environments, testing & deployment is another

- deployment - Linux, ssh, Git, Server Software (Nginx, Apache)
- platforms - Digital Ocean, AWS, Heroku, Azure
- Virtualization - Docker, Vagrant
- Testing - Unit, Integration, functional, system

DevOps fits into the wider world of Agile systems development.
An important part of DevOps is being able to explain to coworkers in your organization what DevOps is and what it isn't.
DevOps is, by definition, a field that spans several disciplines.
You must understand both the technical background and the nontechnical cultural aspects.

The word "DevOps" is a combination of the words "development" and "operation".

The DevOps movement has its roots in Agile software development principles.
The DevOps movement has its roots in Agile software development principles.
The Agile Manifesto was written in 2001 by a number of individuals wanting to
improve the then current status quo of system development and find new ways
of working in the software development industry.

http://agilemanifesto.org/

"Individuals and interactions over processes and tools
Working software over comprehensive documentation
Customer collaboration over contract negotiation
Responding to change over following a plan
That is, while there is value in the items on the right, we value the items on the left
more."
DevOps, then, tends to emphasize that interactions between individuals are
very important, and that technology might possibly assist in making these
interactions happen and tear down the walls inside organizations.
A further benefit would be smaller maintenance costs, since three systems are replaced by one.

Another core goal of DevOps is automation and Continuous Delivery.

The turnaround for DevOps processes must be fast.

DevOps engineers work on making enterprise processes faster, more efficient, and
more reliable. Repetitive manual labor, which is error prone, is removed whenever
possible.
Longer cycles are also common and are called Program Increments, which span
several Scrum Sprint cycles, in Scaled Agile Framework.
cooperation between disciplines in an Agile organization.

Here are some examples of when DevOps can benefit Agile cycles:

- Deployment systems, maintained by DevOps engineers, make the deliveries
  at the end of Scrum cycles faster and more efficient. These can take place
  with a periodicity of two to four weeks.
  In organizations where deployments are done mostly by hand, the time
  to deploy can be several days. Organizations that have these inefficient
  deployment processes will benefit greatly from a DevOps mindset.
- The Kanban cycle is 24 hours, and it's therefore obvious that the deployment
  cycle needs to be much faster than that if we are to succeed with Kanban.
  A well-designed DevOps Continuous Delivery pipeline can deploy code
  from being committed to the code repository to production in the order of
  minutes, depending on the size of the change.

When constructing deployment pipelines, for example, keep in mind why we are
building them in the first place. The goal is to allow people to interact with new
systems faster and with less work. This, in turn, helps people with different roles
interact with each other more efficiently and with less turnaround.

sprint retrospective, where the team gets
together and discusses what went well and what could have gone better during the
sprint. Spend some time here to make sure you are doing the right thing in your
daily work.

DevOps fits well together with many frameworks for Agile or Lean enterprises.
Scaled Agile Framework, or SAFe® , specifically mentions DevOps.

ITIL, which was formerly known as Information Technology Infrastructure Library,
is a practice used by many large and mature organizations.

ITIL is a large framework that formalizes many aspects of the software life cycle.
While DevOps and Continuous Delivery hold the view that the changesets we
deliver to production should be small and happen often, at first glance, ITIL would
appear to hold the opposite view. It should be noted that this isn't really true. Legacy
systems are quite often monolithic, and in these cases, you need a process such as
ITIL to manage the complex changes often associated with large monolithic systems.

## The DevOps process and Continuous Delivery – an overview

For the time being, it is enough to understand that when we work with DevOps,
we work with large and complex processes in a large and complex context.

the developer environments and the Continuous
Integration environment, are normally very similar.

The number and types of testing environments vary greatly. The production
environments also vary greatly.

The developers (on the far left in the figure) work on their workstations.
They develop code and need many tools to be efficient.

Ideally, they would each have production-like environments available to work with
locally on their workstations or laptops. Depending on the type of software that is
being developed, this might actually be possible, but it's more common to simulate,
or rather, mock, the parts of the production environments that are hard to replicate.
This might, for example, be the case for dependencies such as external payment
systems or phone hardware.

Vagrant, AWS cloud, Docker.

My personal preference is to use a development environment
that is similar to the production environment. If the production
servers run Red Hat Linux, for instance, the development
machine might run CentOS Linux or Fedora Linux.

### The revision control system

The revision control system is often the heart of the development environment.

- continouous itegration
- source management
- artifact repository

### The build server

The build server is conceptually simple. It might be seen as a glorified egg timer that
builds your source code at regular intervals or on different triggers.
The most common usage pattern is to have the build server listen to changes in the
revision control system.
When a change is noticed, the build server updates its local
copy of the source from the revision control system. Then, it builds the source and
performs optional tests to verify the quality of the changes. This process is called
**Continuous Integration**.

Jenkins

### The artifact repository

When the build server has verified the quality of the code and compiled it into
deliverables, it is useful to store the compiled binary artifacts in a repository.
This is normally not the same as the revision control system.
In essence, these binary code repositories are filesystems that are accessible over the
HTTP protocol. Normally, they provide features for searching and indexing as well
as storing metadata, such as various type identifiers and version information about
the artifacts.

### Package managers

Red Hat-like systems use a package format called RPM. Debian-like systems use
the .deb format, which is a different package format with similar abilities. The
deliverables can then be installed on servers with a command that fetches them
from a binary repository. These commands are called package managers.

### Test environments

Test environments should normally attempt to be as production-like as is feasible.
Therefore, it is desirable that the they be installed and configured with the same
methods as production servers.

### Staging/production

Staging environments are the last line of test environments. They are interchangeable
with production environments. You install your new releases on the staging servers,
check that everything works, and then swap out your old production servers and
replace them with the staging servers, which will then become the new production
servers. This is sometimes called the blue-green deployment strategy.

Sometimes, it is not possible to have several production systems
running in parallel, usually because production systems are very expensive.

QA Team (staging) -> Operations Team (Production)

Not all organizations have the resources to maintain production-quality staging
servers, but when it's possible, it is a nice and safe way to handle upgrades.

### Release management

We have so far assumed that the release process is mostly automatic. This is the
dream scenario for people working with DevOps.
This dream scenario is a challenge to achieve in the real world. One reason for this
is that it is usually hard to reach the level of test automation needed in order to
have complete confidence in automated deploys. Another reason is simply that the
cadence of business development doesn't always the match cadence of technical
development. Therefore, it is necessary to enable human intervention in the
release process.

## Scrum, Kanban, and the delivery pipeline

Our pipeline can manage both the following types of scenarios:
•
The build server supports the generation of the objective code quality metrics
that we need in order to make decisions. These decisions can either be made
automatically or be the basis for manual decisions.
•
The deployment pipeline can also be directed manually. This can be handled
with an issue management system, via configuration code commits, or both.

---

- The development team has been given the responsibility to develop a change
  to the organization's system. The change revolves around adding new roles
  to the authentication system. This seemingly simple task is hard in reality
  because many different systems will be affected by the change.

- To make life easier, it is decided that the change will be broken down into
  several smaller changes, which will be tested independently and mostly
  automatically by automated regression tests.
- The first change, the addition of a new role to the authentication system,
  is developed locally on developer machines and given best-effort local
  testing. To really know if it works, the developer needs access to systems
  not available in his or her local environment; in this case, an LDAP server
  containing user information and roles.
- If test-driven development is used, a failing test is written even before any
  actual code is written. After the failing test is written, new code that makes
  the test pass is written.
- The developer checks in the change to the organization's revision control
  system, a Git repository.
- The build server picks up the change and initiates the build process. After
  unit testing, the change is deemed fit enough to be deployed to the binary
  repository, which is a Nexus installation.
- The configuration management system, Puppet, notices that there is a new
  version of the authentication component available. The integration test server
  is described as requiring the latest version to be installed, so Puppet goes
  ahead and installs the new component.
- The installation of the new component now triggers automated regression
  tests. When these have been finished successfully, manual tests by the quality
  assurance team commence.
- The quality assurance team gives the change its seal of approval. The change
  moves on to the staging server, where final acceptance testing commences.
- After the acceptance test phase is completed, the staging server is swapped
  into production, and the production server becomes the new staging server.
  This last step is managed by the organization's load-balancing server.

The process is then repeated as needed. As you can see, there is a lot going on!

## Identifying bottlenecks

Identify problem areas.
When things are not working well, a deploy can take days of unexpected hassles.
Here are some possible causes:

- Database schema changes.
- Test data doesn't match expectations.
- Deploys are person dependent, and the person wasn't available.
- There is unnecessary red tape associated with propagating changes.
- Your changes aren't small and therefore require a lot of work to deploy
  safely. This might be because your architecture is basically a monolith.

# How DevOps Affects Architecture

## Introducing software architecture

A functional requirement and non-functional requirement.
Here are two of the non-functional requirements that DevOps and Continuous
Delivery place on software architecture:

- We need to be able to deploy small changes often
- We need to be able to have great confidence in the quality of our changes

# Source

- Joakim Verona, _Practical DevOps_, ISBN 978-1-78588-287-6, Packt Publishing 2016

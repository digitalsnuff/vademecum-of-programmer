# [Home](../README.md)

## Parse

Jak przegladarka parsuje dokument html?

- Po wczytaniu dokumentu parsuje element po elemencie.
- jeśli napotka element `img` wczytuje go w tle i nie przerywa parsowania dalszej części dokumentu
- jeśli napotka element `script`, przykładowo, zadeklarowany w elemencie `head`, przerywa parsowanie, dopóki nie wczyta się cały skrypt. Potem ten skrypt jest wykonywany a następnie następuje kontynuacja parsowania dalszej części dokumentu. Jeżeli element `script` jest umieszczony w dowolnym miejscu elementu `body`, dzieje się analogicznie. Dlatego dobrą praktyką jest umieszczanie elementów skrypt przez znacznikiem zamykającym `body`.

### Async

- Jeżeli do elementu `script` dodamy atrybut `async`, podczas parsowania dokumentu nie następuje przerwanie w momencie wczytywania skryptu. Parsowanie dokumentu nie jest przerywane a wczytanie skryptu nastpuje w tle, jako oddzielny proces w tym samym czasie co parsowanie. Po wczytaniu powyższych, następuje wykonanie skryptów, zaraz po ich wczytaniu, co przerywa parsowanie. Po wykonaniu następuje kontynuacja parsowania.
- Niebezpieczeństwo polega na wykonaniu wczytanego skryptu nie wtedy, kiedy się tego spodziewaliśmy.

### Defer

- Jeżeli do elementu `script` dodamy atrybut `defer`, parsowanie dokumentu nie jest przerywane, wczytywanie skryptu jest wykonywane w tle a wykonanie kodu następuje dopiero po przeparsowaniu sałego dokumentu

## ES6 Modules

Exports:

```js
export default ClassName
export {func1(), func2()}
```

Plik js jako moduł posiada funkcje eksportu funkcjonalności w taki sposób, że eksportuje domyślną funkconalość, jak też inne dołączone do listy. Funkcjonalności moga być prefiksowane.

Jeżeli chcemy importować skrypty jako moduły z poziomu przeglądarki, plik ze skryptem imporującym musi być tylu `module`

```js
<script type="module" defer src="main.js"></script>
```

Skrypt `main` może importować inne pliki jako moduły w następujący sposób:

```js
import User from "/user.js";
```

Jeżeli w pliku importowanym, załóżmy, `User` domyślna eksportowana klasa nazywa się `User`, to przy importcie domyślnej klasy, jeżeli użyjemy nazy `U`, domyślna klasa `User` zostanie zaimportowana jako `U`.

Jeżeli importujemy składniki inne niż `default` należy je importować dosłownie, umieszczjąc je w nawiasach klamerkowych

```js
import U, { printAge, printName } from "/user.js";
```

Możemy przy takim imporcie zastosować aliasy:

```js
import U, { printAge as printUserAge, printName } from "/user.js";
```

`Nomodule`

- Przeglądarka domyślnie ustawia typ skryptu na `text/javascript`. Deklaracja typu: `type="module"` nie nadpisuje typu, lecz jedynie inforuje przeglądarkę o charakterze parsowania i wykonywania skryptu, jako moduł.
- atrybut `nomodule` jest zerojedynkowy, który wstrzymuje wykonywanie skryptu w przeglądarce, która wspiera skrypty modułowe.
- `nomodule` do oddzielenia skryptów dla nowoczesnych przeglądarek wpierających moduły od starszych, które modułów nie obsługują. Tak więc

  ```js
  <script type="module" src="app.js"></script>
  <script nomodule src="classic-app-bundle.js"></script>
  ```

  W mowoczesnej przeglądarce zostanie wykonany skrypt, jako moduł a zosyanie pominięty drugi sprypt zadeklarowany w elemencie z atrybutem `nomodule`. Odwracając sprawę: starsze przeglądarki zignorują typ `module` i pomina wykonywanie skryptu z racji, że jest to nieznany typ a wykonają skrypt oznaczony elemecie atrybutem `noscript`, jednocześnie ignorując ten atrybut.

# Source

[What Is The Fastest Way To Load JavaScript](https://www.youtube.com/watch?v=BMuFBYw91UQ)

[V8 - Modules are deferred by default](<https://v8.dev/features/modules#defer()>)

[Script element](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/script)

[Subresource Integrity](https://developer.mozilla.org/en-US/docs/Web/Security/Subresource_Integrity)

[Content-Security-Policy (CSP)](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/script-src)

[JavaScript Modules](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Modules)

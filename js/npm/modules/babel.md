## [Home](/README.md)

---

# Description

Babel is a JavaScript compiler. Babel is a toolchain that is mainly used to convert ECMAScript 2015+ code into a backwards compatible version of JavaScript in current and older browsers or environments.

Babel can convert JSX syntax! Check out our React preset to get started.

---

# Presets (plugins)

Babel is a compiler (source code => output code). Like many other compilers it runs in 3 stages: parsing, transforming, and printing.

The [plugins](https://babeljs.io/docs/en/plugins) are for convert to plugin standard format.

---

# Babel tools

- [https://babeljs.io/](https://babeljs.io/) - page with runtime compiler to browser-compatible JavaScript

# Tworzenie kodu do wielokrotnego użycia

JavaScript wpisuje sie w idee obiektowego programowania klasycznego, jednakże posiada też własne, często unikatowe cechy.

Inne wzorce zarządzaniania kodem, czyli unikanie zaśmiecania globalnej przestrzeni nazw oraz unikanie zmiennych globalnych.

Przestrzeń nazw.

Wzorce wywołań: moduły, wyrażenia funkcyjne wywoływane natychmiast (`IIFE - function expressions`)

Narzędzia do zarządzania wieloma plikami JS.

## Obiektowy JavaSript

JS jest językiem prototypowym a nie klasycznym.

Język klasyczny, taki jak Java, wymaga klas we wszystkim, co programujemy.

W JS wszystko posiada prototyp, czyli jest ot język prototypowy.

Jęzkyk obiektowy udostępnia wzorce użycia, które pozwalają na łatwiejsze współdzielenie kodu, a także eliminuje powtarzalny wysiłek.

Każdy typ w JS (obiekt, funkcja, data, etc.) posiada prototyp.  
Standard ECMAScript definiuje, że właściwość ta jest ukryta i można się do niej odwołać poprzez konstrukcję [[Prototype]].  
Prototym mozna odczytać za pomocą niestandartowej właściwości `__proto__` lub właściwości `prototype`.  
Wraz ze standardem _ECMAScript 6_ konstrukcja `__proto__` jest oficjalną właściwością typów i powinna być dostępna w każdej zgodnej z nią implementacji.  
Inną metodą jest dostęp do właściwości `prototype` niektórych typów. Wszystkie główne typy JS (Date, String, Array, etc.) dysponują publiczną właściwością `prototype`.  
Dowolny typ JS (utworzony za pomocą konstruktora funkcyjnego), ma publiczna właściwość `prototype`. Jednakże instancje tych typów nie mają tej właściwości. Dzieje sie tak, gdyż w przypadku instancji właściwość `prototype` nie jest dostępna.

Do `literału obiektowego` możemy się odwoływac, jakby był on klasą. Możemy stworzyć wiele obiektów zgodnych ze strukturą wyznaczoną przez ten literał. Aby odwołac się do literału jako typu, tworzymy jego instancję za pomocą `Object.create`.  
Pomiędzy utworzonym obiektem literałowym a typem o tej samej nazwie istnieje związek: właściwość `[[Prototype]]`. Właściwość `__proto__` nowo utorzonego obiektu wskazuje na obiekt literałowy.

Funkcja `Object.create` została dodana do języka JS wraz ze standardem ECMAScript 5 w celu uproszczenia i zwiększenia przejrzystości relacji pomiedzy obiektami. Umożliwia jednokrokowe tworzenie relacji między obiektami. W językach obiektowych tworzenie relacji odbywa się na poziomie klas.

**Prototype chain** (łańcuch prototypów):

- jest jednym z dwóch miejsc, gdzie odwołujemy się, by sprawdzić wartość właściwości obiektu. Silnik sprawdza w samym obiekcie a potem w łańcuchu prototypów.
- mamy tu do czynienia z **delegacją zachowania**, niż z relacją dziecko-rodzic. Relacja delegacji jest tworzona poprzez użycie `Object.create`. Jest to tworzenie relacji pomiędzy typem nadrzędnym i typem podrzędnym.

## Dziedziczenie

Wydaje się, że dziedziczenie za pomocą `Object.create` odbywa się z dołu do góry a nie z góry do dołu.
Argument `Object.create` to obiekt będący typen nadrzędnym.
Zwrócona wartość będzie typem podrzędnym.
Typ nadrzedny może dodać nową funkcjonalność, usunąc istniejącą lub ją zmodyfikować.
Dla sprawdzenia typu operator `instanceof` nie zadziała. Polega on bowiem na właściwości `prototype`, której używa do wyszukiwania relacji obiektu z typem. Prawy argument operatora `instanceof` musi być funkcją (może być konstruktorem funkcyjnym). lewy argument musi być czymś, co zostało utworzone z konstruktora funkcyjnego.  
W tym wypadku przychodzi nam z pomocą funkcja `isPrototypeOf`. Może być wywołaywana na dowolnym obiekcie. Jest ona obecna we wszystkich obiektach JavaScript, analogicznie do funkcji `toString`.  
Funkcja `getPrototypeOf` zwraca referencję do typu, który stanowi podstawę bieżącego obiektu. Do tego możemy też użyć właściwości `__proto__`.  
Właściwość `super` służy do umożliwienia dostępu metodzie nadpisanej do metody klasy nadrzędnej. W ten sposób w przypadku nadpisania metody można wywołać jej oryginalna wersję. Jednakże w JS nie mamy takiej możliwości. Kod obiektowy bazujący na prototypie nie przewiduje funkcji `super()`. Ten problem można rozwiązać na trzy sposoby:

- napisanie metody `super()`
- wywołanie bezpośrednie metody rodzica
- widoczność składowych obiektu

## Widoczność składowych obiektu

W JS rozróżniamy dwa zakresy:

- globalny
- zakres właśnie wykonywanej funkcji

Uzyskiwanie efektu właściwości prywatnych. Definiowanie specjalny poziom dostępu dzięki użyciu `funkcji uprzywilejowanych`.  
W domknięciach, w funkcjach tworzących obiekt nalezy utowrzyć składowe prywatne, używając słowa `var`. W tym zakresie tworzymy funkcję, która w niejawny sposób będzie miała dostęp do danych prywatnych ze względu na ten sam zakres. Tę funkcję należy dodac do obiektu, czyniąc funkcje publiczną.

## Przestrzenie nazw

Hermetyzacja w oddzielnym kontekście.  
Przestrzeń nazw w JS udostępnia kontekst dla zmiennych oraz funkcji.
Przestrzeń nazw sama w sobie jest globalna.  
Implementacja będzie użycie literału obiektowego w celu hermetyzacji kodu, który chcemy ukryć w kontekście globalnym.

## Wzorzec modułowy

Heremtyzuje tworzenie przestrzeni nazw wewnątrz funkcji.
Dynamiczne generowanie przestrzeni nazw przez funkcje.
Moduł to nic innego jak funkcja w której jest zawarty kod: przestrzenie nazw, zmienne, funkcje. Cały kod jest łączony w całość w klasie, która jest ostatecznie zwracana przez tę funkcję.

## Wyrażenia funkcyjne wywoływane natychmiast (IIFE)

Funkcje tworza swój własny kontekst wykonawczy, dlatego też doskonale nadają się do odśmiecania przestrzeni globalnej.  
Umieszcamy funkcję wewnątrz nawiasów, przez co stanowi to wskazówkę dla parsera, ze jest to wyrażenie a nie instrukcja. Nie mamy tu do czynienia z deklaracjami funkcji, które rezerwowały określone nazwy wskazujące instrukcje. Dlatego też nawiasy zawierają kod, który bedzie wykonywany jako wyrażenie a nie same instrukcje.  
W celu natychmiastowego wykonania kodu stawiamy dodatkowy zestaw nawiasów zaraz po nawiasach, w których jest funkcja, lub zaraz po funkcji wewnątrz nawiasów.  
Wyrażenia umieszone są w nawiasach w celu ich "wyrażenia" lub innymi słowy "oddelegowania".  
Drugi zestaw nawiasów jest użyty w celu wywołania wyrażenia.

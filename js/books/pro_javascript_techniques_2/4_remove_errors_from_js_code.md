# Usuwanie błędów z kodu JavaScript

## Narzędzia testujące

Wszystkie nowoczesne przeglądarki dysponują pewnego rodzaju zestawem narzędzi dla programisty.
Są nimi:

- konsola
- debugger
- inspektor DOM
- analizator sieci
- profiler

Poziomy logowania konsoli:

- debug
- info
- warn
- error
- log

### Konsola

Konsola posiada funkcję `dir()`, która udostępnia rekurencyjny i oparty na drzewie interfejs do obiektu.

Nalezy ustalić wzorce użycia funkcji konsoli.

Używajmy konsoli do logowania informacji o stanie aplikacji.

Konsola zawiera API wiersza poleceń:

- debug([nazwaFunkcji]) - w chwili wywołania funkcji nazwaFunkcji debugger automatycznie rozpocznie swoje działanie przed pierwszym wierszem kodu tej funkcji
- undebug([nazwaFunkcji]) - zatrzymanie debugowania funkcji o nazwie nazwaFunkci
- include(url) - dołącza do strony zdalny skrypt, np. bibliotekę testującą, każdy inny kod.
- monitor([nazwaFunkcji]) - włącza logowanie dla wszystkich wywołań funkcji nazwaFunkcji. powoduje zalogowanie nazwy funkcji, jej argumentów, a także ich wartości.
- unmonitor([nazwaFunkcji]) - wyłączalogowanie dla wszystkich wywołań funkcji nazwaFunkcji.
- profile([tytuł]) - Włącza profiler JS. Dla profilu można nadać tytuł.
- profileEnd() - kończy dizałanie uruchomionego profilu i wyświetla raport, który może zawierać tytuł określony w wywołaniu profilu.
- getEventListeners(element) - dla każdego elementu pobiera listę funkcji nasłuchujących zdarzeń.

### Debugger

Ma umożliwić programiście zapauzowanie kodu i sprawdzenie jego obecnego stanu.  
Najprostszym sposobem na uruchomienie debugera jest ustawienie punktu przerwania: `breakpoint`.

Referencja występuje wtedy, gdy obiekt posiada własciwość, której wartością jest inny obiekt.  
Obiekty, które nie są używane w żadnej referencji, są automatycznie czyszczone.  
Żeby usunąć referencję można użyć słowa kluczowego `delete`.

# Funkcje, cechy i obiekty

W języku JavaScript podstawowymi jednostkami są obiekty.
W rzeczywistości prawie wszystko w JS jest obiektem i komunikuje się na poziomie obiektorym.

## referencja i wartość

Zmienne w JS przechowują dane albo za pomocą **kopii**, albo **referencji**,
Każda wartość prymitywna jest kopiowana do zmiennej.

Wartości prymitywne to:

- łańcuchy znakowe,
- liczby,
- wartości logiczne
- wartość pusta (null)
- wartość niezdefiniowana (undefined)

Najważniejszą cecha typów prymitywnych jest to, że są one przypisywane, kopiowane i przekazywane oraz zwracane z funkcji przez wartość.

Cała pozostała część JS bazuje na referencjach.
Jeżeli zmienna nie przechowuje wartości prymitywnych to przechowuje referencje do obiektu.
`Referencja` jest wskaźnikiem do miejsca w pamięci obiektu (tablicy, daty lub czegokolwiek innego). Rzeczywisty obiekt jest nazywany `referentem`.
Zmiany wprowadzone do referenta przez jedną zreferencji są uwzględniane również w innych referencjach.
Referencja wskazuje tylko na obiekt referenta a nie na inną referencję.
Wskazanie jest zawsze na obiekt podstawowy.
W przypadku konkatenacji powstaje nowy obiekt a nie zachodzi modyfikacja referenta. Łańcuchy znakowe zachowują się jak obiekty, jednak ich wykorzystywanie ze zmiennymi lub funkcjami przypominają typy prymitywne.

Obiekty mają dwie cechy: `właściwości` i `metody`. Są ogólnie nazywane `elementami obiektu`.
`Właściwości` zawierają dane obiektu. Moga być typami prymitywnymi, jak też obiektami.
`Metody` są funkcjami, które działają na danych obiektu.

Obiekty mogą modyfikować swoja zawartość, jednakże jest to bardzo rzadkie.

## zakres

`Zakres` - to przestrzeń widoczności zmiennej.
W JS istnieją dwa zakresy:

- funkcjonalny
- globalny

Funkcje mają swój własny zakres.
Bloki (instrukcje while, if, for) nie posiadają go.

`global` jest to zmienna superglobalna dla cli (nodejs).
`window` to zmienna superglobalna w przeglądarkach.
`Zmienne globalne` w przeglądarkach to właściwości obiektu `window`.

Należy zawsze inicjalizować zmienne dla zachowania zakresu. W innym wypadku zmienne są domyślnie w zakresie globalnym.

Każda zmienna zadeklarowana wewnątrz funkcji posiada swoją deklarację podniesioną do najwyższego zakresu. JS używa takiego podejścia w celu upewnienia się, że nazwa zmiennej jest dostępna w całym zakresie.

## domknięcie

`Domknięcia` to środki, za pomocą których funkcja wewnętrzna może odwoływać się do zmiennych obecnych w zawierającej ją funkcji zewnętrznej (w sytuacji, gdy funkcje nadrzędne zakończyły juz swoje działanie).
Domknięcia dostarczaja kontekst dostępny jedynie w oczekiwany przez nas sposób.
Idea domyślnego wypełniania wielu argumentów funkcji poprzez stworzenie nowej prostszej funkcji.

Używanie zmiennych w przestrzeni globalnej uchodzi za złą praktykę., ponieważ te wszystkie dodatkowe zmienne mogą w ukryciu przeszkadzać w działaniu innych bibliotek i powodować tym samym szereg problemów.
Dzieki użyciu wykonującej się samodzielnie funkcji anonimowej można ukryć wszystkie zmienne globalne przed odkryciem przez inny kod.

Domkniecie pozwala na odwołanie do zmiennych, które istnieją w funkcji nadrzędnej. Nie udostępnia, jednakże, wartości zmiennej w chwili jej tworzenia, a jedynie udostępnia ostatnią wartość zmiennej w funkcji nadrzędnej (np. wykonywanie pętli).

Domknięcie jest kombinacją funkcji i `leksykalnego środowiska` w którym ta funkcja została zdeklarowana. To środowisko zawiera każdą zmienną lokalną która była w zasięgu w momencie kiedy domknięcie zostało stworzone.

W przypadku, gdy funkcja zwraca funkcję, jako domknięcie, to zmienna, do której jest przypisany wynik działania funkcji nadrzędnej staje się `referencją` do instancji funkcji wewnętrznej, zwróconej w wyniku. Instancja funkcji z wewnątrz zarządza referencja do jej leksykalnego środowiska, w którym istnieje zmienna.

W JavaScript jest możliwa emulacja prywatnych metod przy użyciu domknięć.
można użyć domknięć do zdefiniowania publicznych funkcji, które mają dostęp do prywatnych funkcji i zmiennych. Używanie dokmnięć w taki sposób znane jest jako `module pattern`.

## kontekst

`Kontekst` - zakres, w którym działa kod.
Do kontekstu można odwołać się za pomocą zmiennej `this` zawsze wskazującej kontekst, w którym znajduje się wykonywany kod.
Skoro zmiene globalne to właściwości obiektu `window`. Oznacza to, że są w kontekście globalnym.

Dla przeglądarek globalnym kontekstem będzie zawsze obiekt `window`.
W Nodejs zakres globalny posiada tak na prawdę zakres modułu. Moduł jest kontekstem wywoływania kodu. Dlatego najwy ższym zakresem będzie moduł. Jest to `module.exports object`.

JS dysponuje dwiema metodami, które pozwalają uruchomić funkcję w dowolnie wybranym określonym kontekście:

- call
- apply

## Przeciązanie funkcji oraz sprawdzanie typów

`Przeciążanie funkcji` służy w językach obiektowych wykonnywania odmiennej logiki w zależności od rodzaju lub liczby przekazanych argumentów.
Wewnatrz dowolnej funkcji JS istnieje kontekstowa zmienna o nazwie `arguments`. Działa ona w charakterze obiektu przypominającego tablicę i zawierajacego wszystkie argumenty przekazane do funkcji.
Obiejt `arguments` nie jest prawdziwą tablicą, ponieważ nie uzywa prototypu Array, a także nie dysponuje funkcjami dostępu do tablicy w rodzaju push lun indexOf.
Każdy niezdefiniowany argument ma wartość `undefined`.
Kopiowanie obiektu `arguments` do tablicy:

```js
var argsArray = Array.prototype.slice.call(arguments, 0);
```

Pamiętamy, by przy sprawdzeniu używać zawsze `typeof`. Dla Obiektów i tablic możemy uzyć `instanceof`.
Innym sposobem jest `Object.isPrototypeOf()`.

## Nowe narzędzia obiektowe

Nowe nNarzędzia moga być używane na literałach obiektorych (przypominających struktury danych), jak i na instancjach obiektów (literal notation vs function construnctor).
Obiekty stworzone za pomocą litarału są `singletons` To znaczy, że kiedy zachodzi zmiana na obiekcie, zmienia ona objekt w całym skrypcie.
Objerkt zdefiniowany za pomocą konstruktora funkcji daje nam liczne instancje tego objektu. To znaczy, że zmiany zachodzą tylko w danej instancji i nie mają miejsca w innych instancjach.

Objekty sa podstawą JavaScript. Praktycznie wszystko w tym języku jest objektem.
Na najbardziej podstawowym poziomie obiekty istnieją jako kolekcja własciwości w sposób bardzo przypominający tablice asocjacyjne w innych językach.

Istnieja trzy metody, dzieki któryym możemy kontrolować, czy objekt może być kontrolowany.

Obiekt w JS może być modyfikowany w dowolnym momencie.

- `Object.preventExtensions()` - - uniemozliwia dodawanie do obiektu nowych właściwości, jednakże możemy uzywać już istniejących. Usiłowanie dowania właściwości spowoduje zwrócenie błędu `TypeError` lub też zostanie zasygnalizowane (np. bez `'use strict'`).
- `Object.seal()` - własciwości nie tylko nie moga zostac dodawane, ale też nie mogą byćusunięte ani przekształcone w metody dostępowe. Generuje `TypeError`
- `Object.freeze()` -nie moga być dodawane, usuwane ani modyfikowane żadne właściwości. Powoduje zwrócenie błedu typu `TypeError`. UWAGA: Jeżeli właściwość jest objektem, wtedy może być uaktualniona. Takie rozwiązanie nazywa się `płytkim zamrożeniem` (`shallow freeze`).

# Document Object Model

DOM stanowi zasadniczy element zestawu narzędzi zawodowego programisty JS.

Interfejs DOM stał się oficjalnym interfejsem nie tylko dokumentów HTML, ale także XML.

DOM Level 4.

Przetwarzanie kodu JavaScript blokuje odświeżanie strony. Dlatego też skrypty JS umieszczamy przed znacznikiem zamykającym `</body>`.  
W czasie analizowania kodu JavaScript przeglądarki nie mogą odświerzać innych elementów strony.

## Struktura DOM

### Stałe

<table>
    <thead>
        <tr>
            <th>NodeType</th>
            <th>Named Constant</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1</td><td>ELEMENT_NODE</td><td></td>
        </tr>
        <tr>
            <td>2</td><td>ATTRIBUTE_NODE</td><td>deprecated</td>
        </tr>
        <tr>
            <td>3</td><td>TEXT_NODE</td><td></td>
        </tr>
        <tr>
            <td>4</td><td>CDATA_SECTION_NODE</td><td>deprecated</td>
        </tr>
        <tr>
            <td>5</td><td>ENTITY_REFERENCE_NODE</td><td>deprecated</td>
        </tr>
        <tr>
            <td>6</td><td>ENTITY_NODE</td><td>deprecated</td>
        </tr>
        <tr>
            <td>7</td><td>PROCESSING_INSTRUCTION_NODE</td><td></td>
        </tr>
        <tr>
            <td>8</td><td>COMMENT_NODE</td><td></td>
        </tr>
        <tr>
            <td>9</td><td>DOCUMENT_NODE</td><td></td>
        </tr>
        <tr>
            <td>10</td><td>DOCUMENT_TYPE_NODE</td><td></td>
        </tr>
        <tr>
            <td>11</td><td>DOCUMENT_FRAGMENT_NODE</td><td></td>
        </tr>
        <tr>
            <td>12</td><td>NOTATION_NODE</td><td>deprecated</td>
        </tr>
    </tbody>
</table>

Każdy z węzłów zawiera właściwość zwaną odpowiednio `nodeType`.  
Najważniejsze węzły to: **dokumenty**, **elementy**, **atrybuty** oraz **tekst**. Każdy z tych elementów dysponuje swoim własnym typem implementacyjnym: odpowiednio `Document`, `Element`, `Attr`, `Text`.

Atrybuty są specjalnym przypadkiem. W DOM Level 4 nie implemenuje interfejsu `Node`.

Element `Document` zarządza całością dokumentu HTML. Każdy znacznik w dokumencie jest obiektem typu `Element`, wyspecjalizowanym w reprezentowaniu okreslonych typów elementów HTML.

Atrybuty elementu reprezentowane są jako instancje typu `Attr`.

Dowolny tekst wewnątrz elementu stanowi węzeł tekstowy reprezentowany przez typ `Text`.

Są to podtypy dziedziczące z typu `Node`.

`Doctype` jest instancją znacznika DOCTYPE.

Element `html` jest pierwszym i głównym elementem.

Każdy pojedynczy węzeł modelu DOM zawiera **zbiór wskaźników**, które mogą być wykorzystane w celu odwołania się do jego krewnych.  
Wskaźniki służą również do przemieszczania się po drzewie DOM.

Jedynym wyjątkiem jest właściwość `childNodes`, stanowiący zbiór wszystkich węzłów dzieci bieżącego węzła.

Jeśli dowolna z tych relacji nie będzie zdefiniowana, wtedy wartość właściwości będzie wynosić `null`.

**Przeszukiwanie drzewa DOM** - dostęp do elementów za pomocą relacji. Jest to sposób nie polecany.



# Links

[HTML DOM nodeType Property](https://www.w3schools.com/jsref/prop_node_nodetype.asp)

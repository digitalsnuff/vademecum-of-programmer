# Zaawansowane techniki języka JavaScript

## Kim jest zawodowy programista?

Jest kimś, kto dobrze rozumie podstawy języka JavaScript (a także prawdopodobnie kilku innych języków) oraz jest zainteresowany jego wszechstronnością. Chciałby poznac dokładniej Obiektowy Model Dokumentu DOM oraz zapoznać się z MVC po stronie klienta.

## Historia

Od momentu swego powstania JavaScript był zależny od przeglądarki. Przegladarka była środowiskiem uruchomieniowym dla programów w tym języku.

Mozilla FireFox jest potomkiem Netscape.

Wsparcie implementacji różnych standardów zalecanych przez konsorcjum World Wide Web (W3C) przez Firefox i Chrome.

Twórcy zarówno Firefoxa, jak i Chrome'a przyczynili sie w dużym stopniu do rozwoju Europejskiego Stowarzyszenia Wytwórców Komputerów (European Computer Manufacturer Association, ECMA). ECMA jest ciałem standaryzującym, które nadzoruje rozwój języka JavaScript (ECMAScript, JavaScript jest znakiem handlowym firmy Oracle).
Wraz z postepem rywalizacji przeglądarek, zaczęto ponownie rozwijać standard ECMAScript. Wersja piąta tego standardu (z roku 2009) zawierała wiele zmian, które powstały w ciągu dziesięciu poprzednich lat, czyli od czasu opublikowania poprzedniej wersji standardu.
Wersja **5.1** powstała w czerwcu 2011 roku. Od roku 2012, wszystkie nowoczesne przegladarki całkowicie obsługują ECMAScript 5.1.
17 czerwca 2015, ECMA International opublikował **szóstą** główną wersję ECMAScript, która oficjalnie nazywa się `ECMAScript 2015`, ale początkowo była nazywana też `ECMAScript 6` lub `ES6`
`ECMAScript 7` został wprowadzony w 2016 roku.
`ECMAScript 8` został wprowadzony w 2018 r.

Przeglądarka Chrome używa silnika V8. Ogłosiła swój debiut w 2008 roku. V8 był szybszy od innych silników przeglądarek. Stał się podstawą narzędzia `Node.js`, które jest interpreterem języka JavaScript niezależnym od przeglądarki.
`Node.js` na początku był zaprojektowany jako serwer., który miał używać JavaScriptu jako swojego podstawowego języka aplikacji. Stał się jednak uniwaersalną platformą do uruchamiania wielu aplikacji stworzonych w JavaScript.

## Przeglądarki i narzędzia

Internet Explorer w wersji 9 jest zdecydowanie przeglądarka starego typu. Nie wspiera on wiekszości funkcjonalności ECMAScript 5.Nie dysponuje również API do obsługi zdarzeń z godnych z zaleceniami W3C.

### Rozkwit bibliotek

Ujednolicanie API dla programistów.

CORS (Cross Origin Resource Sharing) - nowy protokuł AJAX. Pozwala on AJAX'owi na wysyłanie żądania do innych serwerów niż te, z których obsługiwana była strona. Poprzednia praktyką było korzystanie z protokołu JSON-P.

Urządzenia mobilne dominują.

# Links

[JavaScript Mozilla Developer](https://developer.mozilla.org/pl/docs/Web/JavaScript)

# Source

John Resig, Russ Ferguson, John Paxton, Pro JavaScript Techniques, Second Edition, Apress 2015, HELION 2016, ISBN: 978-83-283-2086-4

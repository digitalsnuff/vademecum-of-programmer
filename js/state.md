# State Management

> For larger apps with a framework, you may need to learn methods to manage app-level state

- Redux, Context API
- Apollo (GraphQL Client)
- VueX
- NgRx

## Concepts

- Immutable State
- Stores
- Reducers
- Mutations
- Getters
- Actions
- Observables

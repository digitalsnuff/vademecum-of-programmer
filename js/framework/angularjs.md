# AngularJS

RIA - Rich Internet Applications - bogate aplikacje internetowe.

Aplikacje AngularJS są budowane na podstawie wzorca projektowego o nazwie MVC.

Aplikacja ma mieć następujuące cechy:

- łatwa w rozbudowie
- łatwa w konserwacji
- łatwa do przetestowania - testy jednostkowe jak też E2E
- zgodna ze standardami

Podstawy:

- programowanie sieciowe
- wiedza o HTML i CSS
- wiedza o JS

Komponenty AngularJS:

- moduły
- usługi

Tworzenie SPA, technologie AJAX i API RESTful, testy jednostkowe.

Yeoman - http://yeoman.io

AngularJS działa w każdej nowoczesnej przeglądarce internetowej.  
Budowane aplikacje sieciowe należy testować we wszystkich przeglądarkach internetowych, które mogą być używane przez użytkowników aplikacji.

Aplikacje są tworzone na podstawie co najmniej jednego modułu.
Moduł jest tworzony za pośrednictwem wywołania `angular.module()`. Argumentami są: nazwa modułu, tablica nazw modułów dołączanych jako zależności.
Moduł jest implementowany za pomocą atrybutu `ng-app`. Jeżeli używamy tylko AngularJS w aplikacji, atrybut umieszczamy w elemencie `html`.
Jeżeli do tworzenia aplikacji używamy innych frameworków lub bibliotek, umieszczamy atrybut w określonym elemencie dokumentu.
Angularowe atrybuty nazywamy `dyrektywami`.
Dyrektywy można używać z przedrostkiem `data-`, np. `data-ng-app` dla większego zachowania standardów HTML.

AngularJS obsługuje wzorzec MVC. Jest to podział na:

- model - dane aplikacji,
- kontroler - logika działająca na danych modelu
- widok - logika wyświetlania danych

Separacja danych od sposobu ich wyświetlania to kluczowa koncepcja MVC.

Model może zawierać logikę niezbędną do utworzenia, wczytania, przechowywania i modyfikowania obiektów danych.
W aplikacjach AngularJS logika często znajduje się po stronie serwera i jest wykonywana przez serwer WWW.

Logika odpowiedzialna za przechowywanie lub pobieranie danych stanowi część modelu.

Logika pomagająca w formatowaniu danych i ich wyświetlaniu jest częścią widoku.

Do zadań kontrolera należy reakcja na działania użytkownia, uaktualnianie danych w modelu oraz dostarczenie widokowi wymaganych danych.
Nie zawsze zachodzi potrzeba, by widoki miały dostęp do pełnego modelu.

`Scope` - zasięg. Jest to zbiór danych wyznaczony przez kontroler. Kontroler wyznacza zbiór danych dla widoku.

Utworzenie kontrolera następuje przez wywołanie metody `controller()` obiektu `Module`, zwróconego przez metodę `angular.module()`.
Argumentami metody są nazwa kontrolera oraz funkcja, która będzie wywołana w celu zdefiniowania funkcjonalności kontolera.

Wedle konwencji nazwa kontrolera powinna mieć postać `<Nazwa>Ctrl`, gdzie człon `<Nazwa>` pomoże w określaniucroli kontrolera w aplikacji.
Argumentami funkcji `controller()` jest nazwa oraz funkcja z argumentem `$scope`. Ma znaczenie specjalne, gdyż dostarcza dne i funkcje widokom.

Nazwy zmiennych zaczynające się od `$` oznaczają wbudowane funkcjonalności AngularJS.
Jest to wbudowana usługa, ktróra zazwyczaj jest samodzielnym komponentem dostarczającym funkcjonalności wielu komponentom.

W widoku następuje `data binding`, co umieszcza dane w widoku z modelu.
Technika `dołączania danych` lub `dołącznia modelu`.
`Dołączanie jednokierunkowe`.
Zawartością wyrażenia dołączania danych może być dowolne, prawidłowe polecenie języka JS. Nie zaleca się skomplikowanych wyrażeń.
Wyrażenia sa uzywane równiez zdyrektywami.

Filtry
Metoda `filter()` definiowana przez moduł jest użwana w celu utworzenia _fabryki_ filtrów, która zwraca funkcję stosowaną do filtrowania zbioru obiektów danych.

Zastosowanie argumentów wskazujących bibliotece AngularJS wymagane funkcje jest częścią podejścia nazywanego `wstrzykiwaniem zależności`.

`Promise` - obiekt używany do przedstawienia zadania, którego wykonywanie zakończy się w przyszłości.

Dwa podstawoe rodzaje aplikacji sieciowych:

- oparte na komunikacji dwukierunkowej (`round-trip`)
- pojedyncza strona

Metody definiowane przez obietnicę mozna traktowac jako zdarzenia.

jqLite

## MVC

- `separation of concerns` - podział odpowiedzialności

Wzorce to elastyczne narzedzia a nie sztywno zdefiniowane reguły. Kto tego nie rozumie, staje się fanatykiem wzorców.

Model:

- widoku - przedstawia dane przekazywane z kontrolera do widoku
- domeny - zawiera dane w domenie biznesowej
  wraz z operacjami, transformacjami i regułami dotyczącymi
  tworzenia, przechowywania oraz operowania na wspomnianych danych.
  Inaczej mówiąc jest to `logika modelu`.

Każdy z obszarów MVC może zawierać zarówno logikę, jak i dane, jednakże do tworzenia logiki są rekomendowane repozytoria obok encji. Będzie to jednak logika odpowiednia do zarządzania danymi.

Aplikacje biznesowe to domena biznesowa i model biznesowy.

Wiele modeli w aplikacjach AngularJS przesuwa logikę do serwera i wywołuje ją za pomocą usług sieciowych typu RESTful.

W standardzie HTML5 istnieje zdefiniowane API przeznaczone do obsługi trwałego magazynu danych.

Model w aplikacji zbudowanej w oparciu o wzorzec MVC powinien:

- zawierać dane domeny
- zawierać logikę odpowiedzialną za tworzenie danych domeny, ich modyfikowanie i zarządzanie nimi
- zapewniać jase API udostępniające dane modelu i możliwość przeprowadzania operacji na nich.

Model nie powinien:

- udostępniać szczegółowych informacji o sposobie pobierania danych lub zarządzania nimi (szczegóły mechanizmu przechowywania danych, usługa sieciowa nie powinna być oferowana kontrolerom i widokom.)
- zawierać logiki odpowiedzialnej za transforamcje modelu na podstawie działań podejmowanych przez użytkownika (jest to zadanie kontrolera)
- zawierać logiki przeznaczonej do wyświetlania danych użytkownikowi (jest to zadanie widoku)

Najlepsze modele domeny zawierają logikę przeznaczoną do pobierania i trwałego przechowywania danych,
a także do przeprowadzania operacji tworzenia, odczytu, uaktualnienia i usuwania danych (CRUD).

Kontrolery dodają logikę domeny biznesowej (nazywaną `funkcjami`, `akcjami`) do `zakresu`, który jest podzbiorem modelu.

Kontroler powinien:

- zawierać logikę wymaganą do inicjalizacji zakresu
- zawierać logikę i funkcje niezbędne widokowi do wyświetlania danych zakresu
- zawierać logikę i funkcje niezbędne do uaktualnienia zakresu na podstawie działań podejmowanych przez użytkownika

Kontroler nie powinien:

- zawierać logiki przeznaczonej do przeprowadzania opracji na obiektowym modelu dokumentu (zadanie widoku)
- zawierać logiki odpowiedzialnej za zarządzanie trwałym magazynem danych (model)
- operować na danych pochodzących spoza zakresu

Dane widoku (dane modelu widoku, model widoku).

Metoda GET - `nullipotent` - dane tylko pobieramy i nie modyfikujemy.
Metoda PUT i DELETE - `idempotent` - wiele identycznych żądań będzie miało taki sam efekt.

`Deklaracja zależności` lub `wstrzykiwanie zależności`  
`Rozwiązywanie zależności`

`API fluent`

`Dyrektywa` to samodzielna, gotowa do wielokrotnego użycia jednostka funkcjonalności będąca sercem w programowaniu z użyciem AngularJS.

- przedwczesna optymalizacja
- translacja optymalizacji

Komponenty AngularJS:

- moduły
- dyrektywy
- filtry
- fabryki
- usługi

Moduł jest komponentem najwyższego poziomu.
Wiąże aplikację ze wskazanym fragmentem dokumentu HTML.
działa w charakterze bramy do kluczowych funkcji biblioteki.
Pomaga w organizowaniu kodu i komponentów w aplikacji.

`angular.module()` przyjmuje trzy argumenty:

- nazwa
- wymaga - konfiguracja modułów
- kofiguracja - odpowiednik wywołania `Module.config()`

Metody zdefiniowane przez obiekt `Module` dzielą się na trzy kategorie:

- metody definiujące komponenty dla aplikacji
- metody wspomagające tworzenie elementów konstrukcyjnych
  - `controller()`
  - `directive()` - pozwala na rozszerzenie możliwości HTML
  - `filter()` - używane w widokach w celu sformatowania danych
  - `service()` - usługa, funkcja konstruktora dostarczająca funkcjonalność w całej aplikacji. Tworzy nowy obiekt za pomocą `new`. Jest to zastosowanie w wielu obiektach szablonu dostarczonego przez funkcję konstruktora.
  - `factory()` - usługa, stosujemy funkcję fabryki, która zwraca obiekt usługi. Obiekty usług to wzorzec singleton. Deklarujemy przy module a wstrzykujemy jako di do kontrolera lub dyrektywy, jako argument funkcji zwrotnej.
  - `provider()` - podobnie jak `factory`, lecz wartością zwrotna funkcji fabryki musi być `obiekt dostawcy` definiujący metodę o nazwie `$get`, która z kolei musi zwracać obiekt usługi. Daje możliwość dodania funkcjonalności do metodt dostawcy, która może być używana do konfiguracji obiektu usługi.
  - `value()` - zamiast powoływać do życia samą zmienną należy w AngularJS tworzyć wartość za pomocą tej usługi ze względu na testowanie i di.
- pomagające w zarządzaniu cyklem życia aplikacji.
  - `Module.config()` - rejestruje funkcje wywoływane po wczytaniu modułu.Zadanie metody jest przeprowadzenie konfiguracji modułu, najczęściej przez wstrzyknięcie wartości podanych z servera (połączenia, uwierzytenienie)
  - `Module.run()` - rejestruje funkcje wywoływane po wczytaniu wszystkich modułów.

AngularJS wczytuje wszystkie moduły zdefiniowane w aplikacji a dopiero później przystępuje do rozwiązywania zaleznościi łączenia elementów konstrukcyjnych poszczególnych moduów.

`Dyrektywa` to cecha biblioteki AngularJS, definiuje ogólny styl oprogramowania z użyciem AngularJS oraz nadaje kształt aplikacji. Są to obłsługa zdarzeń, weryfikacja formularzy i praca z szablonami. Są uzywane w całej aplikacji.

Dyrektywy dołączania danych.

Dyrektywa `ng-include` pobiera z serwera fragment zawartości HTML, kompiluje go w celu ewentualnie znajdujących się tam dyrektyw, a następnie dodaje do obiektowego modelu dokumentu. Jest to praca z `widokami częściowymi`.

`ng-srcset`

Kontroler to ogniwo łączące model domeny i widoki. Kontrolery dostarczaja dane i usługi widokom, a także definiuja logikę biznesową wymaganą w celu przełożenia działań użytkownika na zmiany w modelu.

`$scope` nie jest usługą, ale obiektem dostarczanym przez usługę `$rootScope`. Z powodów praktycznych obiekt `$scope` jest uzywany jak usługa.

Kontroler monolityczny - jeden widok dla całej aplikacji.

Ponowne użycie kontrolera:

- umeszczamy dyrektywę `ng-controller` w różnych elementach widoku. Wtedy ten sam kontroler posiada dwa oddzielne egzemplarze z oddzielnym zasięgiem.

Każdy kontroler otrzymuje nowy zakres będący zakresem potomnym zakresu głównego. Zakresy są ułożone hierarchicznie. Zasięg główny pozwala na przekazywanie zdarzeń między zasięgami, czyli pozwala na `komunikację między kontrolerami`. Funkcja fabryki kontrolera z zasięgiem głównym otrzymuje dwa argumenty di: `$scope` i `$rootScope`.

- `$broadcast(name,args)` - przekazuje zdarzenie z zakresu bieżącego do wszystkich zakresów potomnych. Argumentami metody są nazwa zdarzenia i obiekt używany w celu dostarczenia danych towarzyszących zdarzeniu.
- `$emit(name, args)` - przekazuje zdarzenie z zakresu bieżącego do zakresu głównego.
- `$on(name, procedure)` - rejestruje funkcję obsługi, która będzie wywoływana po otrzymaniu przez zakres wskazanego zdarzenia.

Dziedziczenie kontrolerów:

- zagnieżdżanie kontrolerów potomnych w elementach kontolera nadrzednego
- pozwala na uniknięcie powtórzeń funkcjonalności w poszczególnych kontrolerach. Ten dziedziczący może wykorzystywać funkcjonalności tego nadrzędnego
- kontrolery potomne moga nadpisywać dane i fukcje kontrolerów nadrzędnych - dane i funkcje można zastapić wersjami lokalnymi o tej samej nazwie. Dana funkcja jest szukana w danym zakresie. Jeżeli jest znaleziona, wtedy jest wykonywana. Jeżeli jej nie ma, jest wyszukiwana w wyższym zasięgu w hierarchii.

Dziedziczenie prototypu:

- najpierw jest sprawdzane, czy istnieje właściwość lokalna. Jeśli nie, następuje szukanie w górę zakresów w celu sprawdzenia, czy istnieje dziedziczona wartość. Przy użyciu `ng-model` do modyfikacji wartości właściwości jest sprawdzane, czy dany zakres ją posiada. Jeśli jej nie znajdzie, przyjmowane jest założenie, że właściwość powinna być niejawnie zdefiniowana. Powoduje to istnienie dwóch właściwości w dwóch zakresach: nadrzednym i potomnym. Dlatego też funkcja zadeklarowana w zasięgu głównym bedzie operować na właściwości z tego właśnie zakresu. Dlatego też deklarowanie właściwości bezpośrednio w `$scope` nie będzie używana w zasięgu potmomnym przy użyciu `ng-model`. W tej sytuacji należy zadeklarować wspólną właściwość, która to dopiero będzie przyjmować wartości w postaci obiektu.

Kontroler bez zakresu:

- funkcja fabryki nie otrzymuje usługi `$scope`, lecz w jej ciele posługujemy się słowem kluczowym `this.`, zaś w widoku odwoujemy się do zakresu właściwości za pomocą nazwy kontrolera.

Metody integracji zakresu:

- `$apply(expr)` - wprowadza zmianę w zakresie. Oferuje integrację przychodzącą - zmiana w innym frameworku powoduje zmianę w AngularJS.
- `$watch(expr,procedure)` - rejestruje procedurę obsługi, która będzie powiadamiana, gdy zmianie ulegnie wartość we wskazanym wyrażeniu. Zapewnia integrację wychodzącą.
- `$watchCollection(obj,procedure)` - rejestruje procedurę obsługi, która będzie powiadamiana, gdy zmianie ulegnie wartość dowolnej właściwości we wskazanym obiekcie
- uzyskanie bezpośredniej kontroli nad procesem (integracja z innym frameworkiem)

Filter

- oryginalne dane zasięgu nie są modyfikowane
- formatują i filtrują dane
- formatują dane przed przetworzeniem przez dyrektywę i wyświetleniem w widoku

`Transkluzja` - wstawienie w dokumencie odwołania do fragmentu innego dokumentu.

`Usługi` - są używane w celu hermetyzacji funkcjonalności, która ma być ponownie wykorzystywana w aplikacji. `Cross-cutting concern` - zagadnienia przekrojowe. Jest to dowolna funkcjonalność, która wpływa lub na którą wpływa wiele komponentów. Jeżeli zachodzi potrzeba przygotowania funkcjonalności niepasującej do wzorca MVC, to nalezy utworzyć usługę.

`SCE (strict contextual escaping)` -

## Prosty serwer

```sh
# https://www.npmjs.com/package/connect
npm install connect
# https://www.npmjs.com/package/serve-static
npm install serve-static
```

Create file `server.js`

```js
var connect = require("connect");
var serveStatic = require("serve-static");

var app = connect();

app.use(serveStatic("../angularjs")).listen(5000);
```

Alternative usage with: `http`, `express`

## Testy jednostkowe

- Karma
- Jasmine framework

```sh
npm install -g karma
```

## Instalacja

Za pomocą `npm`:

```sh
npm install angular@1.8.0 angular-{touch,animate,mocks,route,sanitize}@1.8.0
```

## Development

```sh
npm install --save bootstrap jquery popper.js
npm install webpack webpack-cli --save-dev # v4 or later
npm i webpack-dev-server --save-dev
npm i html-webpack-plugin --save-dev
npm install --save-dev mini-css-extract-plugin
npm install --save-dev clean-webpack-plugin
npm install -D babel-loader @babel/core @babel/preset-env # https://github.com/babel/babel-loader
npm install exports-loader --save-dev # choose to import Bootstrap’s JavaScript plugins individually as needed
npm install autoprefixer css-loader node-sass postcss-loader sass-loader style-loader --save-dev # install the required loaders and postcss plugins for compiling and bundling Bootstrap precompiled Sass files

touch webpack.config.js

```

## Tools

- LiveReload - [http://livereload.com](http://livereload.com)
- Deployd - [http://deployd.com](http://deployd.com) - narzedzie przeznaczone do modelowania API dla aplikacji sieciowych. Zostało zbudowane na bazie Node.js i MongoDB
- nodemon - https://www.npmjs.com/package/nodemon
- browser-sync - https://www.npmjs.com/package/browser-sync

- Handlebars - https://handlebarsjs.com/guide/#what-is-handlebars - biblioteka JS do interpolacji w szablonach

# Source

- Adam Freeman, AngularJS Profesjonalne techniki, ISBN: 978-83-283-0197-9, APRESS 2014

# Sklep internetowy

Klasyczne podejście

- katalog produktów, któy klienci będą mogli przeglądać według kategorii
- koszyk na zakupy, do którego klienci będą mogli dodawać lub usuwać produkty
- strona finalizacji zakupów z podaniem niezbędnych danych do realizacji zamówienia
- obszar administracyjny dla zarządzania katalogie produktów (CRUD). Jest to obszar chroniony, tylko dla użytkowników zalogowanych w roli administratora
- przetwarzanie płatności

Elementy kategorii mają być generowane dynamicznie na podstawie obiektów danych produktów.

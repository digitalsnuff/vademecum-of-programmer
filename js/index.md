# Vanilla JavaScript

- Data Types
- functions
- conditionals
- loops
- operators

- DOM Manipulation
- Events

- JSON
- Fetch API
- ES6+
  - Arrow functions
  - promises
  - async / await
  - destructuring

# Frameworks and libraries

- Very popular in the industry
- more interactive and interesting UIs
- components and modular front end code
- good for teams
- frameworks:
  - Angular - fading a bit. Used in enterprise.
- libraries
  - Vue - easy to use and really gaining traction
  - React - most popular in the industry
- others:
  - Knockout
  - Backbone
  - jQuery

# What Is TypeScript?

- superset of JavaScript developed by Microsoft
- Inherits features of JS
- Compiles to plain JS
- Easily integrated into JS projects
- designed for developers of large applications

# What Does TypeScript Offer?

- static type checking
- class based object
- compatible with ES6
- modularity
- ES6 features
- syntax closer to Java and other hight level languages

# Static Type Checking

> With TypeScript we can check and assign variable, parameter and function types.

- it is completely optional
- helps us find and prevent bugs and stop future issues from happening
- makes our code much more readable and descriptive

# TypeScript Types

- String
- Number
- Boolean
- Array
- Any
- Void
- Null
- Tuple
- Enum
- Generics

# Class Based Objects

- object oriented programming in JS!
- no prototypes
- encapsulation
- inheritance
- modifiers

# TypeScript Compiler (tsc)

- written in TypeScript itself
- compiles .ts files to .js
- installed as an NPM package (node.js)
- supports ES6 syntax

# Primitive types

JavaScript defines 7 built-in types:

- boolean - true and false
- bigint - integers in the arbitrary precision format
- null - equivalent to the unit type
- number - a double-precision IEEE 754 floating point
- string - an immutable UTF-16 string
- symbol- a unique value usually used as a key
- object - similar to records
- undefined -equivalent to the unit type

which you can use in an interface.  
TypeScript extends this list with a few more. for example: `any` (allow anything), `unknown` (ensure someone using this type declares what the type is), `never` (it’s not possible that this type could happen) `void` (a function which returns undefined or has no return value).

# Union Types

## Unions

A union is a way to declare that a type could be one of many types. For example, you could describe a boolean type as being either `true` or `false`:

```ts
type MyBool = true | false;
type WindowStates = "open" | "closed" | "minimized";
type LockStates = "locked" | "unlocked";
type OddNumbersUnderTen = 1 | 3 | 5 | 7 | 9;
function getLength(obj: string | string[]) {
  return obj.length;
}
```

## Generics

You can get very deep into the TypeScript generic system, but at a 1 minute high-level explanation, generics are a way to provide variables to types.

A common example is an array, an array without generics could contain anything. An array with generics can describe what values are inside in the array.

```ts
type StringArray = Array<string>;
type NumberArray = Array<number>;
type ObjectWithNameArray = Array<{ name: string }>;

interface Backpack<Type> {
  add: (obj: Type) => void;
  get: () => Type;
}
declare const backpack: Backpack<string>;
const object = backpack.get();
backpack.add(23); // Argument of type 'number' is not assignable to parameter of type 'string'.
```

# Structural Type System

One of TypeScript’s core principles is that type checking focuses on the shape which values have. This is sometimes called “duck typing” or “structural typing”.

In a structural type system if two objects have the same shape, they are considered the same.

The shape matching only requires a subset of the object’s fields to match.

# Functions

Function syntax includes parameter names. This is pretty hard to get used to!

```ts
let fst: (a: any, d: any) => any = (a, d) => a;
// or more precisely:
let snd: <T, U>(a: T, d: U) => U = (a, d) => d;
```

Object literal type syntax closely mirrors object literal value syntax:

```ts
let o: { n: number; xs: object[] } = { n: 1, xs: [] };
```

`[T, T]` is a subtype of `T[]`. This is different than Haskell, where tuples are not related to lists.

# Gradual typing

TypeScript uses the type `any` whenever it can’t tell what the type of an expression should be. Compared to Dynamic, calling any a type is an overstatement. It just turns off the type checker wherever it appears.

To get an error when TypeScript produces an any, use `"noImplicitAny": true`, or `"strict": true` in `tsconfig.json`.

# Structural typing

```ts
// @strict: false
let o = { x: "hi", extra: 1 }; // ok
let o2: { x: string } = o; // ok
```

# Unit types

Unit types are subtypes of primitive types that contain exactly one primitive value. 
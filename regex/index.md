# Regular Expressions

Wyrażenia regularne pomagają znaleźć wzorzec w ciągu tekstowym.
Są to ciągi znaków o określonym znaczeniu, które odwzorowują inne ciągi znaków lub ich fragmenty w sposób określony przez naczenie znaków wyrażeń.

> _Wyrażenia regularne to wzorzec określający zestaw znaków w ciągu tekstowym; powoduje dopasowanie konkretnych ciągów tekstowych._
>
> \- Ken Thompson

Idea wyrażeń regularnych była znana juz w latach czterdziestych XX wieku. Pierwsze znane ujęcie zostało zawarte w książce Stephena Kleene _Introduction to Metamathematics_, New York, Van Nostrand, 1952 r.
W środowisku informatycznym wyrażenia regularne znane były w latach siedemdziesiątych XX w. wraz z powstaniem sytemu operacyjnego UNIX (Brian Kerighan, Dennis Ritchie, Ken Tomphson z firmy AT&T Bell Labs) w narzędziach `sed`, `grep`.

- QED editor (Quick Editor)
- Unix tools: vi, vim, grep, awk, ed, sed

PCRE (Perl Compatible Regular Expressions). PCRE posiada zarówno swój własny interfejs, jak i interfejs kompatybilny z wyrażeniami regularnymi POSIX. Jest ona używana m.in. przez KDE, exim, PHP i tin.
Perl

composability

`Code Point` - punkt kodowy; znaki Unicode jako czterocufrowe liczba szesnastkowa (czyli o podstawie 16). Punkty kodowe są użwane w postaci U+0000.

## Metaznaki

`metaznaki` - są to znaki w wyrażeniu regularnym ze specjalnym znaczeniem. Jest znakiem zarezerwowanym.

- [] -`klasa (zestaw) znaków`. Kropka w klasie znaków oznacza kropke a nie znak specjalny
- () - `grupa przechwytywania`
- \1 - `odwołanie wsteczne`
- {} - `kwatyfikator` - wyznacza sakres występowania wystąpień
- ^ - dopasowanie początku wiersza. Znak oznaczający początek wyrażenia lub negację, jeśli nie znajduje sie na poczatku wyrażenia.
- `$` - dopasowanie końca wiersza.
- `|` - oznacza alternatywę

## Znak skrótu (znak sterujący)

`dosłowny ciąg tekstowy`

Żeby nadać znakom specjalnym znaczenia dosłownego nalezy je poprzedzić znakiem `\`.

`znak skrótu (znak sterujący)` - znak wyrażenia regularnego wraz z jego znaczeniem

- `.` - znak wieloznaczny powodujący dopasowanie każdego znaku z wyjątkiem znaku nowego wiersza U+000A (w określonych sytuacjach)
- `\` - początek znaku skrótu
- `\d` - znak dopasowujący dowolna cyfrę arabską z przedziału od 0 do 9 (klasa znaków [0-9])
- `\D` - każdy znak, który nie jest cyfrą

## Kwantyfikatory

- {} - `kwatyfikator` - wyznacza sakres występowania wystąpień
- ? - wystąpienie znaku (grupy znaków) najwyżej jeden raz.
- \+ - wystąpienie znaku bądź brupy znaków przynajmniej jeden raz
- \* - wystapienie ciągu znaków przynajmniej jednen raz bądź wcale

## Przykłady

```sh
# Dopasowanie numeru telefonu w formacie stosowanym w Ameryce Północnej
^(\(\d{3}\)|^\d{3}[.-]?)?\d{3}[.-]?\d{4}$
# Jak wyżej, lecz z zastosowaniem kwantyfikatorów.
\d{3}-?\d{3}-?\d{4}
(\d{3}[.-]?){2}\d{4}
```

```sh
(\d)\d\1
# (\d) - dopasowuje pierwszą cyfrę i ją przechwytuje
# \d - dopasowuje koleją cyfrę, ale jej nie przechwytuje
# \1 - odwołuje sie do przechwyconej zawartości
```

# Tools

[RegexPal](http://regexpal.com)

[Regex Buddy](http://regexbuddy.com)

# Source

- Michael Fitzgerald, _Wyrażenia regularne. Wprowadzenie_, ISBN: 078-83-246-6868-7, Origin: O'REILLY, Wydawnictwo Helion 2013

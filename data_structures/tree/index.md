# Struktury danych

# Terminologia

`Drzewo` - jest strukturą danych zbudowaną z elementów zwanymi węzłami (nodes).

Dane przechowujemy w węzłach drzewa.

Węzły są ze soba powiązane w sposób hierarchiczny za pomocą `krawędzi (edge)`, najczęściej przedsawiane w formie strzałki, okreslające hierarchię.

`Korzeń` - pierwszy węzeł drzewa (`root node`).

`Synowie` - wszystkie węzły wychodzące od rodzica (`child nodes`). Są to węzły podrzedne.

`Bracia` - węzły tego samego ojca (`sibling node`)

`Rodzic` - węzeł nadrzędny względem dziecka

`Liść` - węzeł nie posiadający elementów potomnych (`leaf node`).

`węzeł wewnętrzny` - węzeł posiadający potomstwo (`internal node`, `inner node`, `branch node`)

`ścieżka` - ciąg węzłow połączonych krawędziami (`path`)

`długość ścieżki` - liczba krawędzi łączących węzły w ścieżce

`poziom węzła` - długość ściezki prostej od korzenia do danego węzła (`node level`)

`wysokość drzewa` - jest równa największemu poziomowi węzłów lub najdłuższej ścieżce rozpoczynającej się od korzenia (`tree height`).

`wysokość węzła` - długość najdłuższej ścieżki od tego węzła do liścia (`node height`).

`stopień węzła` - licza krawędzi powiązanych z danym węzłem (`node degree`)

`krawędzie skierowane` - każda krawędź posiada kierunek dla określenia relacji (`directed edge`)

`stopień węzła` - jest sumą stopnia wejściowego i wyjściowego

- `stopień wejściowy` - liczba krawędzi wchodzących do węzła, dla drzewa nigdy nie przekracza 1, a jest równy 0 tylko dla korzenia (`node in-degree`)
- `stopień wyjściowy` - liczba krawędzi wychodzących z węzła, określa liczbę synów (`node out-degree`)

Liście nie będące korzeniem mają zawsze stopień równy 1 (korzeń posiada stopień równy 0).

## Drzewo binarne

`Drzewo binarne (binary tree, B-tree)` – drzewo, w którym stopień każdego wierzchołka jest nie większy od 3, czyli węzły tego drzewa mogą posiadac co najwyżej dwóch synów.

Ukorzenione drzewo binarne to drzewo binarne, w którym wyróżniono jeden z wierzchołków (zwany korzeniem) stopnia najwyżej 2.

W informatyce drzewo binarne to jeden z rodzajów drzewa (struktury danych), w którym liczba synów każdego wierzchołka wynosi nie więcej niż dwa. Wyróżnia się wtedy `lewego syna` i `prawego syna` danego wierzchołka.

Drzewo binarne, w którym liczba synów każdego wierzchołka wynosi albo zero albo dwa, nazywane jest `drzewem regularnym`. Przykładem takich drzew są `drzewa Huffmana`.

Szczególnymi odmianami drzew binarnych są

- `drzewa BST`,
- `drzewa BSP`
- `kopce binarne`

W drzewie binarnym stopień każdego węzła nie przekracza 3. Stopień korzenia nie przekracza 2.

Dla regularnego drzewa binarnego liczba węzłów na poziomie k-tym jest zawsze równa 2^k. `Rozmiar drzewa` (suma wszystkich węzłþów drzewa) jest równa 2^p - 1, gdzie p oznacza liczbę poziomów.

Dla _n_ węzłów liczba poziomów jest równa log2(n+1)

### Zależności

Jeżeli węzeł o numerze k posiada synów, to:

- lewy syn ma numer 2k+1
- prawy syn ma numer 2k+2

Jeżeli węzeł o numerze k posiada ojca, to

- ojciec ma numer [(k-1) / 2], gdzie [...] oznacza część całkowitą.

Węzeł o numerze k znajduje się na poziomie o numerze [log2(k+1)]

Węzeł o numerze k jest wewnętrzny, jeśli 2k+2 < n. W przeciwnym razie węzeł jest liściem.
Właściwości te pozwalaja odwzorować regularne drzewo binarne w ciąg elementów i na odwrót.

`Kompletne drzewo binarne (complete binary tree)` - posiada zapełnione węzłami wszystkie poziomy z wyjątkiem ostatniego, jednakże na ostatnim poziomie węzły są zapełnione począwszy od lewej strony.

Kompletne drzewo binarne również da się odwzorować w ciąg węzłów. W takim drzewie liczba elementów n może być mniejsza od maksymalnej liczby węzłów, ponieważ ostatni poziom nie musi posiadać kompletu węzłów. Jednakże w przeciwieństwie do drzewa regularnego węzeł wewnętrzny może posiadać tylko jednego, lewego syna (u nas węzłem takim jest węzeł 4). Dlatego w kompletnym drzewie binarnym o rozmiarze n dla węzła o numerze k zachodzi:

2k + 2 > n – węzeł jest liściem

2k + 2 = n – węzeł jest ostatnim węzłem wewnętrznym i posiada tylko lewego syna

2k + 2 < n – węzeł jest węzłem wewnętrznym i posiada obu synów.

### poddrzewo

`Poddrzewo (ang. subtree)` jest drzewem zawartym w drzewie, gdy jako korzeń przyjmiemy jeden z węzłów. Dla danego węzła drzewa binarnego mogą istnieć dwa poddrzewa: lewe poddrzewo (ang. left subtree) – korzeniem jest lewy syn i analogicznie prawe poddrzewo (ang. right subtree) – korzeniem jest prawy syn.

# Reprezentacja drzew binarnych w programie

## Kompletne drzewo binarne

W tym przypadku drzewo możemy odwzorować w tablicy n-elementowej. Każdy element tablicy jest węzłem. Hierarchię drzewa przedstawiamy przy pomocy indeksów i ich własności dla kompletnych drzew binarnych. Korzeniem drzewa jest element o indeksie 0. Jego dwoma synami są kolejno elementy o indeksach 1 (lewy syn) i 2 (prawy syn). Postępując podobnie z pozostałymi węzłami otrzymamy całe drzewo binarne.

## Niekompletne drzewo binarne

Drzewo odwzorowujemy podobnie jak listę. Każdy element jest strukturą, która oprócz danych zawiera dwa lub trzy wskaźniki, gdzie:

up – wskazuje ojca danego węzła. Dla korzenia pole to zawiera wskazanie puste
left – wskazuje lewego syna
right – wskazuje prawego syna
data – dane dowolnego typu przechowywane przez węzeł

Wskaźniki pozwalają na przemieszczanie się po węzłach w strukturze drzewa. Wskaźniki left i right umożliwiają przechodzenie w dół drzewa. Wskaźnik up prowadzi w górę do ojca danego węzła. Jeśli ten kierunek nie jest istotny, to wskaźnik może zostać pominięty (wersja uproszczona).

# Reprezentacja drzew dowolnych

Drzewo dowolne może posiadać węzły o dowolnej liczbie synów. Jeśli liczba możliwych węzłów potomnych nie jest duża, to do reprezentacji takiego drzewa można wykorzystać metodę z drzewa binarnego, zwiększając odpowiednio liczbę wskaźników. Na przykład dla drzew czwórkowych (ang. quadtree) możemy zaimplementować następującą strukturę danych, gdzie:

up – wskazuje ojca danego węzła. Dla korzenia pole to zawiera wskazanie puste
ne – wskazuje syna północnowschodniego
nw – wskazuje syna północnozachodniego
se – wskazuje syna południowowschodniego
sw – wskazuje syna południowozachodniego
data – dane dowolnego typu przechowywane przez węzeł

Gdy liczba synów jest duża, to rezerwowanie w każdym węźle pól na wskaźniki przestaje być efektywne. Zamiast prostych pól możemy umieścić w każdym węźle tablicę dynamiczną o wymaganym rozmiarze, której każdy element jest wskaźnikiem do syna danego węzła. Do obsługi takiej struktury będzie potrzebna jeszcze informacja o liczbie elementów w tablicy. Dodatkowo musimy pamiętać o zwolnieniu tablic dynamicznych, gdy drzewo jest usuwane z pamięci.

Gdzie:

up – wskazuje ojca danego węzła. Dla korzenia pole to zawiera wskazanie puste
child – wskazuje tablicę zawierającą wskazania do węzłów synów
n – określa liczbę synów
data – dane dowolnego typu przechowywane przez węzeł

Alternatywnym rozwiązaniem jest zastosowanie listy jednokierunkowej, której elementy przechowują wskazania synów danego węzła. Wymaga to dołączenia do programu metod obsługi takiej listy, a najlepiej zastosowanie odpowiedniego obiektu.

Ostatnia metoda reprezentacji dowolnych drzew wykorzystuje drzewo binarne w pewien szczególny sposób. Zasada jest następująca:

Każdy węzeł drzewa dowolnego jest reprezentowany przez odpowiedni węzeł drzewa binarnego. Różnica występuje w interpretacji krawędzi. Otóż w drzewie binarnym lewy syn węzła oznacza pierwszego syna węzła w drzewie dowolnym. Prawy syn oznacza zawsze kolejnego brata, czyli węzeł posiadający tego samego ojca. Bracia tworzą listę prawych dowiązań.

# Links

[Wiki - Drzewo binarne](https://pl.wikipedia.org/wiki/Drzewo_binarne)

[Podstawowe pojęcia dotyczące drzew](https://eduinf.waw.pl/inf/alg/001_search/0108.php)

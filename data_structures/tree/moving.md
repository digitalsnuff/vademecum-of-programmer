# Przechodzenie drzew binarnych

## algorytmy przechodzenia drzewa (ang. tree traversal algorithm)

Drzewa odzwierciedlają różnego rodzaju struktury. W węzłach są przechowywane różne informacje. Często będziemy spotykać się z zadaniem, które wymaga odwiedzenia każdego węzła drzewa i przetworzenia informacji przechowywanych w węźle. Zadanie to realizują `algorytmy przechodzenia drzewa (ang. tree traversal algorithm)`. Polegają one na poruszaniu się po drzewie od węzła do węzła wzdłuż krawędzi w pewnym porządku i przetwarzaniu danych przechowywanych w węzłach. Istnieje kilka takich algorytmów o różnych własnościach.

### Algorytm DFS

`Odwiedzenie węzła (ang. node visit)` polega na dojściu do danego węzła po strukturze drzewa oraz przetworzenie danych zawartych w węźle.

`Przeszukiwanie w głąb (ang. Depth First Search - DFS)` jest bardzo popularnym algorytmem przechodzenia przez wszystkie węzły poddrzewa, którego korzeniem jest węzeł startowy (jeśli węzłem startowym będzie korzeń drzewa, to DFS przejdzie przez wszystkie węzły zawarte w drzewie). Zasada działania tego algorytmu opiera się na rekurencji lub stosie, którym zastępujemy wywołania rekurencyjne. Ze względu na kolejność odwiedzin węzłów wyróżniamy trzy odmiany DFS (istnieją jeszcze trzy dalsze będące "lustrzanymi odbiciami"):

- `DFS: pre-order – przejście wzdłużne`

W przejściu wzdłużnym (ang. pre-order traversal) najpierw odwiedzamy korzeń poddrzewa, a następnie przechodzimy kolejno lewe i prawe poddrzewo.

- `Algorytm rekurencyjny DFS:preorder dla drzewa binarnego`

Wejście
v – wskazanie węzła startowego drzewa binarnego
Wyjście:
przetworzenie wszystkich węzłów drzewa.
Lista kroków:
K01: Jeśli v = nil, to zakończ ; koniec rekurencji
K02: Odwiedź węzeł wskazany przez v
K03: preorder(v→left) ; przejdź rekurencyjnie przez lewe poddrzewo
K04: preorder(v→right) ; przejdź rekurencyjnie przez prawe poddrzewo
K05: Zakończ

- `Algorytm stosowy DFS:preorder dla drzewa binarnego`

Wejście
v – wskazanie węzła startowego drzewa binarnego
Wyjście:
przetworzenie wszystkich węzłów drzewa.
Elementy pomocnicze:
S – stos wskazań węzłów, rozmiar stosu nie przekracza podwójnej wysokości drzewa.
Lista kroków:
K01: Utwórz pusty stos S
K02: S.push(v) ; zapamiętujemy wskazanie węzła startowego na stosie
K03: Dopóki S.empty() = false, wykonuj K04...K08 ; w pętli przetwarzamy kolejne węzły
K04: v ← S.top() ; pobieramy ze stosu wskazanie węzła
K05: S.pop() ; pobrane wskazanie usuwamy ze stosu
K06: Odwiedź węzeł wskazany przez v ; przetwarzamy węzeł
K07: Jeśli (v→right) ≠ nil, to S.push(v→right) ; umieszczamy na stosie wskazanie prawego syna, jeśli istnieje
K08: Jeśli (v→left) ≠ nil, to S.push(v→left) ; umieszczamy na stosie lewego syna, jeśli istnieje i kontynuujemy pętlę aż do wyczerpania stosu
K09: Zakończ

- `DFS: in-order – przejście poprzeczne`

W przejściu poprzecznym (ang. in-order traversal) najpierw przechodzimy lewe poddrzewo danego węzła, następnie odwiedzamy węzeł i na koniec przechodzimy prawe poddrzewo.

- `Algorytm rekurencyjny DFS:inorder dla drzewa binarnego`

Wejście
v – wskazanie węzła startowego drzewa binarnego
Wyjście:
przetworzenie wszystkich węzłów drzewa.
Lista kroków:
K01: Jeśli v = nil, to zakończ ; koniec rekurencji
K02: inorder(v→left) ; przejdź rekurencyjnie przez lewe poddrzewo
K03: Odwiedź węzeł wskazany przez v
K04: inorder(v→right) ; przejdź rekurencyjnie przez prawe poddrzewo
K05: Zakończ

- `Algorytm stosowy DFS:inorder dla drzewa binarnego`

Algorytm wykorzystuje stos oraz dodatkowy wskaźnik cp, którym przemieszczamy się po drzewie.
Wejście
v – wskazanie węzła startowego drzewa binarnego
Wyjście:
przetworzenie wszystkich węzłów drzewa.
Elementy pomocnicze:
S – stos wskazań węzłów, rozmiar stosu nie przekracza podwójnej wysokości drzewa.
cp – wskaźnik bieżącego węzła

Lista kroków:
K01: Utwórz pusty stos S
K02: cp ← v ; bieżącym węzłem jest korzeń drzewa
K03: Dopóki (S.empty() = false) (cp ≠ nil) wykonuj K04...K11 ; pętla jest wykonywana, jeśli jest coś na stosie lub cp wskazuje węzeł drzewa
K04: Jeśli cp = nil, to idź do K07 ; sprawdzamy, czy wyszliśmy poza liść drzewa
K05: S.push(cp) ; jeśli nie, to umieszczamy wskazanie bieżącego węzła na stosie
K06: cp ← (cp→left) ; bieżącym węzłem staje się lewy syn
K07: Następny obieg pętli K02 ; wracamy na początek pętli
K08: cp ← S.top() ; wyszliśmy poza liść, wracamy do węzła, pobierające jego wskazanie ze stosu
K09: S.pop() ; wskazanie usuwamy ze stosu
K10: Odwiedź węzeł wskazany przez cp ; przetwarzamy dane w węźle
K11: cp ← (cp→right) ; bieżącym węzłem staje się prawy syn
K12: Zakończ

- `DFS: post-order – przejście wsteczne`

W przejściu wstecznym (ang. post-order traversal) najpierw przechodzimy lewe poddrzewo, następnie prawe, a dopiero na końcu przetwarzamy węzeł.

- `Algorytm rekurencyjny DFS:postorder dla drzewa binarnego`

Wejście
v – wskazanie węzła startowego drzewa binarnego
Wyjście:
przetworzenie wszystkich węzłów drzewa.
Lista kroków:
K01: Jeśli v = nil, to zakończ ; koniec rekurencji
K02: postorder(v→left) ; przejdź rekurencyjnie przez lewe poddrzewo
K03: postorder(v→right) ; przejdź rekurencyjnie przez prawe poddrzewo
K04: Odwiedź węzeł wskazany przez v
K05: Zakończ

- `Algorytm stosowy DFS:postorder dla drzewa binarnego`
  Algorytm wykorzystuje stos oraz dwa wskaźniki: pp – wskazanie poprzedniego węzła i cp – wskazanie bieżącego węzła. Są one używane do określenia, czy oba poddrzewa danego węzła zostały przebyte.

Wejście
v – wskazanie węzła startowego drzewa binarnego
Wyjście:
przetworzenie wszystkich węzłów drzewa.
Elementy pomocnicze:
S – stos wskazań węzłów, rozmiar stosu nie przekracza podwójnej wysokości drzewa.
pp – wskaźnik węzła poprzedniego
cp – wskaźnik węzła bieżącego.
Lista kroków:
K01: Utwórz pusty stos S
K02: S.push(v) ; węzeł startowy umieszczamy na stosie
K03: pp ← nil ; zerujemy wskaźnik węzła poprzedniego
K04: Dopóki S.empty() = false: wykonuj K05...K14
K05: cp ← S.top() ; ustawiamy cp na węzeł przechowywany na stosie
K06: Jeśli (pp = nil) ((pp→left) = cp) ((pp→right) = cp), to idź do K11 ; sprawdzamy, czy idziemy w głąb drzewa
K07: Jeśli (cp→left) = pp, to idź do K13 ; sprawdzamy, czy wróciliśmy z lewego poddrzewa
K08: Odwiedź węzeł wskazany przez cp ; oba poddrzewa przebyte, przetwarzamy węzeł
K09: S.pop() ; i usuwamy jego wskazanie ze stosu
K10: Idź do K14
K11: Jeśli (cp→left) ≠ nil, to S.push(cp→left)
inaczej jeśli (cp→right) ≠ nil, to S.push(cp→right) ; jeśli istnieje lewy syn cp, umieszczamy go na stosie
; inaczej umieszczamy na stosie prawego syna, jeśli istnieje
K12: Idź do K14
K13: Jeśli (cp→right) ≠ nil, to S.push(cp→right) ; jeśli istnieje prawy syn, to umieszczamy go na stosie
K14: pp ← cp ; zapamiętujemy cp w pp i wykonujemy kolejny obieg pętli
K15: Zakończ

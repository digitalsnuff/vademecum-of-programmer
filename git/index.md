# GIT

# Config

## Global

```
git config --global user.name '[username]'
git config --global user.email '[user@email]'
```

# Commands

## Commit

- using `git add`
- using `git rm`
- using `git mv`
- by listing files as arguments to the `commit` command (files must be known by Git)
- by using the `-a` switch
- by using the `--interactive` or `--path` switches with the `commit` command to decide one by one which files or hunks should be part of the commit in addition to contents in the index, before finalizing the operations.
- `dry-run` - option can be used to obtain a summary of what is included by any of the above for the next commit by giving the same set of parameters (options and paths).
- `git reset` - If you make a commit and then find a mistake immediately after that, you can recover from it

### switches

- `--allow-empty` - Usually recording a commit that has the exact same tree as its sole parent commit is a mistake, and the command prevents you from making such a commit. This option bypasses the safety, and is primarily for use by foreign SCM interface scripts.
- `--allow-empty-message` - Like --allow-empty this command is primarily for use by foreign SCM interface scripts. It allows you to create a commit with an empty commit message without using plumbing commands
- `--dry-run` - Do not create a commit, but show a list of paths that are to be committed, paths with local changes that will be left uncommitted and paths that are untracked.

## Rebase

## Amend

Adding staged files to last commit without creating a new one

```sh
git log --oneline
git add .
git commit --amend --no-edit
git log --oneline
# There is last id commit changed only.
```

## Reword

```sh
# -i - interactive
git rebase -i HEAD~2
# get 2 commits back from HEAD
# It will appears lines with commits and commented text with instructions
# For change text of selected commit, replace 'pick' by 'reword'
# Save output
# change message on first line - it shows message of commit which was assigned on previous output
# Save output
```

## Delete

```sh
git rebase -i HEAD~3
# replace 'pick' to 'drop' on selected line of commit list
# Save
```

## Reordering

```sh
git rebase -i HEAD~2
# Cut and paste lines with commits in order you want
# Save
```

## Squashing

```sh
git rebase -i HEAD~3
# Change 'pick' to 'fixup' on selected position on list of commits
# It squash to last commit (before)

```

## Splitting

```sh
git rebase -i HEAD~3
# Replace 'pick' to 'edit'
# Now run 'git commit --amend' or 'git rebase  --continue'
git status
# it shows rebase status
git reset HEAD^
# Unstage files from commit with 'edit status'
# Stage selected files and commit it
# Stage the last and commit
git rebase --continue
```

# Make Forked From Another Branch

```sh
git rebase --onto master next topic
```

## Remove

- Unstage file before commit

```
gir rm --cached [filename]
```

# Sources

- [TFVC](https://docs.microsoft.com/pl-pl/azure/devops/repos/tfvc/?view=tfs-2017)
- [Git patterns and anti-patterns for successful developers : Build 2018](https://www.youtube.com/watch?v=ykZbBD-CmP8)
- [git-commit](https://mirrors.edge.kernel.org/pub/software/scm/git/docs/git-commit.html)
- [A better Git workflow with rebase](https://www.youtube.com/watch?v=f1wnYdLEpgI)
- [Git & GitHub Crash Course For Beginners](https://www.youtube.com/watch?v=SWYqp7iY_Tc) - Traversy media

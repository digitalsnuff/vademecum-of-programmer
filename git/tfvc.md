# Patterns and anti-patterns

## Typical Team Foundation Version Control (TFVC) Branching Stucture

- Agile Planning Work Items
- Git and Version Control
- Build, Deploy and Monitor

### Trunk-Based Development

- Code close to master
- Small, simple changes
  - fewer merge conflicts
  - easy to code review
  - encourages pull requests
  - simpler to ship; faster velocity

### GitHub Flow

- pull request

1. Lock the master branch
2. Merge master into the branch to deploy
3. Build and run test suite on the branch to eploy
4. Deploy the branch to canary; monitor for problems
5. Deploy the branch to production; monitor for problems
6. Merge the pull request into master
7. Unlock the master branch

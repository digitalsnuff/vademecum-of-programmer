# GIT Flow

> The Gitflow Workflow defines a strict branching model designed around the project release. This provides a robust framework for managing larger projects. [Vincent Driessen at nvie]

## What is Git Flow?

- A set of guidelines developers can follow when using version control
- Referred to as a "Branching Model"
- Not rules, but guidelines which are not set in stone

## How it works?

1. Central repository
2. Developers work locally and push there branches
3. Two branches used to record project history: `master` and `develop`
4. `develop` serves as an integration branch for **features**
5. `master` stores the official release history

> `develop` branch is a branch of Team

## Creating a feature

1. Jane needs to start working on a new feature
2. Jane will pull the latest copy of `develop`
3. Jane will fork `develop` and create her own `feature branch`
4. Jane will commit her work to this `feature branch`
5. When Jane has completed and tested her code, she will merge her `feature branch` into `develop`

## Types of branches

- **master**
- hotfix
- release
  - major feature for next release
  - feature for future release
- **develop**
- feature
- support

## Hotfix

> Hotfixes are defined as minor fixes to th projects.

- create new hotfix branch form `master`
- commit changes
- testing
- merge to `master` and `develop`
- create new tag

> it may be merged to the current `release` branch

```sh
git checkout master
git merge hotfix_branch
git checkout develop
git merge hotfix_branch
git branch -D hotfix_branch
```

## Feature and Release

- create new `feature branch` from `develop`
- commit changes
- testing
- merge `feature branch` to `develop`
- create `release branch for tag` from develop if there is acquired enough features or a predetermined release date is approaching
- IMPORTANT - **no new features can be added after this point** — only bug fixes, documentation generation, and other release-oriented tasks should go in this branch
- testing
- resolve bugfix problems
- merge to `develop` after changes on `release branch for tag`
- bugfixes from `release branch for tag` may be continuously merged back into develop
- new `feature branch` may be created even if `develop` has merged changes from `release branch for tag`
- finally megre from `release branch for tag` to `master` and `develop`
  - first merge from `release branch for tag` to `master`
  - second merge from `master` to `develop`
- create new tag

## Feature

- `feature branches` use `develop` as their **parent** branch
- Features should never interact directly with `master`

## Release Branches

1. Release branches created by forking `develop`
2. Senior Developer Dan will create a `release branch`
3. The `release branch` will contain a pre-determined amount of features
4. The `release branch` should be deployed to a Staging server for QA Testing
5. Any bugs, needs to be addressed on the `release branch`
6. The `release branch` will have to be merged back into `develop` as well as `master`
7. You should then tag `master` with a version number

## Summary

### Some key takeaways to know about Gitflow are:

- The workflow is great for a release-based software workflow.
- Gitflow offers a dedicated channel for hotfixes to production.

### The overall flow of Gitflow is:

- A `develop` branch is created from `master`
- A `release` branch is created from `develop`
- `Feature` branches are created from `develop`
- When a `feature` is complete it is merged into the `develop` branch
- When the `release` branch is done it is merged into `develop` and `master`
- If an issue in `master` is detected a `hotfix` branch is created from `master`
- Once the `hotfix` is complete it is merged to both `develop` and `master`

# Sources

[Git Flow Part 1 - What is Git Flow](https://www.youtube.com/watch?v=6LhTe8Mz6jM)
[Gitflow Workflow](https://pl.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)

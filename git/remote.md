# GIT remote

## Init repo and add remote

```sh
# Create project folder
mkdir laravel-from-scratch-tutorial
# change directory to created
cd laravel-from-scratch-tutorial
# Create project using Composer
composer create-project --prefer-dist laravel/laravel .
# Init Git repo
git init
# Initial commit
git commit -m "Initial commit" --allow-empty
# Create 'develop' branch
git checkout -b develop
# Create 'support/prepare' branch
git checkout -b support/prepare
# add project files just after install
git add .
# commit staged files
git commit -m "support/prepare - create project"
# add remote origin
git remote add origin git@gitlab.com:digitalsnuff/laravel-from-scratch-tutorial.git
```

## Show remote

```sh
git remote -v
# $ git remote -vvv
# origin  https://github.com/opencart/opencart.git (fetch)
# origin  https://github.com/opencart/opencart.git (push)
```

## Set new url

```sh
git remote set-url origin git@gitlab.com:digitalsnuff/laravel-from-scratch-tutorial.git
```

or

```sh
git remote remove url
# and
git remote add origin url
```

## Rename remote name

```sh
git remote rename origin origin-old
```

## Push to actual upstream

```sh
git push -u origin --all
git push -u origin --tags
```

## Set default configured remote for branch

```sh
git push -u origin master
# or
git branch -u origin/master
# or
# This will set the remote for the currently checked-out branch to origin/master
git branch --set-upstream-to origin/master
# Branch 'master' set up to track remote branch 'master' from 'origin'.
# This will set the remote for the branch named 'branch_name' to 'origin/master'
git branch branch_name --set-upstream-to origin/master

```

## Remove upstream the tracking

```sh
git branch --unset-upstream
```

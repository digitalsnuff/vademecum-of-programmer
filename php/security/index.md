# Statyczna analiza kodu źródłowego

## Podatności w oprogramowaniu

Słabość systemu/programu, polegająca na realizacji zagrożenia, a wjego wyniu powstania potencjalnej straty.


OWASP Top 2017:
Injection
Broken Authentication
Sensitive Data Exposure
XML External Entities (XXE)
Broken Access Control
Security Misconfiguration
Cross-Site Scripting (XSS)
Insecure Deserialization
Using Components with Known Vulnerabilities
Insufficient Logging and Monitoring

## Statyczna analiza

Analiza struktury kodu źródłowego lub kodu skompilowanego bez jego uruchamiania

## Narzędzia analizy

Open Source:
1. AppChecker
2. Exakat
3. Eir
4. Garcon
5. Phortress
6. PHP Inspection
7. PHPvulhunter
8. ProgPilot
9. Psecio:parse
10. Side Channel Analyzer
11. TraintPHP
12. OWASP WAP

SaaS:
1. Codacy
2. RIPs
3. SonarQube (SonarPHP)
4. Checkmarx CxSAST

Ocena pakietów
- wsparcie PHP 7.x
- ostatnia katualizacja w przeciągu 3 ostatnich lat
- aktywność repozytorium
- jakość dokumentacji
- firmy używające konkretnych pakietów

## testy narzędzi

Pobranie wersji aplikacji z podatnościami, czyli poprzednie, starsze wersje:
- Wordpress 4.7
- Moodle 3.4.2
- PrestaShop 1.7.2.4
- Joomla 3.8.3

## własne rozwiązania

## testy własnego rozwiazania
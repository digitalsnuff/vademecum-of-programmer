# PSR-12 Extended Coding Style

## Declare Statements, Namespace, and Import Statements

1. Opening `<?php` tag.
2. File-level docblock.
3. One or more declare statements.
4. The namespace declaration of the file.
5. One or more class-based `use` import statements.
6. One or more function-based `use` import statements.
7. One or more constant-based `use` import statements.
8. The remainder of the code in the file.

Compound namespaces with a `depth of more than two` MUST NOT be used. Therefore the following is the maximum compounding depth allowed:

```php
<?php

use Vendor\Package\SomeNamespace\{
    SubnamespaceOne\ClassA,
    SubnamespaceOne\ClassB,
    SubnamespaceTwo\ClassY,
    ClassZ,
};

```

## Classes, Properties, and Methods

- Any closing brace MUST NOT be followed by any comment or statement on the same line.
- When instantiating a new class, parentheses MUST always be present even when there are no arguments passed to the constructor.

## Method and Function Arguments

Method and function arguments with default values MUST go at the end of the argument list.

# PSR-1: Basic Coding Standard

> This section of the standard comprises what should be considered the standard coding elements that are required to ensure a high level of technical interoperability between shared PHP code.

- Files MUST use only `<?php` and `<?=` tags.
  - PHP code MUST use the long `<?php ?>` tags or the short-echo `<?= ?>` tags; it MUST NOT use the other tag variations.
- Files MUST use only `UTF-8 without BOM` for PHP code.

- Files SHOULD _either_ declare symbols (classes, functions, constants, etc.) _or_ cause side-effects (e.g. generate output, change .ini settings, etc.) but SHOULD NOT do both.

- Namespaces and classes MUST follow an “autoloading” PSR: [PSR-0, PSR-4].

  - each class is in a file by itself, and is in a namespace of at least one level: a top-level vendor name.
  - Class names MUST be declared in `StudlyCaps`.
  - Code written for PHP 5.3 and after MUST use formal namespaces.
  - Code written for 5.2.x and before SHOULD use the pseudo-namespacing convention of `Vendor_` prefixes on class names.

- Class names MUST be declared in `StudlyCaps`.

- Class constants MUST be declared in all upper case with underscore separators.

- Class properties
  - This guide intentionally avoids any recommendation regarding the use of `$StudlyCaps`, `$camelCase`, or `$under_score` property names.
  - Whatever naming convention is used SHOULD be applied consistently within a reasonable scope. That scope may be vendor-level, package-level, class-level, or method-level.
- Method names MUST be declared in `camelCase()`.

# PHP-FIG - PHP Framework Interop Group

> The PHP-FIG is a collaborative project that's worked on by community members all around the world.
>
> The PHP Framework Interoperability Group (The PHP FIG) aims to advance the PHP ecosystem and promote good standards by bringing together projects and people to collaborate. It develops and publicises standards, informed by real-world experience as well as research and experimentation by itself and others, to form PHP Standard Recommendations (PSRs).

# PHP-PSR - PHP Standards Recommendations

## Numerical index

<table style="border: none;">
  <thead>
    <tr>
      <th>NUM</th>
      <th>TITLE</th>
      <th>EDITOR(S)</th>
      <th>STATUS</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>0</td>
      <td>Autoloading Standard</td>
      <td>Matthew Weier O’Phinney</td>
      <td style="color: orange;">Deprecated</td>
    </tr>
    <tr>
      <td>1</td>
      <td>Basic Coding Standard</td>
      <td>Paul M. Jones</td>
      <td style="color: green;">Accepted</td>
    </tr>
    <tr>
      <td>2</td>
      <td>Coding Style Guide</td>
      <td>Paul M. Jones</td>
      <td style="color: orange;">Deprecated</td>
    </tr>
    <tr>
      <td>3</td>
      <td>Logger Interface</td>
      <td>Jordi Boggiano</td>
      <td style="color: green;">Accepted</td>
    </tr>
    <tr>
      <td>4</td>
      <td>Autoloading Standard</td>
      <td>Paul M. Jones</td>
      <td style="color: green;">Accepted</td>
    </tr>
    <tr>
      <td>5</td>
      <td>PHPDoc Standard</td>
      <td>Chuck Burgess</td>
      <td style="color: cyan;">Draft</td>
    </td>
    <tr>
      <td>6</td>
      <td>Caching Interface</td>
      <td>Larry Garfield</td>
      <td style="color: green;">Accepted</td>
    </tr>
    <tr>
      <td>7</td>
      <td>HTTP Message Interface</td>
      <td>Matthew Weier O’Phinney</td> 
      <td style="color: green;">Accepted</td>
    </tr>
    <tr>
      <td>8</td>
      <td>Huggable Interface</td>
      <td>Larry Garfield</td>
      <td style="color: red;">Abandoned</td>
    </tr>
    <tr>
      <td>9</td>
      <td>Security Advisories</td>
      <td>Michael Hess</td>
      <td style="color: red;">Abandoned</td>
    </tr>
    <tr>
      <td>10</td>
      <td>Security Reporting Process</td>
      <td>Michael Hess</td>
      <td style="color: red;">Abandoned</td>
    </tr>
    <tr>
      <td>11</td>
      <td>Container Interface</td>
      <td>Matthieu Napoli, David Négrier</td>
      <td style="color: green;">Accepted</td>
    </tr>
    <tr>
      <td>12</td>
      <td>Extended Coding Style Guide</td>
      <td>Korvin Szanto</td>
      <td style="color: green;">Accepted</td>
    </tr>
    <tr>
      <td>13</td>
      <td>Hypermedia Links</td>
      <td>Larry Garfield</td>
      <td style="color: green;">Accepted</td>
    </tr>
    <tr>
      <td>14</td>
      <td>Event Dispatcher</td>
      <td>Larry Garfield</td>
      <td style="color: green;">Accepted</td>
    </tr>
    <tr>
      <td>15</td>
      <td>HTTP Handlers</td>
      <td>Woody Gilk</td>
      <td style="color: green;">Accepted</td>
    </tr>
    <tr>
      <td>16</td>
      <td>Simple Cache</td>
      <td>Paul Dragoonis</td>
      <td style="color: green;">Accepted</td>
    </tr>
    <tr>
      <td>17</td>
      <td>HTTP Factories</td>
      <td>Woody Gilk</td>
      <td style="color: green;">Accepted</td>
    </tr>
    <tr>
      <td>18</td>
      <td>HTTP Client</td>
      <td>Tobias Nyholm</td>
      <td style="color: green;">Accepted</td>
    </tr>
    <tr>
      <td>19</td>
      <td>PHPDoc tags</td>
      <td>Chuck Burgess</td>
      <td style="color: cyan;">Draft</td>
    </tr>    
  </tbody>
</table>

## PSR Naming Conventions

1. Interfaces MUST be suffixed by `Interface`: e.g. `Psr\Foo\BarInterface`.
2. Abstract classes MUST be prefixed by `Abstract`: e.g. `Psr\Foo\AbstractBar`.
3. Traits MUST be suffixed by `Trait`: e.g. `Psr\Foo\BarTrait`.
4. `PSR-1`, `2` and `4` MUST be followed.
5. The vendor namespace MUST be `Psr`.
6. There MUST be a package/second-level namespace in relation with the PSR that covers the code.
7. Composer package MUST be named `psr/<package>` e.g. `psr/log`. If they require an implementation as a virtual package it MUST be named `psr/<package>-implementation` and be required with a specific version like `1.0.0`. Implementors of that PSR can then provide `"psr/<package>-implementation": "1.0.0"` in their package to satisfy that requirement. Specification changes via further PSRs should only lead to a new tag of the `psr/<package>` package, and an equal version bump of the implementation being required.
8. Special lightweight utility packages MAY be produced alongside PSRs and interfaces and be maintained and updated after the PSR has been accepted. These MUST be under the vendor namespace `Fig`.

## PSR Workflow

### Pre-Draft

> The goal of the Pre-Draft stage is to determine whether a majority of the PHP FIG is interested in publishing a PSR for a proposed concept.

> Interested parties may discuss a possible proposal, including possible implementations, by whatever means they feel is appropriate. That includes informal discussion on official FIG discussion mediums of whether or not the idea has merit and is within the scope of the PHP FIG’s goals.

### Draft

> The goal of the Draft stage is to discuss and polish a PSR proposal up to the point that it can be considered for review by the FIG Core Committee.

> In Draft stage, members of the Working Group may make any changes they see fit via pull requests, comments on GitHub, Mailing List threads, IRC and similar tools. Change here is not limited by any strict rules, and fundamental rewrites are possible if supported by the Editor. Alternative approaches may be proposed and discussed at any time. If the Editor and Sponsor are convinced that an alternative proposal is superior to the original proposal, then the alternative may replace the original. Working Group members are expected to remain engaged throughout the Draft Phase. Discussions are public and anyone, regardless of FIG affiliation, is welcome to offer constructive input. During this phase, the Editor has final authority on changes made to the proposed specification.

### Review

> The Review Phase is an opportunity for the community to experiment with a reasonably fixed target to evaluate a proposal’s practicality. At this stage, the Sponsor is the final authority on changes to the specification as well as any decisions to move the proposal forward or backward, however, the Editor may veto proposed changes they believe are harmful to the design of the specification.

### Accepted

> If the Acceptance Vote passes, then the proposal officially becomes an accepted PSR. At this time the Working Group is automatically dissolved, however the Editor’s input (or a nominated individual communicated to the secretaries) may be called upon in the future should typos, changes or Errata on the specification be raised.

### Deprecated

> A Deprecated PSR is one that has been approved, but is no longer considered relevant or recommended. Typically this is due to the PSR being superseded by a new version, but that is not required.

> A PSR may be Deprecated explicitly as part of the Acceptance Vote for another PSR. Alternatively, it may be marked Deprecated by a Deprecation Vote.

### Abandoned

> An Abandoned PSR is one that is not actively being worked upon. A PSR will can be marked as Abandoned by Secretaries when it is without an Editor for 60 days or a Sponsor for 60 days. After a period of 6 months without significant activity in a Working Group, the Secretaries may also change a PSR to be “Abandoned”. A PSR can also be triggered to move to “Abandoned” upon an Abandonment vote of the Core Committee which may be requested by the Working Group by petitioning a Core Committee member or Secretary.

### Project Referendum

At any time the Editor of a PSR in Draft or Review Phase may call for a non-binding Referendum of Project Representatives on the current state of a PSR. Such a Referendum may be called multiple times if appropriate as the PSR evolves, or never, at the Editor’s discretion. The Core Committee may also require such a Referendum as a condition of an Acceptance Vote if appropriate. Referendum results are non-binding but the Working Group and Core Committee are expected to give the results due consideration.

# Links

[PHP-FIG](https://www.php-fig.org/)  
[PHP Standards Recommendations](https://www.php-fig.org/psr/)

# PSR-16 Common Interface for Caching Libraries

All data passed into the Implementing Library MUST be returned exactly as passed. That includes the variable type.

An instance of CacheInterface corresponds to a single collection of cache items with a single key namespace, and is equivalent to a “Pool” in PSR-6. Different CacheInterface instances MAY be backed by the same datastore, but MUST be logically independent.

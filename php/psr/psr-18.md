# PSR-18 HTTP Client

The goal of this PSR is to allow developers to create libraries decoupled from HTTP client implementations. This will make libraries more reusable as it reduces the number of dependencies and lowers the likelihood of version conflicts.

A second goal is that HTTP clients can be replaced as per the Liskov substitution principle. This means that all clients MUST behave in the same way when sending a request.

`Client` - A Client is a library that implements this specification for the purposes of sending PSR-7-compatible HTTP Request messages and returning a PSR-7-compatible HTTP Response message to a Calling library.
`Calling Library` - A Calling Library is any code that makes use of a Client. It does not implement this specification’s interfaces but consumes an object that implements them (a Client).

If a Client chooses to alter either the HTTP request or HTTP response, it MUST ensure that the object remains internally consistent. For example, if a Client chooses to decompress the message body then it MUST also remove the Content-Encoding header and adjust the Content-Length header.

A Client MUST NOT treat a well-formed HTTP request or HTTP response as an error condition. For example, response status codes in the 400 and 500 range MUST NOT cause an exception and MUST be returned to the Calling Library as normal.

A Client MUST throw an instance of Psr\Http\Client\ClientExceptionInterface if and only if it is unable to send the HTTP request at all or if the HTTP response could not be parsed into a PSR-7 response object.

If a request cannot be sent because the request message is not a well-formed HTTP request or is missing some critical piece of information (such as a Host or Method), the Client MUST throw an instance of Psr\Http\Client\RequestExceptionInterface.

If the request cannot be sent due to a network failure of any kind, including a timeout, the Client MUST throw an instance of Psr\Http\Client\NetworkExceptionInterface.

Clients MAY throw more specific exceptions than those defined here (a TimeOutException or HostNotFoundException for example), provided they implement the appropriate interface defined above.

The intention of this PSR is to provide library developers with HTTP clients that have a well defined behaviour. A library should be able to use any compliant client without special code to handle client implementation details (Liskov substitution principle). The PSR does not try to restrict nor define how to configure HTTP clients.

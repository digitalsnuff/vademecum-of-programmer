# PSR-4 : Autoloader

This PSR describes a specification for autoloading classes from file paths.

1. The fully qualified class name MUST have a top-level namespace name, also known as a “vendor namespace”.
2. The fully qualified class name MAY have one or more sub-namespace names.
3. The fully qualified class name MUST have a terminating class name.
4. Underscores have no special meaning in any portion of the fully qualified class name.
5. Alphabetic characters in the fully qualified class name MAY be any combination of lower case and upper case.
6. All class names MUST be referenced in a case-sensitive fashion.

1) A contiguous series of one or more leading namespace and sub-namespace names, not including the leading namespace separator, in the fully qualified class name (a “namespace prefix”) corresponds to at least one “base directory”.
2) The contiguous sub-namespace names after the “namespace prefix” correspond to a subdirectory within a “base directory”, in which the namespace separators represent directory separators. The subdirectory name MUST match the case of the sub-namespace names.
3) The terminating class name corresponds to a file name ending in .php. The file name MUST match the case of the terminating class name.

Autoloader implementations MUST NOT throw exceptions, MUST NOT raise errors of any level, and SHOULD NOT return a value.

# PSR-4 Meta Document

The purpose is to specify the rules for an interoperable PHP autoloader that maps namespaces to file system paths, and that can co-exist with any other SPL registered autoloader. This would be an addition to, not a replacement for, PSR-0.

“package-oriented autoloading” (as vs the traditional “direct class-to-file autoloading”).

## package-oriented autoloading

1. Implementors MUST use at least two namespace levels: a vendor name, and package name within that vendor. (This top-level two-name combination is hereinafter referred to as the vendor-package name or the vendor-package namespace.)
2. Implementors MUST allow a path infix between the vendor-package namespace and the remainder of the fully qualified class name.
3. The vendor-package namespace MAY map to any directory. The remaining portion of the fully-qualified class name MUST map the namespace names to identically-named directories, and MUST map the class name to an identically-named file ending in .php.

## Scope

1. Retain the PSR-0 rule that implementors MUST use at least two namespace levels: a vendor name, and package name within that vendor.
2. Allow a path infix between the vendor-package namespace and the remainder of the fully qualified class name.
3. Allow the vendor-package namespace MAY map to any directory, perhaps multiple directories.
4. End the honoring of underscores in class names as directory separators
5. Provide a general transformation algorithm for non-class resources

## Approaches

1. Autoloaders in PHP are explicitly designed to be stackable so that if one autoloader cannot load a class another has a chance to do so. Having an autoloader trigger a breaking error condition violates that compatibility.

2. class_exists() and interface_exists() allow “not found, even after trying to autoload” as a legitimate, normal use case. An autoloader that throws exceptions renders class_exists() unusable, which is entirely unacceptable from an interoperability standpoint. Autoloaders that wish to provide additional debugging information in a class-not-found case should do so via logging instead, either to a PSR-3 compatible logger or otherwise.

Split Up Autoloading And Transformation


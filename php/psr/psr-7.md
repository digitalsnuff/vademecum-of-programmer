# PSR-7 HTTP message interfaces

HTTP messages are the foundation of web development. Web browsers and HTTP clients such as cURL create HTTP request messages that are sent to a web server, which provides an HTTP response message. Server-side code receives an HTTP request message, and returns an HTTP response message.

Request contains the HTTP request method, the request target (usually either an absolute URI or a path on the web server), and the HTTP protocol version. This is followed by one or more HTTP headers, an empty line, and the message body.

Response contains the HTTP protocol version, the HTTP status code, and a “reason phrase,” a human-readable description of the status code. Like the request message, this is then followed by one or more HTTP headers, an empty line, and the message body.

`An HTTP message` is either a request from a client to a server or a response from a server to a client. This specification defines interfaces for the HTTP messages `Psr\Http\Message\RequestInterface` and `Psr\Http\Message\ResponseInterface` respectively.

## HTTP Headers

- Case-insensitive header field names

HTTP messages include **case-insensitive** header field names. Headers are retrieved by name from classes implementing the `MessageInterface` in a case-insensitive manner. For example, retrieving the `foo` header will return the same result as retrieving the `FoO` header. Similarly, setting the `Foo` header will overwrite any previously set `foo` header value.

- Headers with multiple values

When working with such headers, consumers of `MessageInterface`-based classes SHOULD rely on the `getHeader()` method for retrieving such multi-valued headers.

- Host header

In requests, the `Host` header typically mirrors the host component of the URI, as well as the host used when establishing the TCP connection. However, the HTTP specification allows the `Host` header to differ from each of the two.

## Streams

HTTP messages consist of a start-line, headers, and a body. The body of an HTTP message can be very small or extremely large. Attempting to represent the body of a message as a string can easily consume more memory than intended because the body must be stored completely in memory. Attempting to store the body of a request or response in memory would preclude the use of that implementation from being able to work with large message bodies. `StreamInterface` is used in order to hide the implementation details when a stream of data is read from or written to. For situations where a string would be an appropriate message implementation, built-in streams such as `php://memory` and `php://temp` may be used.

Each stream instance will have various capabilities: it can be read-only, write-only, or read-write. It can also allow arbitrary random access (seeking forwards or backwards to any location), or only sequential access (for example in the case of a socket, pipe, or callback-based stream).

## Request Targets and URIs

- `origin-form`, which consists of the path, and, if present, the query string; this is often referred to as a relative URL. Messages as transmitted over TCP typically are of origin-form; scheme and authority data are usually only present via CGI variables.
- `absolute-form`, which consists of the scheme, authority (“[user-info@]host[:port]”, where items in brackets are optional), path (if present), query string (if present), and fragment (if present). This is often referred to as an absolute URI, and is the only form to specify a URI as detailed in RFC 3986. This form is commonly used when making requests to HTTP proxies.
- `authority-form`, which consists of the authority only. This is typically used in CONNECT requests only, to establish a connection between an HTTP client and a proxy server.
- `asterisk-form`, which consists solely of the string \*, and which is used with the OPTIONS method to determine the general capabilities of a web server.

Aside from these request-targets, there is often an ‘effective URL’ which is separate from the request target. The effective URL is not transmitted within an HTTP message, but it is used to determine the protocol (http/https), port and hostname for making the request.

The interface provides methods for interacting with the various URI parts, which will obviate the need for repeated parsing of the URI.

`RequestInterface` provides methods for retrieving the request-target or creating a new instance with the provided request-target. By default, if no request-target is specifically composed in the instance, `getRequestTarget()` will return the origin-form of the composed URI (or “/” if no URI is composed). `withRequestTarget($requestTarget)` creates a new instance with the specified request target, and thus allows developers to create request messages that represent the other three request-target forms (absolute-form, authority-form, and asterisk-form). When used, the composed URI instance can still be of use, particularly in clients, where it may be used to create the connection to the server.

## Server-side Requests

`RequestInterface` provides the general representation of an HTTP request message. However, server-side requests need additional treatment, due to the nature of the server-side environment. Server-side processing needs to take into account Common Gateway Interface (CGI), and, more specifically, PHP’s abstraction and extension of CGI via its Server APIs (SAPI). PHP has provided simplification around input marshaling via superglobals such as:

    $_COOKIE`, which deserializes and provides simplified access to HTTP cookies.
    $_GET, which deserializes and provides simplified access to query string arguments.
    $_POST, which deserializes and provides simplified access for urlencoded parameters submitted via HTTP POST; generically, it can be considered the results of parsing the message body.
    $_FILES, which provides serialized metadata around file uploads.
    $_SERVER, which provides access to CGI/SAPI environment variables, which commonly include the request method, the request scheme, the request URI, and headers.

`ServerRequestInterface` extends `RequestInterface` to provide an abstraction around these various superglobals. This practice helps reduce coupling to the superglobals by consumers, and encourages and promotes the ability to test request consumers.

## Uploaded files

# Source

[psr/http-message](https://packagist.org/packages/psr/http-message)

[Uniform Resource Identifier (URI): Generic Syntax](https://tools.ietf.org/html/rfc3986)  
[Hypertext Transfer Protocol (HTTP/1.1): Message Syntax and Routing](https://tools.ietf.org/html/rfc7230)  
[Hypertext Transfer Protocol (HTTP/1.1): Semantics and Content](https://tools.ietf.org/html/rfc7231)

[Signing HTTP Messages](https://tools.ietf.org/id/draft-cavage-http-signatures-08.html)

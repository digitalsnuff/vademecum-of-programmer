# PSR-17 HTTP Factories

An HTTP factory is a method by which a new HTTP object, as defined by PSR-7, is created. HTTP factories MUST implement these interfaces for each object type that is provided by the package.

# Source

[zend-diactoros](https://docs.zendframework.com/zend-diactoros/)  
[Guzzle](https://github.com/guzzle/psr7)  
[Slim Framework](https://www.slimframework.com/)

# PSR-6 Caching Interface

All data passed into the Implementing Library MUST be returned exactly as passed. That includes the variable type. That is, it is an error to return (string) 5 if (int) 5 was the value saved. Implementing Libraries MAY use PHP’s `serialize()`/`unserialize()` functions internally but are not required to do so. Compatibility with them is simply used as a baseline for acceptable object values.  
If it is not possible to return the exact saved value for any reason, implementing libraries MUST respond with a cache miss rather than corrupted data.

## Pool

The Pool represents a collection of items in a caching system. The pool is a logical Repository of all items it contains. All cacheable items are retrieved from the Pool as an Item object, and all interaction with the whole universe of cached objects happens through the Pool.

## Items

An Item represents a single key/value pair within a Pool. The key is the primary unique identifier for an Item and MUST be immutable. The Value MAY be changed at any time.

## Error handling

While caching is often an important part of application performance, it should never be a critical part of application functionality. Thus, an error in a cache system SHOULD NOT result in application failure. For that reason, Implementing Libraries MUST NOT throw exceptions other than those defined by the interface, and SHOULD trap any errors or exceptions triggered by an underlying data store and not allow them to bubble.

- A standard interface for caching allows free-standing libraries to support caching of intermediary data without effort; they may simply (optionally) depend on this standard interface and leverage it without being concerned about implementation details.
- Commonly developed caching libraries shared by multiple projects, even if they extend this interface, are likely to be more robust than a dozen separately developed implementations.
- Any interface standardization runs the risk of stifling future innovation as being “not the Way It’s Done(tm)”. However, we believe caching is a sufficiently commoditized problem space that the extension capability offered here mitigates any potential risk of stagnation.

* This specification adopts a “repository model” or “data mapper” model for caching rather than the more traditional “expire-able key-value” model. The primary reason is flexibility. A simple key/value model is much more difficult to extend.

- “Weak item” approach
- “Naked value” approach
- ArrayAccess Pool

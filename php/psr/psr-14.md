# PSR-14 Event Dispatcher

Event Dispatching is a common and well-tested mechanism to allow developers to inject logic into an application easily and consistently.

The goal of this PSR is to establish a common mechanism for event-based extension and collaboration so that libraries and components may be reused more freely between various applications and frameworks.

## Definitions

- `Event` - An Event is a message produced by an Emitter. It may be any arbitrary PHP object.
- `Listener` - A Listener is any PHP callable that expects to be passed an Event. Zero or more Listeners may be passed the same Event. A Listener MAY enqueue some other asynchronous behavior if it so chooses.
- `Emitter` - An Emitter is any arbitrary code that wishes to dispatch an Event. This is also known as the “calling code”. It is not represented by any particular data structure but refers to the use case.
- `Dispatcher` - A Dispatcher is a service object that is given an Event object by an Emitter. The Dispatcher is responsible for ensuring that the Event is passed to all relevant Listeners, but MUST defer determining the responsible listeners to a Listener Provider.
- `Listener Provider` - A Listener Provider is responsible for determining what Listeners are relevant for a given Event, but MUST NOT call the Listeners itself. A Listener Provider may specify zero or more relevant Listeners.

Event objects MAY be **mutable** should the use case call for Listeners providing information back to the Emitter

If no such bidirectional communication is needed then it is RECOMMENDED that the Event be defined as **immutable**.

Implementers MUST assume that the same object will be passed to all Listeners.

It is RECOMMENDED, but NOT REQUIRED, that Event objects support lossless serialization and deserialization

A `Stoppable Event` is a special case of Event that contains additional ways to prevent further Listeners from being called. It is indicated by implementing the StoppableEventInterface.

## Listeners

- A Listener may be any PHP callable.
- A Listener MUST have one and only one parameter, which is the Event to which it responds.
- Listeners SHOULD type hint that parameter as specifically as is relevant for their use case.
- A Listener SHOULD have a void return, and SHOULD type hint that return explicitly.
- A Dispatcher MUST ignore return values from Listeners.
- A Listener MAY delegate actions to other code.
- A Listener MAY enqueue information from the Event for later processing by a secondary process, using cron, a queue server, or similar techniques.
- A secondary process MUST assume that any changes it makes to an Event object will NOT propagate to other Listeners.

## Dispatcher

A Dispatcher is a service object implementing EventDispatcherInterface. It is responsible for retrieving Listeners from a Listener Provider for the Event dispatched, and invoking each Listener with that Event.

- MUST call Listeners synchronously in the order they are returned from a ListenerProvider.
- MUST return the same Event object it was passed after it is done invoking Listeners.
- MUST NOT return to the Emitter until all Listeners have executed.

If passed a Stoppable Event, a Dispatcher

- MUST call `isPropagationStopped()` on the Event before each Listener has been called. If that method returns true it MUST return the Event to the Emitter immediately and MUST NOT call any further Listeners. This implies that if an Event is passed to the Dispatcher that always returns true from isPropagationStopped(), zero listeners will be called.

A Dispatcher SHOULD assume that any Listener returned to it from a Listener Provider is type-safe. That is, the Dispatcher SHOULD assume that calling $listener($event) will not produce a TypeError.

An Exception or Error thrown by a Listener MUST block the execution of any further Listeners. An Exception or Error thrown by a Listener MUST be allowed to propagate back up to the Emitter.

A Dispatcher MAY catch a thrown object to log it, allow additional action to be taken, etc., but then MUST rethrow the original throwable.

## Listener Provider

A Listener Provider is a service object responsible for determining what Listeners are relevant to and should be called for a given Event.

- Allowing for some form of registration mechanism so that implementers may assign a Listener to an Event in a fixed order.
- Deriving a list of applicable Listeners through reflection based on the type and implemented interfaces of the Event.
- Generating a compiled list of Listeners ahead of time that may be consulted at runtime.
  Implementing some form of access control so that certain Listeners will only be called if the current user has a certain permission.
- Extracting some information from an object referenced by the Event, such as an Entity, and calling pre-defined lifecycle methods on that object.
- Delegating its responsibility to one or more other Listener Providers using some arbitrary logic.

Listener Providers MUST treat parent types identically to the Event’s own type when determining listener applicability.

A Dispatcher SHOULD compose a Listener Provider to determine relevant listeners. It is RECOMMENDED that a Listener Provider be implemented as a distinct object from the Dispatcher but that is NOT REQUIRED.

The Working Group identified four possible workflows for event passing, based on use cases seen in the wild in various systems.

- One-way notification. (“I did a thing, if you care.”)
- Object enhancement. (“Here’s a thing, please modify it before I do something with it.”)
- Collection. (“Give me all your things, that I may do something with that list.”)
- Alternative chain. (“Here’s a thing; the first one of you that can handle it do so, then stop.”)

On further review, the Working Goup determined that:

- Collection was a special case of object enhancement (the collection being the object that is enhanced).
- Alternative chain is similarly a special case of object enhancement, as the signature is identical and the dispatch workflow is nearly identical, albeit with an extra check included.
- One-way notification is a degenerate case of the others, or can be represented as such.

  4.2 Example applications

- Indicating that some change in system configuration or some user action has occurred and allowing other systems to react in ways that do not affect program flow (such as sending an email or logging the action).
- Passing an object to a series of Listeners to allow it to be modified before it is saved to a persistence system.
- Passing a collection to a series of Listeners to allow them to register values with it or modify existing values so that the Emitter may act on all of the collected information.
- Passing some contextual information to a series of Listeners so that all of them may “vote” on what action to take, with the Emitter deciding based on the aggregate information provided.
- Passing an object to a series of Listeners and allowing any Listener to terminate the process early before other Listeners have completed.

  4.4 Listener registration

A Listener:

- could be registered explicitly;
- could be registered explicitly based on reflection of its signature;
- could be registered with a numeric priority order;
- could be registered using a before/after mechanism to control ordering more precisely;
- could be registered from a service container;
- could use a pre-compile step to generate code;
- could be based on method names on objects in the Event itself;
- could be limited to certain situations or contexts based on arbitrarily complex logic (only for certain users, only on certain days, only if certain system settings are present, etc).

The Provider SHOULD be composed as a dependent object.

4.5 Deferred listeners

That allows all of the following behaviors to be legal:

- Providers return callable Listeners that were provided to them.
- Providers return callables that create an entry in a queue that will react to the Event with another callable at some later point in time.
- Listeners may themselves create an entry in a queue that will react to the Event at some later point in time.
- Listeners or Providers may trigger an asynchronous task, if running in an environment with support for asynchronous behavior (assuming that the result of the asynchronous task is not needed by the Emitter.)
- Providers may perform such delay or wrapping on Listeners selectively based on arbitrary logic.

  4.6 Return values

Per the spec, a Dispatcher MUST return the Event passed by the Emitter.

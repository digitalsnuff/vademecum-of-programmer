# PSR-13: Link definition interfaces

read-only or evolvable versions

Response objects by design must be immutable. Other value-object implementations likely would also require an immutable interface.

Different hypermedia standards handle multiple links with the same relationship differently. Some have a single link that has multiple rel’s defined. Others have a single rel entry that then contains multiple links.

Defining each Link uniquely but allowing it to have multiple rels provides a most-compatible-denominator definition.

In many contexts, a set of links will be attached to some other object. Those objects may be used in situations where all that is relevant is their links, or some subset of their links. For example, various different value objects may be defined that represent different REST formats such as HAL, JSON-LD, or Atom. It may be useful to extract those links from such an object uniformly for further processing. For instance, next/previous links may be extracted from an object and added to a PSR-7 Response object as Link headers. Alternatively, many links would make sense to represent with a “preload” link relationship, which would indicate to an HTTP 2-compatible web server that the linked resources should be streamed to a client in anticipation of a subsequent request.

## Link Provider

Link Provider objects may not be value objects but other objects within a given domain, which are able to generate Links on the fly, perhaps off of a database result or other underlying representation. In those cases a writeable provider definition would be incompatible.

[Package psr/link](https://packagist.org/packages/psr/link)

# Source

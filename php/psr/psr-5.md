# PSR-5 PHP Doc Standard

- "PHPDoc" is a section of documentation which provides information on aspects of a "Structural Element".

```
It is important to note that a PHPDoc and a DocBlock are two separate entities. The DocBlock is the combination of a DocComment, which is a type of comment, and a PHPDoc entity. It is the PHPDoc entity that contains the syntax as described in this specification (such as the description and tags).
```

- "Structural Element" is a collection of Programming Constructs which MAY be preceded by a DocBlock. The collection contains the following constructs:
  - require(\_once)
  - include(\_once)
  - class
  - interface
  - trait
  - function (including methods)
  - property
  - constant
  - variables, both local and global scope.

It is RECOMMENDED to precede a "Structural Element" with a DocBlock where it is defined and not with each usage. It is common practice to have the DocBlock precede a Structural Element but it MAY also be separated by an undetermined number of empty lines.

This Standard does not cover this specific instance, as a foreach statement is considered to be a "Control Flow" statement rather than a "Structural Element".

- "DocComment" is a special type of comment which MUST

  - start with the character sequence `/**` followed by a whitespace character
  - end with `*/` and
  - have zero or more lines in between.

- "DocBlock" is a "DocComment" containing a single "PHPDoc" structure and represents the basic in-source representation.
- "Tag" is a single piece of meta information regarding a "Structural Element" or a component thereof.
- "Type" is the determination of what type of data is associated with an element. This is commonly used when determining the exact values of arguments, constants, properties and more.
- "FQSEN" is an abbreviation for Fully Qualified Structural Element Name. This notation expands on the Fully Qualified Class Name and adds a notation to identify class/interface/trait members and re-apply the principles of the FQCN to Interfaces, Traits, Functions and global Constants.

If a Description is provided, then it MUST be preceded by a Summary. Otherwise the Description will be considered the Summary, until the end of the Summary is reached.

Because a Summary is comparable to a chapter title, it is beneficial to use as little formatting as possible.

Any application parsing the Description is RECOMMENDED to support the Markdown mark-up language for this field so that it is possible for the author to provide formatting and a clear way of representing code examples.

Tags provide a way for authors to supply concise meta-data regarding the succeeding "Structural Element". Each tag starts on a new line, followed by an at-sign (@) and a tag-name, followed by white-space and meta-data (including a description).

The description of a tag MUST support Markdown as a formatting language. Due to the nature of Markdown, it is legal to start the description of the tag on the same or the subsequent line and interpret it in the same way.

When the "Type" consists of multiple types, then these MUST be separated with either the vertical bar sign (|) for union type or the ampersand (&) for intersection type. Any interpreter supporting this specification MUST recognize this and split the "Type" before evaluating.

The value represented by "Type" can be an array. The type MUST be defined following the format of one of the following options:

1. unspecified: no definition of the contents of the represented array is given. Example: `@return array`
2. specified containing a single type: the Type definition informs the reader of the type of each array value. Only one type is then expected for each value in a given array.

   Example: `@return int[]`

   Please note that _mixed_ is also a single type and with this keyword it is possible to indicate that each array value contains any possible type.

3. specified as containing multiple types: the Type definition informs the reader of the type of each array value. Each value can be of any of the given types. Example: @return `(int|string)[]`

# Source

https://github.com/php-fig/fig-standards/blob/master/proposed/phpdoc.md  
[ABNF](https://tools.ietf.org/html/rfc5234)  
[Fluent Interface](https://en.wikipedia.org/wiki/Fluent_interface)  
[Domain-Specific Language](https://en.wikipedia.org/wiki/Domain-specific_language)
[General-Purpose Language](https://en.wikipedia.org/wiki/General-purpose_language)

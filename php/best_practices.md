# Bext Practices in PHP

Dobre praktyki, które należy stosować przy budowie aplikacji PHP.
Postępowanie zgodne z tymi wytycznymi pozwala tworzyć szybsze, bezpieczniejsze i stabilniejsze programy.

## Dezynfekcja, sprawdzanie i kontrola wyjścia

NIGDY NIE NALEŻY UFAĆ DANYM POCHODZĄCYM ZE ŹRÓDŁA NIEBĘDĄCEGO POD NASZĄ KONTROLĄ.

Źródła:

- \$\_GET
- \$\_POST
- \$\_REQUEST
- \$\_COOKIE
- \$argv
- php://stdin
- php://input
- `file_get_contents()`
- zdalne bazy danych
- zdalne interfejsy API
- dane od klientów

- Dezynfekuj dane wejściowe (HTML, SQL)
- dokładnie sprawdzaj dane
- kontroluj dane
- kontroluj, co wysyłasz na wyjście

## Hasła

- Nie poznawaj haseł uzytkowników
- Nie ograniczaj haseł użytkownikom
- nigdy nie wysyłaj haseł na adres e-mail
- mieszaj hasła użytkowników za pomocą algorytmu bcrypt
- jeśli chodzi o generowanie i przechowywanie haseł, należy wybierać algorytmy powolne i bezpieczne
- API mieszania haseł (Anthony Ferrara)

Mieszanie (hash) to proces jednokierunkowy

## Data, godzina i strefa czasowa

## Bazy danych

## Łańcuchy wielobajtowe

`Znak wielobajtowy` - każdy znak spoza tradycyjnego zestawu 128 znaków ASCII.

Domyślne funkcje w PHP do pracy z łańcuchami zakładają, że wszystkie znaki mają rozmiar 8 bitów.

Rozszerzenie `mb_string`. Zamiastr użyć np. `strlen()`, można użyć `mb_strlen()`. Rozpoznaje ona wielobajtowe znaki UNICODE.

Należy używać kodowania UTF-8.

Kodowanie znaków to metoda pakowania danych Unicode w formacie przeznaczonym do przechowywania w pamięci lub wysyłania przez internet między serwerem i klientem.

Zawsze należy znać kodowanie znaków.

## Strumienie



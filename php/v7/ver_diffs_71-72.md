# Migrating from PHP 7.1.x to PHP 7.2.x

## New features

### New object type

A new type, object, has been introduced that can be used for (contravariant) parameter typing and (covariant) return typing of any objects.

### Extension loading by name

Shared extensions no longer require their file extension (.so for Unix or .dll for Windows) to be specified. This is enabled in the php.ini file, as well as in the dl() function.

### Abstract method overriding

Abstract methods can now be overridden when an abstract class extends another abstract class.
contravariance
covariance

### Sodium is now a core extension

The modern Sodium cryptography library has now become a core extension in PHP.

### Password hashing with Argon2

Argon2 has been added to the password hashing API, where the following constants have been exposed:

- PASSWORD_ARGON2I
- PASSWORD_ARGON2_DEFAULT_MEMORY_COST
- PASSWORD_ARGON2_DEFAULT_TIME_COST
- PASSWORD_ARGON2_DEFAULT_THREADS

### Extended string types for PDO

PDO's string type has been extended to support the national character type when emulating prepares. This has been done with the following constants:

- PDO::PARAM_STR_NATL
- PDO::PARAM_STR_CHAR
- PDO::ATTR_DEFAULT_STR_PARAM

### Additional emulated prepares debugging information for PDO

The PDOStatement::debugDumpParams() method has been updated to include the SQL being sent to the DB, where the full, raw query (including the replaced placeholders with their bounded values) will be shown. This has been added to aid with debugging emulated prepares (and so it will only be available when emulated prepares are turned on).

### Support for extended operations in LDAP

Support for EXOP has been added to the LDAP extension. This has been done by exposing the following functions and constants:

- ldap_parse_exop()
- ldap_exop()
- ldap_exop_passwd()
- ldap_exop_whoami()

- LDAP_EXOP_START_TLS
- LDAP_EXOP_MODIFY_PASSWD
- LDAP_EXOP_REFRESH
- LDAP_EXOP_WHO_AM_I
- LDAP_EXOP_TURN

### Address Information additions to the Sockets extension

The sockets extension now has the ability to lookup address information, as well as connect to it, bind to it, and explain it. The following four functions have been added for this:

- socket_addrinfo_lookup()
- socket_addrinfo_connect()
- socket_addrinfo_bind()
- socket_addrinfo_explain()

### Parameter type widening

Parameter types from overridden methods and from interface implementations may now be omitted. This is still in compliance with LSP, since parameters types are contravariant.

### Allow a trailing comma for grouped namespaces

A trailing comma can now be added to the group-use syntax introduced in PHP 7.0.

### proc_nice() support on Windows

### pack() and unpack() endian support

The pack() and unpack() functions now support float and double in both little and big endian.

### Enhancements to the EXIF extension

The EXIF extension has been updated to support a much larger range of formats. This means that their format specific tags are now properly translated when parsing images with the exif_read_data() function. The following new formats are now supported:

Samsung
DJI
Panasonic
Sony
Pentax
Minolta
Sigma/Foveon
AGFA
Kyocera
Ricoh
Epson
The EXIF functions exif_read_data() and exif_thumbnail() now support passing streams as their first argument.

### New features in PCRE

The J modifier for setting PCRE_DUPNAMES has been added.

### SQLite3 allows writing BLOBs

SQLite3::openBlob() now allows to open BLOB fields in write mode; formerly only read mode was supported.

### Oracle OCI8 Transparent Application Failover Callbacks

Support for Oracle Database Transparent Application Failover (TAF) callbacks has been added. TAF allows PHP OCI8 applications to automatically reconnect to a preconfigured database when a connection is broken. The new TAF callback support allows PHP applications to monitor and control reconnection during failover.

### Enhancements to the ZIP extension

Read and write support for encrypted archives has been added (requires libzip 1.2.0).

The ZipArchive class now implements the Countable interface.

The zip:// stream now accepts a 'password' context option.

## New functions

### PHP Core

- stream_isatty()
- sapi_windows_vt100_support()

### SPL

- spl_object_id()

### DOM

- DomNodeList::count()
- DOMNamedNodeMap::count()

### FTP

- ftp_append()

### GD

- imagesetclip()
- imagegetclip()
- imageopenpolygon()
- imageresolution()
- imagecreatefrombmp()
- imagebmp()

### Hash

- hash_hmac_algos()

### LDAP

- ldap_parse_exop()
- ldap_exop()
- ldap_exop_passwd()
- ldap_exop_whoami()

### Multibyte String

- mb_chr()
- mb_ord()
- mb_scrub()

### Oracle OCI8

- oci_register_taf_callback()
- oci_unregister_taf_callback()

### Sockets

- socket_addrinfo_lookup()
- socket_addrinfo_connect()
- socket_addrinfo_bind()
- socket_addrinfo_explain()

### Sodium

- sodium_add()
- sodium_bin2hex()
- sodium_compare()
- sodium_crypto_aead_aes256gcm_decrypt()
- sodium_crypto_aead_aes256gcm_encrypt()
- sodium_crypto_aead_aes256gcm_is_available()
- sodium_crypto_aead_aes256gcm_keygen()
- sodium_crypto_aead_chacha20poly1305_decrypt()
- sodium_crypto_aead_chacha20poly1305_encrypt()
- sodium_crypto_aead_chacha20poly1305_ietf_decrypt()
- sodium_crypto_aead_chacha20poly1305_ietf_encrypt()
- sodium_crypto_aead_chacha20poly1305_ietf_keygen()
- sodium_crypto_aead_chacha20poly1305_keygen()
- sodium_crypto_auth_keygen()
- sodium_crypto_auth_verify()
- sodium_crypto_auth()
- sodium_crypto_box_keypair_from_secretkey_and_publickey()
- sodium_crypto_box_keypair()
- sodium_crypto_box_open()
- sodium_crypto_box_publickey_from_secretkey()
- sodium_crypto_box_publickey()
- sodium_crypto_box_seal_open()
- sodium_crypto_box_seal()
- sodium_crypto_box_secretkey()
- sodium_crypto_box_seed_keypair()
- sodium_crypto_box()
- sodium_crypto_generichash_final()
- sodium_crypto_generichash_init()
- sodium_crypto_generichash_keygen()
- sodium_crypto_generichash_update()
- sodium_crypto_generichash()
- sodium_crypto_kdf_derive_from_key()
- sodium_crypto_kdf_keygen()
- sodium_crypto_kx_client_session_keys()
- sodium_crypto_kx_keypair()
- sodium_crypto_kx_publickey()
- sodium_crypto_kx_secretkey()
- sodium_crypto_kx_seed_keypair()
- sodium_crypto_kx_server_session_keys()
- sodium_crypto_pwhash_scryptsalsa208sha256_str_verify()
- sodium_crypto_pwhash_scryptsalsa208sha256_str()
- sodium_crypto_pwhash_scryptsalsa208sha256()
- sodium_crypto_pwhash_str_verify()
- sodium_crypto_pwhash_str()
- sodium_crypto_pwhash()
- sodium_crypto_scalarmult_base()
- sodium_crypto_scalarmult()
- sodium_crypto_secretbox_keygen()
- sodium_crypto_secretbox_open()
- sodium_crypto_secretbox()
- sodium_crypto_shorthash_keygen()
- sodium_crypto_shorthash()
- sodium_crypto_sign_detached()
- sodium_crypto_sign_ed25519_pk_to_curve25519()
- sodium_crypto_sign_ed25519_sk_to_curve25519()
- sodium_crypto_sign_keypair_from_secretkey_and_publickey()
- sodium_crypto_sign_keypair()
- sodium_crypto_sign_open()
- sodium_crypto_sign_publickey_from_secretkey()
- sodium_crypto_sign_publickey()
- sodium_crypto_sign_secretkey()
- sodium_crypto_sign_seed_keypair()
- sodium_crypto_sign_verify_detached()
- sodium_crypto_sign()
- sodium_crypto_stream_keygen()
- sodium_crypto_stream_xor()
- sodium_crypto_stream()
- sodium_hex2bin()
- sodium_increment()
- sodium_memcmp()
- sodium_memzero()
- sodium_pad()
- sodium_unpad()

### ZIP

- ZipArchive::count()
- ZipArchive::setEncryptionName()
- ZipArchive::setEncryptionIndex()

## New global constants

### PHP Core

- PHP_FLOAT_DIG
- PHP_FLOAT_EPSILON
- PHP_FLOAT_MIN
- PHP_FLOAT_MAX
- PHP_OS_FAMILY

### File Information

- FILEINFO_EXTENSION

### JSON

- JSON_INVALID_UTF8_IGNORE
- JSON_INVALID_UTF8_SUBSTITUTE

### GD

- IMG_EFFECT_MULTIPLY
- IMG_BMP

### LDAP

- LDAP_EXOP_START_TLS
- LDAP_EXOP_MODIFY_PASSWD
- LDAP_EXOP_REFRESH
- LDAP_EXOP_WHO_AM_I
- LDAP_EXOP_TURN

### Password Hashing

- PASSWORD_ARGON2I
- PASSWORD_ARGON2_DEFAULT_MEMORY_COST
- PASSWORD_ARGON2_DEFAULT_TIME_COST
- PASSWORD_ARGON2_DEFAULT_THREADS

### PCRE

- PREG_UNMATCHED_AS_NULL

### PDO

- PDO::PARAM_STR_NATL
- PDO::PARAM_STR_CHAR
- PDO::ATTR_DEFAULT_STR_PARAM

### Sodium

- SODIUM_LIBRARY_VERSION
- SODIUM_LIBRARY_MAJOR_VERSION
- SODIUM_LIBRARY_MINOR_VERSION
- SODIUM_CRYPTO_AEAD_AES256GCM_KEYBYTES
- SODIUM_CRYPTO_AEAD_AES256GCM_NSECBYTES
- SODIUM_CRYPTO_AEAD_AES256GCM_NPUBBYTES
- SODIUM_CRYPTO_AEAD_AES256GCM_ABYTES
- SODIUM_CRYPTO_AEAD_CHACHA20POLY1305_KEYBYTES
- SODIUM_CRYPTO_AEAD_CHACHA20POLY1305_NSECBYTES
- SODIUM_CRYPTO_AEAD_CHACHA20POLY1305_NPUBBYTES
- SODIUM_CRYPTO_AEAD_CHACHA20POLY1305_ABYTES
- SODIUM_CRYPTO_AEAD_CHACHA20POLY1305_IETF_KEYBYTES
- SODIUM_CRYPTO_AEAD_CHACHA20POLY1305_IETF_NSECBYTES
- SODIUM_CRYPTO_AEAD_CHACHA20POLY1305_IETF_NPUBBYTES
- SODIUM_CRYPTO_AEAD_CHACHA20POLY1305_IETF_ABYTES
- SODIUM_CRYPTO_AUTH_BYTES
- SODIUM_CRYPTO_AUTH_KEYBYTES
- SODIUM_CRYPTO_BOX_SEALBYTES
- SODIUM_CRYPTO_BOX_SECRETKEYBYTES
- SODIUM_CRYPTO_BOX_PUBLICKEYBYTES
- SODIUM_CRYPTO_BOX_KEYPAIRBYTES
- SODIUM_CRYPTO_BOX_MACBYTES
- SODIUM_CRYPTO_BOX_NONCEBYTES
- SODIUM_CRYPTO_BOX_SEEDBYTES
- SODIUM_CRYPTO_KDF_BYTES_MIN
- SODIUM_CRYPTO_KDF_BYTES_MAX
- SODIUM_CRYPTO_KDF_CONTEXTBYTES
- SODIUM_CRYPTO_KDF_KEYBYTES
- SODIUM_CRYPTO_KX_SEEDBYTES
- SODIUM_CRYPTO_KX_SESSIONKEYBYTES
- SODIUM_CRYPTO_KX_PUBLICKEYBYTES
- SODIUM_CRYPTO_KX_SECRETKEYBYTES
- SODIUM_CRYPTO_KX_KEYPAIRBYTES
- SODIUM_CRYPTO_GENERICHASH_BYTES
- SODIUM_CRYPTO_GENERICHASH_BYTES_MIN
- SODIUM_CRYPTO_GENERICHASH_BYTES_MAX
- SODIUM_CRYPTO_GENERICHASH_KEYBYTES
- SODIUM_CRYPTO_GENERICHASH_KEYBYTES_MIN
- SODIUM_CRYPTO_GENERICHASH_KEYBYTES_MAX
- SODIUM_CRYPTO_PWHASH_ALG_ARGON2I13
- SODIUM_CRYPTO_PWHASH_ALG_DEFAULT
- SODIUM_CRYPTO_PWHASH_SALTBYTES
- SODIUM_CRYPTO_PWHASH_STRPREFIX
- SODIUM_CRYPTO_PWHASH_OPSLIMIT_INTERACTIVE
- SODIUM_CRYPTO_PWHASH_MEMLIMIT_INTERACTIVE
- SODIUM_CRYPTO_PWHASH_OPSLIMIT_MODERATE
- SODIUM_CRYPTO_PWHASH_MEMLIMIT_MODERATE
- SODIUM_CRYPTO_PWHASH_OPSLIMIT_SENSITIVE
- SODIUM_CRYPTO_PWHASH_MEMLIMIT_SENSITIVE
- SODIUM_CRYPTO_PWHASH_SCRYPTSALSA208SHA256_SALTBYTES
- SODIUM_CRYPTO_PWHASH_SCRYPTSALSA208SHA256_STRPREFIX
- SODIUM_CRYPTO_PWHASH_SCRYPTSALSA208SHA256_OPSLIMIT_INTERACTIVE
- SODIUM_CRYPTO_PWHASH_SCRYPTSALSA208SHA256_MEMLIMIT_INTERACTIVE
- SODIUM_CRYPTO_PWHASH_SCRYPTSALSA208SHA256_OPSLIMIT_SENSITIVE
- SODIUM_CRYPTO_PWHASH_SCRYPTSALSA208SHA256_MEMLIMIT_SENSITIVE
- SODIUM_CRYPTO_SCALARMULT_BYTES
- SODIUM_CRYPTO_SCALARMULT_SCALARBYTES
- SODIUM_CRYPTO_SHORTHASH_BYTES
- SODIUM_CRYPTO_SHORTHASH_KEYBYTES
- SODIUM_CRYPTO_SECRETBOX_KEYBYTES
- SODIUM_CRYPTO_SECRETBOX_MACBYTES
- SODIUM_CRYPTO_SECRETBOX_NONCEBYTES
- SODIUM_CRYPTO_SIGN_BYTES
- SODIUM_CRYPTO_SIGN_SEEDBYTES
- SODIUM_CRYPTO_SIGN_PUBLICKEYBYTES
- SODIUM_CRYPTO_SIGN_SECRETKEYBYTES
- SODIUM_CRYPTO_SIGN_KEYPAIRBYTES
- SODIUM_CRYPTO_STREAM_NONCEBYTES
- SODIUM_CRYPTO_STREAM_KEYBYTES

### ZIP

- ZipArchive::EM_NONE
- ZipArchive::EM_AES_128
- ZipArchive::EM_AES_192
- ZipArchive::EM_AES_256

## Backward incompatible changes

### Prevent number_format() from returning negative zero

Previously, it was possible for the number_format() function to return -0. Whilst this is perfectly valid according to the IEEE 754 floating point specification, this oddity was not desirable for displaying formatted numbers in a human-readable form.

### Convert numeric keys in object and array casts

Numeric keys are now better handled when casting arrays to objects and objects to arrays (either from explicit casting or by settype()).

This means that integer (or stringy integer) keys from arrays being casted to objects are now accessible
and integer (or stringy integer) keys from objects being casted to arrays are now accessible.

### Disallow passing NULL to get_class()

Previously, passing NULL to the get_class() function would output the name of the enclosing class. This behaviour has now been removed, where an E_WARNING will be output instead. To achieve the same behaviour as before, the argument should simply be omitted.

### Warn when counting non-countable types

An E_WARNING will now be emitted when attempting to count() non-countable types (this includes the sizeof() alias function).

### Move ext/hash from resources to objects

As part of the long-term migration away from resources, the Hash extension has been updated to use objects instead of resources. The change should be seamless for PHP developers, except for where is_resource() checks have been made (which will need updating to is_object() instead).

### Improve SSL/TLS defaults

The following changes to the defaults have been made:

tls:// now defaults to TLSv1.0 or TLSv1.1 or TLSv1.2
ssl:// an alias of tls://
STREAM*CRYPTO_METHOD_TLS*\* constants default to TLSv1.0 or TLSv1.1 + TLSv1.2, instead of TLSv1.0 only

### gettype() return value on closed resources

Previously, using gettype() on a closed resource would return a string of "unknown type". Now, a string of "resource (closed)" will be returned.

### is_object() and \_\_PHP_Incomplete_Class

Previously, using is_object() on the \_\_PHP_Incomplete_Class class would return FALSE. Now, TRUE will be returned.

### Promote the error level of undefined constants

Unqualified references to undefined constants will now generate an E_WARNING (instead of an E_NOTICE). In the next major version of PHP, they will generate Error exceptions.

### Windows support

The officially supported, minimum Windows versions are now Windows 7/Server 2008 R2.

### Checks on default property values of traits

Compatibility checks upon default trait property values will no longer perform casting.

### object for class names

The object name was previously soft-reserved in PHP 7.0. This is now hard-reserved, prohibiting it from being used as a class, trait, or interface name.

### NetWare support

Support for NetWare has now been removed.

### array_unique() with SORT_STRING

While array_unique() with SORT_STRING formerly copied the array and removed non-unique elements (without packing the array afterwards), now a new array is built by adding the unique elements. This can result in different numeric indexes.

### bcmod() changes with floats

The bcmod() function no longer truncates fractional numbers to integers. As such, its behavior now follows fmod(), rather than the % operator. For example bcmod('4', '3.5') now returns 0.5 instead of 1.

### Hashing functions and non-cryptographic hashes

The hash_hmac(), hash_hmac_file(), hash_pbkdf2(), and hash_init() (with HASH_HMAC) functions no longer accept non-cryptographic hashes.

### json_decode() function options

The json_decode() function option, JSON_OBJECT_AS_ARRAY, is now used if the second parameter (assoc) is NULL. Previously, JSON_OBJECT_AS_ARRAY was always ignored.

### rand() and mt_rand() output

Sequences generated by rand() and mt_rand() for a specific seed may differ from PHP 7.1 on 64-bit machines (due to the fixing of a modulo bias bug in the implementation).

### Removal of sql.safe_mode ini setting

The sql.safe_mode ini setting has now been removed.

### Changes to date_parse() and date_parse_from_format()

The zone element of the array returned by date_parse() and date_parse_from_format() represents seconds instead of minutes now, and its sign is inverted. For instance -120 is now 7200.

## Deprecated features in PHP 7.2.x

### Unquoted strings

Unquoted strings that are non-existent global constants are taken to be strings of themselves. This behaviour used to emit an E_NOTICE, but will now emit an E_WARNING. In the next major version of PHP, an Error exception will be thrown instead.

### png2wbmp() and jpeg2wbmp()

The png2wbmp() and jpeg2wbmp() functions from the GD extension have now been deprecated and will be removed in the next major version of PHP.

### INTL_IDNA_VARIANT_2003 variant

The Intl extension has deprecated the INTL_IDNA_VARIANT_2003 variant, which is currently being used as the default for idn_to_ascii() and idn_to_utf8(). PHP 7.4 will see these defaults changed to INTL_IDNA_VARIANT_UTS46, and the next major version of PHP will remove INTL_IDNA_VARIANT_2003 altogether.

### \_\_autoload() method

The \_\_autoload() method has been deprecated because it is inferior to spl_autoload_register() (due to it not being able to chain autoloaders), and there is no interoperability between the two autoloading styles.

### track_errors ini setting and \$php_errormsg variable

When the track_errors ini setting is enabled, a \$php_errormsg variable is created in the local scope when a non-fatal error occurs. Given that the preferred way of retrieving such error information is by using error_get_last(), this feature has been deprecated.

### create_function() function

Given the security issues of this function (being a thin wrapper around eval()), this dated function has now been deprecated. The preferred alternative is to use anonymous functions.

### mbstring.func_overload ini setting

Given the interoperability problems of string-based functions being used in environments with this setting enabled, it has now been deprecated.

### (unset) cast

Casting any expression to this type will always result in NULL, and so this superfluous casting type has now been deprecated.

### parse_str() without a second argument

Without the second argument to parse_str(), the query string parameters would populate the local symbol table. Given the security implications of this, using parse_str() without a second argument has now been deprecated. The function should always be used with two arguments, as the second argument causes the query string to be parsed into an array.

### gmp_random() function

This function generates a random number based upon a range that is calculated by an unexposed, platform-specific limb size. Because of this, the function has now been deprecated. The preferred way of generating a random number using the GMP extension is by gmp_random_bits() and gmp_random_range().

### each() function

This function is far slower at iteration than a normal foreach, and causes implementation issues for some language changes. It has therefore been deprecated.

### assert() with a string argument

Using assert() with a string argument required the string to be eval()'ed. Given the potential for remote code execution, using assert() with a string argument has now been deprecated in favour of using boolean expressions.

### \$errcontext argument of error handlers

The \$errcontext argument contains all local variables of the error site. Given its rare usage, and the problems it causes with internal optimisations, it has now been deprecated. Instead, a debugger should be used to retrieve information on local variables at the error site.

### read_exif_data() function

The read_exif_data() alias has been deprecated. The exif_read_data() function should be used instead.

## Other changes

### Moving of utf8_encode() and utf8_decode()

The utf8_encode() and utf8_decode() functions have now been moved to the standard extension as string functions, whereas before the XML extension was required for them to be available.

### Changes to mail() and mb_sendmail()

The \$additional_headers parameter of mail() and mb_sendmail() now also accepts an array instead of a string.

### LMDB support

The DBA extension now has support for LMDB.

### Changes to the PHP build system

- Unix: Autoconf 2.64 or greater is now required to build PHP.
- Unix: --with-pdo-oci configure argument no longer needs the version number of the Oracle Instant Client.
- Unix: --enable-gd-native-ttf configure argument has been removed. This was not used since PHP 5.5.0.
- Windows: --with-config-profile configure argument has been added. This can be used to save specific configures, much like the magical config.nice.bat file.

### Changes to GD

- imageantialias() is now also available if compiled with a system libgd.
- imagegd() stores truecolor images as real truecolor images. Formerly, they have been converted to palette.

### Moving MCrypt to PECL

The MCrypt extension has now been moved out of the core to PECL. Given the mcrypt library has not seen any updates since 2007, its usage is highly discouraged. Instead, either the OpenSSL or Sodium extension should be used.

### session_module_name()

Passing "user" to session_module_name() now raises an error of level E_RECOVERABLE_ERROR. Formerly, this has been silently ignored.

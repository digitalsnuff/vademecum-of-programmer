# Migrating from PHP 5.6.x to PHP 7.0.x

## Backward incompatible changes

### Changes to error and exception handling

A fuller description of how errors operate in PHP 7 can be found on [the PHP 7 errors page](https://www.php.net/manual/en/language.errors.php7.php).

Many fatal and recoverable fatal errors have been converted to exceptions in PHP 7. These error exceptions inherit from the Error class, which itself implements the Throwable interface (the new base interface all exceptions inherit).

This means that custom error handlers may no longer be triggered because exceptions may be thrown instead (causing new fatal errors for uncaught Error exceptions).

- `set_exception_handler()` is no longer guaranteed to receive Exception objects

Code that implements an exception handler registered with set_exception_handler() using a type declaration of Exception will cause a fatal error when an Error object is thrown.

If the handler needs to work on both PHP 5 and 7, you should remove the type declaration from the handler, while code that is being migrated to work on PHP 7 exclusively can simply replace the Exception type declaration with Throwable instead.

```php
<?php
// PHP 5 era code that will break.
function handler(Exception $e) { ... }
set_exception_handler('handler');

// PHP 5 and 7 compatible.
function handler($e) { ... }

// PHP 7 only.
function handler(Throwable $e) { ... }
```

[Throwable Exceptions and Errors in PHP 7](https://trowski.com/2015/06/24/throwable-exceptions-and-errors-in-php7/)

- Internal constructors always throw exceptions on failure
- Parse errors throw ParseError
- E_STRICT notices severity changes - All of the E_STRICT notices have been reclassified to other levels. E_STRICT constant is retained, so calls like error_reporting(E_ALL|E_STRICT) will not cause an error.

### Changes to variable handling

**PHP 7 now uses an abstract syntax tree when parsing source files.**

- Changes to the handling of indirect variables, properties, and methods

<table>
    <caption><strong>Old and new evaluation of indirect expressions</strong></caption>
    <thead>
      <tr>
       <th>Expression</th>
       <th>PHP 5 interpretation</th>
       <th>PHP 7 interpretation</th>
      </tr>
     </thead>
     <tbody class="tbody">
      <tr>
       <td>
        <code class="code">$$foo['bar']['baz']</code>
       </td>
       <td>
        <code class="code">${$foo['bar']['baz']}</code>
       </td>
       <td>
        <code class="code">($$foo)['bar']['baz']</code>
       </td>
      </tr>
      <tr>
       <td>
        <code class="code">$foo-&gt;$bar['baz']</code>
       </td>
       <td>
        <code class="code">$foo-&gt;{$bar['baz']}</code>
       </td>
       <td>
        <code class="code">($foo-&gt;$bar)['baz']</code>
       </td>
      </tr>
      <tr>
       <td>
        <code class="code">$foo-&gt;$bar['baz']()</code>
       </td>
       <td>
        <code class="code">$foo-&gt;{$bar['baz']}()</code>
       </td>
       <td>
        <code class="code">($foo-&gt;$bar)['baz']()</code>
       </td>
      </tr>
      <tr>
       <td>
        <code class="code">Foo::$bar['baz']()</code>
       </td>
       <td>
        <code class="code">Foo::{$bar['baz']}()</code>
       </td>
       <td>
        <code class="code">(Foo::$bar)['baz']()</code>
       </td>
      </tr>
     </tbody>
   </table>

Indirect access to variables, properties, and methods will now be evaluated strictly in left-to-right order.

- Changes to list() handling
  - list() no longer assigns variables in reverse order
  - Empty list() assignments have been removed
  - list() cannot unpack strings
- Array ordering when elements are automatically created during by reference assignments has changed
- Parentheses around function arguments no longer affect behaviour

### Changes to foreach

[Foreach](https://www.php.net/manual/en/control-structures.foreach.php)

- In PHP 7, foreach does not use the internal array pointer.  
  Reference of a \$value and the last array element remain even after the foreach loop. It is recommended to destroy it by unset().
- foreach does not support the ability to suppress error messages using '@'.
- Unpacking nested arrays with list()
- foreach by-value operates on a copy of the array
- foreach by-reference has improved iteration behaviour - appending to an array while iterating will now result in the appended values being iterated over
- Iteration of non-Traversable objects - Iterating over a non-Traversable object will now have the same behaviour as iterating over by-reference arrays. This results in the improved behaviour when modifying an array during iteration also being applied when properties are added to or removed from the object.

### Changes to integer handling

- Invalid octal literals - Previously, octal literals that contained invalid numbers were silently truncated (0128 was taken as 012). Now, an invalid octal literal will cause a parse error.
- Negative bitshifts - Bitwise shifts by negative numbers will now throw an ArithmeticError.
- Out of range bitshifts -
- Changes to Division By Zero - Previously, when 0 was used as the divisor for either the divide (/) or modulus (%) operators, an E_WARNING would be emitted and false would be returned. Now, the divide operator returns a float as either +INF, -INF, or NAN, as specified by IEEE 754. The modulus operator E_WARNING has been removed and will throw a DivisionByZeroError exception.

### Changes to string handling

- Hexadecimal strings are no longer considered numeric - filter_var() can be used to check if a string contains a hexadecimal number, and also to convert a string of that type to an integer.
- \u{ may cause errors - Due to the addition of the new Unicode codepoint escape syntax, strings containing a literal \u{ followed by an invalid sequence will cause a fatal error. To avoid this, the leading backslash should be escaped.

### Removed functions

- call_user_method() and call_user_method_array() - These functions were deprecated in PHP 4.1.0 in favour of call_user_func() and call_user_func_array(). You may also want to consider using variable functions and/or the ... operator.
- All ereg\* functions
- mcrypt aliases - The deprecated mcrypt*generic_end() function has been removed in favour of mcrypt_generic_deinit().  
  Additionally, the deprecated mcrypt_ecb(), mcrypt_cbc(), mcrypt_cfb() and mcrypt_ofb() functions have been removed in favour of using mcrypt_decrypt() with the appropriate MCRYPT_MODE*\* constant.
- All ext/mysql functions
- All ext/mssql functions
- intl aliases
- set_magic_quotes_runtime()
- set_socket_blocking() - The deprecated set_socket_blocking() alias has been removed in favour of stream_set_blocking().
- dl() in PHP-FPM
- GD Type1 functions

### Removed INI directives

- always_populate_raw_post_data
- asp_tags
- xsl.security_prefs

### Other backward incompatible changes

- New objects cannot be assigned by reference
- Invalid class, interface and trait names
- ASP and script PHP tags removed
- Calls from incompatible context removed - Non-static method should not be called statically
- yield is now a right associative operator
- Functions cannot have multiple parameters with the same name
- Functions inspecting arguments report the current parameter value - func_get_arg(), func_get_args(), debug_backtrace() and exception backtraces will no longer report the original value that was passed to a parameter, but will instead provide the current value (which might have been modified).
- Switch statements cannot have multiple default blocks
- $HTTP_RAW_POST_DATA removed - $HTTP_RAW_POST_DATA is no longer available. The php://input stream should be used instead.
- \# comments in INI files removed
- ; (semi-colon) should be used instead.
- JSON extension replaced with JSOND
- Internal function failure on overflow
- Fixes to custom session handler return values
- Sort order of equal elements
- Misplaced break and switch statements - break and continue statements outside of a loop or switch control structure are now detected at compile-time instead of run-time as before, and trigger an E_COMPILE_ERROR.
- Mhash is not an extension anymore - The Mhash extension has been fully integrated into the Hash extension.
- declare(ticks)

## New features

### Scalar type declarations

Scalar type declarations come in two flavours: coercive (default) and strict. The following types for parameters can now be enforced (either coercively or strictly):

- strings (string),
- integers (int),
- floating-point numbers (float), and
- booleans (bool).

They augment the other types introduced in PHP 5:

- class names,
- interfaces,
- array and
- callable

To enable strict mode, a single declare directive must be placed at the top of the file. This means that the strictness of typing for scalars is configured on a per-file basis. This directive not only affects the type declarations of parameters, but also a function's return type (see return type declarations, built-in PHP functions, and functions from loaded extensions.

[type declaration](https://www.php.net/manual/en/functions.arguments.php#functions.arguments.type-declaration)

### Return type declarations

[return type declarations](https://www.php.net/manual/en/functions.returning-values.php#functions.returning-values.type-declaration)

### Null coalescing operator

The null coalescing operator (??) has been added as syntactic sugar for the common case of needing to use a ternary in conjunction with isset(). It returns its first operand if it exists and is not NULL; otherwise it returns its second operand.

Coalescing can be chained: this will return the first defined value .

### Spaceship operator

The spaceship operator is used for comparing two expressions. It returns -1, 0 or 1 when $a is respectively less than, equal to, or greater than $b.

[PHP type comparison tables](https://www.php.net/manual/en/types.comparisons.php)

### Constant arrays using define()

Array constants can now be defined with define().

### Anonymous classes

[anonymous class reference](https://www.php.net/manual/en/language.oop5.anonymous.php)

### Unicode codepoint escape syntax

This takes a Unicode codepoint in hexadecimal form, and outputs that codepoint in UTF-8 to a double-quoted string or a heredoc. Any valid codepoint is accepted, with leading 0's being optional.

```
echo "\u{aa}";
echo "\u{0000aa}";
echo "\u{9999}";
```

### Closure::call()

### Filtered unserialize()

This feature seeks to provide better security when unserializing objects on untrusted data. It prevents possible code injections by enabling the developer to whitelist classes that can be unserialized.

### IntlChar

### Expectations

Expectations are a backwards compatible enhancement to the older assert() function. They allow for zero-cost assertions in production code, and provide the ability to throw custom exceptions when the assertion fails.

[Expectations (PHP 7 only)](https://www.php.net/manual/en/function.assert.php#function.assert.expectations)

### Group use declarations

Classes, functions and constants being imported from the same namespace can now be grouped together in a single use statement.

### Generator Return Expressions

This feature builds upon the generator functionality introduced into PHP 5.5. It enables for a return statement to be used within a generator to enable for a final expression to be returned (return by reference is not allowed). This value can be fetched using the new Generator::getReturn() method, which may only be used once the generator has finished yielding values.

### Generator delegation

Generators can now delegate to another generator, Traversable object or array automatically, without needing to write boilerplate in the outermost generator by using the yield from construct.

### Integer division with intdiv()

The new intdiv() function performs an integer division of its operands and returns it.

### Session options

session_start() now accepts an array of options that override the [session configuration directives](https://www.php.net/manual/en/session.configuration.php) normally set in php.ini.  
These options have also been expanded to support session.lazy_write, which is on by default and causes PHP to only overwrite any session file if the session data has changed, and read_and_close, which is an option that can only be passed to session_start() to indicate that the session data should be read and then the session should immediately be closed unchanged.

### preg_replace_callback_array()

### CSPRNG Functions

cryptographically secure pseudo-random number generator (CSPRNG)

Two new functions have been added to generate cryptographically secure integers and strings in a cross platform way: random_bytes() and random_int().

### list() can always unpack objects implementing ArrayAccess

### Class member access on cloning has been added, e.g. (clone \$foo)->bar().

## Deprecated features in PHP 7.0.x

### PHP 4 style constructors

### Static calls to non-static methods

### password_hash() salt option

### capture_session_meta SSL context option

The capture_session_meta SSL context option has been deprecated. SSL metadata is now available through the stream_get_meta_data() function.

### LDAP deprecations

The following function has been deprecated:

- ldap_sort()

## Changed functions

### PHP Core

- debug_zval_dump() now prints "int" instead of "long", and "float" instead of "double"
- dirname() now optionally takes a second parameter, depth, to get the name of the directory depth levels up from the current directory.
- getrusage() is now supported on Windows.
- mktime() and gmmktime() functions no longer accept is_dst parameter.
- preg_replace() function no longer supports "\e" (PREG_REPLACE_EVAL). preg_replace_callback() should be used instead.
- setlocale() function no longer accepts category passed as string. LC\_\* constants must be used instead.
- exec(), system() and passthru() functions have NULL byte protection now.
- shmop_open() now returns a resource instead of an int, which has to be passed to shmop_size(), shmop_write(), shmop_read(), shmop_close() and shmop_delete().
- substr() and iconv_substr() now return an empty string, if string is equal to start characters long.
- xml_parser_free() is no longer sufficient to free the parser resource, if it references an object and this object references that parser resource. In this case it is necessary to additionally unset the \$parser.

## New functions

### Closure

- Closure::call()

### CSPRNG

- random_bytes()
- random_int()

### Error Handling and Logging

- error_clear_last()

### Generator

- Generator::getReturn()

### GNU Multiple Precision

- gmp_random_seed()

### Math

- intdiv()

### PCRE

- preg_replace_callback_array()

### PHP Options/Info

- gc_mem_caches()
- get_resources()

### POSIX

- posix_setrlimit()

### Reflection

- ReflectionParameter::getType()
- ReflectionParameter::hasType()
- ReflectionFunctionAbstract::getReturnType()
- ReflectionFunctionAbstract::hasReturnType()

### Zip

- ZipArchive::setCompressionIndex()
- ZipArchive::setCompressionName()

### Zlib Compression

- inflate_add()
- deflate_add()
- inflate_init()
- deflate_init()

## New Classes and Interfaces

### Intl

- IntlChar

### Reflection

- ReflectionGenerator
- ReflectionType

### Session Handling

- SessionUpdateTimestampHandlerInterface

### Exception Hierarchy

- Throwable
- Error
- TypeError
- ParseError
- AssertionError
- ArithmeticError
- DivisionByZeroError

## New Global Constants

### Core Predefined Constants

- PHP_INT_MIN

### GD

- IMG_WEBP (as of PHP 7.0.10)

### JSON

- JSON_ERROR_INVALID_PROPERTY_NAME
- JSON_ERROR_UTF16

### LibXML

- LIBXML_BIGLINES

### PCRE

- PREG_JIT_STACKLIMIT_ERROR

### POSIX

- POSIX_RLIMIT_AS
- POSIX_RLIMIT_CORE
- POSIX_RLIMIT_CPU
- POSIX_RLIMIT_DATA
- POSIX_RLIMIT_FSIZE
- POSIX_RLIMIT_LOCKS
- POSIX_RLIMIT_MEMLOCK
- POSIX_RLIMIT_MSGQUEUE
- POSIX_RLIMIT_NICE
- POSIX_RLIMIT_NOFILE
- POSIX_RLIMIT_NPROC
- POSIX_RLIMIT_RSS
- POSIX_RLIMIT_RTPRIO
- POSIX_RLIMIT_RTTIME
- POSIX_RLIMIT_SIGPENDING
- POSIX_RLIMIT_STACK
- POSIX_RLIMIT_INFINITY

### Zlib

- ZLIB_ENCODING_RAW
- ZLIB_ENCODING_DEFLATE
- ZLIB_ENCODING_GZIP
- ZLIB_FILTERED
- ZLIB_HUFFMAN_ONLY
- ZLIB_FIXED
- ZLIB_RLE
- ZLIB_DEFAULT_STRATEGY
- ZLIB_BLOCK
- ZLIB_FINISH
- ZLIB_FULL_FLUSH
- ZLIB_NO_FLUSH
- ZLIB_PARTIAL_FLUSH
- ZLIB_SYNC_FLUSH

## Changes in SAPI Modules

### FPM

- Unqualified listen ports now listen on both IPv4 and IPv6

In PHP 5, a listen directive with only a port number would listen on all interfaces, but only on IPv4. PHP 7 will now accept requests made via both IPv4 and IPv6.

This does not affect directives which include specific IP addresses; these will continue to only listen on that address and protocol.

## Removed Extensions and SAPIs

### Removed Extensions

- ereg
- mssql
- mysql
- sybase_ct

### Removed SAPIs

- aolserver
- apache
- apache_hooks
- apache2filter
- caudium
- continuity
- isapi
- milter
- nsapi
- phttpd
- pi3web
- roxen
- thttpd
- tux
- webjames

## Other Changes

### Loosening Reserved Word Restrictions

Globally reserved words as property, constant, and method names within classes, interfaces, and traits are now allowed. This reduces the surface of BC breaks when new keywords are introduced and avoids naming restrictions on APIs.

This is particularly useful when creating internal DSLs with fluent interfaces.

The only limitation is that the class keyword still cannot be used as a constant name, otherwise it would conflict with the class name resolution syntax (ClassName::class).

### Removal of date.timezone Warning

reviously, a warning was emitted if the date.timezone INI setting had not been set prior to using any date- or time-based functions. Now, this warning has been removed (with date.timezone still defaulting to UTC).

# Sources

[Migrating from PHP 5.6.x to PHP 7.0.x](https://www.php.net/manual/en/migration70.php)

# Migrating from PHP 7.2.x to PHP 7.3.x

## New features

### PHP Core

- More Flexible Heredoc and Nowdoc Syntax

The closing marker for doc strings is no longer required to be followed by a semicolon or newline. Additionally the closing marker may be indented, in which case the indentation will be stripped from all lines in the doc string.

- Array Destructuring supports Reference Assignments

Array destructuring now supports reference assignments using the syntax [&$a, [$b, &$c]] = $d. The same is also supported for list().

- Instanceof Operator accepts Literals

instanceof now allows literals as the first operand, in which case the result is always FALSE.

- CompileError Exception instead of some Compilation Errors

A new CompileError exception has been added, from which ParseError inherits. A small number of compilation errors will now throw a CompileError instead of generating a fatal error. Currently this only affects compilation errors that may be thrown by token_get_all() in TOKEN_PARSE mode, but more errors may be converted in the future.

- Trailing Commas are allowed in Calls

Trailing commas in function and method calls are now allowed.

- Argon2id Support

The --with-password-argon2[=dir] configure argument now provides support for both Argon2i and Argon2id hashes in the password*hash(), password_verify(), password_get_info(), and password_needs_rehash() functions. Passwords may be hashed and verified using the PASSWORD_ARGON2ID constant. Support for both Argon2i and Argon2id in the password*\*() functions now requires PHP be linked against libargon2 reference library ≥ 20161029.

### FastCGI Process Manager

New options have been added to customize the FPM logging:

- log_limit
  This global option can be used for setting the log limit for the logged line which allows to log messages longer than 1024 characters without wrapping. It also fixes various wrapping issues.
- log_buffering
  This global option allows an experimental logging without extra buffering.
- decorate_workers_output
  This pool option allows to disable the output decoration for workers output when catch_workers_output is enabled.

### BC Math Functions

bcscale() can now also be used as getter to retrieve the current scale in use.

### Lightweight Directory Access Protocol

Full support for LDAP Controls has been added to the LDAP querying functions and ldap_parse_result():

- A \$serverctrls parameter to send controls to the server in ldap_add(), ldap_mod_replace(), ldap_mod_add(), ldap_mod_del(), ldap_rename(), ldap_compare(), ldap_delete(), ldap_modify_batch(), ldap_search(), ldap_list() and ldap_read() has been added.
- The out parameter \$serverctrls to get controls from the server in ldap_parse_result() has been added.
- Support for LDAP_OPT_SERVER_CONTROLS and LDAP_OPT_CLIENT_CONTROLS in ldap_get_option() and ldap_set_option() has been fixed.

### Multibyte String Functions

- Full Case-Mapping and Case-Folding Support

The different casing mapping and folding modes are available through mb_convert_case():
MB_CASE_LOWER (used by mb_strtolower())
MB_CASE_UPPER (used by mb_strtoupper())
MB_CASE_TITLE
MB_CASE_FOLD
MB_CASE_LOWER_SIMPLE
MB_CASE_UPPER_SIMPLE
MB_CASE_TITLE_SIMPLE
MB_CASE_FOLD_SIMPLE (used by case-insensitive operations)
Only unconditional, language agnostic full case-mapping is performed.

### Case-Insensitive String Operations use Case-Folding

Case-insensitive string operations now use case-folding instead of case- mapping during comparisons. This means that more characters will be considered (case insensitively) equal now.

### MB_CASE_TITLE performs Title-Case Conversion

mb_convert_case() with MB_CASE_TITLE now performs title-case conversion based on the Cased and CaseIgnorable derived Unicode properties. In particular this also improves handling of quotes and apostrophes

### Unicode 11 Support

The Multibyte String data tables have been updated for Unicode 11.

### Long String Support

The Multibyte String Functions now correctly support strings larger than 2GB.

### Performance Improvements

Performance of the Multibyte String extension has been significantly improved across the board. The largest improvements are in case conversion functions.

### Named Captures Support

The mb*ereg*\* functions now support named captures. Matching functions like mb_ereg() will now return named captures both using their group number and their name, similar to PCRE.

Additionally, mb_ereg_replace() now supports the \k<> and \k'' notations to reference named captures in the replacement string:

\k<> and \k'' can also be used for numbered references, which also works with group numbers greater than 9.

### Readline

Support for the completion_append_character and completion_suppress_append options has been added to readline_info(). These options are only available if PHP is linked against libreadline (rather than libedit).

## New Functions

### PHP Core

- array_key_first()
- array_key_last()
- gc_status()
- hrtime()
- is_countable()
- net_get_interfaces()

### FastCGI Process Manager

- fpm_get_status()

### Date and Time

- DateTime::createFromImmutable()

### GNU Multiple Precision

- gmp_binomial()
- gmp_kronecker()
- gmp_lcm()
- gmp_perfect_power()

### Internationalization Functions

- Normalizer::getRawDecomposition()
- normalizer_get_raw_decomposition()
- Spoofchecker::setRestrictionLevel()

### Lightweight Directory Access Protocol

- ldap_add_ext()
- ldap_bind_ext()
- ldap_delete_ext()
- ldap_exop_refresh()
- ldap_mod_add_ext()
- ldap_mod_replace_ext()
- ldap_mod_del_ext()
- ldap_rename_ext()

### OpenSSL

- openssl_pkey_derive()

### Sockets

- socket_wsaprotocol_info_export()
- socket_wsaprotocol_info_import()
- socket_wsaprotocol_info_release()

## New Global Constants

### PHP Core

- PASSWORD_ARGON2ID

### Client URL Library

- CURLAUTH_BEARER
- CURLAUTH_GSSAPI
- CURLE_WEIRD_SERVER_REPLY
- CURLINFO_APPCONNECT_TIME_T
- CURLINFO_CONNECT_TIME_T
- CURLINFO_CONTENT_LENGTH_DOWNLOAD_T
- CURLINFO_CONTENT_LENGTH_UPLOAD_T
- CURLINFO_FILETIME_T
- CURLINFO_HTTP_VERSION
- CURLINFO_NAMELOOKUP_TIME_T
- CURLINFO_PRETRANSFER_TIME_T
- CURLINFO_PROTOCOL
- CURLINFO_PROXY_SSL_VERIFYRESULT
- CURLINFO_REDIRECT_TIME_T
- CURLINFO_SCHEME
- CURLINFO_SIZE_DOWNLOAD_T
- CURLINFO_SIZE_UPLOAD_T
- CURLINFO_SPEED_DOWNLOAD_T
- CURLINFO_SPEED_UPLOAD_T
- CURLINFO_STARTTRANSFER_TIME_T
- CURLINFO_TOTAL_TIME_T
- CURL_LOCK_DATA_CONNECT
- CURL_LOCK_DATA_PSL
- CURL_MAX_READ_SIZE
- CURLOPT_ABSTRACT_UNIX_SOCKET
- CURLOPT_DISALLOW_USERNAME_IN_URL
- CURLOPT_DNS_SHUFFLE_ADDRESSES
- CURLOPT_HAPPY_EYEBALLS_TIMEOUT_MS
- CURLOPT_HAPROXYPROTOCOL
- CURLOPT_KEEP_SENDING_ON_ERROR
- CURLOPT_PRE_PROXY
- CURLOPT_PROXY_CAINFO
- CURLOPT_PROXY_CAPATH
- CURLOPT_PROXY_CRLFILE
- CURLOPT_PROXY_KEYPASSWD
- CURLOPT_PROXY_PINNEDPUBLICKEY
- CURLOPT_PROXY_SSLCERT
- CURLOPT_PROXY_SSLCERTTYPE
- CURLOPT_PROXY_SSL_CIPHER_LIST
- CURLOPT_PROXY_SSLKEY
- CURLOPT_PROXY_SSLKEYTYPE
- CURLOPT_PROXY_SSL_OPTIONS
- CURLOPT_PROXY_SSL_VERIFYHOST
- CURLOPT_PROXY_SSL_VERIFYPEER
- CURLOPT_PROXY_SSLVERSION
- CURLOPT_PROXY_TLS13_CIPHERS
- CURLOPT_PROXY_TLSAUTH_PASSWORD
- CURLOPT_PROXY_TLSAUTH_TYPE
- CURLOPT_PROXY_TLSAUTH_USERNAME
- CURLOPT_REQUEST_TARGET
- CURLOPT_SOCKS5_AUTH
- CURLOPT_SSH_COMPRESSION
- CURLOPT_SUPPRESS_CONNECT_HEADERS
- CURLOPT_TIMEVALUE_LARGE
- CURLOPT_TLS13_CIPHERS
- CURLPROXY_HTTPS
- CURLSSH_AUTH_GSSAPI
- CURL_SSLVERSION_MAX_DEFAULT
- CURL_SSLVERSION_MAX_NONE
- CURL_SSLVERSION_MAX_TLSv1_0
- CURL_SSLVERSION_MAX_TLSv1_1
- CURL_SSLVERSION_MAX_TLSv1_2
- CURL_SSLVERSION_MAX_TLSv1_3
- CURL_SSLVERSION_TLSv1_3
- CURL_VERSION_ALTSVC (as of PHP 7.3.6)
- CURL_VERSION_ASYNCHDNS
- CURL_VERSION_BROTLI
- CURL_VERSION_CONV
- CURL_VERSION_CURLDEBUG (as of PHP 7.3.6)
- CURL_VERSION_DEBUG
- CURL_VERSION_GSSAPI
- CURL_VERSION_GSSNEGOTIATE
- CURL_VERSION_HTTPS_PROXY
- CURL_VERSION_IDN
- CURL_VERSION_LARGEFILE
- CURL_VERSION_MULTI_SSL
- CURL_VERSION_NTLM
- CURL_VERSION_NTLM_WB
- CURL_VERSION_PSL (as of PHP 7.3.6)
- CURL_VERSION_SPNEGO
- CURL_VERSION_SSPI
- CURL_VERSION_TLSAUTH_SRP

### Data Filtering

- FILTER_SANITIZE_ADD_SLASHES

### JavaScript Object Notation

- JSON_THROW_ON_ERROR

### Lightweight Directory Access Protocol

- LDAP_CONTROL_ASSERT
- LDAP_CONTROL_MANAGEDSAIT
- LDAP_CONTROL_PROXY_AUTHZ
- LDAP_CONTROL_SUBENTRIES
- LDAP_CONTROL_VALUESRETURNFILTER
- LDAP_CONTROL_PRE_READ
- LDAP_CONTROL_POST_READ
- LDAP_CONTROL_SORTREQUEST
- LDAP_CONTROL_SORTRESPONSE
- LDAP_CONTROL_PAGEDRESULTS
- LDAP_CONTROL_AUTHZID_REQUEST
- LDAP_CONTROL_AUTHZID_RESPONSE
- LDAP_CONTROL_SYNC
- LDAP_CONTROL_SYNC_STATE
- LDAP_CONTROL_SYNC_DONE
- LDAP_CONTROL_DONTUSECOPY
- LDAP_CONTROL_PASSWORDPOLICYREQUEST
- LDAP_CONTROL_PASSWORDPOLICYRESPONSE
- LDAP_CONTROL_X_INCREMENTAL_VALUES
- LDAP_CONTROL_X_DOMAIN_SCOPE
- LDAP_CONTROL_X_PERMISSIVE_MODIFY
- LDAP_CONTROL_X_SEARCH_OPTIONS
- LDAP_CONTROL_X_TREE_DELETE
- LDAP_CONTROL_X_EXTENDED_DN
- LDAP_CONTROL_VLVREQUEST
- LDAP_CONTROL_VLVRESPONSE

### Multibyte String

- MB_CASE_FOLD
- MB_CASE_LOWER_SIMPLE
- MB_CASE_UPPER_SIMPLE
- MB_CASE_TITLE_SIMPLE
- MB_CASE_FOLD_SIMPLE

### OpenSSL

- STREAM_CRYPTO_PROTO_SSLv3
- STREAM_CRYPTO_PROTO_TLSv1_0
- STREAM_CRYPTO_PROTO_TLSv1_1
- STREAM_CRYPTO_PROTO_TLSv1_2

### PostgreSQL

- PGSQL_DIAG_SCHEMA_NAME
- PGSQL_DIAG_TABLE_NAME
- PGSQL_DIAG_COLUMN_NAME
- PGSQL_DIAG_DATATYPE_NAME
- PGSQL_DIAG_CONSTRAINT_NAME
- PGSQL_DIAG_SEVERITY_NONLOCALIZED

## Backward Incompatible Changes

### PHP Core

- Heredoc/Nowdoc Ending Label Interpretation

Due to the introduction of flexible heredoc/nowdoc syntax, doc strings that contain the ending label inside their body may cause syntax errors or change in interpretation. The indented occurrence of FOO did not previously have any special meaning. Now it will be interpreted as the end of the heredoc string and the following FOO; will cause a syntax error. This issue can always be resolved by choosing an ending label that does not occur within the contents of the string.

- Continue Targeting Switch issues Warning

`continue` statements targeting `switch` control flow structures will now generate a warning. In PHP such `continue` statements are equivalent to `break`, while they behave as `continue` 2 in other languages.

### Strict Interpretation of Integer String Keys on ArrayAccess

Array accesses of type $obj["123"], where $obj implements ArrayAccess and "123" is an integer string literal will no longer result in an implicit conversion to integer, i.e., $obj->offsetGet("123") will be called instead of $obj->offsetGet(123). This matches existing behavior for non-literals. The behavior of arrays is not affected in any way, they continue to implicitly convert integral string keys to integers.

### Static Properties no longer separated by Reference Assignment

In PHP, static properties are shared between inheriting classes, unless the static property is explicitly overridden in a child class. However, due to an implementation artifact it was possible to separate the static properties by assigning a reference. This loophole has been fixed.

### References returned by Array and Property Accesses are immediately unwrapped

References returned by array and property accesses are now unwrapped as part of the access. This means that it is no longer possible to modify the reference between the access and the use of the accessed value.

This makes the behavior of references and non-references consistent. Please note that reading and writing a value inside a single expression remains undefined behavior and may change again in the future.

### Argument Unpacking of Traversables with non-Integer Keys no longer supported

Argument unpacking stopped working with Traversables with non-integer keys. The following code worked in PHP 5.6-7.2 by accident.

### Miscellaneous

The ext_skel utility has been completely redesigned with new options and some old options removed. This is now written in PHP and has no external dependencies.

Support for BeOS has been dropped.

Exceptions thrown due to automatic conversion of warnings into exceptions in EH_THROW mode (e.g. some DateTime exceptions) no longer populate error_get_last() state. As such, they now work the same way as manually thrown exceptions.

TypeError now reports wrong types as int and bool instead of integer and boolean, respectively.

Undefined variables passed to compact() will now be reported as a notice.

getimagesize() and related functions now report the mime type of BMP images as image/bmp instead of image/x-ms-bmp, since the former has been registered with the IANA (see » RFC 7903).

stream_socket_get_name() will now return IPv6 addresses wrapped in brackets. For example "[::1]:1337" will be returned instead of "::1:1337".

### BCMath Arbitrary Precision Mathematics

All warnings thrown by BCMath functions are now using PHP's error handling. Formerly some warnings have directly been written to stderr.

bcmul() and bcpow() now return numbers with the requested scale. Formerly, the returned numbers may have omitted trailing decimal zeroes.

### IMAP, POP3 and NNTP

rsh/ssh logins are disabled by default. Use imap.enable_insecure_rsh if you want to enable them. Note that the IMAP library does not filter mailbox names before passing them to the rsh/ssh command, thus passing untrusted data to this function with rsh/ssh enabled is insecure.

### Multibyte String

Due to added support for named captures, mb*ereg*\*() patterns using named captures will behave differently. In particular named captures will be part of matches and mb_ereg_replace() will interpret additional syntax. See Named Captures for more information.

### MySQL Improved Extension

Prepared statements now properly report the fractional seconds for DATETIME, TIME and TIMESTAMP columns with decimals specifier (e.g. TIMESTAMP(6) when using microseconds). Formerly, the fractional seconds part was simply omitted from the returned values.

### MySQL Functions (PDO_MYSQL)

Prepared statements now properly report the fractional seconds for DATETIME, TIME and TIMESTAMP columns with decimals specifier (e.g. TIMESTAMP(6) when using microseconds). Formerly, the fractional seconds part was simply omitted from the returned values. Please note that this only affects the usage of PDO_MYSQL with emulated prepares turned off (e.g. using the native preparation functionality). Statements using connections having PDO::ATTR_EMULATE_PREPARES=TRUE (which is the default) were not affected by the bug fixed and have already been getting the proper fractional seconds values from the engine.

### Reflection

Reflection export to string now uses int and bool instead of integer and boolean, respectively.

### Standard PHP Library (SPL)

If an SPL autoloader throws an exception, following autoloaders will not be executed. Previously all autoloaders were executed and exceptions were chained.

### SimpleXML

Mathematic operations involving SimpleXML objects will now treat the text as an integer or float, whichever is more appropriate. Previously values were treated as integers unconditionally.

## Deprecated Features

### PHP Core

- Case-Insensitive Constants

The declaration of case-insensitive constants has been deprecated. Passing TRUE as the third argument to define() will now generate a deprecation warning. The use of case-insensitive constants with a case that differs from the declaration is also deprecated.

- Namespaced assert()

Declaring a function called assert() inside a namespace is deprecated. The assert() function is subject to special handling by the engine, which may lead to inconsistent behavior when defining a namespaced function with the same name.

- Searching Strings for non-string Needle

Passing a non-string needle to string search functions is deprecated. In the future the needle will be interpreted as a string instead of an ASCII codepoint. Depending on the intended behavior, you should either explicitly cast the needle to string or perform an explicit call to chr(). The following functions are affected:

- strpos()
- strrpos()
- stripos()
- strripos()
- strstr()
- strchr()
- strrchr()
- stristr()

### Strip-Tags Streaming

The fgetss() function and the string.strip_tags stream filter have been deprecated. This also affects the SplFileObject::fgetss() method and gzgetss() function.

### Data Filtering

The explicit usage of the constants FILTER_FLAG_SCHEME_REQUIRED and FILTER_FLAG_HOST_REQUIRED is now deprecated; both are implied for FILTER_VALIDATE_URL anyway.

### Image Processing and GD

image2wbmp() has been deprecated.

### Internationalization Functions

Usage of the Normalizer::NONE form throws a deprecation warning, if PHP is linked with ICU ≥ 56.

### Multibyte String

The following undocumented mbereg*\*() aliases have been deprecated. Use the corresponding mb_ereg*\*() variants instead.

- mbregex_encoding()
- mbereg()
- mberegi()
- mbereg_replace()
- mberegi_replace()
- mbsplit()
- mbereg_match()
- mbereg_search()
- mbereg_search_pos()
- mbereg_search_regs()
- mbereg_search_init()
- mbereg_search_getregs()
- mbereg_search_getpos()
- mbereg_search_setpos()

### ODBC and DB2 Functions (PDO_ODBC)

The pdo_odbc.db2_instance_name ini setting has been formally deprecated. It is deprecated in the documentation as of PHP 5.1.1.

## Other Changes

### PHP Core

- Set(raw)cookie accepts \$option Argument

setcookie() and setrawcookie() now also support the following signature, where \$options is an associative array which may have any of the keys "expires", "path", "domain", "secure", "httponly" and "samesite".

- New Syslog ini Directives

The following ini Directives have been added to customize logging, if error_log is set to syslog:

syslog.facility
Specifies what type of program is logging the message.
syslog.filter
Specifies the filter type to filter the logged messages. There are three supported filter types - all, no-ctrl and ascii.
syslog.ident
Specifies the ident string which is prepended to every message.

### Garbage Collection

The cyclic GC has been enhanced, which may result in considerable performance improvements.

### Miscellaneous

var_export() now exports stdClass objects as an array cast to an object ((object) array( ... )), rather than using the nonexistent method stdClass::\_\_setState().

debug_zval_dump() was changed to display recursive arrays and objects in the same way as var_dump(). Now, it doesn't display them twice.

array_push() and array_unshift() can now also be called with a single argument, which is particularly convenient wrt. the spread operator.

### Interactive PHP Debugger

The unused constants PHPDBG_FILE, PHPDBG_METHOD, PHPDBG_LINENO and PHPDBG_FUNC have been removed.

### FastCGI Process Manager

The getallheaders() function is now also available.

### Client URL Library

libcurl ≥ 7.15.5 is now required.

### Data Filtering

FILTER_VALIDATE_FLOAT now also supports a thousand option, which defines the set of allowed thousand separator chars. The default ("',.") is fully backward compatible with former PHP versions.

FILTER_SANITIZE_ADD_SLASHES has been added as an alias of the magic_quotes filter (FILTER_SANITIZE_MAGIC_QUOTES). The magic_quotes filter is subject to removal in future versions of PHP.

### FTP

The default transfer mode has been changed to binary.

### Internationalization Functions

Normalizer::NONE is deprecated, when PHP is linked with ICU ≥ 56.

Introduced Normalizer::FORM_KC_CF as Normalizer::normalize() argument for NFKC_Casefold normalization; available when linked with ICU ≥ 56.

### JavaScript Object Notation

A new flag has been added, JSON_THROW_ON_ERROR, which can be used with json_decode() or json_encode() and causes these functions to throw the new JsonException upon an error, instead of setting the global error state that is retrieved with json_last_error() and json_last_error_msg(). JSON_PARTIAL_OUTPUT_ON_ERROR takes precedence over JSON_THROW_ON_ERROR.

### Multibyte String

The configuration option --with-libmbfl is no longer available.

### ODBC (Unified)

Support for ODBCRouter and Birdstep including the birdstep.max_links ini directive has been removed.

### OPcache

The opcache.inherited_hack ini directive has been removed. The value has already been ignored since PHP 5.3.0.

### OpenSSL

The min_proto_version and max_proto_version ssl stream options as well as related constants for possible TLS protocol values have been added.

### Regular Expressions (Perl-Compatible)

The PCRE extension has been upgraded to PCRE2, which may cause minor behavioral changes (for instance, character ranges in classes are now more strictly interpreted), and augments the existing regular expression syntax.

preg_quote() now also escapes the '#' character.

### Microsoft SQL Server and Sybase Functions (PDO_DBLIB)

The attribute PDO::DBLIB_ATTR_SKIP_EMPTY_ROWSETS to enable automatic skipping of empty rowsets has been added.

The PDO::DBLIB_ATTR_TDS_VERSION attribute which exposes the TDS version has been added.

DATETIME2 columns are now treated like DATETIME columns.

### SQLite Functions (PDO_SQLITE)

SQLite3 databases can now be opened in read-only mode by setting the new PDO::SQLITE_ATTR_OPEN_FLAGS attribute to PDO::SQLITE_OPEN_READONLY.

### Session Handling

session_set_cookie_params() now also supports the following signature:

session_set_cookie_params ( array $options ) : bool
where $options is an associative array which may have any of the keys "lifetime", "path", "domain", "secure", "httponly" and "samesite". Accordingly, the return value of session_get_cookie_params() now also has an element with the key "samesite". Furthermore, the new session.cookie_samesite ini option to set the default of the SameSite directive for cookies has been added. It defaults to "" (empty string), so no SameSite directive is set. Can be set to "Lax" or "Strict", which sets the respective SameSite directive.

### Tidy

Building against » tidyp is now also supported transparently. Since tidyp offers no API to get the release date, tidy_get_release() and tidy::getRelease() return 'unknown' in this case.

### XML Parser

The return value of the xml_set_external_entity_ref_handler() callback is no longer ignored if the extension has been built against libxml. Formerly, the return value has been ignored, and parsing did never stop.

### Zip

Building against the bundled libzip is discouraged, but still possible by adding --without-libzip to the configuration.

### Zlib Compression

The zlib/level context option for the compress.zlib wrapper to facilitate setting the desired compression level has been added.

## Windows Support

### PHP Core

- More POSIX Conforming File Deletion

File descriptors are opened in shared read/write/delete mode by default. This effectively maps the POSIX semantics and allows to delete files with handles in use. It is not 100% same, some platform differences still persist. After the deletion, the filename entry is blocked, until all the opened handles to it are closed.

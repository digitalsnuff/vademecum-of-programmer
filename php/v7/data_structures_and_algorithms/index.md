# PHP 7 Data Structures and Algorithms

## PHP Data Types

PHP has eight primitive data types:

- booleans
- integer
- float
- string
- array
- object
- resource
- null

## Abstract Data Type (ADT)

ADT defines a set of possible operations for the data.
Data structures are concrete representation.
Most common example of ADT is `stack` and `queue`

`stack`
It is not only collection of data but also two important operations called `push` and `pop`.
Usually, we put a new entry at the top of the stack which is known as push and when we want to take an item, we take from the top which is also known as pop.
If we consider PHP array as a stack, we will require additional functionality to achieve these push and pop operations to consider it as stack ADT.
Stack works as a `Last-In, First-Out (LIFO)`.

`queue`
is also an ADT with two required operations: to add an item at the end of the queue also known as `enqueue` and remove an item from the beginning of the queue, also known as `dequeue`.
Queue works as a `First-In, First-Out (FIFO)`.

Here are some common ADTs

- List
- Map
- Set
- Stack
- Queue
- Priority queue
- Graph
- Tree

## Data structures

We can categorize `data structures` in to two different groups:

- Linear data structures
- Nonlinear data structures

In linear data structures, items are structured in a linear or sequential manner. Array, list, stack, and queue are examples of linear structures.

In nonlinear structures, data are not structured in a sequential way. Graph and tree are the most common examples of nonlinear data structures.

There are many different types of data structures that exist in the programming world. Out of them, following are the most used ones:

- Struct
- Array
- Linked list
- Doubly linked list
- Stack
- Queue
- Priority queue
- Set
- Map
- Tree
- Graph
- Heap

### Struct

# Source

Mizanur Rahman
**PHP 7 Data Structures and Algorithms**

Leverage the power of PHP data structures and crucial \
algorithms to build high-performance applications

First published: May 2017
Production reference: 1240517
Published by Packt Publishing Ltd.
Livery Place
35 Livery Street
Birmingham
B3 2PB, UK.
ISBN 978-1-78646-389-0

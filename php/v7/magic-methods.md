# Magic methods

> They usually are triggered by the interaction of the class or object, and not by invocations.

- `__construct` - it is invoked directly when new instance of class is creating.
- `__toString` - it is invoked when we try to cast an object to a string.
- `__call` - it is invoked when try to invoke a method on a class that does not exists.
- `__get` - it gets the name of the property that the user was trying to access through parameters, and it can return anything.

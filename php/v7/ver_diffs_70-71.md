# Migrating from PHP 7.0.x to PHP 7.1.x

## New features

### Nullable types

Type declarations for parameters and return values can now be marked as nullable by prefixing the type name with a question mark. This signifies that as well as the specified type, NULL can be passed as an argument, or returned as a value, respectively.

### Void functions

A void return type has been introduced. Functions declared with void as their return type must either omit their return statement altogether, or use an empty return statement. NULL is not a valid return value for a void function.

Attempting to use a void function's return value simply evaluates to NULL, with no warnings emitted. The reason for this is because warnings would implicate the use of generic higher order functions.

### Symmetric array destructuring

The shorthand array syntax ([]) may now be used to destructure arrays for assignments (including within foreach), as an alternative to the existing list() syntax, which is still supported.

### Class constant visibility

Support for specifying the visibility of class constants has been added.

### iterable pseudo-type

A new pseudo-type (similar to callable) called iterable has been introduced. It may be used in parameter and return types, where it accepts either arrays or objects that implement the Traversable interface. With respect to subtyping, parameter types of child classes may broaden a parent's declaration of array or Traversable to iterable. With return types, child classes may narrow a parent's return type of iterable to array or an object that implements Traversable.

### Multi catch exception handling

Multiple exceptions per catch block may now be specified using the pipe character (|). This is useful for when different exceptions from different class hierarchies are handled the same.

### Support for keys in list()

You can now specify keys in list(), or its new shorthand [] syntax. This enables destructuring of arrays with non-integer or non-sequential keys.

### Support for negative string offsets

Support for negative string offsets has been added to the string manipulation functions accepting offsets, as well as to string indexing with [] or {}. In such cases, a negative offset is interpreted as being an offset from the end of the string.

Negative string and array offsets are now also supported in the simple variable parsing syntax inside of strings.

### Support for AEAD in ext/openssl

Support for AEAD (modes GCM and CCM) have been added by extending the openssl_encrypt() and openssl_decrypt() functions with additional parameters.

### Convert callables to Closures with Closure::fromCallable()

A new static method has been introduced to the Closure class to allow for callables to be easily converted into Closure objects.

### Asynchronous signal handling

A new function called pcntl_async_signals() has been introduced to enable asynchronous signal handling without using ticks (which introduce a lot of overhead).

### HTTP/2 server push support in ext/curl

Support for server push has been added to the CURL extension (requires version 7.46 and above). This can be leveraged through the curl_multi_setopt() function with the new CURLMOPT_PUSHFUNCTION constant. The constants CURL_PUSH_OK and CURL_PUSH_DENY have also been added so that the execution of the server push callback can either be approved or denied.

### Stream Context Options

The tcp_nodelay stream context option has been added.

## New functions

### PHP Core

- sapi_windows_cp_get()
- sapi_windows_cp_set()
- sapi_windows_cp_conv()
- sapi_windows_cp_is_utf8()

### Closure

- Closure::fromCallable()

### CURL

- curl_multi_errno()
- curl_share_errno()
- curl_share_strerror()

### OpenSSL

- openssl_get_curve_names()

### Session

- session_create_id()
- session_gc()

### SPL

- is_iterable()

### PCNTL

- pcntl_async_signals()
- pcntl_signal_get_handler()

## New global constants

### Core Predefined Constants

- PHP_FD_SETSIZE

### CURL

- CURLMOPT_PUSHFUNCTION
- CURL_PUSH_OK
- CURL_PUSH_DENY

### Data Filtering

- FILTER_FLAG_EMAIL_UNICODE

### Image Processing and GD

- IMAGETYPE_WEBP

### JSON

- JSON_UNESCAPED_LINE_TERMINATORS

### LDAP

- LDAP_OPT_X_SASL_NOCANON
- LDAP_OPT_X_SASL_USERNAME
- LDAP_OPT_X_TLS_CACERTDIR
- LDAP_OPT_X_TLS_CACERTFILE
- LDAP_OPT_X_TLS_CERTFILE
- LDAP_OPT_X_TLS_CIPHER_SUITE
- LDAP_OPT_X_TLS_KEYFILE
- LDAP_OPT_X_TLS_RANDOM_FILE
- LDAP_OPT_X_TLS_CRLCHECK
- LDAP_OPT_X_TLS_CRL_NONE
- LDAP_OPT_X_TLS_CRL_PEER
- LDAP_OPT_X_TLS_CRL_ALL
- LDAP_OPT_X_TLS_DHFILE
- LDAP_OPT_X_TLS_CRLFILE
- LDAP_OPT_X_TLS_PROTOCOL_MIN
- LDAP_OPT_X_TLS_PROTOCOL_SSL2
- LDAP_OPT_X_TLS_PROTOCOL_SSL3
- LDAP_OPT_X_TLS_PROTOCOL_TLS1_0
- LDAP_OPT_X_TLS_PROTOCOL_TLS1_1
- LDAP_OPT_X_TLS_PROTOCOL_TLS1_2
- LDAP_OPT_X_TLS_PACKAGE
- LDAP_OPT_X_KEEPALIVE_IDLE
- LDAP_OPT_X_KEEPALIVE_PROBES
- LDAP_OPT_X_KEEPALIVE_INTERVAL

### PostgreSQL

- PGSQL_NOTICE_LAST
- PGSQL_NOTICE_ALL
- PGSQL_NOTICE_CLEAR

### SPL

- MT_RAND_PHP

## Backward incompatible changes

### Throw on passing too few function arguments

Previously, a warning would be emitted for invoking user-defined functions with too few arguments. Now, this warning has been promoted to an Error exception. This change only applies to user-defined functions, not internal functions.

### Forbid dynamic calls to scope introspection functions

Dynamic calls for certain functions have been forbidden (in the form of \$func() or array_map('extract', ...), etc). These functions either inspect or modify another scope, and present with them ambiguous and unreliable behavior. The functions are as follows:

- assert() - with a string as the first argument
- compact()
- extract()
- func_get_args()
- func_get_arg()
- func_num_args()
- get_defined_vars()
- mb_parse_str() - with one arg
- parse_str() - with one arg

### Invalid class, interface, and trait names

The following names cannot be used to name classes, interfaces, or traits:

- void
- iterable

### Numerical string conversions now respect scientific notation

Integer operations and conversions on numerical strings now respect scientific notation. This also includes the (int) cast operation, and the following functions: intval() (where the base is 10), settype(), decbin(), decoct(), and dechex().

### Fixes to mt_rand() algorithm

mt_rand() will now default to using the fixed version of the Mersenne Twister algorithm. If deterministic output from mt_srand() was relied upon, then the MT_RAND_PHP with the ability to preserve the old (incorrect) implementation via an additional optional second parameter to mt_srand().

### rand() aliased to mt_rand() and srand() aliased to mt_srand()

rand() and srand() have now been made aliases to mt_rand() and mt_srand(), respectively. This means that the output for the following functions have changed: rand(), shuffle(), str_shuffle(), and array_rand().

### Disallow the ASCII delete control character in identifiers

The ASCII delete control character (0x7F) can no longer be used in identifiers that are not quoted.

### error_log changes with syslog value

If the error_log ini setting is set to syslog, the PHP error levels are mapped to the syslog error levels. This brings finer differentiation in the error logs in contrary to the previous approach where all the errors are logged with the notice level only.

### Do not call destructors on incomplete objects

Destructors are now never called for objects that throw an exception during the execution of their constructor. In previous versions this behavior depended on whether the object was referenced outside the constructor (e.g. by an exception backtrace).

### call_user_func() handling of reference arguments

call_user_func() will now always generate a warning upon calls to functions that expect references as arguments. Previously this depended on whether the call was fully qualified.

Additionally, call_user_func() and call_user_func_array() will no longer abort the function call in this case. The "expected reference" warning will be emitted, but the call will proceed as usual.

### The empty index operator is not supported for strings anymore

Applying the empty index operator to a string (e.g. $str[] = $x) throws a fatal error instead of converting silently to array.Applying the empty index operator to a string (e.g. $str[] = $x) throws a fatal error instead of converting silently to array.

### Assignment via string index access on an empty string

String modification by character on an empty string now works like for non-empty strings, i.e. writing to an out of range offset pads the string with spaces, where non-integer types are converted to integer, and only the first character of the assigned string is used. Formerly, empty strings where silently treated like an empty array.

### Removed ini directives

The following ini directives have been removed:

- session.entropy_file
- session.entropy_length
- session.hash_function
- session.hash_bits_per_character

### Array ordering when elements are automatically created during by reference assignments has changed

The order of the elements in an array has changed when those elements have been automatically created by referencing them in a by reference assignment.

### Sort order of equal elements

The internal sorting algorithm has been improved, what may result in different sort order of elements, which compare as equal, than before.

### Error message for E_RECOVERABLE errors

The error message for E_RECOVERABLE errors has been changed from "Catchable fatal error" to "Recoverable fatal error"

### \$options parameter of unserialize()

The allowed_classes element of the \$options parameter of unserialize() is now strictly typed, i.e. if anything other than an array or a boolean is given, unserialize() returns FALSE and issues an E_WARNING.

### DateTime constructor incorporates microseconds

DateTime and DateTimeImmutable now properly incorporate microseconds when constructed from the current time, either explicitly or with a relative string (e.g. "first day of next month"). This means that naive comparisons of two newly created instances will now more likely return FALSE instead of TRUE

### Fatal errors to Error exceptions conversions

In the Date extension, invalid serialization data for DateTime or DatePeriod classes, or timezone initialization failure from serialized data, will now throw an Error exception from the **wakeup() or **set_state() methods, instead of resulting in a fatal error.

In the DBA extension, data modification functions (such as dba_insert()) will now throw an Error exception instead of triggering a catchable fatal error if the key does not contain exactly two elements.

In the DOM extension, invalid schema or RelaxNG validation contexts will now throw an Error exception instead of resulting in a fatal error. Similarly, attempting to register a node class that does not extend the appropriate base class, or attempting to read an invalid property or write to a readonly property, will also now throw an Error exception.

In the IMAP extension, email addresses longer than 16385 bytes will throw an Error exception instead of resulting in a fatal error.

In the Intl extension, failing to call the parent constructor in a class extending Collator before invoking the parent methods will now throw an Error instead of resulting in a recoverable fatal error. Also, cloning a Transliterator object will now throw an Error exception on failure to clone the internal transliterator instead of resulting in a fatal error.

In the LDAP extension, providing an unknown modification type to ldap_batch_modify() will now throw an Error exception instead of resulting in a fatal error.

In the mbstring extension, the mb_ereg() and mb_eregi() functions will now throw a ParseError exception if an invalid PHP expression is provided and the 'e' option is used.

In the Mcrypt extension, the mcrypt_encrypt() and mcrypt_decrypt() will now throw an Error exception instead of resulting in a fatal error if mcrypt cannot be initialized.

In the mysqli extension, attempting to read an invalid property or write to a readonly property will now throw an Error exception instead of resulting in a fatal error.

In the Reflection extension, failing to retrieve a reflection object or retrieve an object property will now throw an Error exception instead of resulting in a fatal error.

In the Session extension, custom session handlers that do not return strings for session IDs will now throw an Error exception instead of resulting in a fatal error when a function is called that must generate a session ID.

In the SimpleXML extension, creating an unnamed or duplicate attribute will now throw an Error exception instead of resulting in a fatal error.

In the SPL extension, attempting to clone an SplDirectory object will now throw an Error exception instead of resulting in a fatal error. Similarly, calling ArrayIterator::append() when iterating over an object will also now throw an Error exception.

In the standard extension, the assert() function, when provided with a string argument as its first parameter, will now throw a ParseError exception instead of resulting in a catchable fatal error if the PHP code is invalid. Similarly, calling forward_static_call() outside of a class scope will now throw an Error exception.

In the Tidy extension, creating a tidyNode manually will now throw an Error exception instead of resulting in a fatal error.

In the WDDX extension, a circular reference when serializing will now throw an Error exception instead of resulting in a fatal error.

In the XML-RPC extension, a circular reference when serializing will now throw an instance of Error exception instead of resulting in a fatal error.

In the Zip extension, the ZipArchive::addGlob() method will now throw an Error exception instead of resulting in a fatal error if glob support is not available.

### Lexically bound variables cannot reuse names

Variables bound to a closure via the use construct cannot use the same name as any superglobals, \$this, or any parameter.

### long2ip() parameter type change

long2ip() now expects an int instead of a string.

### JSON encoding and decoding

The serialize_precision ini setting now controls the serialization precision when encoding doubles.

Decoding an empty key now results in an empty property name, rather than _empty_ as a property name.

When supplying the JSON_UNESCAPED_UNICODE flag to json_encode(), the sequences U+2028 and U+2029 are now escaped.

### Changes to mb_ereg() and mb_eregi() parameter semantics

The third parameter to the mb_ereg() and mb_eregi() functions (regs) will now be set to an empty array if nothing was matched. Formerly, the parameter would not have been modified.

### Drop support for the sslv2 stream

The sslv2 stream has now been dropped in OpenSSL.

## Deprecated features in PHP 7.1.x

### ext/mcrypt

The mcrypt extension has been abandonware for nearly a decade now, and was also fairly complex to use. It has therefore been deprecated in favour of OpenSSL, where it will be removed from the core and into PECL in PHP 7.2.

### Eval option for mb_ereg_replace() and mb_eregi_replace()

The e pattern modifier has been deprecated for the mb_ereg_replace() and mb_eregi_replace() functions.

## Changed functions

### PHP Core

- getopt() has an optional third parameter that exposes the index of the next element in the argument vector list to be processed. This is done via a by-ref parameter.
- getenv() no longer requires its parameter. If the parameter is omitted, then the current environment variables will be returned as an associative array.
- get_headers() now has an additional parameter to enable for the passing of custom stream contexts.
  output_reset_rewrite_vars() no longer resets session URL rewrite variables.
- parse_url() is now more restrictive and supports RFC3986.
- unpack() now accepts an optional third parameter to specify the offset to begin unpacking from.

### File System

- file_get_contents() now accepts a negative seek offset if the stream is seekable.
- tempnam() now emits a notice when falling back to the system's temp directory.

### JSON

json_encode() now accepts a new option, JSON_UNESCAPED_LINE_TERMINATORS, to disable the escaping of U+2028 and U+2029 characters when JSON_UNESCAPED_UNICODE is supplied.

### Multibyte String

- mb_ereg() now rejects illegal byte sequences.
- mb_ereg_replace() now rejects illegal byte sequences.

### PDO

PDO::lastInsertId() for PostgreSQL will now trigger an error when nextval has not been called for the current session (the postgres connection).

### PostgreSQL

- pg_last_notice() now accepts an optional parameter to specify an operation. This can be done with one of the following new constants: PGSQL_NOTICE_LAST, PGSQL_NOTICE_ALL, or PGSQL_NOTICE_CLEAR.
- pg_fetch_all() now accepts an optional second parameter to specify the result type (similar to the third parameter of pg_fetch_array()).
- pg_select() now accepts an optional fourth parameter to specify the result type (similar to the third parameter of pg_fetch_array()).

### Session

- session_start() now returns FALSE and no longer initializes \$\_SESSION when it failed to start the session.

## Other changes

### Notices and warnings on arithmetic with invalid strings

New E_WARNING and E_NOTICE errors have been introduced when invalid strings are coerced using operators expecting numbers (+ - \* / \*\* % << >> | & ^) or their assignment equivalents. An E_NOTICE is emitted when the string begins with a numeric value but contains trailing non-numeric characters, and an E_WARNING is emitted when the string does not contain a numeric value.

### Warn on octal escape sequence overflow

Previously, 3-octet octal string escape sequences would overflow silently. Now, they will still overflow, but E_WARNING will be emitted.

### Inconsistency fixes to \$this

Whilst $this is considered a special variable in PHP, it lacked proper checks to ensure it wasn't used as a variable name or reassigned. This has now been rectified to ensure that $this cannot be a user-defined variable, reassigned to a different value, or be globalised.

### Session ID generation without hashing

Session IDs will no longer be hashed upon generation. With this change brings about the removal of the following four ini settings:

- session.entropy_file
- session.entropy_length
- session.hash_function
- session.hash_bits_per_character

And the addition of the following two ini settings:

- session.sid_length - defines the length of the session ID, defaulting to 32 characters for backwards compatibility)
- session.sid_bits_per_character - defines the number of bits to be stored per character (i.e. increases the range of characters that can be used in the session ID), defaulting to 4 for backwards compatibility

### Changes to INI file handling

- precision
  If the value is set to -1, then the dtoa mode 0 is used. The default value is still 14.

- serialize_precision
  If the value is set to -1, then the dtoa mode 0 is used. The value -1 is now used by default.

- gd.jpeg_ignore_warning
  The default of this php.ini setting has been changed to 1, so by default libjpeg warnings are ignored.

- opcache.enable_cli
  The default of this php.ini setting has been changed to 1 (enabled) in PHP 7.1.2, and back to 0 (disabled) in PHP 7.1.7.

### Session ID generation with a CSPRNG only

Session IDs will now only be generated with a CSPRNG.

### More informative TypeError messages when NULL is allowed

TypeError exceptions for arg_info type checks will now provide more informative error messages. If the parameter type or return type accepts NULL (by either having a default value of NULL or being a nullable type), then the error message will now mention this with a message of "must be ... or null" or "must ... or be null."

## Windows Support

### Support for long and UTF-8 path

If a web application is UTF-8 conform, no further action is required. For applications depending on paths in non UTF-8 encodings for I/O, an explicit INI directive has to be set. The encoding INI settings check relies on the order in the core:

internal_encoding
default_charset
zend.multibyte
Several functions for codepage handling were introduced:

sapi_windows_cp_set() to set the default codepage
sapi_windows_cp_get() to retrieve the current codepage
sapi_windows_cp_is_utf8()
sapi_windows_cp_conv() to convert between codepages, using iconv() compatible signature
These functions are thread safe.

The console output codepage is adjusted depending on the encoding used in PHP. Depending on the concrete system OEM codepage, the visible output might or might be not correct. For example, in the default cmd.exe and on a system with the OEM codepage 437, outputs in codepages 1251, 1252, 1253 and some others can be shown correctly when using UTF-8. On the same system, chars in codepage like 20932 probably won't be shown correctly. This refers to the particular system rules for codepage, font compatibility and the particular console program used. PHP automatically sets the console codepage according to the encoding rules from php.ini. Using alternative consoles instead of cmd.exe directly might bring better experience in some cases.

Nevertheless be aware, runtime codepage switch after the request start might bring unexpected side effects on CLI. The preferable way is php.ini, When PHP CLI is used in a console emulator, that doesn't support Unicode, it might possibly be required, to avoid changing the console codepage. The best way to achieve it is by setting the default or internal encoding to correspond the ANSI codepage. Another method is to set the INI directives output_encoding and input_encoding to the required codepage, in which case however the difference between internal and I/O codepage is likely to cause mojibake. In rare cases, if PHP happens to crash gracefully, the original console codepage might be not restored. In this case, the chcp command can be used, to restore it manually.

Special awareness for the DBCS systems - the codepage switch on runtime using ini_set() is likely to cause display issues. The difference to the non DBCS systems is, that the extended characters require two console cells to be displayed. In certain case, only the mapping of the characters into the glyph set of the font could happen, no actual font change. This is the nature of DBCS systems, the most simple way to prevent display issues is to avoid usage of ini_set() for the codepage change.

As a result of UTF-8 support in the streams, PHP scripts are not limited to ASCII or ANSI filenames anymore. This is supported out of the box on CLI. For other SAPI, the documentation for the corresponding server is useful.

Long paths support is transparent. Paths longer than 260 bytes get automatically prefixed with \\?\. The max path length is limited to 2048 bytes. Be aware, that the path segment limit (basename length) still persists.

For the best portability, it is strongely recommended to handle filenames, I/O and other related topics UTF-8. Additionally, for the console applications, the usage of a TrueType font is preferable and the usage of ini_set() for the codepage change is discouraged.

### readline

The readline extension is supported through the » WinEditLine library. Thereby, the interactive CLI shell is supported as well (php.exe -a).

### PHP_FCGI_CHILDREN

PHP_FCGI_CHILDREN is now respected. If this environment variable is defined, the first php-cgi.exe process will exec the specified number of children. These will share the same TCP socket.

### ftok()

Added support for ftok()

# Source

[Migrating from PHP 7.0.x to PHP 7.1.x](https://www.php.net/manual/en/migration71.php)

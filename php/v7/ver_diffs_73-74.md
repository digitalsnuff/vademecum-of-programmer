# Migrating from PHP 7.3.x to PHP 7.4.x

## New Features

### PHP Core

- Typed properties

Class properties now support type declarations.

### Arrow functions

Arrow functions provide a shorthand syntax for defining functions with implicit by-value scope binding.

### Limited return type covariance and argument type contravariance

Full variance support is only available if autoloading is used. Inside a single file only non-cyclic type references are possible, because all classes need to be available before they are referenced.

### Null coalescing assignment operator

### Unpacking inside arrays

### Numeric literal separator

### Numeric literals can contain underscores between digits.

### Weak references

Weak references allow the programmer to retain a reference to an object that does not prevent the object from being destroyed.

### Allow exceptions from \_\_toString()

Throwing exceptions from \_\_toString() is now permitted. Previously this resulted in a fatal error. Existing recoverable fatal errors in string conversions have been converted to Error exceptions.

### CURL

CURLFile now supports stream wrappers in addition to plain file names, if the extension has been built against libcurl >= 7.56.0.

### Filter

The FILTER_VALIDATE_FLOAT filter now supports the min_range and max_range options, with the same semantics as FILTER_VALIDATE_INT.

### FFI

FFI is a new extension, which provides a simple way to call native functions, access native variables, and create/access data structures defined in C libraries.

### GD

Added the IMG_FILTER_SCATTER image filter to apply a scatter filter to images.

### Hash

Added crc32c hash using Castagnoli's polynomial. This CRC32 variant is used by storage systems, such as iSCSI, SCTP, Btrfs and ext4.

### Multibyte String

Added the mb_str_split() function, which provides the same functionality as str_split(), but operating on code points rather than bytes.

### OPcache

Support for preloading code has been added.

### Regular Expressions (Perl-Compatible)

The preg_replace_callback() and preg_replace_callback_array() functions now accept an additional flags argument, with support for the PREG_OFFSET_CAPTURE and PREG_UNMATCHED_AS_NULL flags. This influences the format of the matches array passed to to the callback function.

### PDO

The username and password can now be specified as part of the PDO DSN for the mysql, mssql, sybase, dblib, firebird and oci drivers. Previously this was only supported by the pgsql driver. If a username/password is specified both in the constructor and the DSN, the constructor takes precedence.

It is now possible to escape question marks in SQL queries to avoid them being interpreted as parameter placeholders. Writing ?? allows sending a single question mark to the database and e.g. use the PostgreSQL JSON key exists (?) operator.

### PDO_OCI

PDOStatement::getColumnMeta() is now available.

### PDO_SQLite

PDOStatement::getAttribute(PDO::SQLITE_ATTR_READONLY_STATEMENT) allows checking whether the statement is read-only, i.e. if it doesn't modify the database.

PDO::setAttribute(PDO::SQLITE_ATTR_EXTENDED_RESULT_CODES, true) enables the use of SQLite3 extended result codes in PDO::errorInfo() and PDOStatement::errorInfo().

### SQLite3

Added SQLite3::lastExtendedErrorCode() to fetch the last extended result code.

Added SQLite3::enableExtendedResultCodes(\$enable = true), which will make SQLite3::lastErrorCode() return extended result codes.

### Standard

- strip_tags() with array of tag names
  strip_tags() now also accepts an array of allowed tags: instead of strip_tags($str, '<a><p>') you can now write strip_tags($str, ['a', 'p']).

- Custom object serialization
  A new mechanism for custom object serialization has been added, which uses two new magic methods: **serialize and **unserialize. The new serialization mechanism supersedes the Serializable interface, which will be deprecated in the future.

- Array merge functions without arguments ¶
  array_merge() and array_merge_recursive() may now be called without any arguments, in which case they will return an empty array. This is useful in conjunction with the spread operator, e.g. array_merge(...\$arrays).

- proc_open() function
  proc_open() now accepts an array instead of a string for the command. In this case the process will be opened directly (without going through a shell) and PHP will take care of any necessary argument escaping. proc_open() now supports redirect and null descriptors.

- argon2i(d) without libargon
  password_hash() now has the argon2i and argon2id implementations from the sodium extension when PHP is built without libargon.

## New Classes and Interfaces

### Reflection

- ReflectionReference

## New Functions

### PHP Core

- get_mangled_object_vars()
- password_algos()
- sapi_windows_set_ctrl_handler()

### GD

- imagecreatefromtga()

### OpenSSL

- openssl_x509_verify()

### Process Control

- pcntl_unshare()

### SQLite3

- SQLite3::backup()
- SQLite3Stmt::getSQL()

## New Global Constants

###PHP Core ¶

- PHP_WINDOWS_EVENT_CTRL_C
- PHP_WINDOWS_EVENT_CTRL_BREAK

### Multibyte String

- MB_ONIGURUMA_VERSION

### Sockets

Added the following FreeBSD-specific socket options:

- SO_LABEL
- SO_PEERLABEL
- SO_LISTENQLIMIT
- SO_LISTENQLEN
- SO_USER_COOKIE

### Tidy

- TIDY_TAG_ARTICLE
- TIDY_TAG_ASIDE
- TIDY_TAG_AUDIO
- TIDY_TAG_BDI
- TIDY_TAG_CANVAS
- TIDY_TAG_COMMAND
- TIDY_TAG_DATALIST
- TIDY_TAG_DETAILS
- TIDY_TAG_DIALOG
- TIDY_TAG_FIGCAPTION
- TIDY_TAG_FIGURE
- TIDY_TAG_FOOTER
- TIDY_TAG_HEADER
- TIDY_TAG_HGROUP
- TIDY_TAG_MAIN
- TIDY_TAG_MARK
- TIDY_TAG_MENUITEM
- TIDY_TAG_METER
- TIDY_TAG_NAV
- TIDY_TAG_OUTPUT
- TIDY_TAG_PROGRESS
- TIDY_TAG_SECTION
- TIDY_TAG_SOURCE
- TIDY_TAG_SUMMARY
- TIDY_TAG_TEMPLATE
- TIDY_TAG_TIME
- TIDY_TAG_TRACK
- TIDY_TAG_VIDEO

## Backward Incompatible Changes

### PHP Core

- Array-style access of non-arrays ¶
  Trying to use values of type null, bool, int, float or resource as an array (such as \$null["key"]) will now generate a notice.

- get_declared_classes() function ¶
  The get_declared_classes() function no longer returns anonymous classes that have not been instantiated yet.

- fn keyword
  fn is now a reserved keyword. In particular, it can no longer be used as a function or class name. It can still be used as a method or class constant name.

- <?php tag at end of file
  <?php at the end of the file (without trailing newline) will now be interpreted as an opening PHP tag. Previously it was interpreted either as a short opening tag followed by literal php and resulted in a syntax error (with short_open_tag=1) or was interpreted as a literal <?php string (with short_open_tag=0).

* Stream wrappers
  When using include/require on a stream, streamWrapper::stream_set_option() will be invoked with the STREAM_OPTION_READ_BUFFER option. Custom stream wrapper implementations may need to implement the streamWrapper::stream_set_option() method to avoid a warning (always returning FALSE is a sufficient implementation).

* Serialization
  The o serialization format has been removed. As it is never produced by PHP, this may only break unserialization of manually crafted strings.

* Password algorithm constants
  Password hashing algorithm identifiers are now nullable strings rather than integers.

PASSWORD_DEFAULT was int 1; now is NULL
PASSWORD_BCRYPT was int 1; now is string '2y'
PASSWORD_ARGON2I was int 2; now is string 'argon2i'
PASSWORD_ARGON2ID was int 3; now is string 'argon2id'
Applications correctly using the constants PASSWORD_DEFAULT, PASSWORD_BCRYPT, PASSWORD_ARGON2I, and PASSWORD_ARGON2ID will continue to function correctly.

- htmlentities() function
  htmlentities() will now raise a notice (instead of a strict standards warning) if it is used with an encoding for which only basic entity substitution is supported, in which case it is equivalent to htmlspecialchars().

- fread() and fwrite() function
  fread() and fwrite() will now return FALSE if the operation failed. Previously an empty string or 0 was returned. EAGAIN/EWOULDBLOCK are not considered failures.
  These functions now also raise a notice on failure, such as when trying to write to a read-only file resource.

### BCMath Arbitrary Precision Mathematics

BCMath functions will now warn if a non well-formed number is passed, such as "32foo". The argument will be interpreted as zero, as before.

### CURL

Attempting to serialize a CURLFile class will now generate an exception. Previously the exception was only thrown on unserialization.

Using CURLPIPE_HTTP1 is deprecated, and is no longer supported as of cURL 7.62.0.

The \$version parameter of curl_version() is deprecated. If any value not equal to the default CURLVERSION_NOW is passed, a warning is raised and the parameter is ignored.

### Date and Time

Calling var_dump() or similar on a DateTime or DateTimeImmutable instance will no longer leave behind accessible properties on the object.

Comparison of DateInterval objects (using ==, <, and so on) will now generate a warning and always return FALSE. Previously all DateInterval objects were considered equal, unless they had properties.

### Intl

The default parameter value of idn_to_ascii() and idn_to_utf8() is now INTL_IDNA_VARIANT_UTS46 instead of the deprecated INTL_IDNA_VARIANT_2003.

### MySQLi

The embedded server functionality has been removed. It was broken since at least PHP 7.0.

The undocumented mysqli::\$stat property has been removed in favor of mysqli::stat().

### OpenSSL

The openssl_random_pseudo_bytes() function will now throw an exception in error situations, similar to random_bytes(). In particular, an Error is thrown if the number of requested bytes is less than or equal to zero, and an Exception is thrown if sufficient randomness cannot be gathered. The \$crypto_strong output argument is guaranteed to always be TRUE if the function does not throw, so explicitly checking it is not necessary.

### Regular Expressions (Perl-Compatible)

When PREG_UNMATCHED_AS_NULL mode is used, trailing unmatched capturing groups will now also be set to NULL (or [null, -1] if offset capture is enabled). This means that the size of the \$matches will always be the same.

### Reflection

Reflection objects will now generate an exception if an attempt is made to serialize them. Serialization for reflection objects was never supported and resulted in corrupted reflection objects. It has been explicitly prohibited now.

### Standard PHP Library (SPL)

Calling get_object_vars() on an ArrayObject instance will now always return the properties of the ArrayObject itself (or a subclass). Previously it returned the values of the wrapped array/object unless the ArrayObject::STD_PROP_LIST flag was specified.

Other affected operations are:

- ReflectionObject::getProperties()
- reset(), current(), etc. Use Iterator methods instead.
- Potentially others working on object properties as a list.

(array) casts are not affected. They will continue to return either the wrapped array, or the ArrayObject properties, depending on whether the ArrayObject::STD_PROP_LIST flag is used.

SplPriorityQueue::setExtractFlags() will throw an exception if zero is passed. Previously this would generate a recoverable fatal error on the next extraction operation.

ArrayObject, ArrayIterator, SplDoublyLinkedList and SplObjectStorage now support the **serialize() and **unserialize() mechanism in addition to the Serializable interface. This means that serialization payloads created on older PHP versions can still be unserialized, but new payloads created by PHP 7.4 will not be understood by older versions.

### Tokenizer

token_get_all() will now emit a T_BAD_CHARACTER token for unexpected characters instead of leaving behind holes in the token stream.

## Deprecated Features

### PHP Core

- Nested ternary operators without explicit parentheses

Nested ternary operations must explicitly use parentheses to dictate the order of the operations. Previously, when used without parentheses, the left-associativity would not result in the expected behaviour in most cases.

- Array and string offset access using curly braces

The array and string offset access syntax using curly braces is deprecated. Use $var[$idx] instead of $var{$idx}.

- (real) cast and is_real() function

The (real) cast is deprecated, use (float) instead.

The is_real() function is also deprecated, use is_float() instead.

- Unbinding $this when $this is used

Unbinding $this of a non-static closure that uses $this is deprecated.

- parent keyword without parent class

Using parent inside a class without a parent is deprecated, and will throw a compile-time error in the future. Currently an error will only be generated if/when the parent is accessed at run-time.

- allow_url_include INI option

The allow_url_include ini directive is deprecated. Enabling it will generate a deprecation notice at startup.

- Invalid characters in base conversion functions

Passing invalid characters to base_convert(), bindec(), octdec() and hexdec() will now generate a deprecation notice. The result will still be computed as if the invalid characters did not exist. Leading and trailing whitespace, as well as prefixes of type 0x (depending on base) continue to be allowed.

- Using array_key_exists() on objects

Using array_key_exists() on objects is deprecated. Instead either isset() or property_exists() should be used.

- Magic quotes functions

The get_magic_quotes_gpc() and get_magic_quotes_runtime() functions are deprecated. They always return FALSE.

- hebrevc() function

The hebrevc() function is deprecated. It can be replaced with nl2br(hebrev(\$str)) or, preferably, the use of Unicode RTL support.

- convert_cyr_string() function

The convert_cyr_string() function is deprecated. It can be replaced by one of mb_convert_string(), iconv() or UConverter.

- money_format() function
  The money_format() function is deprecated. It can be replaced by the intl NumberFormatter functionality.

- ezmlm_hash() function
  The ezmlm_hash() function is deprecated.

- restore_include_path() function
  The restore_include_path() function is deprecated. It can be replaced by ini_restore('include_path').

- Implode with historical parameter order
  Passing parameters to implode() in reverse order is deprecated, use implode($glue, $parts) instead of implode($parts, $glue).

### COM

Importing type libraries with case-insensitive constant registering has been deprecated.

### Filter

FILTER_SANITIZE_MAGIC_QUOTES is deprecated, use FILTER_SANITIZE_ADD_SLASHES instead.

### Multibyte String

Passing a non-string pattern to mb_ereg_replace() is deprecated. Currently, non-string patterns are interpreted as ASCII codepoints. In PHP 8, the pattern will be interpreted as a string instead.

Passing the encoding as 3rd parameter to mb_strrpos() is deprecated. Instead pass a 0 offset, and encoding as 4th parameter.

### Lightweight Directory Access Protocol

ldap_control_paged_result_response() and ldap_control_paged_result() are deprecated. Pagination controls can be sent along with ldap_search() instead.

### Reflection

Calls to ReflectionType::\_\_toString() now generate a deprecation notice. This method has been deprecated in favor of ReflectionNamedType::getName() in the documentation since PHP 7.1, but did not throw a deprecation notice for technical reasons.

The export() methods on all Reflection classes are deprecated. Construct a Reflection object and convert it to string instead.

### Socket

The AI_IDN_ALLOW_UNASSIGNED and AI_IDN_USE_STD3_ASCII_RULES flags for socket_addrinfo_lookup() are deprecated, due to an upstream deprecation in glibc.

## Removed Extensions

These extensions have been moved to PECL and are no longer part of the PHP distribution. The PECL package versions of these extensions will be created according to user demand.

- Firebird/Interbase - access to an InterBase and/or Firebird based database is still available with the PDO - Firebird driver.
- Recode
- WDDX

## Other Changes

### Performance Improvements

- PHP Core
  A specialized VM opcode for the array_key_exists() function has been added, which improves performance of this function if it can be statically resolved. In namespaced code, this may require writing \array_key_exists() or explicitly importing the function.

- Regular Expressions (Perl-Compatible)

When preg_match() in UTF-8 mode ("u" modifier) is repeatedly called on the same string (but possibly different offsets), it will only be checked for UTF-8 validity once.

### Changes to INI File Handling

`zend.exception_ignore_args` is a new INI directive for including or excluding arguments from stack traces generated from exceptions.

`opcache.preload_user` is a new INI directive for specifying the user account under which preloading code is execute if it would otherwise be run as root (which is not allowed for security reasons).

### Migration to pkg-config

A number of extensions have been migrated to exclusively use pkg-config for the detection of library dependencies. Generally, this means that instead of using --with-foo-dir=DIR or similar only --with-foo is used. Custom library paths can be specified either by adding additional directories to PKG_CONFIG_PATH or by explicitly specifying compilation options through FOO_CFLAGS and FOO_LIBS.

The following extensions and SAPIs are affected:

- CURL:
  - --with-curl no longer accepts a directory.
- Enchant:
  - --with-enchant no longer accepts a directory.
- FPM:
  - --with-fpm-systemd now uses only pkg-config for libsystem checks. The libsystemd minimum required version is 209.
- GD:
  - --with-gd becomes --enable-gd (whether to enable the extension at all) and --with-external-gd (to opt into using an external libgd, rather than the bundled one).
  - --with-png-dir has been removed. libpng is required.
  - --with-zlib-dir has been removed. zlib is required.
  - --with-freetype-dir becomes --with-freetype
  - --with-jpeg-dir becomes --with-jpeg
  - --with-webp-dir becomes --with-webp
  - --with-xpm-dir becomes --with-xpm
- IMAP:
  - --with-kerberos-systemd no longer accepts a directory.
- Intl:
  - --with-icu-dir has been removed. If --enable-intl is passed, then libicu is always required.
- LDAP:
  - --with-ldap-sasl no longer accepts a directory.
- Libxml:
  - --with-libxml-dir has been removed.
  - --enable-libxml becomes --with-libxml.
  - --with-libexpat-dir has been renamed to --with-expat and no longer accepts a directory.
- Litespeed:
  - --with-litespeed becomes --enable-litespeed.
- Mbstring:
  - --with-onig has been removed. Unless --disable-mbregex has been passed, libonig is required.
- ODBC:
  - --with-iodbc no longer accepts a directory.
  - --with-unixODBC without a directory now uses pkg-config (preferred). Directory is still accepted for old versions without libodbc.pc.
- OpenSSL:
  - --with-openssl no longer accepts a directory.
- PCRE:
  - --with-pcre-regex has been removed. Instead --with-external-pcre is provided to opt into using an external PCRE library, rather than the bundled one.
- PDO_SQLite:
  - --with-pdo-sqlite no longer accepts a directory.
- Readline:
  - --with-libedit no longer accepts a directory.
- Sodium:
  - --with-sodium no longer accepts a directory.
- SQLite3:
  - --with-sqlite3 no longer accepts a directory.
- XSL:
  - --with-xsl no longer accepts a directory.
- Zip:
  - --with-libzip has been removed.
  - --enable-zip becomes --with-zip.

### CSV escaping

fputcsv(), fgetcsv(), SplFileObject::fputcsv(), SplFileObject::fgetcsv(), and SplFileObject::setCsvControl() now accept an empty string as \$escape argument, which disables the proprietary PHP escaping mechanism.

The behavior of str_getcsv() has been adjusted accordingly (formerly, an empty string was identical to using the default).

SplFileObject::getCsvControl() now may also return an empty string for the third array element, accordingly.

### Data Filtering

The filter extension no longer exposes --with-pcre-dir for Unix builds and can now reliably be built as shared when using ./configure

### GD

The behavior of imagecropauto() in the bundled libgd has been synced with that of system libgd:

- IMG_CROP_DEFAULT is no longer falling back to IMG_CROP_SIDES
- Threshold-cropping now uses the algorithm of system libgd

The default \$mode parameter of imagecropauto() has been changed to IMG_CROP_DEFAULT; passing -1 is now deprecated.

imagescale() now supports aspect ratio preserving scaling to a fixed height by passing -1 as \$new_width.

### HASH Message Digest Framework

The hash extension cannot be disabled anymore and is always an integral part of any PHP build, similar to the date extension.

### Intl

The intl extension now requires at least ICU 50.1.

ResourceBundle now implements Countable.

### Lightweight Directory Access Protocol

Support for nsldap and umich_ldap has been removed.

### Libxml

All libxml-based extensions now require libxml 2.7.6 or newer.

### Multibyte String

The oniguruma library is no longer bundled with PHP, instead libonig needs to be available on the system. Alternatively --disable-mbregex can be used to disable the mbregex component.

### OPcache

The --disable-opcache-file and --enable-opcache-file configure options have been removed in favor of the opcache.file_cache INI directive.

### Password Hashing

The password_hash() and functions now accept nullable string and integer for \$algo argument.

### PEAR

Installation of PEAR (including PECL) is no longer enabled by default. It can be explicitly enabled using --with-pear. This option is deprecated and may be removed in the future.

### Reflection

The numeric values of the modifier constants (IS_ABSTRACT, IS_DEPRECATED, IS_EXPLICIT_ABSTRACT, IS_FINAL, IS_IMPLICIT_ABSTRACT, IS_PRIVATE, IS_PROTECTED, IS_PUBLIC, and IS_STATIC) on the ReflectionClass, ReflectionFunction, ReflectionMethod, ReflectionObject, and ReflectionProperty classes have changed.

### SimpleXML

SimpleXMLElement now implements Countable.

### SQLite3

The bundled libsqlite has been removed. To build the SQLite3 extension a system libsqlite3 ≥ 3.7.4 is now required. To build the PDO_SQLite extension a system libsqlite3 ≥ 3.5.0 is now required.

Serialization and unserialization of SQLite3, SQLite3Stmt and SQLite3Result is now explicitly forbidden. Formerly, serialization of instances of these classes was possible, but unserialization yielded unusable objects.

The @param notation can now also be used to denote SQL query parameters.

### Zip

The bundled libzip library has been removed. A system libzip >= 0.11 is now necessary to build the zip extension.

## Windows Support

### configure flags

configure now regards additional CFLAGS and LDFLAGS set as environment variables.

### CTRL handling

CTRL+C and CTRL+BREAK on console can be caught by setting a handler function with sapi_windows_set_ctrl_handler().

proc_open() on Windows can be passed a "create_process_group" option. It is required if the child process is supposed to handle CTRL events.

### OPcache

OPcache now supports an arbitrary number of separate caches per user via the the INI directive opcache.cache_id. All processes with the same cache ID and user share an OPcache instance.

### stat

The stat implementation has been refactored.

- An inode number is delivered and is based on the NTFS file index.
- The device number is now based on the volume serial number.

Note that both values are derived from the system and provided as-is on 64-bit systems. On 32-bit systems, these values might overflow the 32-bit integer in PHP, so they're fake.

### libsqlite3

libsqlite3 is no longer compiled statically into php_sqlite3.dll and php_pdo_sqlite.dll, but rather available as libsqlite3.dll. Refer to the installation instructions for SQLite3 and PDO_SQLITE, respectively.

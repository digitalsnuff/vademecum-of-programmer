# Errors in PHP 7

PHP 7 changes how most errors are reported by PHP. Instead of reporting errors through the traditional error reporting mechanism used by PHP 5, most errors are now reported by throwing Error exceptions.

As with normal exceptions, these Error exceptions will bubble up until they reach the first matching catch block. If there are no matching blocks, then any default exception handler installed with set_exception_handler() will be called, and if there is no default exception handler, then the exception will be converted to a fatal error and will be handled like a traditional error.

## Throwable interface

Throwable is the base interface for any object that can be thrown via a throw statement in PHP 7, including Error and Exception.

PHP classes cannot implement the Throwable interface directly, and must instead extend Exception.

## Error class

Error is the base class for all internal PHP errors.

Lists of Throwable and Exception tree as of 7.2.0

```
    Error
      ArithmeticError
        DivisionByZeroError
      AssertionError
      ParseError
      TypeError
        ArgumentCountError
    Exception
      ClosedGeneratorException
      DOMException
      ErrorException
      IntlException
      LogicException
        BadFunctionCallException
          BadMethodCallException
        DomainException
        InvalidArgumentException
        LengthException
        OutOfRangeException
      PharException
      ReflectionException
      RuntimeException
        OutOfBoundsException
        OverflowException
        PDOException
        RangeException
        UnderflowException
        UnexpectedValueException
      SodiumException
```

# Source

[Errors in PHP 7](https://www.php.net/manual/en/language.errors.php7.php)
[Exceptions Tree](https://gist.github.com/mlocati/249f07b074a0de339d4d1ca980848e6a)

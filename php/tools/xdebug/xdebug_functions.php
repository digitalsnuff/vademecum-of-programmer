<?php

/* Basics */

/**
 * Returns the calling class
 * 
 * This function returns the name of the class that defined the current method, 
 * NULL if the stack frame does not exist, or FALSE if no class is associated with this call.
 * 0 - returns the call information of the call to corresponding xdebug_call_* method
 * 1 - returns the call information of the method that executed
 * 2 - (the default) returns the call information of the "grand parent" of the current method
 * 
 * @param int $depth
 * @return mixed
 */
xdebug_call_class($depth = 2);


/**
 * Returns the calling file
 * 
 * This function returns the filename from where the current function/method was executed from, 
 * or NULL if the stack frame does not exist
 * 
 * @param int $depth
 * @return mixed 
 */
xdebug_call_file($depth = 2);

/**
 * Returns the calling function/method
 * 
 * This function returns the name of the current function/method, 
 * NULL if the stack frame does not exist, or FALSE if the stack frame has no function/method information
 * 
 * @param int $depth
 * @return mixed
 */
xdebug_call_function($depth = 2);



/**
 * Returns the calling line number
 * 
 * This function returns the line number 
 * from where the current function/method was called from, or NULL if the stack frame does not exist
 * 
 * @param int $depth
 * @return mixed
 */
xdebug_call_line($depth = 2);

/**
 * Displays information about super globals
 * 
 * This function dumps the values of the elements 
 * of the super globals as specified with the xdebug.dump.* php.ini settings.
 * xdebug.dump.GET=*
 * xdebug.dump.SERVER=REMOTE_ADDR
 * Query string:
 * \?var=fourty%20two&array[a]=a&array[9]=b
 * 
 * @return void
 */
xdebug_dump_superglobals();

/**
 * Returns all collected error messages
 * 
 * This function returns all errors from the collection buffer that contains all errors 
 * that were stored there when error collection was started with xdebug_start_error_collection().
 * 
 * @param bool $emptyList
 * @return array
 */
xdebug_get_collected_errors($emptyList = false);

/**
 * Returns the number of functions that have been called
 * 
 * This function returns the number of functions that have been called so far, including this function itself.
 * 
 * @return int
 */
xdebug_get_function_count();

/**
 * Returns all the headers as set by calls to PHP's header() function
 * 
 * Returns all the headers that are set with PHP's header() function, 
 * or any other header set internally within PHP (such as through setcookie()), as an array.
 * 
 * @return array
 */
xdebug_get_headers();

/**
 * Returns the current memory usage
 * 
 * @return  int
 */
xdebug_memory_usage();

/**
 * Returns the peak memory usage
 * 
 * @return int
 */
xdebug_peak_memory_usage();

/**
 * Starts recording all notices, warnings and errors and prevents their display
 * 
 * @return void
 */
xdebug_start_error_collection();

/**
 * Stops recording of all notices, warnings and errors as started by xdebug_start_error_collection()
 * 
 * @return void
 */
xdebug_stop_error_collection();

/**
 * Returns the current time index
 * 
 * @return float
 */
xdebug_time_index();

/* Variable Display Features */

/**
 * Displays detailed information about a variable
 * 
 * @param mixed $var
 * @return void
 */
var_dump($var);

/**
 * Displays information about a variable
 * 
 * This function displays structured information 
 * about one or more variables that includes its type, value and refcount information. 
 * Arrays are explored recursively with values.
 * 
 * @param string $varname
 * @return void
 */
xdebug_debug_zval($varname);

/**
 * Returns information about variables to stdout
 * 
 * @param string $varname
 * @return void
 */
xdebug_debug_zval_stdout($varname);

/**
 * Displays detailed information about a variable
 * 
 * @param mixed $variable
 * @return void
 */
xdebug_var_dump($variable);

/* Stack Traces */

/**
 * Returns declared variables
 * 
 * Returns an array where each element is a variable name which is defined in the current scope. 
 * The setting xdebug.collect_vars needs to be enabled.
 * 
 * @return array
 */
xdebug_get_declared_vars();

/**
 * Returns information about the stack
 * 
 * Returns an array which resembles the stack trace up to this point. 
 * 
 * @return array
 */
xdebug_get_function_stack();

/**
 * Returns information about monitored functions
 * 
 * Returns a structure which contains information about where the monitored functions were executed in your script. 
 * 
 * @return array
 */
xdebug_get_monitored_functions();

/**
 * Returns the current stack depth level
 * 
 * Returns the stack depth level. 
 * The main body of a script is level 0 and each include and/or function call adds one to the stack depth level.
 * 
 * @return int
 */
xdebug_get_stack_depth();

/**
 * Displays the current function stack
 * 
 * The bitmask "options" allows you to configure a few extra options. The following options are currently supported:
 * XDEBUG_STACK_NO_DESC - If this option is set, then the printed stack trace will not have a header. 
 *     This is useful if you want to print a stack trace from your own error handler, 
 *     as otherwise the printed location is where xdebug_print_function_stack() was called from.
 * 
 * @param string $message
 * @param int $options
 * @return void
 */
xdebug_print_function_stack($message = "user triggered", $options = 0);

/**
 * Starts function monitoring
 * 
 * This function starts the monitoring of functions 
 * that were given in a list as argument to this function. 
 * Function monitoring allows you to find out where in your code the functions 
 * that you provided as argument are called from. 
 * This can be used to track where old, or, discouraged functions are used.
 * The defined functions are case sensitive, and a dynamic call to a static method will not be caught.
 * 
 * @param array $listOfFunctionsToMonitor
 * @return void
 */
xdebug_start_function_monitor($listOfFunctionsToMonitor);

/**
 * Stops monitoring functions
 * 
 * @return void
 */
xdebug_stop_function_monitor();

/* Function Traces */

/**
 * Returns the name of the function trace file
 * 
 * This is useful when traces are made with xdebug.auto_trace is enabled.
 * 
 * @return mixed
 */
xdebug_get_tracefile_name();

/**
 * Starts a new function trace
 * 
 * Options:
 *     XDEBUG_TRACE_APPEND (1)
 *         makes the trace file open in append mode rather than overwrite mode
 *     XDEBUG_TRACE_COMPUTERIZED (2)
 *        creates a trace file with the format as described under 1 "xdebug.trace_format".
 *     XDEBUG_TRACE_HTML (4)
 *         creates a trace file as an HTML table
 *     XDEBUG_TRACE_NAKED_FILENAME (8)
 *         Normally, Xdebug always adds ".xt" to the end of the filename that you pass in as first argument to this function. 
 *         With the XDEBUG_TRACE_NAKED_FILENAME flag set, ".xt" is not added. (New in Xdebug 2.3).
 * 
 * @param string|null $traceFile
 * @param  int $options
 * @return string
 */
xdebug_start_trace($traceFile = null,$options = 0 );

/**
 * Stops the current function trace
 * 
 * @return string
 */
xdebug_stop_trace();

/* Code Coverage Analysis */

/**
 * Returns whether code coverage is active
 * Returns whether code coverage has been started.
 *
 * @return bool 
 */
xdebug_code_coverage_started();

/**
 * Returns code coverage information
 * 
 * Returns a structure which contains information about 
 * which lines were executed in your script (including include files). 
 * The information that is collected consists of an two dimensional array 
 * with as primary index the executed filename and as secondary key the line number. 
 * The value in the elements represents 
 * whether the line has been executed or whether it has unreachable lines.
 * The returned values for each line are:
 * - 1: this line was executed
 * - -1: this line was not executed
 * - -2: this line did not have executable code on it
 * Value -1 is only returned when the XDEBUG_CC_UNUSED is enabled 
 * and value -2 is only returned when both XDEBUG_CC_UNUSED 
 * and XDEBUG_CC_DEAD_CODE are enabled when starting Code Coverage Analysis 
 * through xdebug_start_code_coverage().
 * If path and branch checking has been enabled 
 * with the XDEBUG_CC_BRANCH_CHECK flag to xdebug_start_code_coverage() 
 * then the returned format is different. 
 * The lines array is returned in a sub-array element lines, 
 * and separate information is returned for each function in the functions element.
 * The index is the starting opcode, and the fields mean:
 *   op_start
 *     The starting opcode. This is the same number as the array index.
 *   op_end
 *     The last opcode in the branch
 *   line_start
 *     The line number of the op_start opcode.
 *   line_end
 *     The line number of the op_end opcode. This can potentially be a number that is lower 
 *     than line_start due to the way the PHP compiler generates opcodes.
 *   hit
 *     Whether the opcodes in this branch have been executed or not.
 *   out
 *     An array containing the op_start opcodes for branches that can follow this one.
 *   out_hit
 *     Each element matches the same index as in out and indicates whether this branch exit has been reached.
 * Each function also contains a paths element, 
 * which shows all the possible paths through the function, 
 * and whether they have been hit.
 * The index is a normal PHP array index, and the fields mean:
 *   path
 *     An array containing the op_start opcodes indicating the branches that make up this path. 
 *     In the example, 9 features twice because this path (the loop)
 *     has after branch 9 an exit to opcode 5 (the start of the loop), 
 *     and opcode 12 (the next branch after the loop).
 *   hit
 *     Whether this specific path has been followed.
 * The Xdebug source contains a file containing a dump_branch_coverage function, 
 * which you can use the show the information in a more concise way. 
 * @return array
 */
xdebug_get_code_coverage();


/**
 * Set filter
 * 
 * Group:
 * - XDEBUG_FILTER_TRACING
 * - XDEBUG_FILTER_CODE_COVERAGE
 * List Type:
 * - XDEBUG_PATH_WHITELIST
 * - XDEBUG_PATH_BLACKLIST
 * - XDEBUG_NAMESPACE_WHITELIST
 * - XDEBUG_NAMESPACE_BLACKLIST
 * - XDEBUG_FILTER_NONE
 * 
 * @param int $group
 * @param int $listType
 * @param array $configuration
 * @return void
 */
xdebug_set_filter($group, $listType, $configuration);


/**
 * Starts code coverage
 * 
 * Options:
 * - XDEBUG_CC_UNUSED
 * - XDEBUG_CC_DEAD_CODE
 * - XDEBUG_CC_BRANCH_CHECK
 * 
 * @param int $options
 * @return void
 */
xdebug_start_code_coverage($options = 0);

/**
 * 
 * @param bool $cleanUp
 * @return void
 */
xdebug_stop_code_coverage($cleanUp = true);

/* Garbage Collection Statistics */

/**
 * Returns the number of garbage collection runs that have been triggered so far
 * 
 * The function returns the number of times the garbage collection has been triggered in the currently running script.
 * This number is available even if the xdebug.gc_stats_enable INI setting is set to false.
 * 
 * @return int
 */
xdebug_get_gc_run_count();

/**
 * Returns the number of variable roots that have been collected so far
 * 
 * The function returns the number of variable roots 
 * that the garbage collection has collected during all runs of the garbage collector in the current script.
 * This number is available even if the xdebug.gc_stats_enable INI setting is set to false.
 * 
 * @return int
 */
xdebug_get_gc_total_collected_roots();

/**
 * Returns the garbage collection statistics filename
 * 
 * Returns the name of the file which is used to save garbage collection information to, 
 * or false if statistics collection is not active.
 * 
 * @return mixed
 */
xdebug_get_gcstats_filename();

/**
 * Start the collection of garbage collection statistics
 * 
 * Start tracing garbage collection attempts from this point to the file in the gcstatsFile parameter. 
 * If no filename is given, then garbage collection stats file will be placed 
 * in the directory as configured by the xdebug.gc_stats_output_dir setting.
 * In case a file name is given as first parameter, the name is relative to the current working directory. 
 * This current working directory might be different than you expect it to be, 
 * so please use an absolute path in case you specify a file name. 
 * Use the PHP function getcwd() to figure out what the current working directory is.
 * If xdebug.gc_stats_enable is enabled, then filename depends on the xdebug.gc_stats_output_name setting.
 * The full path and filename to which Xdebug collects statistics into is returned from this function. 
 * This will be either the filename you pass in, or the auto generated filename if no filename has been passed in.
 * 
 * @param string|null $gcstatsFile
 * @return mixed
 */
xdebug_start_gcstats($gcstatsFile = null);

/**
 * Stops the current garbage collection statistics collection
 * Stop garbage collection statistics collection and closes the output file.
 * The function returns the filename of the file where the statistics were written to.
 * 
 * @return string
 */
xdebug_stop_gcstats();

/* Profiling PHP Scripts */

/**
 * Returns the profile information filename
 * 
 * Returns the name of the file which is used to save profile information to, 
 * or false if the profiler is not active.
 * 
 * @return 
 */
xdebug_get_profiler_filename();

/* Step Debugging */

/**
 * Emits a breakpoint to the debug client
 * 
 * This function makes the debugger break on the specific line 
 * as if a normal file/line breakpoint was set on this line.
 * 
 * @return bool
 */
xdebug_break();

/**
 * Returns whether a debugging session is active
 * 
 * Returns true if a debugging session 
 * through DBGp is currently active with a client attached; false, if not.
 * 
 * @return bool
 */
xdebug_is_debugger_active();



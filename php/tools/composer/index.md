# Composer

## Component

- unikatowa nazwa dostawcy i pakietu. Preferowane używanie małych liter

- każdy komponent jest przypisany do własnej przestrzeni nazw. Jednakże przestrzeń nazw nie ma związku z nazwami producenta i pakietu.

### Hierarchia plików

- `src/` - katalog zawierający kod źródłowy komponentu (np. pliki klas)
- `tests/` - katalog zawierający testy komponentu.
- `composer.json` - plik konfiguracyjny Composera. Zawiera opis komponentu i informację dla autoloadera, aby mapował przestrzeń nazw komponentu na katalog `src/`
- `README.md` - plik markdown zawierający pomocne informacje o komponencie
- `CONTRIBUTING.md` - plik markdown zawierający informacje o tym, jak inni mogą pomóc w rozwijaniu komponentu
- `LICENSE` - plik tekstowy zawierający licencję komponentu
- `CHANGELOG.md` - plik markdown zawierający listę zmian wprowadzanych w każdej kolejnej wersji komponentu

# Source

[The League of Extraordinary Packages](https://github.com/thephpleague)

# PhpBrew

## Install php5.x.x with `openssl`

[source](https://gist.github.com/marulitua/f8932064ec5bfe6a5be9fadac7c5a141)
[Git HuB Openssl](https://github.com/openssl/openssl)

### OpenSSL

| PHP version | OpenSSL version |

| 5.2.17 | 0.9.8 |

| 5.3.x | 0.9.8 |

| 5.4.x | 0.9.8 |

| 5.5.x | 1.0.1 |

| 5.6.0 > 5.6.26 | 1.0.1 |

| 5.6.28 > 5.6.x | 1.0.2 |

| 7.0.x | 1.0.2 |

| 7.1.x | 1.0.2 |

| 7.2.x | 1.1.0 |

| 7.3.x | 1.1.0 |

```sh
# get form source
wget https://www.openssl.org/source/openssl-1.0.2.tar.gz
# unpack
tar -xzvf openssl-1.0.2.tar.gz
# go to unpaked folder
pushd openssl-1.0.2
# we will install it in /usr/local/openssl so it don't interfere with openssl from apt
# we will compile it as dynamic library
./config -fPIC shared --prefix=/usr/local --openssldir=/usr/local/openssl

# classic make
make

# test it
make test

# install
sudo make install

popd

# build php with shared openssl dependency
export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig

# to install default extensions and openssl, we have to use the following command
phpbrew -d install php-5.6 +default -- --with-openssl=shared
phpbrew use $(phpbrew list | grep php-5.6)
phpbrew -d ext install openssl

# confirmation
php -m | grep openssl
php -r "echo OPENSSL_VERSION_NUMBER;"
```

## Install php7.x.x with `openssl`

```sh
# get form source
#OPENSSL_V=1.0.2
OPENSSL_V=1.1.0 # for >= 7.2.x
wget https://www.openssl.org/source/openssl-$OPENSSL_V.tar.gz
# unpack
tar -xzvf openssl-$OPENSSL_V.tar.gz
# go to unpaked folder
pushd openssl-$OPENSSL_V
# we will install it in /usr/local/openssl so it don't interfere with openssl from apt
# we will compile it as dynamic library
./config -fPIC shared --prefix=/usr/local/openssl-$OPENSSL_V --openssldir=/usr/local/openssl-$OPENSSL_V
# classic make
make

# test it
make test

# install
sudo make install

popd

# build php with shared openssl dependency
export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig

# to install default extensions and openssl, we have to use the following command
phpbrew -d install php-5.6 +default -- --with-openssl=shared
# phpbrew install 5.4 +default +apxs2=/usr/bin/apxs2 +openssl=/usr/local/openssl-1.0.2 -- --with-openssl=shared
# phpbrew install 7.3 +default +apxs2=/usr/bin/apxs2 +mysql + pgsql +sqlite +iconv +gd +curl +session +soap

phpbrew use $(phpbrew list | grep php-5.6)
phpbrew -d ext install openssl

# confirmation
php -m | grep openssl
php -r "echo OPENSSL_VERSION_NUMBER;"
```

## Phpbrew with Apache2.4

```sh
sudo chmod -R oga+rw /usr/lib/apache2/modules
sudo chmod -R oga+rw /etc/apache2
sudo apt-get install apache2-dev
phpbrew install x.x.x +apxs2=/usr/bin/apxs2
sudo chmod -R oga-rw /usr/lib/apache2/modules
sudo chmod -R oga-rw /etc/apache2
```

### Switch php modules in Apache

[Link](https://stackoverflow.com/questions/20584156/tell-apache-to-use-a-specific-php-version-installed-using-phpbrew)

Add content to `~/.phpbrew/bashrc`

```sh
if [[ $(which apache2) ]] ;  then
    APACHE_SO_PATH=/usr/lib/apache2/modules
    APACHE_PHP5_CONFFILE_PATH=/etc/apache2/mods-available/php5.load
    APACHE_PHP7_CONFFILE_PATH=/etc/apache2/mods-available/php7.load

    if [[ -z "$PHPBREW_PHP" ]]
    then
        echo "Currently using system php"
    else
        PHPBREW_PHPVERSION=$(php -r 'echo phpversion();')
        echo "PHP VERSION NUMBER: " $PHPBREW_PHPVERSION
        APACHE_SO_FILE=$APACHE_SO_PATH/libphp$PHPBREW_PHPVERSION.so

        if [[ -f $APACHE_SO_FILE ]]; then
        echo "APACHE_SO_FILE: " $APACHE_SO_FILE
        fi

        if [[ $PHPBREW_PHP =~ ^(php)?[-]?7\. ]]; then
        echo "PHPBREW_PHP: " $PHPBREW_PHP
        fi

        if [[ -f $APACHE_SO_FILE ]]; then
            if [[ $PHPBREW_PHP =~ ^(php)?[-]?7\. ]]; then
                FILECONTENTS="LoadModule php7_module $APACHE_SO_FILE"
                CONFFILE=$APACHE_PHP7_CONFFILE_PATH
                echo $FILECONTENTS > $CONFFILE
                echo "AddType application/x-httpd-php .php" >> $CONFFILE
                echo "Updated $CONFFILE"

                sudo a2dismod php5*
                sudo a2enmod php7
            else
                FILECONTENTS="LoadModule php5_module $APACHE_SO_FILE"
                CONFFILE=$APACHE_PHP5_CONFFILE_PATH
                echo $FILECONTENTS > $CONFFILE
                echo "AddType application/x-httpd-php .php" >> $CONFFILE
                echo "Updated $CONFFILE"
                sudo a2dismod php7*
                sudo a2enmod php5
            fi

            sudo service apache2 restart
            echo "Apache restarted"
        else
            echo $VERSION "is not configured for Apache"
            phpbrew list
        fi
        echo "Currently using $PHPBREW_PHP"
    fi
fi
```

# PHP Unit Testing

PHPUnit is a programmer-oriented testing framework for PHP.
It is an instance of the xUnit architecture for unit testing frameworks.

PHPUnit 9 requires PHP ^7.3 version.

> Nalezy opracować listę czynności, które system powinien wykonywać.

- testy akceptacyjne - najczęściej z poziomu interfejsu użytkownika, symulując różne sposoby interakcji użytkownika z systemem. Jest to rodzaj testów funkcjonalnych.
- testy funkcjonale - operują z zewnątrz
- testy jednostkowe - działają od wewnątrz. Koncentrują się na pojedynczych klasach, z metodami testowymi zgrupowanymi w przypadki testowe. Przypadek testowy(`test case`) oznacza sprawdzenie poprawności pojedynczej klasy poddawanej rozmaitym operacjom - poprawna implementacja metod, odpowiednia reakcja na błędy. Testowanie każdego z komponentów w ścisłej izolacji.

- `fixture` - konfiguracja testu. Test zaczyna się metodą `setUp()` a kończy wywołaniem `tearDown()`
- klasa testująca podlega manipulacji poprzez Reflection API, dlatego metoda testu nie powinna posiadać żadnych parametrów.

## Installation

```sh
# Composer
composer require --dev phpunit/phpunit ^9
./vendor/bin/phpunit --version
./vendor/bin/phpunit tests # for run tests
./vendor/bin/phpunit --testdox tests # for run test with name of a test
```

PHPUnit requires the dom and json extensions, which are normally enabled by default.

PHPUnit also requires the pcre, reflection, and spl extensions. These standard extensions are enabled by default and cannot be disabled without patching PHP’s build system and/or C sources.

The code coverage report feature requires the Xdebug (2.7.0 or later) and tokenizer extensions. Generating XML reports requires the xmlwriter extension.

Please note that it is not recommended to install PHPUnit globally, as /usr/bin/phpunit or /usr/local/bin/phpunit, for instance.

Instead, PHPUnit should be managed as a project-local dependency.

## Writing Tests for PHPUnit

### Test Dependencies

A `producer` is a test method that yields its unit under test as return value.
A `consumer` is a test method that depends on one or more producers and their return values.

`@depends` annotation
`@depends clone`
`@depends shallowClone`

### Data providers

`@dataProvider additionProvider`
`additionProvider` is a method returnings array of arrays or objects or Iterator object. When using a large number of datasets it’s useful to name each one with string key instead of default numeric.

Iterator class must be implemented as `Iterator` type from `Iterator` interface.

# Source

[xUnit](https://en.wikipedia.org/wiki/XUnit)
[PHPUnit Framework](https://phpunit.readthedocs.io/en/9.2/)
[PHPUnit Documentation on GitHub](https://github.com/sebastianbergmann/phpunit-documentation-english/)
[PHPUnit: A Security Risk?](https://thephp.cc/news/2020/02/phpunit-a-security-risk)
[RFC](https://pl.wikipedia.org/wiki/Request_for_Comments)

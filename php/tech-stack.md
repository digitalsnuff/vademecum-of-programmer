# Tech Stack

- GIT
- PHPUnit
- Documentation: PHPDoc, ApiDoc, Swagger
- agile software development -
- automation building
- eXtreme programming (XP) by Kent Beck (Agile Manifesto)
- Testing: Selenium, Jenkins
- UML

## Diagrams

[PlantULM for PHP Storm](https://www.jetbrains.com/help/idea/markdown.html#diagrams)
[PlantUML for VS Code](https://marketplace.visualstudio.com/items?itemName=jebbs.plantuml)

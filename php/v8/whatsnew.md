# PHP 8 -  What \'s new?

Data publikacji: 26 listopada 2020 r. 

## Moduły

- moduły jądra - Część silnika PHP; zawsze aktywne.
- Moduły oficjalne - Element każdej dystrybucji PHP; aktywowane ręcznie przez administratora serwera.
- Repozytorium PECL - Darmowe moduły o otwartym źródle tworzone przez programistów z całego świata, przeznaczone do samodzielnej kompilacji. Począwszy od wydania PHP 5 do PECL przeniesionych zostało wiele wcześniejszych modułów oficjalnych, najczęściej tych niestabilnych lub rzadko używanych.
- Repozytorium PEAR - Zbiór realizujący typowe zadania klas o ujednoliconej budowie.

## Nowe funkcjonalności

- Union types (Unie)

Unia pozwala na definiowanie zestawu typów (dwóch lub więcej), zarówno dla danych wejściowych, jak i zwracanych. Dotychczasz składnia języka nie pozwalała na korzystanie z unii, a problem ten był obchodzony poprzez definiowanie typów w adnotacjach.
Niedopuszczalne jest definiowanie wśród zwracanych typów typu void, ponieważ typ ten już określa, że dana funkcja nie zwróci żadnej wartości. Możemy natomiast bez problemu wśród zwracanych typów zdefiniować null do oznaczenia tzw. wartości nullable.

- Named arguments (nazwane argumenty)

Named arguments pozwalają na przekazanie parametru, bazując na jego nazwie a nie na kolejności.

- Constructor property promotion

- Attributes (Atrybuty)

Po burzliwej batalii dotyczącej tego, w jaki sposób adnotacje powinny być oznaczane, wybrano podwójny znak większości i mniejszości jako otwarcie, i zamknięcie atrybutu.

```php
use Php\Attributes\Deprecated;
 
<<Deprecated("Use bar() instead")>>
function foo() {}
```

- Match expression

Dzięki temu mechanizmowy możemy zwrócić interesującą nas wartość na podstawie parametru wejściowego, bez użycia dodatkowych słów kluczowych tj. break, return. Co więcej, match używa silnego typowania do porównia wartości (analogicznie do użycia znaku porównania ===), a jeżeli żaden z przypadków nie jest spełniony, to domyślnie wyrzucany jest wyjątek UnhandeledMatchError.

- JIT (Just in time compiler)

- nullsafe operator - odpowiednik operatora null coalescing dla metod np. $item->getField()?->getValue();
- static - dodano jako typ zwracanej wartości, wcześniej dostępny był jednie self;
- mixed - nowy typ wartości, który oznacza to samo co znana wartość mixed w komentarzach, a zatem dowolny typ prosty;
- WeakMaps - mechanizm do przechowywania referencji, który jednocześnie pozwala na ich usuwanie przez garbage collector;
- użycie magicznej metody ::class na obiektach - pozwala na uzyskanie tej samej wartości, jaka zwracana jest z funkcji get_class();
- trailing comma w liście parametrów funkcji;
- przechwytywanie wyjątków bez konieczności przypisania do zmiennej;
- nowy interfesj Stringable - automatycznie przypisany do wszystkich klas, które implementują metodę __toString();
- walidacja metod abstrakcyjnych pochodzących z trait'ów

## Funkcje

- str_contains() - Z pewnością nie raz przyszło wam sprawdzać, czy dany string zawiera w sobie jakąś zdefiniowaną subfrazę z użyciem funkcji strpos i sprawdzeniem, czy zwracana wartość jest różna od FALSE. Funkcja str_contains potrafi wykonać to sprawdzenie za nas w tle
- str_starts_with() i str_ends_with() - Obie funkcje zajmujące się dokładnie tym, na co nazwa wskazuje, a zatem sprawdzeniem, czy dany string zaczyna lub kończy się na określoną wartość
- get_debug_type() - To funkcja, która jest rozwinięciem funkcji gettype() z tym, że zwraca ona dokładny typ, taki jaki definujemy w kodzie, a zatem int zamiast integer, float zamiast double, czy \Foo\Bar zamiast object

## Ogólne

- zwracanie wyjątków TypeError i ValueError dla funkcji wbudowanych
- Poprawiono mechanizmy porównywania zmiennych różnych typów, kolejności konkatenacji, czy weryfikacji typów przy operacjach arytmetycznych i bitowych
- Usunięto możliwość wywoływania statycznie metod, które nie są statyczne
- Ponadto usunięto takie funkcje jak: create_function(), each(), natomiast zmieniono mechanizmy funkcjonowania m.in. funkcji array_key_exists() czy define()
- Ważne zmiany zaszły także w domyślnych mechanizmach dotyczących kontroli i wyświetlania błędów. Domyślną wartością dla raportowania błędów będzie E_ALL, a operator @ nie będzie już więcej maskował błędów krytycznych


# Source

- https://www.droptica.pl/blog/php-8-co-nowego/

# PHP Magic Methods

All magic methods MUST be declared as public

- \_\_construct() and \_\_destruct()

- \_\_sleep() and \_\_wakeup()

- \_\_serialize() and \_\_unserialize()

This feature is available since PHP 7.4.0.

If both \_\_serialize() and \_\_sleep() are defined in the same object, only \_\_serialize() will be called. \_\_sleep() will be ignored. If the object implements the Serializable interface, the interface's serialize() method will be ignored and \_\_serialize() used instead.

- \_\_toString()

- `__invoke()`

As of 5.3.0.

- `__set_state()`

As of 5.1.0

- \_\_debugInfo()

- `__clone()`

## Property overloading

`__set()` is run when writing data to inaccessible (protected or private) or non-existing properties.

`__get()` is utilized for reading data from inaccessible (protected or private) or non-existing properties. As of 5.0.0

`__isset()` is triggered by calling `isset()` or `empty()` on inaccessible (protected or private) or non-existing properties. As of 5.1.0

`__unset()` is invoked when `unset()` is used on inaccessible (protected or private) or non-existing properties. As of 5.1.0

These magic methods will not be triggered in static context. Therefore these methods should not be declared static.

## Method overloading

`__call()` is triggered when invoking inaccessible methods in an object context.

`__callStatic()` is triggered when invoking inaccessible methods in a static context. As of 5.3.0

# Source

[PHP Magic Methods](https://www.php.net/manual/en/language.oop5.magic.php)

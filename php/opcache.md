# OPCache

OPcache improves PHP performance by storing precompiled script bytecode in shared memory, thereby removing the need for PHP to load and parse scripts on each request.

This extension is bundled with PHP 5.5.0 and later.

If you want to use `OPcache` with `Xdebug`, you must load OPcache before Xdebug.

You can use the `zend_extension` configuration directive to load the OPcache extension into PHP.

## Recommended php.ini settings

The following settings are generally recommended as providing good performance:

```ini
opcache.memory_consumption=128
opcache.interned_strings_buffer=8
opcache.max_accelerated_files=4000
opcache.revalidate_freq=60
opcache.fast_shutdown=1
opcache.enable_cli=1
```

You may also want to consider disabling opcache.save_comments and enabling opcache.enable_file_override, however note that you will have to test your code before using these in production as they are known to break some frameworks and applications, particularly in cases where documentation comment annotations are used.

On Windows, opcache.file_cache_fallback should be enabled, and opcache.file_cache should be set to an already existing and writable directory.

## A full list of configuration directives supported by OPcache

<table class="doctable table">
   <caption><strong>OPcache configuration options</strong></caption>
    <thead>
     <tr>
      <th>Name</th>
      <th>Default</th>
      <th>Changeable</th>
      <th>Changelog</th>
     </tr>
    </thead>
    <tbody class="tbody">
     <tr>
      <td><a href="opcache.configuration.php#ini.opcache.enable" class="link">opcache.enable</a></td>
      <td>"1"</td>
      <td>PHP_INI_ALL</td>
      <td class="empty">&nbsp;</td>
     </tr>
     <tr>
      <td><a href="opcache.configuration.php#ini.opcache.enable-cli" class="link">opcache.enable_cli</a></td>
      <td>"0"</td>
      <td>PHP_INI_SYSTEM</td>
      <td>Between PHP 7.1.2 and 7.1.6 inclusive, the default was "1"</td>
     </tr>
     <tr>
      <td><a href="opcache.configuration.php#ini.opcache.memory-consumption" class="link">opcache.memory_consumption</a></td>
      <td>"128"</td>
      <td>PHP_INI_SYSTEM</td>
      <td>Before PHP 7.0.0 the default was "64"</td>
     </tr>
     <tr>
      <td><a href="opcache.configuration.php#ini.opcache.interned-strings-buffer" class="link">opcache.interned_strings_buffer</a></td>
      <td>"8"</td>
      <td>PHP_INI_SYSTEM</td>
      <td>Before PHP 7.0.0 the default was "4"</td>
     </tr>
     <tr>
      <td><a href="opcache.configuration.php#ini.opcache.max-accelerated-files" class="link">opcache.max_accelerated_files</a></td>
      <td>"10000"</td>
      <td>PHP_INI_SYSTEM</td>
      <td>Before PHP 7.0.0 the default was "2000"</td>
     </tr>
     <tr>
      <td><a href="opcache.configuration.php#ini.opcache.max-wasted-percentage" class="link">opcache.max_wasted_percentage</a></td>
      <td>"5"</td>
      <td>PHP_INI_SYSTEM</td>
      <td class="empty">&nbsp;</td>
     </tr>
     <tr>
      <td><a href="opcache.configuration.php#ini.opcache.use-cwd" class="link">opcache.use_cwd</a></td>
      <td>"1"</td>
      <td>PHP_INI_SYSTEM</td>
      <td class="empty">&nbsp;</td>
     </tr>
     <tr>
      <td><a href="opcache.configuration.php#ini.opcache.validate-timestamps" class="link">opcache.validate_timestamps</a></td>
      <td>"1"</td>
      <td>PHP_INI_ALL</td>
      <td class="empty">&nbsp;</td>
     </tr>
     <tr>
      <td><a href="opcache.configuration.php#ini.opcache.revalidate-freq" class="link">opcache.revalidate_freq</a></td>
      <td>"2"</td>
      <td>PHP_INI_ALL</td>
      <td class="empty">&nbsp;</td>
     </tr>
     <tr>
      <td><a href="opcache.configuration.php#ini.opcache.revalidate-path" class="link">opcache.revalidate_path</a></td>
      <td>"0"</td>
      <td>PHP_INI_ALL</td>
      <td class="empty">&nbsp;</td>
     </tr>
     <tr>
      <td><a href="opcache.configuration.php#ini.opcache.save-comments" class="link">opcache.save_comments</a></td>
      <td>"1"</td>
      <td>PHP_INI_SYSTEM</td>
      <td class="empty">&nbsp;</td>
     </tr>
     <tr style="color: red;">
      <td><a href="opcache.configuration.php#ini.opcache.load-comments" class="link">opcache.load_comments</a></td>
      <td>"1"</td>
      <td>PHP_INI_ALL</td>
      <td>Removed in PHP 7.0.0.</td>
     </tr>
     <tr style="color: red;">
      <td><a href="opcache.configuration.php#ini.opcache.fast-shutdown" class="link">opcache.fast_shutdown</a></td>
      <td>"0"</td>
      <td>PHP_INI_SYSTEM</td>
      <td>Removed in PHP 7.2.0</td>
     </tr>
     <tr>
      <td><a href="opcache.configuration.php#ini.opcache.enable-file-override" class="link">opcache.enable_file_override</a></td>
      <td>"0"</td>
      <td>PHP_INI_SYSTEM</td>
      <td class="empty">&nbsp;</td>
     </tr>
     <tr>
      <td><a href="opcache.configuration.php#ini.opcache.optimization-level" class="link">opcache.optimization_level</a></td>
      <td>"0x7FFEBFFF"</td>
      <td>PHP_INI_SYSTEM</td>
      <td>Changed from 0x7FFFBFFF in PHP 7.3.0, and from 0xFFFFFFFF in PHP 5.6.18</td>
     </tr>
     <tr style="color: red;">
      <td><a href="opcache.configuration.php#ini.opcache.inherited-hack" class="link">opcache.inherited_hack</a></td>
      <td>"1"</td>
      <td>PHP_INI_SYSTEM</td>
      <td>Removed in PHP 7.3.0</td>
     </tr>
     <tr>
      <td><a href="opcache.configuration.php#ini.opcache.dups-fix" class="link">opcache.dups_fix</a></td>
      <td>"0"</td>
      <td>PHP_INI_ALL</td>
      <td class="empty">&nbsp;</td>
     </tr>
     <tr>
      <td><a href="opcache.configuration.php#ini.opcache.blacklist-filename" class="link">opcache.blacklist_filename</a></td>
      <td>""</td>
      <td>PHP_INI_SYSTEM</td>
      <td class="empty">&nbsp;</td>
     </tr>
     <tr>
      <td><a href="opcache.configuration.php#ini.opcache.max-file-size" class="link">opcache.max_file_size</a></td>
      <td>"0"</td>
      <td>PHP_INI_SYSTEM</td>
      <td class="empty">&nbsp;</td>
     </tr>
     <tr>
      <td><a href="opcache.configuration.php#ini.opcache.consistency-checks" class="link">opcache.consistency_checks</a></td>
      <td>"0"</td>
      <td>PHP_INI_ALL</td>
      <td class="empty">&nbsp;</td>
     </tr>
     <tr>
      <td><a href="opcache.configuration.php#ini.opcache.force-restart-timeout" class="link">opcache.force_restart_timeout</a></td>
      <td>"180"</td>
      <td>PHP_INI_SYSTEM</td>
      <td class="empty">&nbsp;</td>
     </tr>
     <tr>
      <td><a href="opcache.configuration.php#ini.opcache.error-log" class="link">opcache.error_log</a></td>
      <td>""</td>
      <td>PHP_INI_SYSTEM</td>
      <td class="empty">&nbsp;</td>
     </tr>
     <tr>
      <td><a href="opcache.configuration.php#ini.opcache.log-verbosity-level" class="link">opcache.log_verbosity_level</a></td>
      <td>"1"</td>
      <td>PHP_INI_SYSTEM</td>
      <td class="empty">&nbsp;</td>
     </tr>
     <tr>
      <td><a href="opcache.configuration.php#ini.opcache.preferred-memory-model" class="link">opcache.preferred_memory_model</a></td>
      <td>""</td>
      <td>PHP_INI_SYSTEM</td>
      <td class="empty">&nbsp;</td>
     </tr>
     <tr>
      <td><a href="opcache.configuration.php#ini.opcache.protect-memory" class="link">opcache.protect_memory</a></td>
      <td>"0"</td>
      <td>PHP_INI_SYSTEM</td>
      <td class="empty">&nbsp;</td>
     </tr>
     <tr>
      <td><a href="opcache.configuration.php#ini.opcache.mmap-base" class="link">opcache.mmap_base</a></td>
      <td><strong><code>NULL</code></strong></td>
      <td>PHP_INI_SYSTEM</td>
      <td class="empty">&nbsp;</td>
     </tr>
     <tr>
      <td><a href="opcache.configuration.php#ini.opcache.restrict-api" class="link">opcache.restrict_api</a></td>
      <td>""</td>
      <td>PHP_INI_SYSTEM</td>
      <td class="empty">&nbsp;</td>
     </tr>
     <tr>
      <td><a href="opcache.configuration.php#ini.opcache.file_update_protection" class="link">opcache.file_update_protection</a></td>
      <td>"2"</td>
      <td>PHP_INI_ALL</td>
      <td class="empty">&nbsp;</td>
     </tr>
     <tr>
      <td><a href="opcache.configuration.php#ini.opcache.huge_code_pages" class="link">opcache.huge_code_pages</a></td>
      <td>"0"</td>
      <td>PHP_INI_SYSTEM</td>
      <td class="empty">&nbsp;</td>
     </tr>
    <tr>
      <td><a href="opcache.configuration.php#ini.opcache.lockfile_path" class="link">opcache.lockfile_path</a></td>
      <td>"/tmp"</td>
      <td>PHP_INI_SYSTEM</td>
      <td class="empty">&nbsp;</td>
     </tr>
     <tr style="color: lightblue;">
      <td><a href="opcache.configuration.php#ini.opcache.opt_debug_level" class="link">opcache.opt_debug_level</a></td>
      <td>"0"</td>
      <td>PHP_INI_SYSTEM</td>
      <td>Available as of PHP 7.1.0</td>
     </tr>
     <tr style="color: lightblue;">
      <td><a href="opcache.configuration.php#ini.opcache.file-cache" class="link">opcache.file_cache</a></td>
      <td>NULL</td>
      <td>PHP_INI_SYSTEM</td>
      <td>Available as of PHP 7.0.0</td>
     </tr>
     <tr style="color: lightblue;">
      <td><a href="opcache.configuration.php#ini.opcache.file-cache-only" class="link">opcache.file_cache_only</a></td>
      <td>"0"</td>
      <td>PHP_INI_SYSTEM</td>
      <td>Available as of PHP 7.0.0</td>
     </tr>
     <tr style="color: lightblue;">
      <td><a href="opcache.configuration.php#ini.opcache.file-cache-consistency-checks" class="link">opcache.file_cache_consistency_checks</a></td>
      <td>"1"</td>
      <td>PHP_INI_SYSTEM</td>
      <td>Available as of PHP 7.0.0</td>
     </tr>
     <tr style="color: orange;">
      <td><a href="opcache.configuration.php#ini.opcache.file-cache-fallback" class="link">opcache.file_cache_fallback</a></td>
      <td>"1"</td>
      <td>PHP_INI_SYSTEM</td>
      <td>Windows only. Available as of PHP 7.0.0</td>
     </tr>
     <tr style="color: lightblue;">
      <td><a href="opcache.configuration.php#ini.opcache.validate-permission" class="link">opcache.validate_permission</a></td>
      <td>"0"</td>
      <td>PHP_INI_SYSTEM</td>
      <td>Available as of PHP 7.0.14</td>
     </tr>
     <tr style="color: lightblue;">
      <td><a href="opcache.configuration.php#ini.opcache.validate-root" class="link">opcache.validate_root</a></td>
      <td>"0"</td>
      <td>PHP_INI_SYSTEM</td>
      <td>Available as of PHP 7.0.14</td>
     </tr>
     <tr style="color: lightblue;">
      <td><a href="opcache.configuration.php#ini.opcache.preload" class="link">opcache.preload</a></td>
      <td>""</td>
      <td>PHP_INI_SYSTEM</td>
      <td>Available as of PHP 7.4.0</td>
     </tr>
     <tr style="color: lightblue;">
      <td><a href="opcache.configuration.php#ini.opcache.preload-user" class="link">opcache.preload_user</a></td>
      <td>""</td>
      <td>PHP_INI_SYSTEM</td>
      <td>Available as of PHP 7.4.0</td>
     </tr>
     <tr style="color: orange;">
      <td><a href="opcache.configuration.php#ini.opcache.cache-id" class="link">opcache.cache_id</a></td>
      <td>"1"</td>
      <td>PHP_INI_SYSTEM</td>
      <td>Windows only. Available as of PHP 7.4.0</td>
     </tr>
    </tbody>
  </table>

```ini
; boolean
; Enables the opcode cache. When disabled, code is not optimised or cached.
; The setting `opcache.enable` can not be enabled at runtime through ini_set(),
; it can only be disabled.
; Trying to enable it at in a script will generate a warning.
opcache.enable

; boolean
; Enables the opcode cache for the CLI version of PHP.
opcache.enable_cli

; integer
; The size of the shared memory storage used by OPcache, in megabytes.
; The minimum permissible value is "8", which is enforced if a smaller value is set.
opcache.memory_consumption

; integer
; The amount of memory used to store interned strings, in megabytes.
; This configuration directive is ignored in PHP < 5.3.0.
opcache.interned_strings_buffer

; integer
; The maximum number of keys (and therefore scripts) in the OPcache hash table.
; The actual value used will be the first number in the set of prime numbers
; { 223, 463, 983, 1979, 3907, 7963, 16229, 32531, 65407, 130987, 262237, 524521, 1048793 }
; that is greater than or equal to the configured value.
; The minimum value is 200.
; The maximum value is 100000 in PHP < 5.5.6, and 1000000 in later versions.
; Values outside of this range are clamped to the permissible range.
opcache.max_accelerated_files

; integer
; The maximum percentage of wasted memory that is allowed before a restart is scheduled.
; The maximum permissible value is "50", which is enforced if a larger value is set.
opcache.max_wasted_percentage

; boolean
; If enabled, OPcache appends the current working directory to the script key,
; thereby eliminating possible collisions between files with the same base name.
; Disabling this directive improves performance, but may break existing applications.
opcache.use_cwd

; boolean
; If enabled, OPcache will check for updated scripts every `opcache.revalidate_freq` seconds.
; When this directive is disabled, you must reset OPcache manually
; via `opcache_reset()`, `opcache_invalidate()` or by restarting the Web server
; for changes to the filesystem to take effect.
opcache.validate_timestamps

; integer
; How often to check script timestamps for updates, in seconds.
; 0 will result in OPcache checking for updates on every request.
; This configuration directive is ignored if `opcache.validate_timestamps` is disabled.
opcache.revalidate_freq

; boolean
; If disabled, existing cached files using the same `include_path` will be reused.
; Thus, if a file with the same name is elsewhere in the `include_path`, it won't be found.
opcache.revalidate_path

; boolean
; If disabled, all documentation comments will be discarded from the opcode cache
; to reduce the size of the optimised code.
; Disabling this configuration directive may break applications and frameworks
; that rely on comment parsing for annotations,
; including Doctrine, Zend Framework 2 and PHPUnit.
opcache.save_comments

; boolean
; If disabled, documentation comments won't be loaded from the opcode cache even if they exist. ; This can be used with `opcache.save_comments` to only load comments for applications
; that require them.
opcache.load_comments

; removed
; boolean
; If enabled, a fast shutdown sequence is used that doesn't free each allocated block,
; but relies on the Zend Engine memory manager to deallocate
; the entire set of request variables en masse.
;
; This directive has been removed in PHP 7.2.0.
; A variant of the fast shutdown sequence has been integrated into PHP
; and will be automatically used if possible.
;; opcache.fast_shutdown

; boolean
; When enabled, the opcode cache will be checked for
; whether a file has already been cached when
; `file_exists()`, `is_file()` and `is_readable()` are called.
; This may increase performance in applications
; that check the existence and readability of PHP scripts,
; but risks returning stale data if `opcache.validate_timestamps` is disabled.
opcache.enable_file_override

; integer
; A bitmask that controls which optimisation passes are executed.
opcache.optimization_level

; ignored
; boolean
; In PHP < 5.3, OPcache stores the places where `DECLARE_CLASS` opcodes used inheritance;
; when the file is loaded, OPcache then tries to bind the inherited classes
; using the current environment.
; The problem is that while the `DECLARE_CLASS` opcode may not be needed
; for the current script, if the script requires
; that the opcode be defined, it may not run.
;
; This configuration directive is ignored in PHP 5.3 and later.
;; opcache.inherited_hack

; boolean
; This hack should only be enabled to work around "Cannot redeclare class" errors.
opcache.dups_fix

; string
; The location of the OPcache blacklist file.
; A blacklist file is a text file containing the names of files
; that should not be accelerated, one per line.
; Wildcards are allowed, and prefixes can also be provided.
; Lines starting with a semi-colon are ignored as comments.
;
; A simple blacklist file might look as follows:
;
; Matches a specific file.
; /var/www/broken.php
; A prefix that matches all files starting with x.
; /var/www/x
; A wildcard match.
; /var/www/*-broken.php
opcache.blacklist_filename

; integer
; The maximum file size that will be cached, in bytes.
; If this is 0, all files will be cached.
opcache.max_file_size

; integer
; If non-zero, OPcache will verify the cache checksum every N requests,
; where N is the value of this configuration directive.
; This should only be enabled when debugging, as it will impair performance.
opcache.consistency_checks

; integer
; The length of time to wait for a scheduled restart to begin
; if the cache isn't active, in seconds.
; If the timeout is hit, then OPcache assumes that something is wrong
; and will kill the processes holding locks on the cache to permit a restart.
;
; If `opcache.log_verbosity_level` is set to 2 or above,
; a warning will be recorded in the error log when this occurs.
opcache.force_restart_timeout

; string
; The error log for OPcache errors.
; An empty string is treated the same as stderr,
; and will result in logs being sent to standard error
; (which will be the Web server error log in most cases).
opcache.error_log

; integer
; The log verbosity level.
; By default, only fatal errors (level 0) and errors (level 1) are logged.
; Other levels available are warnings (level 2),
; information messages (level 3)
; and debug messages (level 4).
opcache.log_verbosity_level

; string
; The preferred memory model for OPcache to use.
; If left empty, OPcache will choose the most appropriate model,
; which is the correct behaviour in virtually all cases.
;
; Possible values include mmap, shm, posix and win32.
opcache.preferred_memory_model

; boolean
; Protects shared memory from unexpected writes while executing scripts. This is useful for internal debugging only.
opcache.protect_memory

; string
; The base used for shared memory segments on Windows.
; All PHP processes have to map shared memory into the same address space.
; Using this directive allows "Unable to reattach to base address" errors to be fixed.
opcache.mmap_base

; string
; Allows calling OPcache API functions only from PHP scripts
; which path is started from specified string.
; The default "" means no restriction.
opcache.restrict_api

; string
; Prevents caching files that are less than this number of seconds old.
; It protects from caching of incompletely updated files.
; In case all file updates on your site are atomic,
; you may increase performance by setting it to "0".
opcache.file_update_protection

; boolean
; Enables or disables copying of PHP code (text segment) into HUGE PAGES.
; This should improve performance, but requires appropriate OS configuration.
; Available on Linux as of PHP 7.0.0, and on FreeBSD as of PHP 7.4.0.
opcache.huge_code_pages

; string
; Absolute path used to store shared lockfiles (for *nix only)
opcache.lockfile_path

; string
; Produces opcode dumps for debugging different stages of optimizations.
; 0x10000 will output opcodes as the compiler produced them
; before any optimization occurs while 0x20000 will output optimized codes.
opcache.opt_debug_level

; string
; Enables and sets the second level cache directory.
; It should improve performance when SHM memory is full, at server restart or SHM reset.
; The default "" disables file based caching.
opcache.file_cache

; boolean
; Enables or disables opcode caching in shared memory.
opcache.file_cache_only

; boolean
; Enables or disables checksum validation when script loaded from file cache.
opcache.file_cache_consistency_checks

; boolean
; Implies `opcache.file_cache_only=1` for a certain process
; that failed to reattach to the shared memory (for Windows only).
; Explicitly enabled file cache is required.
;
; Caution: Disabling this configuration option may prevent processes to start,
; and is therefore discouraged.
opcache.file_cache_fallback

; boolean
; Validates the cached file permissions against the current user.
opcache.validate_permission

; boolean
; Prevents name collisions in chroot'ed environments.
; This should be enabled in all chroot'ed environments
; to prevent access to files outside the chroot.
opcache.validate_root

; string
; Specifies a PHP script that is going to be compiled and executed at server start-up,
; and which may preload other files, either by includeing them
; or by using the `opcache_compile_file()` function.
; All the entities (e.g. functions and classes) defined in these files
; will be available to requests out of the box, until the server is shut down.
opcache.preload

; string
; Preloading code as root is not allowed for security reasons.
; This directive facilitates to let the preloading to be run as another user.
opcache.preload_user

; string
; On Windows, all processes running the same PHP SAPI under the same user account
; having the same cache ID share a single OPcache instance.
; The value of the cache ID can be freely chosen.
opcache.cache_id
```

## Functions

```php
/**
 * Compiles and caches a PHP script without executing it
 *
 * This function compiles a PHP script and adds it to the opcode cache without executing it.
 * This can be used to prime the cache after a Web server restart by pre-caching files that will be included in later requests.
 * If file cannot be loaded or compiled, an error of level E_WARNING is generated. You may use @ to suppress this warning.
 *
 * @param string $file
 * @return boolean
 */
opcache_compile_file($file);
```

```php
/**
 *  Get configuration information about the cache
 *
 * Returns an array of information, including ini, blacklist and version
 * If opcache.restrict_api is in use and the current path is in violation of the rule, an E_WARNING will be raised; no status information will be returned.
 *
 * @return void
*/
opcache_get_configuration();
```

```php
/**
 * Get status information about the cache
 *
 * Returns an array of information, optionally containing script specific state information, or FALSE on failure.
 *
 * @param bool $get_scripts
 * @return array
*/
opcache_get_status($get_scripts = true);
```

```php
/**
 * Invalidates a cached script
 *
 * This function invalidates a particular script from the opcode cache. If force is unset or FALSE, the script will only be invalidated if the modification time of the script is newer than the cached opcodes.
 * Returns TRUE if the opcode cache for script was invalidated or if there was nothing to invalidate, or FALSE if the opcode cache is disabled.
 *
 * @param string $script
 * @param bool $force
 * @return bool
*/
opcache_invalidate($script, $force = false);
```

```php

/**
 * Tells whether a script is cached in OPCache
 *
 * This function checks if a PHP script has been cached in OPCache. This can be used to more easily detect the "warming" of the cache for a particular script.
 * Returns TRUE if file is cached in OPCache, FALSE otherwise.
 *
 * @param string $file
 * @return bool
*/
opcache_is_script_cached($file);
```

```php
/**
 * Resets the contents of the opcode cache
 *
 * This function resets the entire opcode cache. After calling opcache_reset(), all scripts will be reloaded and reparsed the next time they are hit.
 * Returns TRUE if the opcode cache was reset, or FALSE if the opcode cache is disabled.
 *
 * @return bool
 */

opcache_reset();
```

# Source

[OPcache](https://www.php.net/manual/en/book.opcache.php)
[A full list of configuration directives supported by OPcache](https://www.php.net/manual/en/opcache.configuration.php)

# Project Concepts

## KISS

> Keep It Simple Stupid

## DRY

> Don't Repeat Yourself

## YAGNI

> You Ain't Gona Need It

## TDA

> Tell Don't Ask

## SCA

> Separation Of Concepts

# Links

[Principles of object programming](https://devcave.pl/notatnik-juniora/zasady-projektowania-kodu)

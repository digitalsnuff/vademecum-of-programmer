# Symfony 5 - Fast Track

## Good working environment

`PostgreSQL` is going to be our choice for the database engine.
`RabbitMQ` is the winner for queues.

`Docker` for services and `Docker Compose`.

Latest version of PHP.

PHP extensions:

- intl,
- pdo_pgsql,
- xsl,
- amqp,
- gd,
- openssl,
- sodium
- curl
- redis
- php-fpm
- php-cgi

Symfony CLI

Install the Symfony CLI and move it under your \$PATH. Create a
SymfonyConnect account if you don’t have one already and log in via
symfony login.
To use HTTPS locally, we also need to install a CA to enable TLS support.

```sh
symfony server:ca:install
```

Check requirements

```sh
symfony book:check-requirements

```

If you want to get fancy, you can also run the Symfony proxy. It is optional
but it allows you to get a local domain name ending with .wip for your
project.

When executing a command in a terminal, we will almost always prefix it
with symfony like in `symfony composer` instead of just `composer`, or `symfony console` instead of `./bin/console`.

The main reason is that the Symfony CLI automatically sets some
environment variables based on the services running on your machine
via Docker. These environment variables are available for HTTP requests
because the local web server injects them automatically. So, using symfony
on the CLI ensures that you have the same behavior across the board.

Coding
an application on your personal computer while reading a book about
Symfony is even better.

https://github.com/sebastianbergmann/phploc

symfony/flex

Automation is a key feature of Symfony.

The config/ directory is made of a set of default and sensible
configuration files. One file per package. You will barely change them,
trusting the defaults is almost always a good idea.

Being able to see
the little “under construction” image on a production server would be a
great step forward. And you know the motto: deploy early and often.

- The Symfony Recipes Server, where you can find all the available
  recipes for your Symfony applications;
- The repositories for the official Symfony recipes and for the recipes
  contributed by the community, where you can submit your own
  recipes;
- The Symfony Local Web Server;
- The SymfonyCloud documentation.

Teaching is about repeating the same thing again and again.

Git Strategy:

- don’t forget to commit your changes
- Deploying to Production Continuously

Besides “regular”
Composer packages, we will work with two “special” kinds of packages:

- Symfony Components: Packages that implement core features and low level abstractions that most applications need (routing, console, HTTP client, mailer, cache, ...);
- Symfony Bundles: Packages that add high-level features or provide integrations with third-party libraries (bundles are mostly contributed by the community).

To begin with, let’s add the Symfony Profiler, a time saver when you need to find the root cause of a problem:

```sh
symfony composer req profiler --dev
```

`profiler` is an alias for the `symfony/profiler-pack` package.

`Aliases` are not a Composer feature, but a concept provided by Symfony to make your life easier (`orm`,`api`,`cache`).  
These aliases are automatically resolved to
one or more regular Composer packages.

https://github.com/symfony/recipes

The .env file is committed to the repository and describes the default
values from production. You can override these values by creating a
.env.local file. This file should not be committed and that’s why the
.gitignore file is already ignoring it.
Never store secret or sensitive values in these files. We will see how to
manage secrets in another step.

## Logger

```sh
# Logger
symfony composer req logger
# Degugger in development
symfony composer req debug --dev
```

## Debugger

Logs are also quite useful in debugging sessions. Symfony has a convenient command to tail all the logs (from the web server, PHP, and
your application):

```sh
symfony server:log
```

Symfony dump() function

- SymfonyCasts Environments and Config Files tutorial;
- SymfonyCasts Environment Variables tutorial;
- SymfonyCasts Web Debug Toolbar and Profiler tutorial;
- Managing multiple .env files in Symfony applications.

A route is the link between the request path and a
PHP callable, a function that creates the HTTP response for that request. These callables are called “controllers”.

Symfony Maker Bundle

```sh
symfony composer req maker --dev
symfony console list make
```

Before creating the first controller of the project, we need to decide on the
configuration formats we want to use. Symfony supports YAML, XML,
PHP, and annotations out of the box.

For configuration related to packages, YAML is the best choice. This is
the format used in the config/ directory. Often, when you install a new
package, that package’s recipe will add a new file ending in .yaml to that
directory.

For configuration related to PHP code, annotations are a better choice as
they are defined next to the code.

To manage annotations, we need to add another dependency:

```sh
symfony composer req annotations
```

- The Symfony Routing system;
- SymfonyCasts Routes, Controllers & Pages tutorial;
- Annotations in PHP;
- The HttpFoundation component;
- XSS (Cross-Site Scripting) security attacks;
- The Symfony Routing Cheat Sheet.

## PostgreSQL

If you run psql via the Symfony CLI, you don’t need to remember anything.
The Symfony CLI automatically detects the Docker services running for the project and exposes the environment variables that psql needs to connect to the database.
Thanks to these conventions, accessing the database via symfony run is much easier:

```sh
symfony run psql
# or without psql
docker exec -it guestbook_database_1 psql -U main -W main
# connect to SymfonyCloud via ssh
symfony tunnel:open --expose-env-vars
```

```sh
symfony var:export
# symfony tunnel:open --expose-env-vars
```

- SymfonyCloud services;
- SymfonyCloud tunnel;
- PostgreSQL documentation;
- docker-compose commands.

## Database

```sh
symfony composer req orm
```

This command installs a few dependencies: Doctrine DBAL (a database
abstraction layer), Doctrine ORM (a library to manipulate our database
content using PHP objects), and Doctrine Migrations.

```sh
# tworzymy nową encję
symfony console make:entity Conference
# tworzymy migrację ze zmian
php bin/console make:migration
# realizujemy zmiany w bazie danych
php bin/console doctrine:migrations:migrate
# żeby zrobić zmiany na encji należy odpalić ponownie
symfony console make:entity Conference
```

A migration is a class that describes the changes needed to update a
database schema from its current state to the new one defined by the
entity annotations. As the database is empty for now, the migration
should consist of two table creations.

## Updating the Production Database

The steps needed to migrate the production database are the same as the ones you are already familiar with: commit the changes and deploy.

- Databases and Doctrine ORM in Symfony applications;
- SymfonyCasts Doctrine tutorial;
- Working with Doctrine Associations/Relations;
- DoctrineMigrationsBundle docs.

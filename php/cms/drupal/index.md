# Drupal

Drupal is a content management system used to build websites for small businesses, e commerce, enterprise systems, and many more.  
Build highly scalable and enterprise-ready websites.

# Source

- Nick Abbott, Richard Jones, Matt Glaman, Chaz Chumley, _Drupal 8: Enterprise Web Development_, ISBN 978-1-78728-319-0 Packt Publishing 2016

[Vim Plugin For Drupal](https://www.drupal.org/project/vimrc)
[Development Tools](https://www.drupal.org/docs/develop/development-tools)

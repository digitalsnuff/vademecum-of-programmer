# Example 1 - Docker Drupal

1. Create a new directory in your home folder called `my_drupal` and cd into it:

```sh
mkdir ~/my_drupal/
cd ~/my_drupal
```

2. Create a file named `docker-compose.yml` in this folder and add the following contents. Set your own password for the `POSTGRES_PASSWORD` option.

```Dockerfile
version: '3.3'

services:
  drupal:
    image: drupal:latest
    ports:
      - 8085:80
    volumes:
      - drupal_modules:/var/www/html/modules
      - drupal_profiles:/var/www/html/profiles
      - drupal_themes:/var/www/html/themes
      - drupal_sites:/var/www/html/sites
    restart: always
  postgres:
    image: postgres:11
    environment:
      POSTGRES_PASSWORD: [pass]
    volumes:
      - db_data:/var/lib/postgresql/data
    restart: always

volumes:
  drupal_modules:
  drupal_profiles:
  drupal_themes:
  drupal_sites:
  db_data:
```

3. From the `my_drupal` directory, start your Docker containers:

```sh
$ docker-compose up -d
```

4. Go to http://localhost:8085/ address go though installation step. PostgreSQL host's name is the same as service name.

## Usage and Maintenance

`restart: always` option in `docker-compose.yml` tells Docker Compose to automatically start your services when the server boots.

### Stop Drupal application

This will stop the running Drupal and PostgreSQL container, but will not remove them.

```sh
cd ~/my_drupal/
docker-compose stop
```

### Restart Drupal application

```sh
cd ~/my_drupal/
docker-compose start
```

### Stop and remove containers, networks and images created by the `docker-compose.yml` file

```sh
cd ~/my_drupal/
docker-compose down
```

> When a Docker container is taken down, it is also deleted; this is how Docker is designed to work. However, your Drupal files and data will be preserved, as the `docker-compose.yml` file was configured to create persistent volumes for that data.
>
> If you want to remove this data and start over with your Drupal site, you can add the `--volumes` flag to the previous command. **This will permanently delete the Drupal customizations you’ve made so far**.

```sh
docker-compose down --volumes
```

## Update Drupal

the `docker-compose.yml` specifies the `latest` version of the Drupal image, so it's easy to update your Drupal version:

```sh
docker-compose down
docker-compose pull && docker-compose up -d
```

# Links

[Install Drupal with Docker Compose](https://www.linode.com/docs/quick-answers/linux/drupal-with-docker-compose/)

# Zend 1 Hyperlinks

- internal addresses
- external addresses
- map
- helper function `url()`

## Example of a map some addresses in `application.ini`:

```sh
resources.router.routes.[route-name].route = '/any/external/address.html'
resources.router.routes.[route-name].defaults.controller = "controller-name"
resources.router.routes.[route-name].defaults.action = "action-name"
```

## `url()` helper function

It runs in layouts and views. `href` value, which is set by `url()` function, is converted for output as external address. Helper method accepts ane of internal address in second parameter.

```php
<a href="<?php echo $this->url(array(),'controller-name'); ?>">x</a>
```

Output:

```html
<a href="external/address.html">x</a>
```

It is called as **routing of URL addresses** or for short **routing**

> Using `url()` without adding route rules in `application.ini`
>
> ```php
> <?php echo $this->url(array('controller'=>'lorem', 'action'=>'ipsum'),default); ?>
> ```
>
> It is preferred to adding routing rules in config file

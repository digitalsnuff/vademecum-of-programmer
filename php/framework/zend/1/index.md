# Zend Framework 1

## Installation

The simplest way to install `zf` tool on Ubuntu is:

```sh
sudo apt install zendframework-bin
```

1. Clone from Git Hub

[Git Hub repository link](https://github.com/zendframework/zf1)

Create new folder

```sh
mkdir ~/zendframework/
cd ~/zendframework/
```

```sh
# go to new folder and clone to current folder
git clone https://github.com/zendframework/zf1.git .
```

Quick access to `zf` tool is set alias to path in project:

```sh
alias zf.sh=[path/to/ZendFramework]/bin/zf.sh
```

2. Copy `zf` tool to paths

Copy `zf.php`

```sh
sudo mkdir -m 755 -p /usr/share/zendframework/bin
sudo cp ~/zendframework/bin/zf.php /usr/share/zendframework/bin/zf.php
sudo chown root:root /usr/share/zendframework/bin/zf.php
```

Copy `zf.sh`

```sh
cp ~/zendframework/bin/zf.sh /usr/bin/zf
sudo cp ~/zendframework/bin/zf.sh /usr/bin/zf && sudo chown root:root /usr/bin/zf
```

and change in `zf` file content in last line

```sh
# from
"$PHP_BIN" -d safe_mode=Off -f "$PHP_DIR/zf.php" -- "$@"
# to
"$PHP_BIN" -d safe_mode=Off -f "/usr/share/zendframework/bin/zf.php" -- "$@"
```

3. Switch php version to >= 5.2.11

Use Phpbrew tool for set php version:

```sh
# run command   in project root folder
$ phpbrew switch 5.6.40
$ ZEND_TOOL_INCLUDE_PATH_PREPEND=/usr/share/php/
$ export ZEND_TOOL_INCLUDE_PATH_PREPEND
$ php -v
$ source ~/.bashrc
```

The `ZEND_TOOL_INCLUDE_PATH_PREPEND` may be include in `/home/serenus/.phpbrew/bashrc` file:

```sh
[[ -z "$ZEND_TOOL_INCLUDE_PATH_PREPEND" ]] && export ZEND_TOOL_INCLUDE_PATH_PREPEND="/usr/share/php/"
```

If no `Zend` library, set path to `library/` in cloned repo:

```sh
$ ZEND_TOOL_INCLUDE_PATH_PREPEND="~/zendframework/library/"
```

or copy `~/zendframework/library/` content to path with shared libraries:

```sh
$ cp /zendframework/library/* /usr/share/php/
```

4. Create new project

```sh
zf create project . hello-world
```

Add path to `Zend` library in `public/index.php` if project was not created inside cloned `zf1` repo folder:

```sh
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    realpath('/usr/share/php'), // added path
    get_include_path(),
)));
```

## Launch application in browser

```sh
$  APPLICATION_ENV=development php -S localhost:8086 -t public/
```

The best way is launch app on configured Apache 2.4 or Nginx.

## `zf` tool commands

Create new controller

```sh
 zf create controller wierszyk
# Output
# Note: PHPUnit is required in order to generate controller test stubs.
# Note: The canonical controller name that is used with other providers is "Wierszyk"; not "wierszyk" as supplied
# Creating a controller at [path/to/project]/application/controllers/WierszykController.php
# Creating an index action method in controller Wierszyk
# Creating a view script for the index action method at [path/to/project]/application/views/scripts/wierszyk/index.phtml
# Updating project profile '[path/to/project]/.zfproject.xml'
```

Create new action in controller

```sh
zf create action pokaz wierszyk
# Output
# Note: PHPUnit is required in order to generate controller test stubs.
# Note: The canonical controller name that is used with other providers is "Wierszyk"; not "wierszyk" as supplied
# Creating an action named pokaz inside controller at [path/to/project]/application/controllers/WierszykController.php
# Updating project profile '[path/to/project]/.zfproject.xml'
# Creating a view script for the pokaz action method at [path/to/project]/application/views/scripts/wierszyk/pokaz.phtml
# Updating project profile '[path/to/project]/.zfproject.xml'

```

> If it will be run command:
>
> ```sh
> zf create action `SomeActionName` controller
> ```
>
> then:
>
> - method of action will be named `someActionNameAction()`
> - The view of this action will be `some-action-name.phtml` file
> - internal address get form like `controller/some-action-anme`

Set default controller in `application.ini`

```ini
...
resources.frontController.defaultControllerName = "wierszyk"
resources.frontController.defaultAction = "pokaz"
```

Decoration of application:

```sh
zf enable layout
# Output
# A layout entry has been added to the application config file.
# A default layout has been created at [path/to/project]/application/layouts/scripts/layout.phtml
# Updating project profile '[path/to/project]/.zfproject.xml'

```

Set in `.htaccess` file environment ot `development`

```htaccess
SetEnv APPLICATION_ENV development
```

Run application with set env

```sh
APPLICATION_ENV=development php -S localhost:8086 -t public/
```

### Prepare `zf` tool to use

[See Zend Tool Framework](https://framework.zend.com/manual/1.12/en/zend.tool.framework.clitool.html)

After cloning `zf1` we have folder named `bin`, which contains `zf.sh` script.

Another way is copy `bin/zf.sh` and `bin/zf.php` to one of the patches:

- php binary
- `/usr/bin`
- `/usr/local/bin`
- `/usr/local/ZendServer/bin`
- `/Applications/ZendServer/bin`

Add `/library` to the `include_path`.
To find out where `include_path` is located, execute `php -i` and look fot the `include_path` variable or execute `php -i | grep include_path`.

## Links

[Zend Framework Quick Start](https://framework.zend.com/manual/1.12/en/learning.quickstart.html)

[Zend Framework Documentation](https://docs.zendframework.com/#zend-framework-documentation)

## Sources

- Włodzimierz Gajda, _Zend Framework od podstaw_, Wydawnictwo HELION, ISBN: 978-83-246-3052-3, Gliwice 2011

---

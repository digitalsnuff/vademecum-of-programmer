# Eloquent ORM

> Laravel includes the Eloquent object relational mapper

- Makes querying and working with the DB very easy
- We can still use raw SQL queries if needed

```php
use App\Todo;

$todo = new Todo;
$todo->title = 'Some Todo';
$todo->save();
```

# Laravel

- Open Source PHP Framework
- Aims to make dev process pleasing without sacrificing quality
- One of the most popular and respected PHP frameworks
- Uses The **MVC** (Model View Controller) Design Pattern

## What Does Laravel Do?

- Route Handling
- Security Layer
- Models and DB Migrations
- Views/Templates
- Authentication
- Session
- Compile Assets
- Storage and File Management
- Error handling
- Unit Testing
- Email and Config
- Cache Handling

## Artisan CLI

> Laravel includes the Artisan CLI which handles many tasks

- Creating controllers nad models
- Creating database migrations files and running migrations
- Create providers, events, jobs, form requests, etc.
- Show routes
- Session commands
- Run Tinker
- Create custom commands

## Server Requirements

> All of these requirements are satisfied by the `Laravel Homestead` virtual machine

> It's highly recommended that you use Homestead as your local Laravel development environment.

openssl, mbstring, tokenizer, json, zip, xml, bcmath, ctype

- PHP >= 7.1.3
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- Ctype PHP Extension
- JSON PHP Extension
- BCMath PHP Extension

## Installing Laravel

### Via Laravel Installer

```sh
composer global require laravel/installer
# ...
cd [project/folder]
laravel new blog
```

### Via Composer Create-Project

```sh
composer create-project --prefer-dist laravel/laravel blog
```

## Local Development Server

```sh
php artisan serve # http://localhost:8000
```

## Configuration

### Public Directory

> After installing Laravel, you should configure your web server's document / web root to be the `public` directory. The `index.php` in this directory serves as the front controller for all HTTP requests entering your application.

### Configuration Files

> All of the configuration files for the Laravel framework are stored in the `config` directory. Each option is documented, so feel free to look through the files and get familiar with the options available to you.

### Directory Permissions

> After installing Laravel, you may need to configure some permissions. Directories within the `storage` and the `bootstrap/cache` directories should be **writable** by your web server or Laravel will not run. If you are using the `Homestead` virtual machine, these permissions should already be set.

### Application Key

> The next thing you should do after installing Laravel is set your application key to a random string. If you installed Laravel via Composer or the Laravel installer, this key has already been set for you by the `php artisan key:generate` command.

> Typically, this string should be 32 characters long. The key can be set in the `.env` environment file. If you have not renamed the `.env.example` file to `.env`, you should do that now. **If the application key is not set, your user sessions and other encrypted data will not be secure!**

### Additional Configuration

> Laravel needs almost no other configuration out of the box. You are free to get started developing! However, you may wish to review the `config/app.php` file and its documentation. It contains several options such as `timezone` and `locale` that you may wish to change according to your application.

> You may also want to configure a few additional components of Laravel, such as:

- Cache
- Database
- Session

## Web Server Configuration

### Pretty URLs

#### Apache

> Laravel includes a `public/.htaccess` file that is used to provide URLs without the `index.php` front controller in the path. Before serving Laravel with Apache, be sure to enable the `mod_rewrite` module so the `.htaccess` file will be honored by the server.

> If the `.htaccess` file that ships with Laravel does not work with your Apache installation, try this alternative:

```conf
Options +FollowSymLinks -Indexes
RewriteEngine On

RewriteCond %{HTTP:Authorization} .
RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]

RewriteCond %{REQUEST_FILENAME} !-d
RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule ^ index.php [L]
```

#### Nginx

> If you are using Nginx, the following directive in your site configuration will direct all requests to the `index.php` front controller:

```conf
location / {
    try_files $uri $uri/ /index.php?$query_string;
}
```

# Links

[Laravel Home Page](https://laravel.com/)

[Laravel From Scratch [Part 1] - Series Introduction](https://www.youtube.com/watch?v=EU7PRmCpx-0&list=PLillGF-RfqbYhQsN5WMXy6VsDMKGadrJ-) from Traversy Media

# Tinker

## Run Tinker

```sh
php artisan tinker
```

## Using model

```sh
App\Post::count();
```

```sh
$post = new App\Post();
$post->title = 'Post One';
$post->body = 'This is the post body';
$post->save();
```

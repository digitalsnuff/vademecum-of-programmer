# Artisan

## Examples of Artisan Commands

```sh
# list Artisan's commands
php artisan list
# help migrate command
php artisan help migrate
# create controller
php artisan make:controller TodosController
# create model with migration
php artisan make:model Todo -m
# make migration
php artisan make:migration add_todos_to_db -table=todos
# run migrations
php artisan migrate
# run Tinker
php artisan tinker
```

### Create new controller

```sh
php artisan make:controller PagesController
```

### Create new controller with resource

```sh
php artisan make:controller PagesController --resource
```

### Create new model

```sh
php artisan make:model Post
```

### Create new model with migration

```sh
php artisan make:model Post -m
```

### Run migrations

```sh
php artisan migrate
```

### Cache configuration file

```sh
php artisan config:cache
```

### Clear cache

```sh
php artisan config:clear
```

### Get route list

```sh
php artisan route:list
```

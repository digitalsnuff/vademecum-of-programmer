# Homestead PostgreSQl

## Access to Postgres

```sh
# With switching account
sudo -i -u postgres
psql
```

```sh
# Without switching account
sudo -u postgres psql
```

## `psql`

- `\du` - list users

# Links

[CONFIGURE LARAVEL TO USE POSTGRESQL WITH HOMESTEAD](http://billmartin.io/blog/2018/04/29/Configure-Laravel-to-Use-PostgreSQL-with-Homestead/)
[How To Install and Use PostgreSQL on Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-16-04)

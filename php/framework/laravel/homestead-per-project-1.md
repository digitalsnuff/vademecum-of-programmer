# Homestead Per Project Installation

> System: Ubuntu 18.04 x64 LTS

1. Create project root folder:

```sh
mkdir -p -m 755 ~/workspace/projects/proj-1
```

2. Go to already created folder:

```sh
cd ~/workspace/projects/proj-1
```

3. Create Laravel project using Composer:

```sh
composer create-project --prefer-dist laravel/laravel .
```

4. Install Homestead directly into created project using Composer:

```sh
composer require laravel/homestead --dev
```

5. Generate `Vagrantfile` and `Homestead.yaml` file using `make` command:

```sh
# Mac / Linux
php vendor/bin/homestead make
```

6. Add to `/etc/hosts` file entry for `homestead.test` or the whatever domain

```sh
echo "192.168.10.10  homestead.test" | sudo tee -a /etc/hosts
```

7. Add install options to `Homestead.yaml` file:

```yml
box: laravel/homestead
ip: '192.168.10.10'
memory: 4096
cpus: 4
provider: virtualbox

# Installing MariaDB
mariadb: true
# Installing MongoDB
mongodb: true
# Installing ElasticSearch
elasticsearch: 7
# Installing Neo4j
neo4j: true
```

## Usage

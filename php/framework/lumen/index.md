# Lumen

## Installation

PHP extensions:

- filter
- hash

```sh
PROJ_NAME=lumen-tutorial
mkdir -p ~/workspace/tutorial/$PROJ_NAME
pushd $PROJ_NAME
composer create-project --prefer-dist laravel/lumen .
popd
```

### Run project locally

```sh
php -S localhost:8015 -t public/
```

### Install Homestead

```sh
composer require laravel/homestead --dev
```

# Links

[Lumen Tutorial](https://lumen.laravel.com/docs/5.8)
[lumen tutorial 1 - introduction and installation](https://www.youtube.com/watch?v=ZRAHW80PQSo&list=PL8p2I9GklV45FoEJM8xdXyR8fbTVVQmP4)

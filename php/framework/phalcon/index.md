# Phalcon - PHP framework

# Installation

Phalcon v4 supports only PHP 7.2 and above.
Phalcon requires the PSR extension.

There are is explained installation on Debian 10 for PHP 7.4 version.

## Installation of php-zephir-parser

[PHP Zephir parser - Github](https://github.com/phalcon/php-zephir-parser)
[Zephir - Documentation](https://docs.zephir-lang.com/0.12/en/installation)

1. First make php-zephir-parser extension and enable it in PHP
2. Grab Zephir
3. Run `composer install` in root project directory
4. add symlink `sudo ln -sfn $(pwd)/zephir /usr/bin/zephir`

## Installation php-zephir-parser, psr and phalcon from source

[Psr Github](https://github.com/jbboehr/php-psr)
[Phalcon Github](https://github.com/phalcon/cphalcon)

```sh
# PHP Zephir Parser
sudo apt install re2c # Important
git clone git://github.com/phalcon/php-zephir-parser.git
cd php-zephir-parser
phpize
./configure
make
sudo make install
# change access file
sudo chmod 644 /usr/lib/php/20190902/zephir_parser.so
```

```sh
# PSR
git clone https://github.com/jbboehr/php-psr.git
cd php-psr
phpize
./configure
# specific PHP versions
# ./configure --with-php-config=/usr/local/bin/php-config
make
make test
sudo make install
# change access file
sudo chmod 644 /usr/lib/php/20190902/psr.so
```

```sh
# Phalcon
git clone https://github.com/phalcon/cphalcon.git
cd cphalcon/
git checkout tags/v4.0.0 ./
# if module is installed
# zephir fullclean
# zephir compile
cd ext
phpize
./configure
make && make install
# change access file
sudo chmod 644 /usr/lib/php/20190902/phalcon.so
```

The compilation is placed on:

- Installing shared extensions: `/usr/lib/php/20190902/`
- Installing header files: `/usr/include/php/20190902/`

## Adding php-zephir-parser, psr, phalcon extension to php.ini

```sh
PHALCON_PHP_VER=7.4
PHALCON_PHP_MODES=(apache2 cli fpm)
pushd /etc/php/$PHALCON_PHP_VER/mods-available/
sudo tee zephir_parser.ini <<EOL
; configuration for php zephir_parser module
; priority=20
extension=zephir_parser.so
EOL
sudo tee psr.ini <<EOL
; configuration for php psr module
; priority=20
extension=psr.so
EOL
sudo tee phalcon.ini <<EOL
; configuration for php phalcon module
; priority=25
extension=phalcon.so
EOL
popd
pushd /etc/php/$PHALCON_PHP_VER/
for item in ${PHALCON_PHP_MODES[*]}
do
    sudo ln -sfn /etc/php/7.4/mods-available/zephir_parser.ini $item/conf.d/20-zephir_parser.ini
    sudo ln -sfn /etc/php/7.4/mods-available/psr.ini $item/conf.d/20-psr.ini
    sudo ln -sfn /etc/php/7.4/mods-available/phalcon.ini $item/conf.d/25-phalcon.ini
done
popd

```

## Suplement

### Checking PHP loaded extensions in terminal:

```sh
php -r 'print_r(get_loaded_extensions());'
```

### Apache module reload

```sh
NOTICE: Not enabling PHP 7.4 FPM by default.
NOTICE: To enable PHP 7.4 FPM in Apache2 do:
NOTICE: a2enmod proxy_fcgi setenvif
NOTICE: a2enconf php7.4-fpm
NOTICE: You are seeing this message because you have apache2 package installed.
```

### Phalcon devtools

[Phalcon devtools](https://github.com/phalcon/phalcon-devtools)  
[Nanobox](https://nanobox.io/)

```sh
# Path
cd phalcon-devtools
ln -s $(pwd)/phalcon /usr/bin/phalcon
chmod ugo+x /usr/bin/phalcon

# Alias
alias phalcon=/home/[USERNAME]/phalcon-devtools/phalcon
```

## Performance tools

[Performance](https://docs.phalcon.io/4.0/pl-pl/performance)

- [profiling](<https://en.wikipedia.org/wiki/Profiling_(computer_programming)>) server
- [XDebug](https://xdebug.org/docs) - use [Webgrind](https://github.com/jokkedk/webgrind) tool. Webgrind is an Xdebug profiling web frontend in PHP
  ```ini
  xdebug.profiler_enable = On
  ```
- `Xhprof` - is another extension to profile PHP applications. To enable it, all you need is to add the following line to the start of the bootstrap file:

  ```php
  <?php

  xhprof_enable(XHPROF_FLAGS_CPU + XHPROF_FLAGS_MEMORY);
  ```

  Then at the end of the file save the profiled data:

  ```php
  <?php

  $xhprof_data = xhprof_disable('/tmp');

  $XHPROF_ROOT = '/var/www/xhprof/';
  include_once $XHPROF_ROOT . '/xhprof_lib/utils/xhprof_lib.php';
  include_once $XHPROF_ROOT . '/xhprof_lib/utils/xhprof_runs.php';

  $xhprof_runs = new XHProfRuns_Default();
  $run_id = $xhprof_runs->save_run($xhprof_data, 'xhprof_testing');

  echo "https://localhost/xhprof/xhprof_html/index.php?run={$run_id}&source=xhprof_testing\n";
  ```

  Xhprof provides a built-in HTML viewer to analyze the profiled data.  
  As mentioned above, profiling can increase the load on your server. In the case of Xhprof, you can introduce a conditional that would start profiling only after X requests.

- SQL Statements

  MariaDB / MySql / AuroraDb offer configuration settings that enable a `slow-query` log. To enable this feature you will need to add this to `my.cnf` (don’t forget to restart your database server)

  ```ini
  log-slow-queries = /var/log/slow-queries.log
  long_query_time = 1.5
  ```

- Client
  Another area to focus on is the client. Improving the loading of assets such as images, stylesheets, javascript files can significantly improve performance and enhance user experience. There are a number of tools that can help with identifying bottlenecks on the client:

  - Browsers: Most modern browsers have tools to profile a page’s loading time. Those are easily called web inspectors or developer tools.

    A relatively easy fix for increasing client performance is to set the correct headers for assets so that they expire in the future vs. being loaded from the server on every request. Additionally, CDN providers can help with distributing assets from their distribution centers that are closest to the client originating the request.

  - Yahoo! YSlow - analyzes web pages and suggests ways to improve their performance based on a set of [rules for high performance web pages](https://developer.yahoo.com/performance/rules.html)

- PHP
  PHP is becoming faster with every new version. Using the latest version improves the performance of your applications and also of Phalcon.

  - Bytecode Cache

    [OPcache](https://www.php.net/manual/en/book.opcache.php) as many other bytecode caches helps applications reduce the overhead of read, tokenize and parse PHP files in each request. The interpreted results are kept in RAM between requests as long as PHP runs as fcgi (fpm) or mod_php. OPcache is bundled with php starting 5.5.0. To check if it is activated, look for the following entry in php.ini:

    ```ini
    opcache.enable = On
    opcache.memory_consumption = 128    ;default
    ```

    Furthermore, the amount of memory available for opcode caching needs to be enough to hold all files of your applications. The default of 128MB is usually enough for even larger codebases.

  - Serverside cache

    [APCu](https://www.php.net/manual/en/book.apcu.php) can be used to cache the results of computational expensive operations or otherwise slow data sources like webservices with high latency. What makes a result cacheable is another topic, as a rule of thumb: the operations needs to be executed often and yield identical results. Make sure to measure through profiling that the optimizations actually improved execution time.

    ```ini
    apc.enabled = On
    apc.shm_size = 32M  ;default
    ```

    As with the aforementioned opcache, make sure, the amount of RAM available suits your application. Alternatives to APCu would be `Redis` or `Memcached` - although they need extra processes running on your server or another machine.

- Slow Tasks

  There are also a variety of queue services available that you can leverage using the relevant PHP libraries:

  - [NATS](https://nats.io/)
  - [RabbitMQ](https://www.rabbitmq.com/)
  - [Redis](https://redis.io/)
  - [Resque](https://github.com/chrisboulton/php-resque)
  - [SQS](https://aws.amazon.com/sqs/)
  - [ZeroMQ](https://zeromq.org/)

- Page Speed

[`mod_pagespeed`](https://www.modpagespeed.com/) speeds up your site and reduces page load time. This open-source Apache HTTP server module (also available for nginx) automatically applies web performance best practices to pages, and associated assets (CSS, JavaScript, images) without requiring you to modify your existing content or workflow.

## Tests

### Unit

[Codeception](https://codeception.com/)  
[Codeception - Introduction](https://codeception.com/docs/01-Introduction)  
[Writing Great Unit Tests: Best and Worst Practices](https://blog.stevensanderson.com/2009/08/24/writing-great-unit-tests-best-and-worst-practises/)  
[Mocking](https://www.clariontech.com/blog/what-is-mocking-in-php-unit-testing)

### Integration

## TDD

[TDD](https://www.sitepoint.com/re-introducing-phpunit-getting-started-tdd-php/)

## Development

Zephir

## Services

The available services are:

- Memcached
- Mongodb
- Mysql
- Postgresql
- Redis

# Phalcon Coding Standard

Phalcon is written in Zephir, a language that the Phalcon Team invented and is actively developing. Therefore, there are no established coding standards that developers can follow, should they wish to.

In this document we outline the coding standard that Phalcon is using for editing Zephir files. The coding standard is a variant of PSR-12 developed by PHP-FIG

## PhpStorm

[PhpStorm - Phalcon 4 Autocomplete Plugin](https://plugins.jetbrains.com/plugin/12776-phalcon-4-autocomplete)

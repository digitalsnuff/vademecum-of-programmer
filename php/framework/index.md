# Frameworks

We frameworkach chodzi o strategie rozwiązań. Jako programiści powinnismy rozumieć problemy rozwiązywane przez frameworki i stosowane w nich strategie rowiązań.

Powinniśmy być w stanie oceniać jakość frameworków nie tylko po zakresie oferowanych funkcji, ale też na podstawie decyzji projektowych podejmowanych przez ich twórców i na bazie jakości implementacji tych decyzji.

Nie zaszkodzi zbudować od podstaw całkiem własna, choćby niewielką aplikacje, a przy najróżniejszych okazjach montować własny zbiór kodu bibliotecznego.

Zwinne metodologie wytwarzania oprogramowania.

Projekty powinny być dzielone na bardzo małe etapy.

Testy jednostkowe. Testy mają być zautomatyzowane i projektowane jeszcze przed przystapieniem do implementacji danego kodu projektu (XP).

Dokumentacja.

Automatyczny system budowania aplikacji.

W latach siedemdziesiatych koncepcja wzorców projektowych pojawiła się jako metoda opisu problemów wraz z sednem ich rozwiązań.

Technika nazywania, identyfikowania i rozwiazywania problemów z dziedziny **inżynierii oprogramowania**.

eXtremem Programmin - Kent Beck.

Refaktoryzacja - Martin Fowler.

Testy zautomatyzowane (JUnit).

Late Static Binding - Późne wiązanie składowych statycznych

## Narzędzia

`Phing` - alternatywa dla Ant (Java) dla PHP.

- sledzenie obserwowanych błędów
- promocja współpracy w zespole
- ułatwienie w instalacji kodu
- zwiększenie przejrzystości kodu

Testing: Selenium, Jenkins

CI/CD



# MVC

# Source

- Matt Zandstra, _PHP. Obiekty, wzorce, narzędzia_, wyd. IV, ISBN: 978-83-246-9178-4, Apress 2013

# PHP - MVC Pattern

MVC jest wzorcem architektonicznym.

Jest wzorcem złożnonym, wykorzystującym sprostsze wzorce. Najczęściej używane wzorce projektowe to: Obserwator, Strategia, Kompozyt, jak też: Metoda Wytwórcza i Dekorator.

Określa się też jako kompozycja siedmiu wzorców: Obserwator, Kompozyt, Most, Łańcuch zobowiązań, Metoda wytwórcza, Polecenie oraz niewchodzący w skład kanonu View Handler.

Nie jest traktowany jako samodzielny wzorzec.

Projekt `KohanaPHP` narodził się z `Codeigniter`.

## Wzorce pochodne

W późniejszych latach na bazie wzorca MVC powstało wiele wzorców pochodnych o nieco innej strukturze oraz właściwościach:

- `Model-View-Presenter` — zmniejszenie znaczenia widoku na rzecz prezentera, który posiada pewną wiedzę o GUI oraz o tym, jak mapować poszczególne akcje użytkownika na zmiany modelu oraz zmiany widoku.
- `Presentation-Abstraction-Control` — odmiana MVP, w której rozpatrujemy hierarchiczne drzewo agentów `Control` zawiadujących przypisanymi im częściami Prezentacji i Abstrakcji, a także podrzędnymi agentami.
- `Hierarchical Model-View-Controller` — wzorzec analogiczny do PAC, lecz oparty na oryginalnym MVC.
- `Pasywny widok` — modyfikacja MVC oraz MVP zakładająca zerwanie powiązania między widokiem a modelem. Widok nie jest już odpowiedzialny za samodzielną aktualizację — logika jego działania przeniesiona jest do kontrolera.
- `Model View ViewModel (MVVM)` — modyfikacja wzorca MVC, zawierająca specjalizację modelu prezentacji. Część ViewModel jest tutaj odpowiedzialna za udostępnianie danych z modelu do widoku w odpowiedni sposób. ViewModel zawiera często większą część logiki związanej z prezentacją danych.

## Model

- w modelu zapisujemy logikę biznesową działania aplikacji
  - algorytmy przechowywania danych
  - korzystanie z zewnętrznych usług
  - pobieranie informacji z bazy danych

Sposób definiowania modeli czasami nazywa sie **odwzorowaniem** lub **mapowaniem obiektowo-relacyjnym** (Object Relational Mapping - ORM).
Biblioteka ORM stanowi szczelną warstwę komunikacyjną miedzy dwoma systemami powiazanymi ze sobą ze względu na dane.
# Source

Chris Pitt, _Wzorzec MVC w PHP dla profesjonalistów_, ISBN: 978-83-246-7015-4, HELION, 2013

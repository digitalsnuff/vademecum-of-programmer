# composer

## Releasing package kickstart

Co ma zawierać paczka:
- README
- LICENSE
- CHANGELOG
- PHP Package Checklist

## Version constraints

- any * - wszystkie wesje danej biblioteki są dla nas dobre
- 1.0.0 dev-master - pozwolenie tylko na jedną wersję
- wildcard range 1.0.*, 2.*
- hyphen range 1.0 - 2.0 (>=1.0.0 <2.1)
- unbounded range >=2.0
- operators [space], ||
- next significant release ~1.2 (>=1.2.0 <2.0.0)
- caret/semver operator ^1.2.3 (>=1.2.3 <2.0.0)

Wersjonujemy biblioteki, ale też wersję PHP, na którą ją wypuszczamy.
Możemy wymagać rozszerzeń PHP.

maglnet/composer-require-checker - rozwiazywanie przestarzałych zależności zalezności względem głównych zależności aplikacji. 
Poprawia `composer update`. 

Pięć poziomów stabilności
- dev
- alpha
- beta
- RC
- stable

Pozwolenie na rózne wersje stabilności: "^1.0@alpha", "^1.0@dev"
Obniżanie stabilności: "minimum-stability": "beta"

Composer bierze tę wersje PHP, w jakiej jest uruchomiony.

`composer.lock`
Każda komenda `composer install` i `composer update` tworzy ten plik.
Composer instaluje pliki, umieszcza je w folderze `vendor` a następnie tworzy plik `composer.lock`.
Jeżeli istnieje plik `composer.lock`, Composer nie jest zainteresowany plikiem `composer.json` a instalacja odbywa się szybciej.
Dobrze mieć composer.lock w repozytorium git, jeśli chodzi o aplikację. Jeśli chodzi o biblioteki, dodajemy `composer.lock` do `gitignore`.

Przy pracy z bibliotekami, bez composer.lock:
- `composer update --prefer-lowest` - zainstaluje najniższe wersje bibliotek na jakie pozwalamy w naszym manifeście w composer.json

Aplikacje
optymalizacja autoload
`composer dump-autoload --optimize` - przedudowanie autoload i tworzenie tablicy i sprawdza w tej tablicy czy ta klasa jest. Gdy nie znajdzie takiej klasy przechodzi do autoload.
`composer dump-autoload --classmap-authoritative` - przedudowanie autoload i tworzenie tablicy. Zatrzymuje się, gdy nie znajdzie klasy w wygenerowanej tablicy i nie szuka w autoload. Rzuca wyjątek.
Zaleca się te dwa powyższe stosować na produkcji.

Komendy
Jest około pięćdziesięciu koment eventowych.
Możemy tworzyć własne eventy.
analyze, test, verify - najczęściej implementowane.

Plugins
brainmaestro/composer-git-hooks - 
ergebnis/composer-normalize
Awesome Composer # Plugins

Features
depends/prohibits(why/why-not) `composer prohibits php:8`
`composer outdated` - czy są nowe wersje bibliotek.

`composer self-update`
`composer diagnose` - mini audyt
`composer update -v` 
`rm -rf vendor && composer update`

Automating
`composer validate --strict --with-dependencies`
`composer normalize --dry-run`
`composer require sensiolabs/security-checker` -> `vendor/bin/security-checker security:check`

Composer 2.0
- 1100 nowych commitów
- performance improvement - bez prestissimo - instalacja równoległa
- architectural changes and determinizm
- runtime features - 
- error reporting improvement
- backwards compatibility breaks

Useful links
https://semver.mwl.be  
https://packanalyst.com  
https://repo-stats.github.io  
https://github.com/ziados/awesome-php   


http://phppackagechecklist.com/#1,2,3,4,5,6,7,8,9,10,11,12,13,14
https://kubawerlos.github.io/slides/2020-12-11_Warszawskie_Dni_Informatyki_Composer_good_practices.html#/
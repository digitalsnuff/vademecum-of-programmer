# PHP SPL

SPL uses class constants since PHP 5.1. Prior releases use global constants in the form RIT_LEAVES_ONLY.

## Datastructures

SPL provides a set of standard datastructures. They are grouped here by their underlying implementation which usually defines their general field of application.

### SplDoublyLinkedList

Doubly Linked List (DLL) is a list of nodes linked in both directions to each other. Iterator's operations, access to both ends, addition or removal of nodes have a cost of O(1) when the underlying structure is a DLL. It hence provides a decent implementation for stacks and queues.

### SplStack

### SplQueue

### SplHeap

### SplMaxHeap

### SplMinHeap

### SplPriorityQueue

### SplFixedArray

### SplObjectStorage

## Iterators

## Interfaces

## Exceptions

## SPL Functions

## File handling

## Miscellaneous Classes and Interfaces

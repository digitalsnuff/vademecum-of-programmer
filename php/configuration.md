# Configuration

## php.ini

[php.ini](https://www.php.net/manual/en/configuration.file.php)
[.user.ini.filename](https://www.php.net/manual/en/ini.core.php#ini.user-ini.filename)
[List of php.ini directives](https://www.php.net/manual/en/ini.list.php)

It is possible to scan multiple directories by separating them with the platform-specific path separator (; on Windows, NetWare and RISC OS; : on all other platforms; the value PHP is using is available as the `PATH_SEPARATOR` constant).

```sh
# If a blank directory is given in PHP_INI_SCAN_DIR, PHP will also scan the directory given at compile time via --with-config-file-scan-dir.
PHP_INI_SCAN_DIR
--with-config-file-scan-dir
```

Within each directory, PHP will scan all files ending in .ini in alphabetical order. A list of the files that were loaded, and in what order, is available by calling `php_ini_scanned_files()`, or by running PHP with the `--ini` option.

```sh
Assuming PHP is configured with --with-config-file-scan-dir=/etc/php.d,
and that the path separator is :...

$ php
  # PHP will load all files in /etc/php.d/*.ini as configuration files.

$ PHP_INI_SCAN_DIR=/usr/local/etc/php.d php
  # PHP will load all files in /usr/local/etc/php.d/*.ini as configuration files.

$ PHP_INI_SCAN_DIR=:/usr/local/etc/php.d php
  # PHP will load all files in /etc/php.d/*.ini, then /usr/local/etc/php.d/*.ini as configuration files.

$ PHP_INI_SCAN_DIR=/usr/local/etc/php.d: php
  #  PHP will load all files in /usr/local/etc/php.d/*.ini, then /etc/php.d/*.ini as configuration files.
```

```sh
PHP_INI_SCAN_DIR=:$(pwd) php -S localhost:2222
```

## .user.ini files

Since PHP 5.3.0, PHP includes support for configuration INI files on a per-directory basis. These files are processed only by the CGI/FastCGI SAPI.

If you are running PHP as Apache module, use .htaccess files for the same effect.

Only INI settings with the modes `PHP_INI_PERDIR` and `PHP_INI_USER` will be recognized in `.user.ini-style` INI files.

Two new INI directives, `user_ini.filename` and `user_ini.cache_ttl` control the use of user INI files.

`user_ini.filename` sets the name of the file PHP looks for in each directory; if set to an empty string, PHP doesn't scan at all. The default is `.user.ini`.

`user_ini.cache_ttl` controls how often user INI files are re-read. The default is 300 seconds (5 minutes).

## php_flag vs php_value

[PHP: php_value vs php_admin_value and the use of php_flag explained](https://ma.ttias.be/php-php_value-vs-php_admin_value-and-the-use-of-php_flag-explained/)

## PHP settings in Nginx conf

[How to set php ini settings in nginx config for just one host](https://stackoverflow.com/questions/32100834/how-to-set-php-ini-settings-in-nginx-config-for-just-one-host)

## php.ini directives

[List of php.ini directives]https://www.php.net/manual/en/ini.list.php

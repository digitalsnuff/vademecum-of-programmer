# OOP

Obiekty są definiowane przez klasy.

Klasa definiuje nowy typ, nową kategorię danych.

Obiekt stanowi dane, zadeklarowane zgodnie z definicją. Obiek nazwywany egzemplarzem klasy lub konkretyzacją. Klasa definiuje typ obiektu. W momencie tworzenia obiektu, jest przypisywany indetyfikator obiektu w obrębie tego samego procesu.

Metody i właściwości statyczne służą do odwołania się do składowych w kontekście klasy a nie obiektu. Są to składowe o zasiegu klasy. Nie mogą odwoływac się do skłladowych zwyczajnych z racji przynalezności do obiektu.

`Klasa abstrakcyjna` to taka, której nie da się skonkretyzować. Jej jedyną funkcją jest definiowanie lub częściowe implementowanie intefejsu dla ewentualnych klas pochodnych. Aby klasa była abstrakcyjna, umieszczamy słowo kluczowe `abstract` w jej deklaracji. W większości przypadków klasa abstrakcyjna zawiera przynajmniej jedną metodę abstrakcyjną. Metoda jest deklarowana ze słowem kluczowym `abstract`. Metoda abstrakcyjna nie może posiadać implementacji. Deklaruje się ją w zwykły sposób, jednakże deklarację kończy się średnikiem, pomijając ciało metody.
Każda klasa rozszerzająca (specjalizująca) klasę abstrakcyjną powinna albo implementować wszystkie jej metody abstrakcyjne, albo sama zostać oznaczona jako abstrakcyjna. Klasa pochodna przy tym jest odpowiedzialna nie tylko za implementację każdej metody abstrakcyjnej, ale i za dokładne powtórzenie jej sygnatury: dostęp do metody nie może być określony ściślej niż w klasie bazowej. Taka implementacja metody zakłada identyczną liczbę argumentów.
Klasa abstrakcyjna może być jednak fragmentarycznie zaimplementowana.

`Interfejsy` są czystymi szablonami. Wyznacza zakres funkcji. Nie może implementować metod. Może zawierać deklaracje właściwości i metod. Nie może definiować ciał swoich metod.
Można implementowac dowolną liczbę interfejsów. Interfejsy pozwalaja na kojarzenie zupełnie obcych typów.
Interfejsy definiują typy pozbawione implementacji.

Klasa implemetująca zyskuje typ klasy abstrakcyjnej, którą rozszerza i równocześnie typ interfejsu, który implementuje.

`Cecha typowa` to struktura przypominająca klasę, która sama w sobie nie nadaje się do konkretyzacji, ale może być wcielana do innych klas.
Zmienia strukturę klasy, ale nie zmienia jej typu. Cechy pozwalają na uniknięcie dziedziczenia niepotrzebnych metod i właściwości.

## Wzorce

`Wzorce projektowe` to opisy problemów programistycznych wraz z propozycjami ich rozwiązania. Wzorce promują i równoczesnie spełniają podstawowe zasady projektowania.

Należy właściwie zarządzać elementami architektury ze względu na ryzyko błędu.

## Obsługa błędów

Nie należy zostawiać decyzji o kontynuowaniu albo przerwaniu wykonywania programu pierwszej lepszej klasie.

`Wyjątek` jest specjalnym obiektem, egzemplarzem wbudowanej klasy `Exception` albo jej klasy pochodnej. Obiekty klasy `Exception` służą do przechowywania i zgłaszania informacji o błędach. Konstruktor klasy przyjmuje dwa argumenty: komunikat o błędzie i kod błędu.

Instrukcja `throw` powoduje przerwanie wykonania bieżącej metody oraz przekazanie sterowania waz z odpowiedzialnością za obsługę błędu do wywołującego.

`try` - blok kodu chronionego uzupełniony blokiem kodu przechwytującym wyjątki - klauzulą `catch`.

Specjalizacje klasy wyjątku:

- rozszerzanie funkcjonalności klasy wyjątku
- definiowanie nowego typu dla efektywniejszej obsługi błędów

Stos wywołań błędu

- kod kliencki
- zarejestrowana funkcjonalnośc przechytywania błędów
- Fatal Error (najwyżej stosu)

Blok `finally` nie zostanie wykonany, jeśli po drodze będzie wywołanie `die()` lub `exit()`

## Dictionary

`Interceptor methods` - wbudowane metody przechwytujące służące do przechwytywania wywołań niezdefiniowanych metod i właściwości. Jest to tzw. przeciążanie (odmienne znaczenie w odróżnieniu od C++ lub Java).

`Delegowanie` - obiekt przekazujący wywołanie metody do innego obiektu.

`__destruct()` - metoda wywoływana tuz przed operacją zbierania nieuzytków (`garbage collection`), czyli bezpośrednio przed usunieciem obiektu z pamięci.

`Closures` - funkcja anonimowa zapamiętuje kontekst, w którym została utworzona.

`Pakiety` (moduły) - organizowanie kodu w kategorie logiczne. Jest to zbiór powiazą nych ze sobą klas. Służą do wyodrębniania i rodzielania poszczególnych części systemu.

`Reflection API` - retrospekcja, dynamiczne odwołanie do informacji o klasach.

`Polimorfizm` - przełączanie klas. Utrzymywanie implementacji wspólnego interfejsu. Inną forma hemetyzacji jest polimorfizm.

## Projektowanie

Projektowanie obiektowe vs projektowanie proceduralne

`Projektowanie kodu` to inaczej definiowanie systemu - określanie dla systemu wymagań i zakresu jego zadań. Definiowanie uczestników systemu i relacji między nimi.

Właściwe relacje nawiązują się w czasie wykonania.

Kod proceduralny przyjmuje postać sekwencji poleceń i wywołań metod. Do obsługi różnych stanów wydziela sie kod kontrolujący

- odpowiedzialność
- spójność
- sprzęganie - oddzielne części kodu systemu są ze obą związane tak, że zmiany w jednej z nich wymuszają zmiany w pozostałych
- ortogonalność - ponowne wykorzystanie komponetów

System należy konstruować z maksymalnie niezaleznych komponentów.

> Należy programować pod kątem interfejsu a nie implementacji.

> Faworyzuj kompozycję przed dziedziczeniem.

> Kod ma uzywac intefejsów a nie implementacji

> Nalezy zmienne koncepcje hermetyzować

Jeśli w hierarchi dziedziczenia mamy do czynienia z więcej, niż jednym kryterium specjalizacji, możemy wtedy wydzielić zmienne koncepcje. Innym symptomem zmienności kandydującej do hermetyzacji jest wyrazenie warunkowe.

Dobrze zaprojektowane systemy wymieniają dane za pośrednictwem obiektów przekazywanych pomiędzy wywołaniami metod.

UML:

- relacja realizacji - relacja pomiędzy interfejsem a klasami implementującymi ten interfejs
- dziedziczenie
- powiązanie - gdy właściwość klasy przechowuje referencję egzemplarza innej klasy
- agregacja i kompozycja - relację agregacji oznaczamy pustym rombem a relacje kompozycji zapełnionym rombem.
- zalezność (relacja użycia)

## Pattern

`Wzorzec` to rozwiązanie problemu w danym kontekście. Wzorce się odkrywa a nie wynajduje (Martin Fowler).

Abstract Factory - udostępnianie intefejsu do tworzenia rodzin obiektów powiązanych lub współzaleznych, bez jawnego wymieniania konkretnych klas.

Nazywanie wzorca projektowego:

- nazwy mają stanowić słownictwo do powszechnego użytku

Wzorzec projektowy składa się z czterech częsci:

- nazwy
- problemu
- rozwiązania
- konsekwencji

`Strategia` - zastosowanie kompozycji. Przeniesienie zestawu algorytmów do odrębnego typu.

### Wzorce genrerowania obiektów

> ukierunkowane na korzystanie z interfejsów, jak też na dobre zaplanowania konkretyzacji klas abstrakcyjnych.

- `singleton` - klasa generująca jedyny w systemie egzemplarz obiektu  
  Nie przechowujemy obiektów w przestrzeni globalnej ze względu na łatwe zamazanie.  
  Obiekt ma być dostępny dla wszystkich obiektów systemu.  
  W systemie może istnieć tylko i tylko jeden obiekt danej klasy.  
  Klasa, której nie da się skonkretyzować poza nią samą.  
  Konstruktor jest metodą prywatną.  
  Metoda statyczna nie może z definicji odwoływać się do składowych obiektu, ponieważ nie jest wywoływana na rzecz żadnego konkretnego obiektu, a jedynie na rzecz klasy.  
  Singleton, podobnie jak klasyczne zmienne globalne, jest dostępny z dowolnego miejsca systemu, może więc służyć do zawiązania trudnych do wykrycia zależności. Zmiana w klasie rodzaju Singleton może wymusić zmiany w wykorzystujących go klasach. Problemem jest globalna natura Singletona, która pozwala programiście na pomijanie kanałów komunikacji definiowanych interfejsami klas. Zależność jest ukryta w ciele metody. Utrudnia to identyfikację zależności w systemie. Singletona nalezy używac z rozwagą i umiarkowaniem. Może jednak pomóc w wyeliminowaniu z systemu niepotrzebne przekazywanie obiektów pomiędzy elementami systemu.  
  To jakby ulepszona wersja zmiennej globalnej.  
  Singletona nie da się przypadkowo zamazać niepoprawnymi danymi.  
  Przydatne tam, gdzie nie mamy do dyspozycji przestrzeni nazw.
- `factory method` - konstruowanie hierarchii dziedziczenia klas-generatorów.
  Staramy się operować uogólnieniami a nie specjalizacjami.  
  Implementacja jest poboczna względem abstrakcji.  
  Każde uzupełnienie rodziny produktówzmusza do utworzenia odpowiedniego konkretnego wytwórcy.
  Problem tworzenia egzemplarzy klas, kiedy kod operuje typami abstrakcyjnymi.
- `abstract factory` - konsolidacja tworzenia produktów powiązanych  
  fabryka produkująca powiązane zestawy klas
- `prototype` - generowanie obiektów przez klonowanie
  Wykonujemy duplikaty istniejących konkretnych produktów. Bazą generowania produktów staja się wtedy same klasy konkretnych produktów.  
  Wzorzec pozwala na zastapienie dziedziczenia kompozycją.  
  Redukuje to liczbę klas, które trzeba wytworzyć.  
  Jest to jedna z wariacji wzorca Abstract Factory.  
  Rodziny obiektów tworzymy przy tworzeniu instancji fabryki, np. przez argumenty konstruktora przypisane do właściwości prywatnych, przechowujących typy.  
  Utworzone obiekty klonujemy w metodach i je zwracamy.

### Wzorce organizacji obiektów i klas

> Kombinacje obiektów i klas w odnisieniu do relacji między nimi.

> Zasada projektowania: zalety elastyczności kompozycji względem dziedziczenia.

> Struktura klas powinna możliwiać konstruowanie użytecznych struktur obiektów w czasie wykonania.

- `Composite` - układanie struktur, w ramach których w roli pojedynczych obiektów występują całe ich grupy
  Dziedziczenie w służbie kompozycji.  
  Prosta metoda agregacji i zarządzania grupami podobnych do siebie obiektów w taki sposób, aby pojedynczy obiekt uczestniczący w agregacji był dla użytkownika nieodróżnialny od grupy obiektów.  
  Hierarchiczne dziedziczenia to struktury drzewiaste, zakorzenione w klasie nadrzędnej, bazowej i rozgałęziające się wzdłuż poszczególnych specjalizacji.  
  Służy do generowania i przeglądania drzew obiektów.  
  Modelowanie zależności między komponentami a kolekcjami w kodzie.  
  Jeśłi obiekt zawierający dzieli interfejs z obiektami zawierającymi, wszystkie one powinny dzielić rodzinę typu.  
  Wzorzec definiuje pojedynczą hierarchię dziedziczenia, w której ujmuje dwa różne zbiory zadań. Klasy wzorca muszą obsługiwać zestaw wspólnych operacji stanowiących ich zadania podstawowe.  
  Kompozyt to obiekt składający się z innych obiektów.  
  Liście bądż końcówki (leaves) - klasy reprezentujące końcowe węzły struktury drzewiastej przystosowane do obsługi podstawowych operacji hierarchii, ale nie nadające się do przechowywania innych jej obiektów.
- `Decorator` - elastyczny mechanizm kombinowania obiektów w czasie wykonania
  Ułatwia modyfikowanie zakresu funkcji konkretnych komponentów agregatów.  
  Podstawa wzorca jest dynamiczna kompozycja obiektów.  
  Kompozycja i delegacja.  
  Dekoratory przechowują egzemplarze innych klas.  
  Implementują operacje, których wywołania są delegowane do obiektów klas przechowywanych, ale dopiero po wykonaniu (lub przed) własnych zadań dekoratora uzupełniajacych operację docelową.  
  Wstrzyknięta klasa przez konstruktor jest tego samego typu (ten sam interfejs). Implementujemy metodę zadeklarowaną w interfejsie w ten sposób, że wywołujemy w niej tę samą metodę, ale na zasadzie delegacji z wstrzyknietego obiektu.  
  Wszystkie obiekty uczestniczące w implementacji wzorca dziedziczą po klasie bazowej.
- `Facade` - tworzenie prostego intefejsu systemów złozonych i zmiennych  
  Udostępnianie prostych i zwartyych interfejsów dla złożonych systemów.  
  Strategia dla całych systemów chowania implementaacji interfejsów.  
  Wydzielanie warstw systemu. Zależności między warswami powinny być jak najsłabsze, aby zmiany w jednej warstwie miały jak najmniejszy oddźwięk w pozostałych. Daje to możliwość na bezinwazyjnej rezygnacji z dowolnej warstwy.  
  Brama podsystemu.

### Wzorce zadaniowe

> mechanizmy, za pomocą których klasy i obiekty współpracują ze sobą w osiąganiu celu

> Interpretacja minijęzyków

> hermetyzacja algotytmów

- `Interpreter` - konstruowanie interpreterów minijęzyków nadających się do wbudowywania w aplikacje interfejsów skryptowych  
  DSL (Domain Specific Language) język specjalizowany - wewnętrzny język programowania.  
  Analiza leksykalna. Opis gramatyki języka.  
  EBNF (Extended Backus-Naur Form) - rozszerzona notacja Backusa-Naura.  
  ~~Tworzymy klasę, która przechowuje wartości: klucz-wartość. Tworzymy klasę , która interpretuje dostarczoną parę w ten sposób, że podajemy jako argument metody klucz a następnie w minnej metodzie podajemy jako argument kontener (kontekst).~~  
  ~~Wywołujemy składowe we wnętrzu danego obiektu w metodach innego obiektu.~~
  Tworzymy klasy typu `Expression` (literal, number, operator, variable).  
  Interpreter jest kontekstem i zarazem magazynem wyrazeń.
- `Strategy` - identyfikowanie algorytmów stosowanych w systemie i ich hermetyzację do postaci osobnych, własnych typów.  
  Wydzielanie algorytmu do jego własnego typu.  
  Zaniechanie rozbudowy hierarchi klas i wyodrębnienie funkcjonalności do osobnego typu, gdy klasy muszą obsługiwać wielkorakie implementacje interfejsu.
- `Observer` - tworzy zaczepy umożliwiające powiadamianie obiektów o zdarzeniach zachodzących w systemie.  
  rozdzielenie elementów użytkujących (obserwatorów) od klasy centralnej (podmiotu obserwacji).  
  Obserwatory muszą być informowane o zdarzeniach zachodzących w podmiocie obserwacji.  
  Umożliwiamy obserwatorom rejestrowanie się w klasie podmiotu.  
  Typ Observable implementuje metody: attach(Observer), detach(Observer), notify.  
  Można też rejestrować w obserwerach obiekty obserwowane.  
  SplObserver, SplSubject, SplObjectStorage.
- `Visitor` - rozwiązuje problem aplikacji wykonania okreslonej operacji na wszystkich węzłach drzewa obiektów  
  W kompozycie dodajemy wizytora a następnie ten wizytor jest dodawany do każdego obiektu kolekcji.  
  Double dispatch mechanizm.  
  struktura obiektów zawiera różne klasy z różnymi interfejsami i chcemy wykonać opercje w zależności od konkretnych klas  
  Wykonanie rożłącznych i bez relacji operacji na obiektach bez zanieczyszczania klas konkretnych  
  struktura obiektów nie będzie się zmieniać - wtedy można dodać nowe operacje poprzez dodanie nowego wizytora z zachowaniem niezmienionej struktury obiektów.
- `Command` - obiekty poleceń przekazywane pomiędzy częściami systemu  
  klient, invoker, odbiorca

### Wzorce korporacyjne

> odnoszą się do różnych elementów warstw logiki aplikacji, prezentacji i utrwalania danych (warstwy bazy danych)

Projektowanie aplikacji warstwowych - podzielnonych na warstwy funkcjonalne.

Wzorce

- `Registry` - wzorzec rejestru dla udostepniania danych dla klas. Przy odpowiednim stosowaniu serializacji mozna również zapewnić dostępność informacji pomiędzy żądaniami (w obrębie sesji), a nawet między instancjami aplikacji. Jest to po prostu klasa, która udostępnia dane za pomocą metod statycznych. (Whiteboard, Blackboard).
- `Front Controller` - Wykorzystywany w wiekszych systemach, w których jest potrzebna możliwie duża elastyczność i mozliwość zarządzania potencjalnie wieloma widokami i poleceniami.
- `Application Controller` - tworzy klasę zarządzającą logiką widoku i wyboru poleceń
- `Template View` - tworzy strony zarządzajace wyłącznie interfejsem użytkownika i wyświetlaniem danych, wtłaczając informacje dynamicznie do formatu znacznikowego za pomocą mozliwie najmniejszej ilości gołego kodu.
- `Page Controller` - lżejsza, ale mniej elastyczna wersja Front Controllera, realizująca jednak identyczne zadanie. Wzorzec użyteczny przy zarządzaniu żądaniami i obsługą logiki widoku tam, gdzie chcemy szybkich rezultatów i nie przewidujemy przyszłego znaczącego zwiększenia złozoności systemu.
- `Transaction Script` - kiedy chcemy szybko i dobrze, przy minimalnym nakładzie na planowanie, zwracamy się w logice aplikacji do biblioteki kodu proceduralnego. Wzorzec ten słabo się skaluje.
- `Domain Model` - drugi biegun względem wzorca Transaction Script; wykorzystujemy go do budowania obiektowych modeli procesów i komponentów biznesowych. To prezentacja rzeczywistych uczestników i procesów systemu. Podmioty i ich czynności.

Aplikacje i warstwy

Promowanie podziału aplikacji na szereg jak najmniej zależnych od siebie warstw. Warstwy w systemie korporacyjnym pełnią rolę specjalizującą. Istnieją różne strategie komunikacji między warstwami. Wybór konkretnej architektury jest uzależniony od stopnia złożoności systemu.

- `Warstwa widoku` - obejmuje interfejs prezentowany końcowym użytkownikom systemu, za pośrednictwem którego komunikują się oni z sytemem. Interfejs ten odpowiedzialny jest za prezentowanie użytkownikom wyników przetwarzania ich żądań oraz za udostępnianie mechanizmów, za pośrednictwem których użytkownicy mogą kierować żądania do systemu.
- `Warstwa poleceń i kontroli` - przetwarza żądania odbierane od użytkownika. Po analizie żądań deleguje je do warstwy logiki biznesowej, w ramach której odbywa się właściwe przetwarzanie niezbędne do wypełnienia żądania. Następnie warstwa poleceń i kontroli wybiera sposób prezenacji wyników przetwarzania. Często łączy się z warstwą widoku tworząc `warstwę prezentacji`
- `Warstwa logiki biznesowej` - jest odpowiedzialna za właściwe przetwarzanie żądania. Wykonuje wszelkie niezbędne obliczenia i porządkuje wyniki. Logika biznesowa to to, do czego system powołano i jego jedyne prawdziwe zadanie. Cała reszta tylko wspomaga wykonywanie tego zadania.
- `Warstwa danych` - to cała reszta systemu, obejmująca mechanizmy utrwalania i pozyskiwania danych. W niektórych systemach warstwa poleceń i kontroli korzysta z warstwt danych celem pobierania z niej obiektów biznesowych, które mają ostatecznie zrealizować żądania. W innych systemach warstwa danych jest starannie ukrywana przed resztą.

### Wzorce bazodaneowe

- `Interfejs warstwy danych` - wzorce, które definiują punkty styku pomiędzy warstwą utrwalania danych aa reszta systemu
- `Obserwowanie obiektów` - śledzenie, unikanie powielania, autoamtyzacja utrwalania i wstawiania do baz danych
- `Elastyczne zapytania` - konstruowanie zapytań bez konieczności uwzględniania specyfiki bazy danych
- `Zarządzanie komponentami bazodanowymi` - powrót wzorca Abstract Factory.

Wzorce:

- `Data Mapper` - DAO (Data Access Object), odwzorowanie danych - to klasa odpowiedziana za obsługe przejścia od bazy danych do obiektu i na odwrót.
- `Identity Map` - mapa tożsamości, to obiekt, któreg zadaniem jest śledzenie wszystkich obiektów w systemie i dbanie o to, aby nie doszło nigdzie do rozdwojenia obiektu, który miał wystepować w jednym egzemplarzu.
- `Unit of Work` - jednostka pracy, pomaga w zapisywaniu obiektów jedynie wtedy, kiedy jest to niezbędne.
- `Lazy Load` - opóźnione ładowanie, redukcja masowych odwołań do bazy danych
- `Domain Object factory` - Jest to tworzenie obiektu nie w mapperze a w fabryce. Jest to przeniesienie twoorzenia obiektów domentowych do fabryki jako osobny typ. Odziela wiersze bazy danych od danych składowanych w składowych obiektów dziedziny.
- `Identity Object` - hermetyzuje warunkowy aspekt zapytania do bazy daych, tak żeby było można montować różne warianty warunkowe w czasie wykonania.
- `Selection Factory` - przeniesienie odpowiedzialności zapytania na inny typ.
- `Update Factory` - przeniesienie odpowiedzialności zapytania na inny typ.
- `Domain Object Assembler`

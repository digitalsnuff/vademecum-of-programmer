# Magento 2

## Requirements

Magento works only on Linux.  
Minimum memory required is 2GB. `swap lile` may to use for allocate memory.  
Using `Composer` is recomended.  
Web servers may be `Apache 2.4` or `Nginx 1.x`.

Magento is also compatible with MySQL NDB Cluster 7.4.\*, MariaDB 10.0, 10.1, 10.2, Percona 5.7, and other binary-compatible MySQL technologies.

PHP from 7.1.3 but recommended version is ~7.3.0 version

If you install Magento via cloning from the github repository then make sure you have the `ext-sockets` installed on your instance.

PHP modules:

- ext-bcmath
- ext-ctype
- ext-curl
- ext-dom
- ext-gd
- ext-hash
- ext-iconv
- ext-intl
- ext-mbstring
- ext-openssl
- ext-pdo_mysql
- ext-simplexml
- ext-soap
- ext-xsl
- ext-zip
- lib-libxml

We **strongly recommend** you verify that `PHP OPcache` is enabled for performance reasons.  
Enable `opcache.save_comments`

We recommend particular PHP configuration settings, such as `memory_limit`, that can avoid common problems when using Magento.

A valid security certificate is required for HTTPS.  
Self-signed SSL certificates are not supported.  
Transport Layer Security (TLS) requirement - PayPal and repo.magento.com both require TLS 1.2 or later.

Magento requires the following system tools for some of its operations:

- bash
- gzip
- lsof
- mysql
- mysqldump
- nice
- php
- sed
- tar

Mail server: Mail Transfer Agent (MTA) or an SMTP server.

Technologies Magento can use

- `Redis` for page caching and session storage. Version 5.0 is highly recommended.
- `Varnish` version 6.x (tested with 6.3.1)
- `Elasticsearch` ~6.8.x
- `RabbitMQ` 3.8.x - can be used to publish messages to queue and to define the consumers that receive the messages asynchronously.

Three master databases recommended.

Optional

- `php_xdebug` 2.5.x or later (development environments only; can have an adverse effect on performance)
- `PHPUnit` (as a command-line tool) 6.2.0

## Installation

From Magento page or https://github.com/magento/magento2

```sh
git clone https://github.com/magento/magento2.git
```

## Supported browsers

Storefront and Admin:

- Internet Explorer 11 or later, Microsoft Edge, latest–1
- Firefox latest, latest–1 (any operating system)
- Chrome latest, latest–1 (any operating system)
- Safari latest, latest–1 (Mac OS only)
- Safari Mobile for iPad 2, iPad Mini, iPad with Retina Display (iOS 12 or later), for desktop storefront
- Safari Mobile for iPhone 6 or later; iOS 12 or later, for mobile storefront
- Chrome for mobile latest–1 (Android 4 or later) for mobile storefront
- Here, latest–1 means one major version earlier than the latest released version.

## TLS 1.2 requirement for PayPal

PayPal recently announced they will require Transport Layer Security (TLS) version 1.2 to process payments in a live environment. (PayPal already requires TLS 1.2 in the sandbox.)

## TLS requirement for repo.magento.com

# Source

https://devdocs.magento.com/guides/v2.3/install-gde/system-requirements-tech.html

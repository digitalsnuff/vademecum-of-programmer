#include <iostream>

using namespace std;

unsigned long int fib(int x)
{
    if (x < 2)
        return x;
    else
        return fib(x - 1) + fib(x - 2);
}

int main()
{
    cout << "Suma ciągu fibonaciego z liczny 10: " << fib(10);
}
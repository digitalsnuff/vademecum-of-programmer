#include <iostream>

using namespace std;

unsigned long int MacCarthy(int x)
{
    if (x > 100)
        return (x - 10);
    else
        return MacCarthy(MacCarthy(x + 11));
}

int main()
{
    cout << "MacCarthy " << MacCarthy(99) << endl;
    cout << "MacCarthy " << MacCarthy(1000) << endl;
}
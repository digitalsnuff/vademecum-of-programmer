#include <QApplication>
#include <QPainter>
#include <QPushButton>
#include <QtWidgets>
#include <QDesktopWidget>
#include <QMainWindow>
#include <QStyle>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QDesktopWidget dw;
    QMainWindow w;

    QPicture pi;
    QPainter p(&pi);
    p.setRenderHint(QPainter::Antialiasing);
    p.setPen(QPen(Qt::black, 12, Qt::DashDotLine, Qt::RoundCap));
    p.drawLine(0, 0, 200, 200);
    p.end(); // Don't forget this line!

    QLabel l;
    l.setPicture(pi);
    l.show();
    return a.exec();
}
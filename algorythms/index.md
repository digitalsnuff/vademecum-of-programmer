# Algorytmy

Algorytmika, jako gałąź wiedzy, dostarcza wielu efektywnych narzędzi wspomagających rozwiązywanie różnorodnych zagadnień za pomocą komputera. Jest to szkoła umiejeętności skutecznego rozwiązywania problemów.

`Algorytm`

- to skończony ciąg/sekwencja reguł, które aplikuje się na skończonej liczbie danych, pozwalający rozwiązać zbliżone do siebie klasy problemów
- zespół reguł charakterystycznych dla pewnych obliczeń lub czynności informatycznych

## Cechy

Każdy algorytm:

- posiada dane wejściowe (w ilości większej lub równej zero) pochodzące z dobrze zdefiniowanego zbioru (np, algorytm Euklidesa operuje na dwóch liczbach całkowitych)
- produkuje pewnien wynik (niekoniecznie numeryczny)
- jest precyzyjnie zdefiniowany (każdy krok algorytmu musi być jednoznacznie określony)
- jest skończony(wynik algorytmu musi zostać kiedyś dostarczony - mając algorytm A i dane wejściowe D, powinno być możliwe precyzyjne określenie czasu wykonania T(A))
- daje się zastosowac do rozwiązywania całej klasy zagadnień, a nie tylko jednego konkretnego zadania.

Alborytm ma byc `efektywny`, czyli ma wykonywać swoje zadanie w jak najkrótszym czasie i wyykorzystywać jak najmniej ilości pamięci.

## Historia

- pojęcie pochodzi najprawdopodobniej od nazwiska perskiego pisarza matematyka Muhammada ibn Musa al-Chuwarizmi (żył w IX wieku n.e.) Jego zasługą jest dostarczenie klarownych reguł wyjaśniających krok po kroku zasady operacji arytmetycznych wykonywanych na liczbach dziesiętnych.
- pojęcie jest łączone z imieniem greckiego matematyka `Euklidesa` (365 - 300 p.n.e.) i jego słynnym przepisem na obliczanie największego wspólnego dzielnika dwóch liczb _a_ i _b_ (NWD)

Można sie pokusić o potraktowanie algorytmiki i matematyki jakopewna całość wywodząca się naturalnie z systemów obliczeniowych. Warto tu wspomnieć o ludach sumeryjskich, wynalazców tabliczek obliczeniowych, własnego kalendarza i sześćdziesiętnego systemu pomiarowego (24-godzinna doba to ich wynalazek).
Chińczycy wynaleźli Abakusa - najsłynniejszego liczydła w historii ludzkości.
Blaise pascal w ok. 1645 roku wymyslił maszyne do dodawania. To samo niezależnie stworzył G. W. Leibniz.

1801 rok - Francuz Joseph Marie Jacquard wynalazł krosno tkacie programowane przez karty perforowane. Wynalazł też maszyne do produkcji sieci rybackich. Zainspirowany tym J.M. Carnot ściągnął go do Paryża i pomógł załatwić stypendium rządowe. Automatyzacja spowodowała bunt ludności ze strachu przez likwidacja stanowisk pracy. Karta perforowana z zakodowaną logiką dwustanową jest prekursorem współczesnych pamięci.
1833 rok - Anblik Charles Babbage zbudował częściowo maszynę do wyliczania niektórych formuł matematycznych (np. astronomia, nawigacja). Był autorem koncepcji **maszyny analiycznej**. Ada Lovelace, córka lorda Byrona, opracowała pierwsze reoretyczne programy dla tej nieistniejącej jeszcze maszyny, stając się pierwszą uznaną programistką w historii informatyki.
1890 rok - Amerykanin Herman Hollerith stworzył maszynę do opracowań statystycznych przy okazji spisu ludności. Jego przedsiębiorstqo przekształciło się w 1911 roku w IBM.
Lata trzydzieste - Rejewski, Zygalski, Różycki, Goedel, Markow. Dawid Hilbert w 1928 roku na międzynarodowym kongresie matematyków publicznie postawił pytanie, czy istnieje metoda pozwalająca rozstrzygnąć prawdziwość dowolnego twierdzenia matematycznego w wyniku tylko mechanicznych opercji na symbolach? Maszyna Rejewskiego (błędnie nazywana maszyną Turinga. Sam Turing stworzył niezależnie samą teorię, jednak żadnej maszyny nie wynalazł.) była odpowiedzią na postawione pytanie. Owa maszyna, o nazwie `Enigma`, łamała szyfry niemieckich oddziałów. Wynalazek Rejewskiego oraz teoria Turinga jest niejako modelem schematu działania według zadanego algorytmu.
Lata czterdzieste - MARK 1 kalkulator skonstruowany w 1944 roku przez Amerykanina Howarda Aikena z Uniwersytetu Harwarda. W 1946 roku powstał pierwszy elektroniczny komputer ENIAC wynaleziony przez J. P. Eckerta i J. W. Mauchly z Uniwersytetu w Pensylwanii. Jednakże za pierwszy komputer uważa się EDVAC (1956 rok, Uniwersytet Princeton). Matematyk Johannes von Neumann stworzył ideę wykonywania operacji na danych w pamięci komutera - pobierania z pamięci i zapisywania do pamięci.

## Metodologia programowania

Artykuł McCarthy, _A basis for a mathematical theory of computation_: "Zamiast sprawdzaniaprogramów komputerowych metodą prób i błędów aż do momentu ich całkowitego odpluskwienia powinniśmy udowadniać, że posiadaja one pożądane właśności".
Dijkstra, Hoare, Floyd, Wirth - prace na temat metodologii programowania.

## Prezentowanie algorytmów

Zwyczajowo przujęło się prezentowanie algorytmów w dwojaki sposób:

- za pomocą instniejącego języka programowania
- używając pseudojęzyka programowania (mieszanki języka naturalnego i form składniowych pochodzących z kilku reprezentatywnych języków programowania).

## Poprawność algorytmów:

Dwa sposoby stosowane do badania poprawności algorytmów:

- sprawdzanie stanu punktów kontrolnych za pomocą debuggera (odczytujemy wartości pewnych ważnych zmiennych i sprawdzamy, czy zachowują się poprawnie dla pewnych reprezentacyjnych danych wejściowych)
- formalne udowodnienie (np. przez indukcję matematyczną) zachowania niezmienników dla dowolnych danych wejsciowych.

## Rekurencja (rekursja)

Zastosowanie: fraktale, struktury drzewiaste.
Programy zapisane w formie rekurencyjnej mogą być przekształcone na klasyczną postać iteracyjną.
Cechy algorytmów rekurencyjnych:

- zakończenie algorytmu jest jasno określone
- złożony problem został rozłożony na problemy elementarne i na problemy o mniejszym stopniu skomplikowania niż ten, z którym mieliśmy do czynienia na początku.

Algorytm rekurencyjny to algorytm, który odwołuje się do samego siebie.

Techniczne przekazywanie parametrów odbywa się za pośrednictwem twz. stosu, czyli specjalnego miejsca w pamięci operacyjnej, które jest uzywane do zapamiętyywania informacji potrzebnych podczas wykonywania programów i dynamicznego przydzielania pamięci.

`Ciag Fibonacciego` - elementy tego ciągu stanowią liczby naturalne tworzące ciąg o takiej własności, że kolejny wyraz (z wyjątkiem dwóch pierwszych), jest suma dwóch poprzednich.

Nalezy zwracać baczna uwagę na:

- niewłaściwe używanie zasobów, np. przepłnienie stosu
- nieskończone pętle
- brak pamięci
- nieprawidłowe lub niejasne okreslenie warunków zakończenia programu
- błąd programowania

Programy rekurencyjne są zazwyczaj pamięciożerne. Z każdym wywołaniem rekurencyjnym wiąże się konieczność zachowania pewnych informacji niezbędnych do odtworzenia stanu sprzed wywołania.

Należy sprawdzać czy dla wartości wejściowych rekurencja skończy się.

Regułą jest, że wszystkie parametry funkcji rekurencyjnej są oblicznae jako pierwsze, a następnie dokonywane jest wywołanie samej funkcji. Taki sposób pracy jest zwany **wywołaniem przez wartość**.

Typy metod rekurencyjnych:

- rekurencja naturalna
- rekurencja z parametrem dodatkowym

Parametr dodatkowy służy do przekazywania elementów wyniku końcowego.

Nie powinno się używać rozwiązań rekurencyjnych:

- gdy w miejsce algorytmu rekurencyjnego można podać czytelny lub szybki odpowiednik iteracyjny
- algorytm rekurencyjny jest niestabilny (zapętlanie, specyfika kompilatora, cechy platformy sprzętowej)

Rekurencja skrośna.

## Analiza złożoności algorytmów

Podstawowe kryteria pozwalające na wybór właściwego algorytmu zależą głównie od kontekstu, w jakim zamierzamy go używać.

Prostota algorytmu.

Podstawowe kryteria oceny programu:

- sposób komunikacji z użytkownikiem
- szybkość wykonywania podstawowych funkcji programu
- awaryjność

Dwa główne czynniki wpływające na dobre samopoczucie użytkownika programu:

- czas wykonywania
- zajętość pamięci

Dobór właściwej miary złożoności obliczeniowej.  
Jakiego komutera uzywa się?  
Jaka była liczba przetważanych informacji?  
Jaka jest częstotliwość taktowania procesora?  
Priorytet wśród wykonywanych współbieżnie programów.  
Jaki kompilator został użyty?  
Czy zostały włączone opcje optymalizacji kodu podcas kompilacji?

Notacja dużego O - jest czasami zwana notacją Landaua.

Najbardziej czasochłonne sa instrukcje porównania.

# Sources

Piotr Wróblewski, _Algorytmy, struktury danych i techniki programowania_,HELION 2010
